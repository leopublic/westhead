<style>
    table tr th{text-align: left;}
    table tr td {vertical-align: top;}
    table.check{border-spacing: 5px; font-size:12px}
    p.check {border: solid 1px black; width:100%;}
    div.box{padding:10px; border:solid 1px black;}
    div.subtitulo{width:100%; text-align:left;color:white;font-size:16px; padding:5px;margin-top:15px; margin-bottom:5px;background-color: #ccc;}
</style>
<body>
    <div style="width:100%; text-align:center;font-size:30px;border-bottom:solid 2px #999;">CHECK LIST – RN 72/06</div>
    <div class="subtitulo">OS {{substr('000000'.$os->numero , -6)}}</div>
    <table>
        <tr><th>Data de elaboração</th><th>:</th><td>___________________</td></tr>
        <tr><th>Cliente</th><th>:</th><td>{{ $os->empresa->razaosocial}}</td></tr>
        <tr><th>Embarcação</th><th>:</th><td>{{ $os->projeto->descricao }}</td></tr>
        <tr><th>Centro de custo</th><th>:</th><td>{{ $os->centrodecusto->nome }}</td></tr>
    </table>

    <div class="subtitulo">Itens a serem verificados</div>
    <table class="check">
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Pré Cadastro (2 vias)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Formulário de Requerimento de Autorização de Trabalho</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Modelo I (Formulário Candidato / Empresa)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Carta de Experiência Profissional (mínimo de 03 anos de exp.) </td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Declaração Geral de Responsabilidade</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia da Página de Identificação do Passaporte</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>GRU Cód. 14055-4 Original Paga (R$16,93)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia da Procuração - (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do Contrato Social ou Estatuto com carimbo Jucerja - (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do Ato de Eleição, Designação ou Nomeação do Representante ou Administrador da Empresa Requerente – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do CNPJ</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Contrato de Trabalho devidamente assinado pelas partes</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia de documento comprovando escolaridade (é dispensado para latino americano)</td></tr>
    </table>

    <div class="subtitulo">Candidato(s)</div>
    <div class="box">
        <? $br = '';?>
        @foreach($os->candidatos as $candidato)
        {{ $br.$candidato->nome_completo }}
        <? $br = '<br/>';?>
        @endforeach
    </div>
    <div class="subtitulo">Processo elaborado por</div>
    <div class="box">{{$os->usuarioabertura->nome}}</div>
    <div class="subtitulo">Número do pré-cadastro no MTE</div>
    <div class="box">&nbsp;</div>

</body>
