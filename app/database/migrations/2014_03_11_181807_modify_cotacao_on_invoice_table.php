<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ModifyCotacaoOnInvoiceTable extends Migration {

    public function up() {
        Schema::table('invoice', function(Blueprint $table) {
            if (Schema::hasColumn('invoice', 'cotacao')){
                $table->dropColumn('cotacao');
            }
        });
        Schema::table('invoice', function(Blueprint $table) {
            $table->decimal('cotacao', 10, 4)->unsigned()->nullable();
        });
    }

    public function down() {
    }

}
