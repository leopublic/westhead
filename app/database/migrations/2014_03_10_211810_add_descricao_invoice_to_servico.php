<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDescricaoInvoiceToServico extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('servico', function(Blueprint $table) {
			$table->string('descricao_invoice', 1000)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('servico', function(Blueprint $table) {
			$table->dropColumn('descricao_invoice');
		});
	}

}
