<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdPerfilToUsuarioPerfil extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuario_perfil', function(Blueprint $table) {
			$table->foreign('id_perfil')->references('id_perfil')->on('perfil');			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuario_perfil', function(Blueprint $table) {
			$table->dropForeign('usuario_perfil_id_perfil_foreign');
		});
	}

}
