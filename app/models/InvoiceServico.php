<?php

class InvoiceServico extends Modelo {

    protected $table = 'invoice_servico';
    protected $primaryKey = 'id_invoice_servico';
    protected $guarded = array();

    public function invoice(){
        return $this->belongsTo('invoice', 'id_invoice');
    }
}
