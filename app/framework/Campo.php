<?php
class Campo{
	protected $label;
	protected $nome;
	protected $tipo;
	protected $atributos = array();
	protected $valor;
	protected $dica;
	protected $classes = array();
	
	public function get_label()
	{
		return $this->label;
	}
	
	public function get_dica()
	{
		return $this->dica;
	}
	
	public function get_nome()
	{
		return $this->nome;
	}

	public function set_valor($valor)
	{
		$this->valor = $valor;
	}
	
	public function get_classes()
	{
		return $this->classes;
	}
	
	public function AdicionaClasse($classe)
	{
		$this->classes[] = $classe;
	}
	
	
}
?>
