@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Ordem de serviço {{substr('000000'.$obj->numero, -6)}}</h1>
    @if (is_object($obj->empresa))
    <h4>{{$obj->empresa->razaosocial}}</h4>
    @endif
    @if (is_object($obj->projeto))
    <h6>{{$obj->projeto->descricao}}</h6>
    @endif
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>&nbsp;</h3>
                <ul class="nav nav-tabs pull-left">
                    <li class=""><a href="/os/show/{{$obj->id_os}}" ><i class="icon-tags"></i> Dados da OS</a></li>
                    <li class="active"><a href="#" ><i class="icon-group"></i> Candidatos</a></li>
                    <li class=""><a href="/os/anexos/{{$obj->id_os}}" ><i class="icon-folder-open"></i> Arquivos</a></li>
                    <li class=""><a href="/os/despesas/{{$obj->id_os}}" ><i class="icon-dollar"></i> Despesas</a></li>
                    <li class=""><a href="/os/desmembrar/{{$obj->id_os}}" ><i class="icon-exchange"></i> Desmembrar</a></li>
                </ul>
                <div class="box-tool">
                    @include('os.menu_os', array('obj', $obj))
                </div>
            </div>

            <div class="box-content">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::open(array("class"=>"form-horizontal", "id" => "buscaCandidato")) }}
                                {{ Form::hidden("id_os", $obj->id_os)}}
                                <div class="box">
                                    <div class="box-title">
                                        <h3><i class="icon-group"></i>Adicionar candidatos</h3>
                                        <div class="box-tool">
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="nome_completo" name="nome_completo">
                                                    <span class="input-group-btn">
                                                        <a class ="btn" type="button" id="busca_candidato" name="busca_candidato" onClick="ChamaRota('#buscaCandidato', '/candidato/foradaos', '#candidatosEncontrados');"><i class="icon-search"></i></a>
                                                    </span>
                                                </div><!-- /input-group -->
                                            </div><!-- /.col-lg-6 -->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" id="candidatosEncontrados">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::open(array("url"=>"/candidato/storeos", "class"=>"form-horizontal", "id" => "buscaCandidato")) }}
                                {{ Form::hidden("id_os", $obj->id_os)}}
                                <div class="box">
                                    <div class="box-title">
                                        <h3><i class="icon-group"></i>Cadastrar novo candidato</h3>
                                        <div class="box-tool">
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <div class="row">
                                            @include('html/campo-texto', array('qtdCol' => 2, 'id' => 'nome_completo', 'label' => 'Nome completo', 'valor' => '',  'help' => '',  array()))
                                            @include('html/campo-texto', array('qtdCol' =>  2, 'id' => 'nome_mae', 'label' => 'Nome da mãe', 'valor' => '',  'help' => '',  array()))
                                            @include('html/campo-data', array('qtdCol' =>  2, 'id' => 'dt_nascimento', 'label' => 'Data de nascimento', 'valor' => '',  'help' => '',  array()))
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Cadastrar e adicionar à OS</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                        <div class="row">
                            <!-- coluna candidatos -->
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-title">
                                        <h3><i class="icon-group"></i>Candidatos adicionados</h3>
                                    </div>
                                    <div class="box-content">
                                        @if(isset($candidatos))
                                        <table class="table table-striped table-hover fill-head">
                                            <thead>
                                                <tr>
                                                    <th style="width: 40px">#</th>
                                                    <th style="width: 400px">Ação</th>
                                                    <th style="width: auto">Nome</th>
                                                    <th style="width: auto;">Nome mãe</th>
                                                    <th style="width: 150px;">Data nasc.</th>
                                                    <th style="width: 80px;">Passaporte</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <? $i = 0; ?>
                                                @foreach($candidatos as $registro)
                                                <? $i++; ?>
                                                <tr>
                                                    <td><? print $i; ?></td>
                                                    <td>
                                                        <a class="btn btn-primary btn-sm" href="{{ URL::to('/candidato/show/'.$registro->id_candidato) }}" target="_blank"><i class="icon-search"></i></a>
                                                        @if(is_object($obj->servico) && ( ($obj->servico->id_tipo_servico == 1) || ($obj->servico->id_tipo_servico == 2) ) )
                                                        <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/form104/'.$obj->id_os.'/'.$registro->id_candidato) }}" title="Gerar o formulário 104" target="_blank"><i class="icon-print"></i></a>
                                                            <div class="btn-group">
                                                                <a href="#" data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle" title="Gerar o formulário 104" ><i class="icon-print"></i></a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="{{ URL::to('/os/form104/'.$obj->id_os.'/'.$registro->id_candidato.'/'.date('d-m-Y')) }}" target="_blank">Com data {{ date('d/m/Y') }}</a></li>
                                                                    <li><a href="#">Com outra data</a></li>
                                                                </ul>
                                                            </div>



                                                        @endif
                                                        @if(is_object($obj->servico) && $obj->servico->id_tipo_servico == 5 )
                                                        <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/declaracaoresp/'.$obj->id_os.'/'.$registro->id_candidato) }}" title="Gerar a declaração de responsabilidade" target="_blank"><i class="icon-print"></i></a>
                                                        <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/anexoprorrogacao/'.$obj->id_os.'/'.$registro->id_candidato) }}" title="Gerar o anexo de prorrogação" target="_blank"><i class="icon-print"></i></a>
                                                        @endif
                                                        <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/formtrans/'.$obj->id_os.'/'.$registro->id_candidato) }}" title="Gerar o formulário 1344" target="_blank"><i class="icon-print"></i></a>
                                                        <a class="btn btn-danger btn-sm" href="{{ URL::to('/os/removercandidato/'.$obj->id_os.'/'.$registro->id_candidato) }}"><i class="icon-trash"></i></a>

                                                    </div>

                                                    </td>
                                                    <td>{{ $registro->nome_completo }}
                                                            @if ($registro->id_candidato_principal > 0)
                                                            <br/>(
                                                                @if ($registro->id_parentesco > 0)
                                                                {{$registro->parentesco->descricao}}&nbsp;de&nbsp;
                                                                @endif
                                                                <a href="{{URL::to('/candidato/cadastro/'.$registro->id_candidato_principal)}}"  target="_blank" title="Clique para ir para o perfil do responsável" style="text-decoration: underline;">{{$registro->candidatoprincipal->nome_completo}}</a>)
                                                            @else
                                                                @if (is_object($registro->dependentes) && count($registro->dependentesforadaos($obj->id_os)) > 0)
                                                                    <div class="btn-group">
                                                                        <a href="#" class="btn btn-xs">adicionar dependentes</a><a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-xs"><span class="caret"></span></a>
                                                                        <ul class="dropdown-menu pull-left">
                                                                    @foreach($registro->dependentesforadaos($obj->id_os) as $dep)
                                                                    <li class="pull-left"><a class="" href="{{ URL::to('/os/adicionarcandidato/'.$obj->id_os.'/'.$dep->id_candidato) }}" title="Clique para incluir esse dependente na OS"><i class="icon-plus"></i>&nbsp;{{$dep->nome_completo}} ({{$dep->parentesco->descricao}})</a></li>
                                                                    @endforeach
                                                                        </ul>
                                                                    </div>
                                                                @endif
                                                                @if (is_object($registro->dependentes) && count($registro->dependentesnaos($obj->id_os)) > 0)
                                                                <br/><ul class="iconic">
                                                                    @foreach($registro->dependentesnaos($obj->id_os) as $dep)
                                                                        <li>
                                                                            <a class="btn btn-xs" href="{{ URL::to('/candidato/cadastro/'.$dep->id_candidato) }}"" title="Clique para ver os detalhes desse dependente" target="_blank"><i class="icon-search"></i></a>
                                                                            <a class="btn btn-xs" href="{{ URL::to('/os/formtrans/'.$obj->id_os.'/'.$dep->id_candidato) }}"" title="Clique para gerar o formulário de transformação para esse dependente" target="_blank"><i class="icon-print"></i></a>
                                                                            <a class="btn btn-xs" href="{{ URL::to('/os/removercandidato/'.$obj->id_os.'/'.$dep->id_candidato) }}"" title="Clique para retirar esse dependente na OS"><i class="icon-trash"></i></a>
                                                                            &nbsp;{{$dep->nome_completo}} ({{$dep->parentesco->descricao}})
                                                                        </li>
                                                                    @endforeach
                                                                    </ul>
                                                                
                                                                @endif
                                                            @endif
                                                            
                                                    </td>
                                                    <td>{{ $registro->nome_mae }}</td>
                                                    <td>{{ $registro->dt_nascimento }}</td>
                                                    <td>{{ $registro->nu_passaporte }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @else
                                        (nenhum candidato na OS)
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- /coluna candidatos -->
                        </div>
                        <!-- /row candidatos -->

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

</div>
<div class="modal fade" tabindex="-1" role="dialog" id="mymodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



@stop

@section('scripts')
<script>
    function ChamaRota(form, rota, retorno)
    {
        var xdata = $(form).serialize();
        $(retorno).html('<div style="text-align:center"><i class="icon-spinner icon-spin icon-3x"></i> carregando...</div>');
        $.ajax({
            type: 'POST'
            , url: rota
            , data: xdata
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        $(retorno).html(data);
                    }
        });
    }


    $(document).ready(function() {
        $(window).keydown(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });

    $("#nome_completo").keyup(function(e) {
        if (e.keyCode == 13) {
            ChamaRota('#buscaCandidato', '/candidato/foradaos', '#candidatosEncontrados');
            return false;
        }
    });



</script>

@stop
