<?php
class ReparticaoController extends \CadastroController {
	const raiz = "reparticao";
	const campoChave = 'id_reparticao';
	const nomeSingular = 'Repartição consular';
	const nomePlural = 'Repartições consulares';
	const artigo = 'a';
	public function RegistrosIndex()
	{
		$data = Reparticao::orderBy('descricao')->get();
		return $data;
	}

	public function ColunasIndex()
	{
		$data = array(
				array("titulo" => "Nome", "campo" => "descricao"),
				array("titulo" => "Cidade", "campo" => "cidade")
			);
		return $data;
	}
	
	public function ObtemInstancia($id = 0)
	{
		if($id > 0){
			$this->obj = Reparticao::find($id);
		}
		else{
			$this->obj = new Reparticao;
		}
	}
	
	public function TelaIndex()
	{
		$data = array();
		$data['titulo_tela'] = self::nomeSingular;
		$data['titulo_grid'] = "Lista";
		$data['subTitulo'] = "Administrar ".self::nomePlural;
		$data['urlEdit'] = self::raiz.'/show';
		$data['urlDel'] = self::raiz.'/del';
		$data['urlCreate'] = self::raiz.'/create';
		$data['nomeCampoChave'] = self::campoChave;
        $data['menu'] = 'menuTabelas';
        $data['item'] = 'itemReparticao';
		return $data;
	}

	public function get_ViewShow()
	{
		return self::raiz.'/show';
	}

	public function MontaCampos()
	{
	//	$this->campos[] = new 
	}

	public function getDel($id = 0)
	{
		try{
			Reparticao::destroy($id);			
			return Redirect::to(self::raiz)
					->with('flash_msg', self::nomeSingular.' excluíd'.self::artigo.' com sucesso!');		
		}
		catch(Exception $e){
			return Redirect::to(self::raiz)
					->with('flash_error', $e->getMessage());		
		}
	}

	public function postUpdate($id = 0)
	{
		if($id > 0){
			$obj = Reparticao::find($id);
		}
		else{
			$obj = new Reparticao();
		}
		$obj->fill(Input::all());

		if($obj->save()){
			return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular.' atualizad'.self::artigo.' com sucesso!');		
		}
		else{
			Input::flash();
			return Redirect::to(self::raiz.'/show/'.$id)->withErrors($obj->errors);			
		}
	}
}