<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFlAbreVistoToServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasColumn('servico', 'fl_abre_servico')){
			Schema::table('servico', function(Blueprint $table) {
				$table->boolean('fl_abre_servico')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasColumn('servico', 'fl_abre_servico')){
			Schema::table('servico', function(Blueprint $table) {
				$table->dropColumn('fl_abre_servico');
			});
		}
	}

}
