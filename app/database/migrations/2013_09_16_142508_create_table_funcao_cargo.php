<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableFuncaoCargo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('funcao', function( $table) {
            $table->increments('id_funcao');
			$table->string('descricao',500);
			$table->string('descricao_en', 500)->nullable();
			$table->string('codigo_pf', 10)->nullable();
			$table->text('atividades')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('funcao');
	}

}