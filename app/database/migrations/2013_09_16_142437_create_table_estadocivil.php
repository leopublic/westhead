<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableEstadocivil extends Migration {

	/**
	 * Run the migrations.
	 * xx
	 * @return void
	 */
	public function up()
	{
        Schema::create('estadocivil', function($table) {
            $table->increments('id_estadocivil');
			$table->string('descricao',500);
			$table->string('descricao_en', 500)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estadocivil');
	}

}