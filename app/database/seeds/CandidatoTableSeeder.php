<?php

class CandidatoTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
        DB::table('candidato')->truncate();

        $obj = array("nome_completo" => "Thomas Blaine", "dt_nascimento" => "1960-09-15", "nu_passaporte"=> "VC349933", "dt_validade_passaporte"=> "2013-08-30" );
        DB::table('candidato')->insert($obj);
        $obj = array("nome_completo" => "William Jones Sark", "dt_nascimento" => "1968-03-04", "nu_passaporte"=> "FA003222", "dt_validade_passaporte"=> "2013-12-01" );
        DB::table('candidato')->insert($obj);
        $obj = array("nome_completo" => "Aaron Wilkies", "dt_nascimento" => "1958-03-04", "nu_passaporte"=> "RT399493", "dt_validade_passaporte"=> "2016-05-19" );
        DB::table('candidato')->insert($obj);
        $obj = array("nome_completo" => "Alistar Jones", "dt_nascimento" => "1964-07-26", "nu_passaporte"=> "AX49954", "dt_validade_passaporte"=> "2019-11-25" );
        DB::table('candidato')->insert($obj);
    }

}