<?php

class CentrodecustoController extends \CadastroController {

    public function RegistrosIndex() {
        $data = DB::table('centro_de_custo')->leftJoin('empresa', 'empresa.id_empresa', '=', 'centro_de_custo.id_empresa')->select(array('id_centro_de_custo', 'nome', 'razaosocial'))->get();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(
            array("titulo" => "Empresa", "campo" => "razaosocial")
            , array("titulo" => "Nome", "campo" => "nome")
        );
        return $data;
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = Centrodecusto::find($id);
        } else {
            $this->obj = new Centrodecusto;
        }
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = "Centros de custos";
        $data['titulo_grid'] = "Lista";
        $data['subTitulo'] = "Administrar Centro de custos";
        $data['urlEdit'] = 'centrodecusto/show';
        $data['urlDel'] = 'centrodecusto/del';
        $data['urlCreate'] = 'centrodecusto/create';
        $data['nomeCampoChave'] = 'id_centro_de_custo';
        $data['menu'] = 'menuCadastros';
        $data['item'] = 'itemCentros';
        return $data;
    }

    public function get_ViewShow() {
        return 'centrodecusto/show';
    }

    public function getDel($id) {
        try {

            Centrodecusto::destroy($id);
            return Redirect::to('/centrodecusto')
                            ->with('flash_msg', 'Centro de custo excluído com sucesso!');
        } catch (Exception $e) {
            return Redirect::to('/centrodecusto')
                            ->with('flash_error', $e->getMessage());
        }
    }

    public function MontaCampos() {
        //	$this->campos[] = new
    }

    public function postUpdate($id = 0) {
        if ($id > 0) {
            $centrodecusto = Centrodecusto::find($id);
        } else {
            $centrodecusto = new Centrodecusto();
        }
        $centrodecusto->fill(Input::all());
        if ($centrodecusto->save()) {
            return Redirect::to('centrodecusto')->with('flash_msg', 'Centro de custo atualizado com sucesso!');
        } else {
            Input::flash();
            return Redirect::to('centrodecusto/show/' . $id)->withErrors($centrodecusto->errors);
        }
    }

    public function getDaempresajson($id_empresa) {
        return Response::json($this->centrosDaEmpresa($id_empresa, '(não informado)'));
    }

    public function getDaempresajsonfiltro($id_empresa) {
        return Response::json($this->centrosDaEmpresa($id_empresa, '(todos)'));
    }

    public function centrosDaEmpresa($id_empresa, $default) {
        $projetos = DB::table('centro_de_custo')
                        ->select(array('id_centro_de_custo as id', 'nome as descricao'))
                        ->where('id_empresa', '=', $id_empresa)
                        ->orderBy('nome')->get();
        array_unshift($projetos, array('id' => '0', 'descricao' => $default));
        return $projetos;
    }

}
