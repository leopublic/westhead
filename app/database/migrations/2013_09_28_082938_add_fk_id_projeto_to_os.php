<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdProjetoToOs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('os', function(Blueprint $table) {
			$table->foreign('id_projeto')->references('id_projeto')->on('projeto');			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('os', function(Blueprint $table) {
			$table->dropForeign('os_id_projeto_foreign');
		});
	}

}
