<?php

/**
 * @property int $id_usuario
 * @property int $id_empresa
 * @property varchar $nome
 * @property varchar $username
 * @property varchar $password
 * @property varchar $email
 * @property tinyint $fl_interno
 * @property text $observacao
 * @property timestamp $deleted_at
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property datetime $dt_ult_acesso
 * @property tinyint $fl_alterar_senha 
 * @property int $qtd_tentativas_erradas Quantidade de vezes que o usuário errou a senha desde o último sucesso
 * @property datetime $dthr_primeiro_erro Data e hora da primeira tentativa
 * @property tinyint $fl_suspenso  
 * 
 */
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Modelo implements UserInterface, RemindableInterface {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuario';
    protected $primaryKey = 'id_usuario';
    protected $guarded = array('password_confirmation');
    protected $fillable = array('nome', 'username', 'email', 'observacao');
    protected $acessos_salvo;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    public function assumeDefaults() {
        
    }

    public static function regras() {
        return array(
            'nome' => 'required',
            'username' => 'required',
        );
    }

    public static function mensagens() {
        return array(
            'nome.required' => 'O nome é obrigatório!',
            'username.required' => 'O login é obrigatório!',
        );
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword() {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail() {
        return $this->email;
    }

    public function atualizaUltimoAcesso() {
        $this->dt_ult_acesso = date('Y-m-d H:i:s');
        $this->dthr_primeiro_erro = null;
        $this->qtd_tentativas_erradas = 0;
        $this->save();
    }

    public function getCreatedAtAttribute($data) {
        return $this->dtHrModelParaView($data);
    }

    public function getDtUltAcessoAttribute($data) {
        if (isset($this->attributes['dt_ult_acesso'])) {
            return $this->dtHrModelParaView($this->attributes['dt_ult_acesso']);
        } else {
            return '';
        }
    }

    public function perfis() {
        return $this->belongsToMany('Perfil', 'usuario_perfil', 'id_usuario', 'id_perfil');
    }

    public function listaPerfis($sep = ',') {
        $ret = '';
        $virg = '';
        $perfis =$this->perfis;
        foreach ($perfis as $perfil) {
            $ret .= $virg . $perfil->id_perfil;
            $virg = $sep;
        }
        return $ret;
    }

    public function listaNomePerfis($sep = ',') {
        $ret = '';
        $virg = '';
        $perfis =$this->perfis;
        foreach ($perfis as $perfil) {
            $ret .= $virg . $perfil->nome;
            $virg = $sep;
        }
        return $ret;
    }
    public function getListaNomePerfisAttribute() {
        return $this->listaNomePerfis('<br/>');
    }

    public function acessos() {
        if ($this->acessos_salvo != '') {
            return $this->acessos_salvo;
        } else {
            $sql = "select concat(tela.codigo,'_', acao.codigo ) codigo
                    from acesso, acao, tela
                    where acao.id_acao = acesso.id_acao
                    and tela.id_tela = acao.id_tela
                    and acesso.id_perfil in (select id_perfil from usuario_perfil where id_usuario = " . $this->id_usuario . ")";
            $acessos = \DB::select(\DB::raw($sql));

            $ret = '';
            foreach ($acessos as $acesso) {
                $ret.= '#' . $acesso->codigo . '#';
            }
            $this->acessos_salvo = $ret;
            return $ret;
        }
    }

    public function temAcessoA($tela, $acao) {
        $acesso = $tela . "_" . $acao;
        $acessos = $this->acessos();
        if (str_contains($acessos, $acesso)) {
            return true;
        } else {
            return false;
        }
    }

    public function temAcessoAoMenu($tela) {
        $acesso = '#' . $tela;
        $acessos = $this->acessos();
        if (str_contains($acessos, $acesso)) {
            return true;
        } else {
            return false;
        }
    }

    public function getRememberToken() {
        return $this->remember_token;
    }

    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

}
