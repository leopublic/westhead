<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdArmadorToProjeto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projeto', function(Blueprint $table) {
			$table->foreign('id_empresa_armador')->references('id_empresa')->on('empresa');			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projeto', function(Blueprint $table) {
			$table->dropForeign('projeto_id_empresa_armador_foreign');
		});
	}

}
