<?php

/**
 * @property int $id_processo
 * @property int $id_tipoprocesso
 * @property int $id_candidato
 * @property int $id_empresa
 * @property int $id_projeto
 * @property int $id_os
 * @property int $id_visto
 * @property int $id_status_processo
 * @property int $id_servico
 * @property int $ordem
 * @property date $date_1
 * @property date $date_2
 * @property date $date_3
 * @property date $date_4
 * @property date $date_5
 * @property date $date_6
 * @property varchar $string_1
 * @property varchar $string_2
 * @property varchar $string_3
 * @property varchar $string_4
 * @property varchar $string_5
 * @property varchar $string_6
 * @property int $integer_1
 * @property int $integer_2
 * @property int $integer_3
 * @property int $integer_4
 * @property int $integer_5
 * @property int $integer_6
 * @property text $observacao
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property date $dt_envio_bsb
 * @property int $id_usuario
 * @property varchar $controller
 * @property varchar $metodo
 */
class Processo extends Modelo {

    protected $table = 'processo';
    protected $primaryKey = 'id_processo';
    protected $guarded = array();

    public function assumeDefaults() {
        
    }

    public static function regras() {
        return array(
            'nome_completo' => 'required',
            'dt_nascimento' => 'required',
            'nome_mae' => 'required'
        );
    }

    public static function mensagens() {
        return array(
            'nome_completo.required' => 'O nome é obrigatório!',
            'dt_nascimento.required' => 'A data de nascimento é obrigatória!',
            'nome_mae.required' => 'O nome da mãe é obrigatório!'
        );
    }

    /* =================================================================
     * Relacionamentos
     * =================================================================
     */

    public function candidato() {
        return $this->belongsTo('Candidato', 'id_candidato');
    }

    public function visto() {
        return $this->belongsTo('Visto', 'id_visto');
    }

    public function os() {
        return $this->belongsTo('Os', 'id_os');
    }

    /**
     * @return Servico Serviço do processo
     */
    public function servico() {
        return  $this->belongsTo('Servico', 'id_servico');
    }

    /**
     * @return Empresa Empresa do processo
     */
    public function empresa() {
        return $this->belongsTo('Empresa', 'id_empresa');
    }

    /**
     * @return Projeto Projeto do processo
     */
    public function projeto() {
        return $this->belongsTo('Projeto', 'id_projeto');
    }

    /* =================================================================
     * Rotinas
     * =================================================================
     */

    /**
     * Atualiza o processo com informações que devem vir da OS.
     * Cada processo deve especificar que informações deseja importar da OS
     */
    public function recupereDadosDaOs() {
        $this->id_servico = $this->os->id_servico;
        $this->id_empresa = $this->os->id_empresa;
        $this->id_projeto = $this->os->id_projeto;
    }

    /*
     * Torna esse processo o principal do visto
     */
    public function torneSePrincipal($controller = 'processo', $metodo = 'torneSePrincipal') {
        if ($this->id_tipoprocesso == 1 || $this->id_tipoprocesso == 5){
            $visto = $this->visto;

            $visto->id_processo_principal = $this->id_processo;
            $visto->id_usuario = Auth::user()->id_usuario;
            $visto->controller = $controller;
            $visto->metodo = $metodo;
            $visto->save();
        } else {
            Processo::where('id_visto', '=', $this->id_visto)
                ->where('id_tipoprocesso', '=', $this->id_tipoprocesso)
                ->update(array('fl_mais_recente' => 0));
            $this->fl_mais_recente = 1;
            $this->save();
        }

    }

    public function existemOutrosVistos() {
        $qtd = \Visto::where('id_candidato', '=', $this->id_candidato)
                ->where('id_visto', '<>', $this->id_visto)
                ->count();
        return $qtd;
    }

    public function outrosVistos() {
        if ($this->id_visto > 0){
            return \Visto::where('id_candidato', '=', $this->id_candidato)
                            ->where('id_visto', '<>', $this->id_visto)
                            ->get();
        } else {
            return \Visto::where('id_candidato', '=', $this->id_candidato)
                            ->get();
        }
    }

    /* =================================================================
     * Acessors
     * =================================================================
     */

    public function getDtEnvioBsbFmtAttribute($valor) {
        return $this->dtModelParaView($this->attributes['dt_envio_bsb']);
    }

    public function setDtEnvioBsbFmtAttribute($valor) {
        if ($valor != '' && $valor != '00/00/0000') {
            $this->attributes['dt_envio_bsb'] = $this->dtViewParaModel($valor);
        } else {
            $this->attributes['dt_envio_bsb'] = null;
        }
    }

    public function getData1FmtAttribute($valor) {
        return $this->dtModelParaView($this->attributes['data_1']);
    }

    public function setData1FmtAttribute($valor) {
        if ($valor != '' && $valor != '00/00/0000') {
            $this->attributes['data_1'] = $this->dtViewParaModel($valor);
        } else {
            $this->attributes['data_1'] = null;
        }
    }

    public function getTituloAttribute() {
        return '';
    }

    public function getTituloCompletoAttribute($valor) {
        $serv = $this->servico;
        if (is_object($serv)) {
            return $this->titulo . ': ' . $serv->descricao;
        } else {
            return $this->titulo . ': (?)';
        }
    }

    public function getRazaosocialEmpresaAttribute($valor) {
        if (is_object($this->empresa)) {
            return $this->empresa->razaosocial;
        } else {
            return '--';
        }
    }

    public function getDescricaoProjetoAttribute($valor) {
        if (is_object($this->projeto)) {
            return $this->projeto->descricao;
        } else {
            return '--';
        }
    }

    public function getDescricaoServicoAttribute($valor) {
        if (is_object($this->servico)) {
            return $this->servico->descricao;
        } else {
            return '--';
        }
    }

    public function getCampoOrdemMaisRecenteAttribute(){
        return '';
    }

    /**
     * Indica se esse processo é o processo principal do visto
     * @param type $valor
     * @return boolean
     */
    public function getEhPrincipalAttribute($valor) {
        if (is_object($this->visto)) {
            return $this->visto->id_processo_principal == $this->id_processo;
        } else {
            return false;
        }
    }

    public function andamentos(){
        $regs = HistoricoAndamento::where('id_processo', '=', $this->id_processo)
                ->leftJoin('status_andamento', 'status_andamento.id_status_andamento', '=', 'historico_andamento.id_status_andamento')
                ->leftJoin('usuario', 'usuario.id_usuario', '=', 'historico_andamento.id_usuario')
                ->select(array(DB::raw('status_andamento.nome as status_andamento_nome'), DB::raw('usuario.nome as usuario_nome'), DB::raw("date_format(historico_andamento.datahora, '%d/%m/%Y %H:%i:%s') data_fmt"), 'obs_andamento'))
                ->orderBy('historico_andamento.datahora', 'desc')
                ->get();
        return $regs;
    }
}
