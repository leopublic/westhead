<div class="btn-group">
    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-warning">Impressão <span class="caret"></span></a>
    <ul class="dropdown-menu pull-right">
        <li><a href="{{ URL::to('/os/capa/'.$obj->id_os)}}" target="_blank">Capa</a></li>
        <li><a href="{{ URL::to('/empresa/relacaoembarcacoes/'.$obj->id_empresa)}}" target="_blank">Declaração de embarcações</a></li>
        <li><a href="{{ URL::to('/os/relatoriodespesas/'.$obj->id_os)}}" target="_blank">Relatório de despesas</a></li>
    </ul>
</div>
@if(is_array($obj->getChecklists()))
<div class="btn-group">
    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-info">Checklist <span class="caret"></span></a>
    <ul class="dropdown-menu pull-right">
        @foreach ($obj->getChecklists() as $checklist => $nome)
        <li><a href="{{ URL::to('/checklist/gere/'.$checklist.'/'.$obj->id_os)}}" target="_blank">{{$nome}}</a></li>
        @endforeach
    </ul>
</div>
@endif

