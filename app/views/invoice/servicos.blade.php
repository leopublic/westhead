@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Invoice {{$invoice->numeroFmt}}</h1>
    @if (is_object($invoice->centrodecusto))
    <h4>{{$invoice->centrodecusto->nome}}</h4>
    @endif
</div>
<!-- END Page Title -->

@include('html/mensagens')


<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3>Serviços</h3>
                <div class="box-tool">
                    <div class="btn-group">
                        @if ($invoice->cotacao > 0)
                        <a href="{{URL::to('/invoice/demonstrativo/'.$invoice->id_invoice)}}" target="_blank" class="btn btn-primary">Demonstrativo</a>
                        @else
                        <a href="#" onClick="alert('Só é possível emitir o demonstrativo depois de informada a cotação do dólar.')" class="btn btn-primary">Demonstrativo</a>
                        @endif
                    </div>
                </div>
            </div>

            {{Form::open(array("class"=>"form-horizontal")) }}
            {{Form::hidden('id_invoice', $invoice->id_invoice)}}
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-hover fill-head">
                            <thead>
                                <tr>
                                    <th style="width: 120px">Serviço</th>
                                    <th style="width: 60px;text-align: center;">Qtd</th>
                                    <th style="width: auto;">Descrição</th>
                                    <th style="width: 120px; text-align: right;">Valor desp. R$</th>
                                    <th style="width: 120px; text-align: right;">Valor unit. R$</th>
                                    <th style="width: 120px; text-align: right;">Valor total R$</th>
                                </tr>
                            </thead>
                            @if(isset($servicos))
                            <tbody>
                                @foreach($servicos as $servico)
                                <tr>
                                    <td>{{ Form::hidden('id_servico[]', $servico->id_servico) }}{{$servico->descricao}}
                                        <br><a href="{{ URL::to('/servico/show/'.$servico->id_servico) }}" class="btn btn-xs" target="_blank">Editar</a>
                                        <br><a href="{{ URL::to('/invoice/recarregaservico/'.$invoice->id_invoice.'/'.$servico->id_servico) }}" class="btn btn-xs" title="Atualiza as descrições do serviço e das despesas conforme modelo do serviço">Recarregar</a>
                                    </td>
                                    <td style="text-align: center;">{{ $servico->qtd }}</td>
                                    <td>
                                        {{ Form::textarea('descricao_invoice'.$servico->id_servico, $servico->descricao_invoice, array('class'=>'form-control','style'=> 'width:100%;height:9em; font-size:0.9em;')) }}
                                        {{ Form::textarea('descricao_despesas'.$servico->id_servico, $servico->descricao_despesas, array('class'=>'form-control','style'=> 'width:100%;height:9em; font-size:0.9em;')) }}
                                    </td>
                                    <td style="text-align: right;">{{ Form::text('val_despesas'.$servico->id_servico, number_format($servico->val_despesas, 2, ",", "."), array('class'=>'form-control','style'=> 'width:100%;text-align:right;')) }}</td>
                                    <td style="text-align: right;">{{ Form::text('val_unitario'.$servico->id_servico, number_format($servico->val_unitario, 2, ",", "."), array('class'=>'form-control','style'=> 'width:100%;text-align:right;')) }}</td>
                                    <td style="text-align: right;">{{ Form::text('val_total'.$servico->id_servico, number_format($servico->val_total, 2, ",", "."), array('class'=>'form-control', 'style'=> 'width:100%;text-align:right;')) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            @endif
                        </table>
                        <div class="centered" style="text-align: center">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@stop
@section('scripts')
<script>
    @if (isset($abreDemonstrativo) && $abreDemonstrativo == true)
        window.open('/invoice/demonstrativo/{{$invoice->id_invoice}}');
    @endif
</script>
@stop
