<?php
use Westhead\Repositorios;

class RepositorioUsuarioTest extends TestCase {

    public function testAtualizaUltimoAcesso(){
        $agora = date('Y-m-d H:i:s');
        $usuario= new \Usuario;
        $usuario->username = 'Teste';
        $usuario->password = Hash::make('teste');
        $usuario->dthr_primeiro_erro = $agora;
        $usuario->qtd_tentativas_erradas = 10;
        $usuario->save();
        
        
        $usu = \Usuario::find($usuario->id_usuario);
        $usu->atualizaUltimoAcesso();
        
        $this->assertEmpty($usu->dthr_primeiro_erro);
        $this->assertEmpty($usu->qtd_tentativas_erradas);
        $this->assertGreaterThan($usuario->dt_ult_acesso, $usu->dt_ult_acesso);
        
        $usuario->delete();
    }
    

}