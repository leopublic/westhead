<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdTipoAnexoToAnexo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('anexo', function(Blueprint $table) {
			$table->foreign('id_tipo_anexo')->references('id_tipo_anexo')->on('tipo_anexo');			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('anexo', function(Blueprint $table) {
			$table->dropForeign('anexo_id_tipo_anexo_foreign');
		});
	}

}
