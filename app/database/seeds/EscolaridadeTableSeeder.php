<?php

class EscolaridadeTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('escolaridade')->truncate();

		$rep = array("descricao" =>"Segundo Grau", "descricao_en"=>"High School");
		DB::table('escolaridade')->insert($rep);
		$rep = array("descricao" =>"Superior", "descricao_en"=>"Technical College");
		DB::table('escolaridade')->insert($rep);
		$rep = array("descricao" =>"MBA", "descricao_en"=>"MBA");
		DB::table('escolaridade')->insert($rep);
		$rep = array("descricao" =>"Curso Técnico", "descricao_en"=>"Technical School");
		DB::table('escolaridade')->insert($rep);
		$rep = array("descricao" =>"Primeiro Grau", "descricao_en"=>"Basic Education");
		DB::table('escolaridade')->insert($rep);
		$rep = array("descricao" =>"MESTRADO", "descricao_en"=>"");
		DB::table('escolaridade')->insert($rep);
		$rep = array("descricao" =>"Doutorado", "descricao_en"=>"Doctor");
		DB::table('escolaridade')->insert($rep);
		// Uncomment the below to run the seeder
	}

}
