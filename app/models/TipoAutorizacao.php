<?php
class TipoAutorizacao extends Modelo {
	protected $table = 'tipo_autorizacao';
	protected $primaryKey = 'id_tipo_autorizacao';

	protected $guarded = array();

	public function assumeDefaults()
	{
	}

	public static function regras()
	{
		return array(
				'descricao' => 'required'
				);
	}

	public static function mensagens()
	{
		return array(
				'descricao.required' => 'O nome é obrigatório!'
			);
	}
}
