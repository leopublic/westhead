<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFlBsbToServicoTable extends Migration {

    public function up() {
        Schema::table('servico', function(Blueprint $table) {
            $table->boolean('fl_bsb')->nullable();
        });
    }

    public function down() {
        Schema::table('servico', function(Blueprint $table) {
            $table->dropColumn('fl_bsb');
        });
    }

}
