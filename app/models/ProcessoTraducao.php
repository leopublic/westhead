<?php
class ProcessoTraducao extends Processo {

    protected $guarded = array('id_status_andamento', 'obs_andamento', 'tem_andamento');

    public static function boot() {
        parent::boot();

        static::saving(function($modelo) {
        });

        static::saved(function($modelo) {
            // registra log
        });
    }

    public function assumeDefaults() {
        $this->id_tipoprocesso = 10;
    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

    public function getDataRequerimentoAttribute($valor) {
        return $this->dtModelParaView($this->attributes['date_1']);
    }

    public function setDataRequerimentoAttribute($valor) {
        $this->attributes['date_1'] = $this->dtViewParaModel($valor);
    }


    public function getTituloAttribute(){
        return 'Tradução juramentada';
    }

}
