<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new CarregaAcoesCommand);
Artisan::add(new AdminAtualizaDelegaciasRegistroCommand);
Artisan::add(new AdminRessetaSenhaUsuariosCommand);
Artisan::add(new AdminNovaSenhaUsuarioCommand);
Artisan::add(new AdminListaProcessosFaltando);
Artisan::add(new AdminCorrigeProcessosFaltando);
Artisan::add(new AdminOrdenaProcessos);
