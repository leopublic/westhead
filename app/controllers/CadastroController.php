<?php

class CadastroController extends BaseController {

    protected $campos;
    protected $obj;

    public function getIndex() {
        $registros = $this->RegistrosIndex();
        $colunas = $this->ColunasIndex();
        $tela = $this->TelaIndex();
        if (!key_exists('menu', $tela)){
            $tela['menu'] = 'xx';
        }
        if (!key_exists('item', $tela)){
            $tela['item'] = 'xx';
        }
        return View::make($this->get_ViewIndex())
                        ->with('view_acoes', $this->get_ViewAcoes())
                        ->with('tela', $tela)
                        ->with('colunas', $colunas)
                        ->with('registros', $registros)
        ;
    }

    public function get_ViewIndex() {
        return 'table';
    }

    public function get_ViewAcoes() {
        return 'acoes';
    }

    public function getShow($id) {
        $this->ObtemInstancia($id);
        $tela = $this->TelaIndex();
        return View::make($this->get_ViewShow())
                        ->with('tela', $tela)
                        ->with('obj', $this->obj)
        ;
    }

    public function getCreate() {
        $this->ObtemInstancia();
        $tela = $this->TelaIndex();
        return View::make($this->get_ViewShow())
                        ->with('tela', $tela)
                        ->with('obj', $this->obj)
        ;
    }

    public function postUpdate($id) {
        
    }

}
