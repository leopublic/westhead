                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 150px">Data</th>
                            <th>Usuário</th>
                            <th>Status</th>
                            <th>Observações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($processo->andamentos() as $reg)
                        <tr>
                            <td>{{ $reg->data_fmt }}</td>
                            <td>{{ $reg->usuario_nome }}</td>
                            <td>{{ $reg->status_andamento_nome }}</td>
                            <td>{{ $reg->obs_andamento }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
