<?php
namespace Westhead\Repositorios;

use Mailgun\Mailgun;

class RepositorioUsuario extends Repositorio{

    public function altereSenha($post){
        $id_usuario = $post['id_usuario'];
        $usuario = \Usuario::findOrFail($id_usuario);
        $this->validador = new \Appvendas\Validadores\Senha($post);
        if ($this->validador->passes()) {
            $usuario->password = \Hash::make($post['password']);
            $usuario->save();
            return true;
        } else {
            return false;
        }
    }

    public function altereMinhaSenha($post){
        $usuario = \Auth::user();
        if (isset($post['atual'])) {
            if (\Hash::check($post['atual'], $usuario->password)){
                $this->validador = new \Westhead\Validadores\Senha($post);
                if ($this->validador->passes()) {
                    $usuario->password = \Hash::make($post['password']);
                    $usuario->fl_alterar_senha = 0;
                    $usuario->save();
                    return true;
                } else {
                    return false;
                }                
            } else {
                $this->registreErro('A senha atual não confere');
                return false;                
            }
        } else {
            $this->registreErro('A senha atual deve ser informada');
            return false;
        }
    }
    
    public function enviaNovaSenha(\Usuario $usuario){
        $seg = new \Westhead\Servicos\Seguranca();
        $novasenha = $seg->rand_string(10);
        $usuario->password = \Hash::make($novasenha);
        $usuario->qtd_tentativas_erradas = 0;
        $usuario->save();
        $data = array('usuario' => $usuario, 'novasenha' => $novasenha);

        $mg = new Mailgun("key-2a01524c53cadc4520ea83dc53c83197");
        $domain = "mg.m2software.com.br";

        $html = \View::make('emails.nova_senha', $data);
        //Customise the email - self explanatory
        $mg->sendMessage($domain, array(
        'from'=>'patricia.valente@lplaw.com.br',
        'replyTo'=>'patricia.valente@lplaw.com.br',
        'to'=> $usuario->nome." <".$usuario->email.">",
        'subject' => 'Alteração de senha',
        'html' => $html
            )
        );

        // \Mailgun::send('emails.nova_senha', $data, function($message) use($usuario) {
        //     $message->to($usuario->email, $usuario->nome)->subject('Alteração de senha');
        // });        
        
    }
    
    public function enviaNovaSenhaOld(\Usuario $usuario){
        $seg = new \Westhead\Servicos\Seguranca();
        $novasenha = $seg->rand_string(10);
        $usuario->password = \Hash::make($novasenha);
        $usuario->qtd_tentativas_erradas = 0;
        $usuario->save();
        $data = array('usuario' => $usuario, 'novasenha' => $novasenha);

        \Mail::send('emails.nova_senha', $data, function($message) use($usuario) {
            $message->to($usuario->email, $usuario->nome)->subject('Alteração de senha');
        });        
        
    }
    
    public function exclua(\Usuario $usuario){
        $usuario->delete();
    }

    public function edita($post){
        $id_usuario = $post['id_usuario'];
        if ($id_usuario > 0){
            $usuario = \Usuario::find($id_usuario);
        } else {
            $usuario = new \Usuario;
        }
        $this->validador = new \Appvendas\Validadores\Usuario($post);
        $this->validador->id_unique = $id_usuario;
        if ($this->validador->passes()) {
            $usuario->fill($post);
            if (array_key_exists('senha', $post) && $post['senha'] != ''){
                $usuario->password = \Hash::make($post['senha']);
            }
            $usuario->save();
            if (isset($post['id_perfil'])){
                $perfis = $post['id_perfil'];
            } else {
                $perfis = array();
            }
            $usuario->perfis()->sync($perfis);
            return $usuario;
        } else {
            return false;
        }
    }
}
