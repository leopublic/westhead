<?php
class ProcessoAutorizacaoController extends \ProcessoGenericoController {

    public function getShow($id){
        $processo = ProcessoAutorizacao::find($id);

        return View::make('processo.1')
                        ->with('processo', $processo)
                ;
    }

    public function postUpdate($id){
        $processo = ProcessoAutorizacao::find($id);
        try {

            $processo->fill(Input::all());
            $processo->save();

            if (Input::has('tem_andamento')){
                $this->repositorio()->atualiza_status_andamento($processo, Input::get('id_status_andamento'), Input::get('obs_andamento'), \Auth::user()->id_usuario);
            }

            if ($processo->dataRequerimento != '' && $processo->dataRequerimento != '00/00/0000' && is_object($processo->os)){
                $processo->os->concluiPeloProcesso($processo->dataRequerimento);
            }
            Session::flash($processo->id_processo."_success", "Processo atualizado com sucesso!");
            $proccontrol = new \ProcessoController();
            return $proccontrol->rotaParaVisualizarProcesso($processo);
        } catch (Exception $ex) {
            Session::flash($processo->processo_id."_error", $ex->getMessage());
            return Redirect::to('/candidato/show/' . $processo->id_candidato . '#processo' . $id);
        }
    }

    public function getAdicione($id_visto){
        $rep = new Westhead\Repositorios\RepositorioProcesso;
        $processo = $rep->criaProcessoNoVisto(1, $id_visto);
        $proccontrol = new \ProcessoController();
        return $proccontrol->rotaParaVisualizarProcesso($processo);
    }

    public function getAdicionarandamento($id_os){
        $os = Os::find($id_os);
        $statuses_andamento = array('0' => '(não informado)') +  StatusAndamento::orderBy('nome')->lists('nome', 'id_status_andamento');
        $data = array(
                'statuses_andamento' => $statuses_andamento
                , 'os' => $os
                , 'datahora' => date('d/m/Y')
                
            );

        return View::make('processo.andamento_novo', $data);

    }

    public function postAdicionarandamento(){
        $ret = array();
        try{
            $msg = '';
            try{
                $datahora = Carbon::createFromFormat( 'd/m/Y', Input::get('datahora')) ;
            } catch(\Exception $e){
                $msg .= "<br/>- data inválida. Informe dd/mm/aaaa.";
            }
            if (intval(Input::get('id_status_andamento')) == 0){
                $msg .= "<br/>- status não informado";
            }
            if ($msg == ''){
                $rep = new Westhead\Repositorios\RepositorioProcesso;
                $rep->atualiza_status_andamento_os(Input::get('id_os'), Input::get('id_status_andamento'), Input::get('obs_andamento'), \Auth::user()->id_usuario, Input::get('datahora'));
                $ret['codret'] = 1;
            } else {
                $ret['codret'] = 2;
                $ret['msg'] = $msg;
            }
        } catch(\Exception $e){
            $ret['codret'] = 2;
            $ret['msg'] = $e->getMessage().' '.$e->getTraceAsString();

        }
        return json_encode($ret);
    }
}