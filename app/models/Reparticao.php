<?php
/**
 */
class Reparticao extends Eloquent {
	protected $table = 'reparticao';
	protected $primaryKey = 'id_reparticao';

	protected $guarded = array();

	public function assumeDefaults()
	{
	}

	public static function regras()
	{
		return array(
				'descricao' => 'required',
				'id_pais' => 'required'
				);
	} 

	public static function mensagens()
	{
		return array(
				'descricao.required' => 'O nome da repartição é obrigatório!',
				'id_pais.required' => 'O país é obrigatório!'
			);
	}

}