<?php

namespace Westhead\Repositorios;

class RepositorioOs implements \InterfaceRepositorio{
    protected $validador;
    protected $errors;

    public function all($filtros = null, $orderBy = null, $qtd = 20, $fetch= true) {
        $reg = new \Os;

        if (isset($filtros['id_empresa']) && $filtros['id_empresa'] > 0) {
            $reg = $reg->daEmpresa($filtros['id_empresa']);
        }
        if (isset($filtros['id_servico'])) {
            $reg = $reg->doServico($filtros['id_servico']);
        }
        if (isset($filtros['id_empresa_armador']) && $filtros['id_empresa_armador'] > 0) {
            $reg = $reg->doArmador($filtros['id_empresa_armador']);
        }
        if (isset($filtros['id_projeto']) ) {
            $reg = $reg->doProjeto($filtros['id_projeto']);
        }
        if (isset($filtros['id_centro_de_custo']) ) {
            $reg = $reg->doCentro($filtros['id_centro_de_custo']);
        }
        if (isset($filtros['numero'])) {
            $reg = $reg->numero($filtros['numero']);
        }
        if (isset($filtros['dt_solicitacao_ini'])) {
            if (isset($filtros['dt_solicitacao_fim']) && $filtros['dt_solicitacao_fim'] != '') {
                $reg = $reg->SolicitadaEntre($filtros['dt_solicitacao_ini'], $filtros['dt_solicitacao_fim']);
            } else {
                $reg = $reg->SolicitadaEm($filtros['dt_solicitacao_ini']);
            }
        }
        if (isset($filtros['dt_execucao_ini'])) {
            if (isset($filtros['dt_execucao_fim']) && $filtros['dt_execucao_fim'] != '' ) {
                $reg = $reg->ExecutadaEntre($filtros['dt_execucao_ini'], $filtros['dt_execucao_fim']);
            } else {
                $reg = $reg->ExecutadaEm($filtros['dt_execucao_ini']);
            }
        }
        if (isset($filtros['nome_completo'])) {
            $reg = $reg->doCandidatoNome($filtros['nome_completo']);
        }
        if (isset($filtros['id_funcao'])) {
            $reg = $reg->doCandidatoFuncao($filtros['id_funcao']);
        }
        if (isset($filtros['id_status_os'])) {
            $reg = $reg->statusos($filtros['id_status_os']);
        }
        $reg = $reg->leftJoin('empresa', 'empresa.id_empresa', '=', 'os.id_empresa')
            ->leftJoin('empresa as armador', 'armador.id_empresa', '=', 'os.id_empresa_armador')
            ->leftJoin('projeto', 'projeto.id_projeto', '=', 'os.id_projeto')
            ->leftJoin('servico', 'servico.id_servico', '=', 'os.id_servico')
            ->leftJoin('status_os', 'status_os.id_status_os', '=', 'os.id_status_os')
            ->select(array('os.id_os', 'os.numero', 'os.dt_solicitacao', 'os.dt_execucao', 'os.id_status_os', 'empresa.razaosocial', 'armador.razaosocial as razaosocial_armador', 'projeto.descricao as descricao_projeto', 'servico.descricao as descricao_servico', 'status_os.nome as nome_status'))
            ->orderBy('numero', 'desc');
        if ($fetch){
            if ($qtd > 0){
                $reg = $reg->paginate($qtd);
            } else {
                $reg = $reg->get();
            }            
        }
        
        return $reg;
    }

    /**
     * Recupera ou cria um visto atual para o candidato informado
     * @param type $candidato
     * @return \Visto
     */
    public function adicionaCandidato(\Os $os, \Candidato $candidato) {
        $os->candidatos()->attach($candidato->id_candidato);
        // Recupera o visto atual do candidato
        $candidato->atualizaEmpresaProjeto($os->id_empresa, $os->id_projeto);

        // Recupera o processo
        $repP = new \Westhead\Repositorios\RepositorioProcesso();
        $processo = $repP->criaProcessoDaOs($os, $candidato);
        // if(is_object($processo)){
        //     // Associa o processo ao visto atual, porque o candidato acabou de ser adicionado na OS
        //     $repV = new \Westhead\Repositorios\RepositorioVisto();
        //     $vistoAtual = $repV->recuperaVistoAtual($candidato);
        //     $repP->vinculaProcessoAoVisto($processo, $vistoAtual);
        // }
        
    }

    public function removeCandidato(\Os $os, \Candidato $candidato) {
        \Processo::where('id_os', '=', $os->id_os)
                 ->where('id_candidato', '=', $candidato->id_candidato)->delete();
        $os->candidatos()->detach($candidato->id_candidato);    
        return true;
    }
    
    public function executadaHoje(\Os $os){
        $os->dt_execucao = date('d/m/Y');
        $os->id_usuario_execucao = \Auth::user()->id_usuario;
        $os->save();
    }
    
    public function concluida(\Os $os, $id_usuario = ''){
        $os->id_status_os = \StatusOs::stCONCLUIDA;
        $os->dt_conclusao = date('Y-m-d H:i:s');
        $os->id_usuario_conclusao = \Auth::user()->id_usuario;
        $os->save();
    }
    
    public function faturada(\Os $os, $id_usuario = ''){
        $os->id_status_os = \StatusOs::stFATURADA;
        $os->dt_faturamento = date('Y-m-d H:i:s');
        $os->id_usuario_faturamento = \Auth::user()->id_usuario;
        $os->save();
    }
    
    public function liberada(\Os $os, $id_usuario = ''){
        $os->id_status_os = \StatusOs::stLIBERADA;
        $os->dt_liberacao = date('Y-m-d H:i:s');
        $os->id_usuario_liberacao = \Auth::user()->id_usuario;
        $os->save();
    }

    public function nova(){

    }

    public function salvar($post, $id_usuario){

    }

    public function excluir(\Os $os, $id_usuario){
        \DB::beginTransaction();
            \Processo::where('id_os', '=', $os->id_os)->delete();
            $os->delete();
        \DB::commit();
    }
    
    public function desmembrar(\Os $os, $id_candidatos){
        $novaOs = new \Os;
        $novaOs->fill($os->attributesToArray());
        $novaOs->id_os = null;
        $novaOs->dt_execucao = null;
        $novaOs->dt_assinatura = null;
        $novaOs->dt_solicitacao = date('d/m/Y');
        $novaOs->created_at = null;
        $novaOs->updated_at = null;
        $novaOs->save();
        foreach($id_candidatos as $id_candidato){
            $cand = \Candidato::find($id_candidato);
            $this->removeCandidato($os, $cand);
            $this->adicionaCandidato($novaOs, $cand);
            // Leva os dependentes junto
            $deps = \Candidato::where('id_candidato_principal', '=', $id_candidato)
                    ->whereIn('id_candidato', function($query) use ($os){
                           $query->select('id_candidato')->from('os_candidato')->where('id_os','=', $os->id_os);
                    })
                    ->get();
            foreach($deps as $dep){
                $this->removeCandidato($os, $dep);
                $this->adicionaCandidato($novaOs, $dep);
            }
            
        }
        return $novaOs;
    }

    public function getErrors(){

    }
}
