<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCentroDeCustoTable extends Migration {
	public function up()
	{
		Schema::create('centro_de_custo', function(Blueprint $table) {
			$table->increments('id_centro_de_custo');
			$table->integer('id_empresa')->unsigned();
			$table->string('nome', 500);
			$table->text('observacao')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('centro_de_custo');
	}

}
