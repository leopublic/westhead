<?php
class Armador extends Empresa {
	
	public function __construct()
	{
		$this->fl_armador = 1;
	}

	public function scopeTodos($query)
	{
		return $query->where('fl_armador', '=', 1);
	}	
}
