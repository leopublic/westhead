<?php

class ProtocolobsbController extends BaseController {

    protected $campos;
    protected $obj;

    public function getPendentes() {
        $dt_protocolo_fmt = Input::old('dt_protocolo', date('d/m/Y'));
        try {
            $dt_protocolo = \Carbon::createFromFormat('d/m/Y', $dt_protocolo_fmt)->format('Y-m-d');
        } catch (Exception $ex) {
            Session::flash('flash_msg', 'Informe uma data de protocolo válida.');
            $dt_protocolo = '';
        }
        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", date_format(os.dt_solicitacao, '%d/%m/%Y') dt_solicitacao"
                . ", empresa.razaosocial"
                . ", empresa.apelido"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.string_1"
                . ", date_format(processo.date_1, '%d/%m/%Y') date_1"
                . ", processo.dt_envio_bsb"
                . ", date_format(processo.dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt"
                . " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join processo on processo.id_os = os.id_os"
                . " where servico.fl_bsb = 1"
                . " and processo.dt_envio_bsb is null ";
        if (Input::get('tipo_servico', Input::old('tipo_servico')) != '') {
            $sql .= " and servico.id_tipo_servico in (" . Input::get('tipo_servico', Input::old('tipo_servico')) . ")";
        }
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $sql .= " and os.id_empresa = " . Input::get('id_empresa', Input::old('id_empresa'));
        }
        if (Input::get('id_projeto', Input::old('id_projeto')) > 0) {
            $sql .= " and os.id_projeto = " . Input::get('id_projeto', Input::old('id_projeto'));
        }
        if (Input::get('numero', Input::old('numero')) > 0) {
            $sql .= " and os.numero = " . Input::get('numero', Input::old('numero'));
        }
        $sql .= " limit 100";
        $registros = \DB::select(\DB::raw($sql));

        $empresas = array('' => '(todas)') + \Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        $servicos = array('' => '(todos)') + \Servico::orderBy('descricao')->lists('descricao', 'id_servico');
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $projetos = \Projeto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))
                    ->orderBy('descricao')
                    ->lists('descricao', 'id_projeto');
        } else {
            $projetos = array();
        }
        return View::make('protocolobsb.pendente')
                        ->with('registros', $registros)
                        ->with('dt_protocolo', $dt_protocolo)
                        ->with('dt_protocolo_fmt', $dt_protocolo_fmt)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
                        ->with('servicos', $servicos)
        ;
    }

    public function postPendentes() {
        Input::flash();
        return $this::getPendentes();
    }

    public function getEnviados($dt_protocolo = '') {

        if ($dt_protocolo == '') {
            $dt_protocolo_ini = Input::get('dt_protocolo_ini', Input::old('dt_protocolo_ini', date('Y-m-d')));
        } else {
            $dt_protocolo_ini = $dt_protocolo;
        }
        $dt_protocolo_fim = Input::get('dt_protocolo_fim', Input::old('dt_protocolo_fim'));
        
        if ($dt_protocolo_fim == '' || $dt_protocolo_fim == $dt_protocolo_ini){
            $dt_protocolo = $dt_protocolo_ini;
            $dt_protocolo_fim = $dt_protocolo_ini;
        } else {
            $dt_protocolo = '';
        }
        
        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", empresa.razaosocial"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.string_1"
                . ", date_format(processo.date_1, '%d/%m/%Y') date_1"
                . ", processo.dt_envio_bsb"
                . ", date_format(processo.dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt"
                . " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join processo on processo.id_os = os.id_os"
                . " where servico.fl_bsb = 1"
                . " and processo.dt_envio_bsb >= '" . $dt_protocolo_ini . "'"
                . " and processo.dt_envio_bsb <= '" . $dt_protocolo_fim . "'";

        if (Input::get('tipo_servico', Input::old('tipo_servico')) != '') {
            $sql .= " and servico.id_tipo_servico in (" . Input::get('tipo_servico', Input::old('tipo_servico')) . ")";
        }
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $sql .= " and os.id_empresa = " . Input::get('id_empresa', Input::old('id_empresa'));
        }
        if (Input::get('id_projeto', Input::old('id_projeto')) > 0) {
            $sql .= " and os.id_projeto = " . Input::get('id_projeto', Input::old('id_projeto'));
        }
        if (Input::get('numero', Input::old('numero')) > 0) {
            $sql .= " and os.numero = " . Input::get('numero', Input::old('numero'));
        }
        $registros = \DB::select(\DB::raw($sql));

        $empresas = array('' => '(todas)') + \Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        $servicos = array('' => '(todos)') + \Servico::orderBy('descricao')->lists('descricao', 'id_servico');
        $protocolos = array(date('Y-m-d') => date('d/m/Y')) + \ProtocoloBsb::orderBy('dt_protocolo', 'desc')->lists(\DB::raw("date_format(dt_protocolo, '%d/%m/%Y')"), 'dt_protocolo');
        $protocolos_fim = array(date('Y-m-d') => date('d/m/Y')) + \ProtocoloBsb::orderBy('dt_protocolo', 'desc')->lists(\DB::raw("date_format(dt_protocolo, '%d/%m/%Y')"), 'dt_protocolo');
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $projetos = \Projeto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))
                    ->orderBy('descricao')
                    ->lists('descricao', 'id_projeto');
        } else {
            $projetos = array();
        }
        if ($dt_protocolo != ''){
            $protocolo = \ProtocoloBsb::where('dt_protocolo', '=', $dt_protocolo)->first();
            if ($protocolo == null) {
                $protocolo = new \ProtocoloBsb;
            }
            $dt_protocolo_fmt = \Carbon::createFromFormat('Y-m-d', $dt_protocolo)->format('d/m/Y');            
        } else {
            $dt_protocolo_fmt = '';
            $protocolo = null;
        }

        return View::make('protocolobsb.enviados')
                        ->with('registros', $registros)
                        ->with('protocolo', $protocolo)
                        ->with('dt_protocolo', $dt_protocolo)
                        ->with('dt_protocolo_ini', $dt_protocolo_ini)
                        ->with('dt_protocolo_fim', $dt_protocolo_fim)
                        ->with('dt_protocolo_fmt', $dt_protocolo_fmt)
                        ->with('protocolos', $protocolos)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
                        ->with('servicos', $servicos)
        ;
    }

    public function postEnviados() {
        Input::flash();
        if (Input::has('acao') && Input::get('acao') == 'salvar') {
            $dt_protocolo = Input::get('dt_protocolo');
            $prot = \ProtocoloBsb::where('dt_protocolo', '=', $dt_protocolo)->first();
            $prot->observacao = Input::get('observacao');
            $prot->save();
            Session::flash('flash_msg', 'Protocolo atualizado com sucesso');
            return Redirect::to('protocolobsb/enviados/' . $dt_protocolo);
        } else {
            return $this->getEnviados();
        }
    }

    public function getAdicionar($dt_protocolo, $id_os) {
        try {
            \Processo::where('id_os', '=', $id_os)->update(array('dt_envio_bsb' => $dt_protocolo));
            $prot = \ProtocoloBsb::where('dt_protocolo', '=', $dt_protocolo)->first();
            if ($prot == null) {
                $prot = new \ProtocoloBsb;
                $prot->dt_protocolo = $dt_protocolo;
                $prot->save();
            }
            Session::flash('flash_msg', 'Ordem de serviço adicionada com sucesso!');
            return Redirect::to('protocolobsb/pendentes')->with('dt_protocolo', $dt_protocolo);
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
            return Redirect::back();
        }
    }

    public function getRetirar($id_os) {
        try {
            \Processo::where('id_os', '=', $id_os)->update(array('dt_envio_bsb' => null));
            Session::flash('flash_msg', 'Ordem de serviço adicionada com sucesso!');
            return Redirect::back();
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
            return Redirect::back();
        }
    }

    public function getEnviadosexcel($dt_protocolo) {
        $dt_protocolo_fmt = \Carbon::createFromFormat('Y-m-d', $dt_protocolo)->format('d/m/Y');

        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", os.prazo_solicitado"
                . ", empresa.razaosocial"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.string_1 numero_processo"
                . ", processo.string_2"
                . ", date_format(processo.date_1, '%d/%m/%Y') dt_requerimento"
                . ", processo.dt_envio_bsb"
                . ", date_format(processo.dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt"
                . ", cc.nome nome_centro_de_custos"
                . ", reparticao.descricao descricao_reparticao"
                . ", candidato.nome_completo"
                . " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join os_candidato on os_candidato.id_os= os.id_os"
                . " join candidato on candidato.id_candidato= os_candidato.id_candidato"
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join tipo_servico on tipo_servico.id_tipo_servico = servico.id_tipo_servico"
                . " join processo on processo.id_os = os.id_os and processo.id_candidato = os_candidato.id_candidato"
                . " left join centro_de_custo cc on cc.id_centro_de_custo = os.id_centro_de_custo"
                . " left join reparticao on reparticao.id_reparticao = processo.integer_2"
                . " where processo.dt_envio_bsb = '" . $dt_protocolo . "'"
                . " and tipo_servico.id_tipoprocesso in  (1,8)";

        $registros = \DB::select(\DB::raw($sql));

        $trab = View::make('protocolobsb.enviados_excel')
                ->with('registros', $registros)
                ->with('titulo', 'MINISTÉRIO DO TRABALHO')
                ->with('dt_protocolo_fmt', $dt_protocolo_fmt)
        ;
        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", os.prazo_solicitado"
                . ", empresa.razaosocial"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.string_1 numero_processo"
                . ", processo.string_2"
                . ", date_format(processo.date_1, '%d/%m/%Y') dt_requerimento"
                . ", processo.dt_envio_bsb"
                . ", date_format(processo.dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt"
                . ", cc.nome nome_centro_de_custos"
                . ", reparticao.descricao descricao_reparticao"
                . ", candidato.nome_completo"
                . " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join os_candidato on os_candidato.id_os= os.id_os"
                . " join candidato on candidato.id_candidato= os_candidato.id_candidato"
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join tipo_servico on tipo_servico.id_tipo_servico = servico.id_tipo_servico"
                . " join processo on processo.id_os = os.id_os and processo.id_candidato = os_candidato.id_candidato"
                . " left join centro_de_custo cc on cc.id_centro_de_custo = os.id_centro_de_custo"
                . " left join reparticao on reparticao.id_reparticao = processo.integer_2 "
                . " where processo.dt_envio_bsb = '" . $dt_protocolo . "'"
                . " and tipo_servico.id_tipoprocesso = 5";

        $registros = \DB::select(\DB::raw($sql));

        $just = View::make('protocolobsb.enviados_excel')
                ->with('registros', $registros)
                ->with('titulo', 'MINISTÉRIO DA JUSTIÇA')
                ->with('dt_protocolo_fmt', $dt_protocolo_fmt)
        ;
        
        $ret = '<style> th{border:solid 1px #000;} td{border:solid 1px #000;} </style>';
        $ret .= '<table>'.$trab.$just.'</table>';

        $ret = iconv('UTF-8', 'ISO-8859-1', $ret);

        
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel; charset=utf-8',
            'Content-Disposition' => 'attachment; filename="Protocolo-'.$dt_protocolo.'.xls"',
        );
         return Response::make($ret, 200, $headers);

    }

}
