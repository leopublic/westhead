<?php
/**
 * @property int $id_visto
 * @property int $id_candidato
 * @property varchar $processo_atual
 * @property date $dt_prazo_estada
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property int $id_usuario
 * @property varchar $controller
 * @property varchar $metodo
 */

class Visto extends Modelo {

    protected $table = 'visto';
    protected $primaryKey = 'id_visto';
    protected $guarded = array();

    public function assumeDefaults() {
        
    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

    public function processos() {
        return $this->hasMany('Processo', 'id_visto');
    }

    public function processosemordem() {
        return $this->hasMany('Processo', 'id_visto')->orderBy('ordem', 'desc');
    }

    public function candidato() {
        return $this->belongsTo('Candidato', 'id_candidato');
    }

    public function scopeDocandidato($query, $id_candidato){
        return $query->where('id_candidato', '=', $id_candidato);
    }
    
    public function scopeInativos($query){
        return $query->whereRaw('coalesce(processo_atual, 0) = 0');
    }
    
    public function reordeneProcessos() {
        $processos = \Processo::where('id_visto', '=', $this->id_visto)
                ->orderBy('ordem')
                ->orderBy('id_processo')
                ->get()
        ;
        $ordem = 10;
        foreach ($processos as $processo) {
            $processo->ordem = $ordem;
            $processo->save();
            $ordem = $ordem + 10;
        }
    }

    /**
     * @return Processo Processo principal do visto
     */
    public function processo_principal() {
        return \Processo::where('id_processo', '=', $this->id_processo_principal)
                        ->first();
    }

    /**
     * Retorna a data em que o processo original do visto foi solicitado
     */
    public function getDtRequerimentoAttribute(){
        if(is_object($this->processo_principal)){
            return $this->processo_principal->date_1;
        }
    }
    /**
     * Retorna o serviço do processo que originou o visto
     */
    public function getDescricaoServicoAttribute(){
        if(is_object($this->processo_principal)){
            return $this->processo_principal->descricao_servico;
        }
        
    }
    public function getTituloAttribute($data) {
        $processo = $this->processo_principal();
        if (is_object($processo)) {
            $ret = '"';
            $servico = $processo->servico;
            if (count($servico) > 0) {
                $ret .= $servico->descricao;
            }
            if (is_object($processo)) {
                if ($processo->string_1 != '') {
                    $ret .= ' ('.$processo->string_1.')';
                } else {
                    $ret .= ' (sem número)';
                }
            } else {
                $ret .= ' (??)';
            }
            $ret .= '"';
        } else {
            $ret = '--';
        }
        return $ret;
    }

    
    
}
