<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdTipoAutorizacaoToServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('servico', function(Blueprint $table) {
			$table->integer('id_tipo_autorizacao')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('servico', function(Blueprint $table) {
			$table->dropColumn('id_tipo_autorizacao');
		});
	}

}
