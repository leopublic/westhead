<?php

class ProcessoRegistroController extends \ProcessoGenericoController {

    public function getEmaberto() {
        return $this->registrosEmAberto();
    }

    public function postEmaberto() {
        Input::flash();
        return $this->registrosEmAberto();
    }

    public function registrosEmAberto(){
        set_time_limit(0);
        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", empresa.apelido"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.id_processo"
                . ", processo.string_1"
                . ", processo.string_2"
                . ", date_format(processo.date_1, '%d/%m/%Y') date_1"
                . ", date_format(processo.date_2, '%d/%m/%Y') date_2"
                . ", candidato.id_candidato"
                . ", candidato.nome_completo"
                . ", candidato.nu_rne"
                . " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join os_candidato osc on osc.id_os = os.id_os"                
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join processo on processo.id_os = os.id_os and processo.id_candidato = osc.id_candidato"
                . " join candidato on candidato.id_candidato = osc.id_candidato"
                . " where servico.id_tipo_servico = 3 ";
        $filtrou = false;
        if (Input::get('situacao', Input::old('situacao', 'P')) == 'P') {
            $sql .= " and (processo.string_1 is null or processo.string_1 = '') ";
        }
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $sql .= " and os.id_empresa = " . Input::get('id_empresa', Input::old('id_empresa'));
            $filtrou = true;
        }
        if (Input::get('id_projeto', Input::old('id_projeto')) > 0) {
            $sql .= " and os.id_projeto = " . Input::get('id_projeto', Input::old('id_projeto'));
            $filtrou = true;
        }
        if (Input::get('numero', Input::old('numero')) > 0) {
            $sql .= " and os.numero = " . Input::get('numero', Input::old('numero'));
            $filtrou = true;
        }
        $sql .= " order by os.numero desc, nome_completo asc";
        if ($filtrou) {
            $registros = \DB::select(\DB::raw($sql));
        } else {
            $registros = null;
        }
        $empresas = array('' => '(todas)') + \Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        $servicos = array('' => '(todos)') + \Servico::orderBy('descricao')->lists('descricao', 'id_servico');
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $projetos = \Projeto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))
                    ->orderBy('descricao')
                    ->lists('descricao', 'id_projeto');
            $projetos = array('' => '(todos)') + $projetos;
        } else {
            $projetos = array();
        }
        return View::make('processo.registroemaberto')
                        ->with('registros', $registros)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
                        ->with('servicos', $servicos)
        ;
        
    }
    
    public function postUpdate($id) {
        $processo = ProcessoRegistro::find($id);
        try {
            $msg = '';
            if (strlen(str_replace("_", "", Input::get('numero_processo'))) < 20) {
                $msg .= "<br/>- O número do processo está incompleto.";
            }
            if (Input::get('data_requerimento') == '' || Input::get('data_requerimento') == '__/__/____') {
                $msg .= "<br/>- A data do requerimento é obrigatória.";
            } else {
                if (!Modelo::dataOk(Input::get('data_requerimento'))){
                    $msg .= "<br/>- A data do requerimento é inválida: '".Input::get('data_requerimento')."'";
                }
            }
            if (Input::get('validade_protocolo') == '' || Input::get('validade_protocolo') == '__/__/____') {
            } else {
                if (!Modelo::dataOk(Input::get('validade_protocolo'))){
                    $msg .= "<br/>- A data de validade do protocolo é inválida: '".Input::get('validade_protocolo')."'";
                }
            }
            if ($msg != '') {
                Session::flash($processo->id_processo."_error", "O processo não foi atualizado. Verifique o preenchimento:".$msg);
            } else {
                $processo->fill(Input::all());
                if ($processo->id_empresa == '' || $processo->id_empresa == 0){
                    $processo->id_empresa = null;
                }
                if ($processo->id_projeto == '' || $processo->id_projeto == 0){
                    $processo->id_projeto = null;
                }
                $processo->save();
                if ($processo->dataRequerimento != '' && $processo->dataRequerimento != '00/00/0000' && is_object($processo->os)) {
                    $processo->os->concluiPeloProcesso($processo->dataRequerimento);
                }
                $msg = "Processo atualizado com sucesso!";
                if ($processo->servico->tiposervico->fl_monitorar_mais_recente){
                    $repV = new Westhead\Repositorios\RepositorioVisto;
                    $mais_recente = $repV->atualiza_mais_recente($processo);
                    if ($mais_recente == $processo->id_processo){
                        $msg .= '<strong> ATENÇÃO: Esse processo é o mais atual do visto</strong>';
                    }
                }


                Session::flash($processo->id_processo."_success", $msg);
            }
        } catch (Exception $ex) {
            Session::flash($processo->id_processo."_error", $ex->getMessage());
        }
        $proccontrol = new \ProcessoController();
        return $proccontrol->rotaParaVisualizarProcesso($processo);
    }

    public function getAdicione($id_visto) {
        $rep = new Westhead\Repositorios\RepositorioProcesso;
        $processo = $rep->criaProcessoNoVisto(3, $id_visto);
        $proccontrol = new \ProcessoController();
        Session::flash("processoregistro_msg", "Processo adicionado com sucesso!");
        return $proccontrol->rotaParaVisualizarProcesso($processo);
    }
    
}
