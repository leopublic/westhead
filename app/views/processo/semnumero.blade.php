@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Processos sem número', 'subTitulo' => ''))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
                {{ Form::open(array( "class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'numero', 'label' => 'Número', 'valor' => Input::old('numero', Session::get('numero')),  'help' => '',  array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => Input::old('id_empresa', Session::get('id_empresa')), 'valores' => $empresas , 'help' => '', 'valores', array()))
                    </div>

                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Emb./Proj.', 'valor' => Input::old('id_projeto', Session::get('id_projeto')), 'valores' => $projetos , 'help' => '', 'valores', array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'tipo_servico', 'label' => 'Serviço', 'valor' => Input::old('tipo_servico'), 'valores' => array(''=> '(todos)', '1,2'=>'Autorizações', '5'=>'Prorrogações', '8'=>'Cancelamentos') , 'help' => '', 'valores', array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'situacao', 'label' => 'Situação', 'valor' => Input::old('situação'), 'valores' => array('P'=> 'Somente pendentes', 'T'=>'Todos') , 'help' => '', 'valores', array()))
                    </div>
                </div>
            </div>
        </div>
            {{ Form::close()}}
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Processos</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                @if(isset($registros))
                <table class="table table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 80px; text-align:center;">Os</th>
                            <th style="width: auto;">Empresa<br/><span style="font-style:italic;">Projeto/Emb.</span></th>
                            <th style="width: 200px">Candidatos</th>
                            <th style="width: auto;">Serviço</th>
                            <th style="width: auto; text-align: center;">Processo</th>
                            <th style="width: auto; text-align: center;">Dt. req.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $registro)
                        <tr>
                            <td><a class="btn btn-primary btn-sm" href="{{ URL::to('/os/show/'.$registro->id_os) }}" target="_blank">{{ substr('000000'.$registro->numero, -6) }}</a></td>
                            <td>{{ $registro->apelido}}<br/><span style="font-style: italic; color:#AAAAAA;">{{ $registro->descricao_projeto }}</span></td>
                            <td>
                                <? $br = '';
                                $os = \Os::find($registro->id_os);
                                $candidatos = $os->candidatos;
                                ?>
                                @foreach($candidatos as $candidato)
                                {{$br}}<a href="{{URL::to('/candidato/vistoatual/'.$candidato->id_candidato)}}" target="_blank" title="Clique para ver o candidato em uma nova janela">{{ $candidato->nome_completo }}</a>
                                <? $br = '<br/>';?>
                                @endforeach
                            </td>
                            <td>
                               {{ $registro->descricao_servico  }}
                            </td>
                            <td style="text-align: center;"><input type="text" value="{{$registro->string_1}}" class="input-block-level" name="processo" data-mask="99999.999999/9999-99" onblur="javascript:atualizaCampo(this, '9');" data-parameters='{"controller":"processo", "metodo":"atualizacampo", "campo_chave":"id_os", "valor_chave":"{{$registro->id_os}}", "campo":"string_1"  }' data-valor-original="{{$registro->string_1}}" data-mask="99999.999999/9999-99" placeholder/></td>
                            <td style="text-align: center;"><input type="text" value="{{$registro->date_1}}"  class="input-block-level" name="processo" data-mask="99/99/9999" onblur="javascript:atualizaCampo(this, 'D');" data-parameters='{"controller":"processo", "metodo":"atualizacampodata", "campo_chave":"id_os", "valor_chave":"{{$registro->id_os}}", "campo":"date_1"  }' data-valor-original="{{$registro->date_1}}"/></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                Informe um filtro
                @endif
            </div>
    </div>
    </div>
</div>
@stop
@section('scripts')
<script>
$( document ).ready(function(){
    VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajsonfiltro/', '(selecione a empresa)', false);
});
$( document ).ready(function(){
    $('#menuAcompanhamento').addClass('active');
    $('#itemProcessosNovos').addClass('active');
});


function atualizaCampo(pinput, ptipo){
    var valor = $(pinput).val();
    var td = $(pinput).parent();
    if (ptipo == '9'){
        var valor_limpo = valor.replace(/\D/g,'');        
    } else {
        var valor_limpo = valor;        
    }
    valor_original = $(pinput).attr('data-valor-original');
    if (valor_limpo != valor_original  && valor.indexOf("_") == -1){
        td.animate({backgroundColor: "#ede32d"}, 250 );
        var pParametros = $(pinput).attr('data-parameters');
        var parm = jQuery.parseJSON(pParametros);
        var url = '{{URL::to('')}}/'+parm.controller+'/'+parm.metodo;
        parm['valor'] = valor;
        $.noty.closeAll();
        $.ajax({
            type: 'POST'
          , url: url
          , data: parm
          , dataType: 'json'
          , success:
                function(data) 
                {
                    if(data.msg.tipoMsg == 'success'){
                        $(pinput).attr('data-valor-original', valor);
                        td.animate({backgroundColor: "#8de580"}, 250 );
                        if(data.novoConteudo){
                            $(td).html(data.novoConteudo);
                        }
                    }
                    else{
                        if(data.msg.tipoMsg == 'nop'){
                            $.noty.closeAll();
                            data.msg.textoMsg = 'Não foi possível atualizar devido ao seguinte erro: '+data.msg.textoMsg;
                            var xx= noty({text: data.msg.textoMsg, timeout: false, type: 'error', closeWith: 'click', layout:'top' });
                        }
                    }
                }
        });        
    }
}

</script>
@stop
