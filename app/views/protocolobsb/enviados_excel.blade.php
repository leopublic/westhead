        <tr>
            <th colspan="11">{{$titulo}}- {{$dt_protocolo_fmt}}</th>
        </tr>
        <tr>
            <th style="width: 80px; text-align: center;">O.S.</th>
            <th style="width: auto;">Cliente</th>
            <th style="width: auto;">C.c.</th>
            <th style="width: 200px">Estrangeiro</th>
            <th style="width: auto;">Projeto/Emb.</th>
            <th style="width: auto;">Serviço</th>
            <th style="width: auto; text-align: center;">Prazo</th>
            <th style="width: auto; text-align: center;">Consulado</th>
            <th style="width: auto; text-align: center;">Data prot.</th>
            <th style="width: auto; text-align: center;">N&ordm; prot.</th>
            <th style="width: auto; text-align: center;">Aut</th>
        </tr>
        @foreach($registros as $registro)
        <tr>
            <td style="text-align: center;">{{ substr('000000'.$registro->numero, -6) }}</td>
            <td>{{ $registro->razaosocial }}</td>
            <td>{{ $registro->nome_centro_de_custos }}</td>
            <td>{{ $registro->nome_completo}}</td>
            <td>{{ $registro->descricao_projeto }}</td>
            <td>{{ $registro->descricao_servico  }}</td>
            <td style="text-align: center;">{{ $registro->prazo_solicitado  }}</td>
            <td>
                @if ($registro->descricao_reparticao != '')
                {{ $registro->descricao_reparticao }}
                @else
                --
                @endif
            </td>
            <td style="text-align: center;">{{ $registro->dt_requerimento  }}</td>
            <td style='mso-number-format:"\@";'>{{ $registro->numero_processo  }}</td>
            <td>&nbsp;</td>
        </tr>
        @endforeach
