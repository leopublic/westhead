<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDespesaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('despesa');
		Schema::create('despesa', function(Blueprint $table) {
			$table->increments('id_despesa');
			$table->integer('id_tipo_despesa')->unsigned();
			$table->integer('id_os')->unsigned();
			$table->decimal('qtd', 10, 2);
			$table->dateTime('dt_ocorrencia');
			$table->text('observacao');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('despesa');
	}

}
