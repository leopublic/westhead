@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>{{ $tela['titulo_tela'] }}</h1>
                        <h4>{{ $tela['subTitulo'] }}</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Layout</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if(count($errors->all())> 0)
                <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-danger">
                        <ul class="errors">
                          @foreach($errors->all() as $message)
                          <li>{{ $message }}</li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                </div>
                @endif
                <!-- BEGIN Main Content -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-picture"></i> Logo atual</h3>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                      <div class="col-md-12">
                                            <img src="/arquivos/logos/{{$obj->id_empresa}}.{{$obj->extensao_logo}}"/>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Informe os dados da empresa</h3>
                            </div>
                            <div class="box-content">

                                {{ Form::open(array('url' => 'empresa/update/'.$obj->id_empresa, "class"=>"form-horizontal")) }}
                                    <div class="form-group">
                                      {{Form::label('razaosocial', 'Razão social', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::text('razaosocial', Input::old('razao_social', $obj->razaosocial), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Razão social</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('nomefantasia', 'Nome fantasia', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::text('nomefantasia', Input::old('nomefantasia', $obj->nomefantasia), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Nome de fantasia</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('cnpj', 'CNPJ', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9  controls">
                                          {{ Form::text('cnpj', Input::old('cnpj', $obj->cnpj), $attributes = array('class' => 'form-control', 'data-mask' => '99.999.999/9999-99')); }}
                                          <span class="help-inline">CNPJ</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('insc_estadual', 'Inscrição estadual', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-3 controls">
                                          {{ Form::text('insc_estadual', Input::old('insc_estadual', $obj->insc_estadual), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Inscrição estadual</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('insc_municipal', 'Inscrição municipal', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-3 controls">
                                          {{ Form::text('insc_municipal', Input::old('insc_municipal', $obj->insc_municipal), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Inscrição municipal</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('id_procurador', 'Procurador', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::select('id_procurador', array('0' =>'(n/d)') + Procurador::orderBy('nome')->lists("nome", 'id_procurador'), Input::old('id_procurador', $obj->id_procurador), $attributes = array('class' => 'form-control')); }}
                                       </div>
                                    </div>
                                    @include('html/campo-texto', array('id' => 'atividade_economica', 'label' => 'Atividade econômica', 'valor' => $obj->atividade_economica,  'help' => '',  array()))
                                    @include('html/campo-textarea', array('id' => 'objeto_social', 'label' => 'Objeto social', 'valor' => $obj->objeto_social,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'valor_capital_inicial', 'label' => 'Valor do capital inicial', 'valor' => $obj->valor_capital_inicial,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'valor_capital_atual', 'label' => 'Valor do capital atual', 'valor' => $obj->valor_capital_atual,  'help' => '',  array()))
                                    @include('html/campo-data', array('id' => 'dt_constituicao', 'label' => 'Data de constituição da empresa', 'valor' => $obj->dt_constituicao,  'help' => '',  array()))
                                    @include('html/campo-data', array('id' => 'dt_cadastro_bc', 'label' => 'Data de cadastro no Banco Central', 'valor' => $obj->dt_cadastro_bc,  'help' => '',  array()))
                                    @include('html/campo-data', array('id' => 'dt_alteracao_contratual', 'label' => 'Data da última alteração contratual', 'valor' => $obj->dt_alteracao_contratual,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'nome_empresa_estrangeira', 'label' => 'Nome da empresa estrangeira', 'valor' => $obj->nome_empresa_estrangeira,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'valor_investimento_estrangeiro', 'label' => 'Valor do investimento estrangeiro', 'valor' => $obj->valor_investimento_estrangeiro,  'help' => '',  array()))
                                    @include('html/campo-data', array('id' => 'dt_investimento_estrangeiro', 'label' => 'Data do investimento estrangeiro', 'valor' => $obj->dt_investimento_estrangeiro,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'qtd_empregados', 'label' => 'Qtd total de empregados', 'valor' => $obj->qtd_empregados,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'qtd_empregados_estrangeiros', 'label' => 'Qtd empregados estrangeiros', 'valor' => $obj->qtd_empregados_estrangeiros,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'nome_administrador', 'label' => 'Nome do administrador', 'valor' => $obj->nome_administrador,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'cargo_administrador', 'label' => 'Cargo do administrador', 'valor' => $obj->cargo_administrador,  'help' => '',  array()))
                                    @include('html/campo-textarea', array('id' => 'administradores', 'label' => 'Administradores (form 104)', 'valor' => $obj->administradores,  'help' => '',  array()))
                                    @include('html/campo-textarea', array('id' => 'associadas', 'label' => 'Associadas (form 104)', 'valor' => $obj->associadas,  'help' => '',  array()))
                                    <h3>Endereço e contato</h3>
                                    @include('html/campo-texto', array('id' => 'endereco', 'label' => 'Endereço', 'valor' => $obj->endereco,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'end_complemento', 'label' => 'Complemento', 'valor' => $obj->end_complemento,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'end_bairro', 'label' => 'Bairro', 'valor' => $obj->end_bairro,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'end_municipio', 'label' => 'Município', 'valor' => $obj->end_municipio,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'end_cep', 'label' => 'Cep', 'valor' => $obj->end_cep,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'end_uf', 'label' => 'Estado (sigla)', 'valor' => $obj->end_uf,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'tel_ddd', 'label' => 'DDD', 'valor' => $obj->tel_ddd,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'tel_num', 'label' => 'Telefone', 'valor' => $obj->tel_num,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'email', 'label' => 'Email', 'valor' => $obj->email,  'help' => '',  array()))

                                    <h3>Situação cadastral</h3>
                                    <div class="form-group">
                                        {{ Form::label('fl_ativa', 'Essa empresa está ativa?', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                            <label class="radio-inline">
                                                {{ Form::radio('fl_ativo', 1, $obj->fl_ativo)}} Sim
                                            </label>
                                            <label class="radio-inline">
                                                {{ Form::radio('fl_ativo', 0, !$obj->fl_ativo)}} Não
                                            </label>
                                            <br/><span class="help-inline">Indica se a empresa está ativa para novas ordens de serviço</span>
                                       </div>
                                    </div>
                                    <h3>Customização de relatórios</h3>
                                    <div class="form-group">
                                      {{Form::label('rev_embarcacoes', 'Versão da relação de embarcações', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::text('rev_embarcacoes', Input::old('rev_embarcacoes', $obj->rev_embarcacoes), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Observação sobre a versão do formulário que deve sair no rodapé da relação de embarcações</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('fl_obs_embarcacoes', 'Mostra embarcações ignoradas?', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                            <label class="radio-inline">
                                                {{ Form::radio('fl_obs_embarcacoes', 1, $obj->fl_obs_embarcacoes )}} Sim
                                            </label>
                                            <label class="radio-inline">
                                                {{ Form::radio('fl_obs_embarcacoes', 0, !$obj->fl_obs_embarcacoes)}} Não
                                            </label>
                                            <br/><span class="help-inline">Indica se a observação das embarcações fora do prazo de exigência de proporcionalidade deve ser exibida ou não no relatório de relação de embarcações da empresa</span>
                                       </div>
                                    </div>
                                    <h3>Cobrança</h3>
                                    @include('html/campo-select', array('id' => 'id_tipo_cobranca', 'label' => 'Modalidade cobrança', 'valor' => $obj->id_tipo_cobranca, 'valores' => array('1' => 'NFE', '2' => 'Invoice') , 'help' => '',  array()))
                                    @include('html/campo-select', array('id' => 'id_tipo_calc_desp', 'label' => 'Conversão US$', 'valor' => $obj->id_tipo_calc_desp, 'valores' => array('1' => 'Cotação Westhead', '2' => 'Cotação específica') , 'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'valor_cotacao_propria', 'label' => 'Valor cotação', 'valor' => $obj->valor_cotacao_propria,  'help' => '',  array()))

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                           {{ HTML::link('empresa', 'Cancelar', array('class' => 'btn')) }}
                                        </div>
                                    </div>
                                 {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-camera"></i> Substitua o logo</h3>
                            </div>
                            <div class="box-content">
                                <form action="/empresa/novologo/{{$obj->id_empresa}}" class=" dropzone hidden-xs dz-clickable" id="my-awesome-dropzone">
                                    <div class="dz-default dz-message" style="background-image:none;">
                                        <span>Arraste o arquivo para cá ou clique</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


             </div>
@stop

@section('scripts')
    <script src="/assets/dropzone-3.8.3/downloads/dropzone.min.js"></script>
    <script>
    Dropzone.options.myAwesomeDropzone = {
      acceptedFiles: 'image/*', 
      dictInvalidFileType: 'Tipo de arquivo inválido. A logo deve ser um arquivo imagem, exemplo: jpg, bmp, gif, etc.'
    };
    </script>
@stop
