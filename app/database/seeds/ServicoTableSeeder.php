<?php

class ServicoTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('servico')->truncate();
		$servico = array('descricao' =>'Visto Consular');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Visto - Emergência');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Visto - RN 72');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Visto - RN 99');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Visto - RN 100');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Visto - RN 61');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Visto - RN 62');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Visto - RN 77');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Procedimento Administrativo para Dilação da Proporcionalidade exigida pela RN 72');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Naturalização');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Permanência com base cônjuge brasileiro ou filho(a) brasileiro(a) - Reunião Familiar - RN36');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Prorrogação - RN 72');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Prorrogação - Visto de Turismo');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Prorrogação - RN 61');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Prorrogação - RN 99');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Transformação em Permanente - RN 99 ');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Processo de Alteração de Assentamentos');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Assistência Imigração - Chegada no Brasil');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Assistência Imigração - Partida do Brasil');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Registro');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Restabelecimento');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Renovação de Protocolo RNE');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Segunda via de Protocolo RNE');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Coleta da CIE');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Retificação VAF');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Segunda via de VAF');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Segunda via de Protocolo de Prorrogação');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Cancelamento de Visto');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Arquivamento de Visto');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Desarquivamento de Visto');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Republicação DOU');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Transferência de Empregador');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Troca de Embarcação');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Cumprimento de Exigência');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Juntada');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Esclarecimento');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Dilação');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Troca de RCB');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Revalidação do Prazo para Coleta de Visto (MRE)');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Revalidação do Prazo para Entrada no Brasil após Coleta de Visto (RCB)');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Coleta de Visto junto a RCB no exterior');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Diligência - Coleta de Documentos');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Diligência - Reunião MTE');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Diligência - Reunião Sindimar');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Emissão de CPF');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Emissão de CNH');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Emissão de CTPS');
		DB::table('servico')->insert($servico);
		$servico = array('descricao' =>'Envio de Documento ao Exterior via Courier');
		DB::table('servico')->insert($servico);
	}

}
