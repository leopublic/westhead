<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CarregaStatusOsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('status_os')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $obj = array("nome"=> "nova", "sigla" => "N");
        DB::table('status_os')->insert($obj);
        $obj = array("nome"=> "concluída", "sigla" => "C");
        DB::table('status_os')->insert($obj);
        $obj = array("nome"=> "faturada", "sigla" => "F");
        DB::table('status_os')->insert($obj);
        DB::table('os')->update(array('id_status_os' => 1));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
