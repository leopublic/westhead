<?php

use Westhead\Repositorios;

class RepositorioCandidatoTest extends TestCase {

    public function setUp(){
        parent::setUp();
        \Auth::setUser(\Usuario::find(2));
    }
    
    public function testExclua() {
        $cand = new \Candidato;
        $cand->nome_completo = 'abc';
        $cand->nome_mae = "xyz";
        $cand->dt_nascimento = '12/12/2012';
        $cand->save();
        $this->assertNotEmpty($cand->id_candidato);
        $id_candidato = $cand->id_candidato;

        $repCand = new \Westhead\Repositorios\RepositorioCandidato;
        $repVisto = new \Westhead\Repositorios\RepositorioVisto;
        $visto = $repVisto->recuperaVistoAtual($cand);
        $this->assertNotEmpty($visto->id_visto);
        $id_visto = $visto->id_visto;

        $processo = new \ProcessoAutorizacao;
        $processo->id_candidato = $cand->id_candidato;
        $processo->id_visto = $visto->id_visto;
        $processo->save();
        $id_processo = $processo->id_processo;

        $this->assertNotEmpty($processo->id_processo);

        $x = \Processo::where('id_processo', '=', $id_processo)->get();
        $this->assertGreaterThan(0, count($x));

        $y= \Visto::where('id_visto', '=', $id_visto)->get();
        $this->assertGreaterThan(0, count($y));

        $z= \Candidato::where('id_candidato', '=', $id_candidato)->get();
        $this->assertGreaterThan(0, count($z));
        unset($x);
        unset($y);
        unset($z);

        try{
            $repCand->exclua($cand);
        } catch (Exception $ex) {
            $this->fail($ex->getMessage());
        }

        $x = \Processo::where('id_processo', '=', $id_processo)->get();
        $this->assertEquals(0,count($x));

        $y= \Visto::where('id_visto', '=', $id_visto)->get();
        $this->assertEquals(0,count($y));

        $z= \Candidato::where('id_candidato', '=', $id_candidato)->get();
        $this->assertEquals(0,count($z));




        if (isset($processo)){
            $processo->delete();
        }
        if (isset($visto)){
            $visto->delete();
        }
        if (isset($cand)){
            $cand->delete();
        }
    }
    
    public function dadosLimpaInputString(){
        return array(
            array("		teste", "teste")
            ,array("	 	 teste ", "teste")
            ,array("      tes     te    ", "tes te")
        );
    }
    
    /**
     * @dataProvider dadosLimpaInputString
     */
    public function testLimpaInputString($atual, $limpa){
        $repCand = new \Westhead\Repositorios\RepositorioCandidato;
        $res = $repCand->limpaInputString($atual);
        $this->assertEquals($limpa, $res);        
    }
    
    public function dadosCrie(){
        return array(
            array("", "teste", "12/11/2010")
            ,array("teste nome", "", "12/11/2010")
            ,array("teste nome", "teste", "")
        );
    }
    
    /**
     * @dataProvider dadosCrie
     */
    public function testCrie_incompleto($nome_completo, $nome_mae, $dt_nascimento){
        $repCand = new \Westhead\Repositorios\RepositorioCandidato;
        
        $post = array(
            "nome_completo" => $nome_completo
            ,"nome_mae"     => $nome_mae
            ,"dt_nascimento"=> $dt_nascimento
        );
        
        $cand = $repCand->crie($post);
        if ($cand){
            $this->assertFailed();
        }
        $errors = $repCand->getErrors();
        var_dump($errors);
        $this->assertGreaterThan(0, count($errors));
    }
    
    public function testCrie_existente(){
        $repCand = new \Westhead\Repositorios\RepositorioCandidato;
        
        $candOrig = new \Candidato;
        $candOrig->nome_completo = $this->generate_random_string(20);
        $candOrig->nome_mae = $this->generate_random_string(20);
        $candOrig->dt_nascimento = date('d/m/Y');
        $candOrig->save();
        
        $post = array(
            "nome_completo" => $candOrig->nome_completo
            ,"nome_mae"     => $candOrig->nome_mae
            ,"dt_nascimento"=> $candOrig->dt_nascimento
        );
        
        $cand = $repCand->crie($post);
        if ($cand){
            $this->assertFailed();
        }
        $errors = $repCand->getErrors();
        $this->assertGreaterThan(0, count($errors));
        
        $candOrig->delete();
    }
    
    public function testCrie_existenteComEspaco(){
        $repCand = new \Westhead\Repositorios\RepositorioCandidato;
        
        $candOrig = new \Candidato;
        $candOrig->nome_completo = $this->generate_random_string(20);
        $candOrig->nome_mae = $this->generate_random_string(20);
        $candOrig->dt_nascimento = date('d/m/Y');
        $candOrig->save();
        
        $post = array(
            "nome_completo" => "		".$candOrig->nome_completo
            ,"nome_mae"     => " ".$candOrig->nome_mae." 	"
            ,"dt_nascimento"=> $candOrig->dt_nascimento
        );
        
        $cand = $repCand->crie($post);
        if ($cand){
            $this->Fail();
        }
        $errors = $repCand->getErrors();
        $this->assertGreaterThan(0, count($errors));
        
        $candOrig->delete();
    }
}
