<?php

class PerfilController extends \CadastroController {

    const raiz = "perfil";
    const campoChave = 'id_perfil';
    const nomeSingular = 'Perfil de usuário';
    const nomePlural = 'Perfis de usuário';
    const artigo = 'o';

    public function RegistrosIndex() {
        $data = Perfil::all();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(
            array("titulo" => "Descrição", "campo" => "nome"),
        );
        return $data;
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = Perfil::find($id);
        } else {
            $this->obj = new Perfil;
        }
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = self::nomeSingular;
        $data['titulo_grid'] = "Lista";
        $data['subTitulo'] = "Administrar " . self::nomePlural;
        $data['urlEdit'] = self::raiz . '/show';
        $data['urlDel'] = self::raiz . '/del';
        $data['urlCreate'] = self::raiz . '/create';
        $data['nomeCampoChave'] = self::campoChave;
        return $data;
    }

    public function get_ViewIndex() {
        return self::raiz . '/list';        
    }
    
    public function get_ViewShow() {
        return self::raiz . '/show';
    }

    public function MontaCampos() {
        //	$this->campos[] = new 
    }

    public function getDel($id = 0) {
        try {
            Perfil::destroy($id);
            return Redirect::to(self::raiz)
                            ->with('flash_msg', self::nomeSingular . ' excluíd' . self::artigo . ' com sucesso!');
        } catch (Exception $e) {
            return Redirect::to(self::raiz)
                            ->with('flash_error', $e->getMessage());
        }
    }

    public function postUpdate($id = 0) {
        if ($id > 0) {
            $obj = Perfil::find($id);
        } else {
            $obj = new Perfil();
        }
        $obj->fill(Input::all());

        if ($obj->save()) {
            return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular . ' atualizad' . self::artigo . ' com sucesso!');
        } else {
            Input::flash();
            return Redirect::to(self::raiz . '/show/' . $id)->withErrors($obj->errors);
        }
    }

    public function getAcessos($id_perfil){
        $perfil = \Perfil::find($id_perfil);
        $sql = "select acao.id_acao, acao.id_tela, acao.descricao descricao_acao, tela.descricao descricao_tela, if (acesso.id_acesso is null , 0 , 1) tem_acesso"
                . " from acao"
                . " join tela on tela.id_tela = acao.id_tela"
                . " left join acesso on acesso.id_acao = acao.id_acao and acesso.id_perfil = ".$id_perfil
                . " order by tela.descricao, acao.descricao";
        $acoes = \DB::select(\DB::raw($sql));
        
        return View::make('perfil/acessos')
                    ->with('perfil', $perfil)
                    ->with('acoes', $acoes)
            ;
        
    }
    
    public function postAcessos(){
        try{
            $id_perfil = Input::get('id_perfil');
            $acoes = Input::get('id_acao');
            \Acesso::where('id_perfil', '=', $id_perfil)->whereNotIn('id_acao', $acoes)->delete();
            foreach($acoes as $id_acao){
                $acesso = \Acesso::where('id_perfil', '=', $id_perfil)->where('id_acao', '=', $id_acao)->first();
                if (!is_object($acesso)){
                    $acesso = new \Acesso;
                    $acesso->id_acao = $id_acao;
                    $acesso->id_perfil = $id_perfil;
                    $acesso->save();
                }
            }
            Session::flash('flash_msg', 'Acessos do perfil atualizados com sucesso!');
        } catch (Exception $ex) {
            Session::flash('flash_error', $ex->getMessage());
        }
        return Redirect::to('/perfil/acessos/'.$id_perfil);
    }
}
