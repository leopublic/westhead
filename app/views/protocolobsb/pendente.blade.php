@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Protocolo de Brasília - Não enviados', 'subTitulo' => ''))

@include('html/mensagens')
<? $qtdCol=2; ?>
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array( "class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                    <a data-action="collapse"><i class="icon-chevron-up"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    @include('html/campo-texto', array('id' => 'numero', 'label' => 'Número', 'valor' => Input::old('numero', Session::get('numero')),  'help' => '',  array()))
                    @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => Input::old('id_empresa', Session::get('id_empresa')), 'valores' => $empresas , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Emb./Proj.', 'valor' => Input::old('id_projeto', Session::get('id_projeto')), 'valores' => $projetos , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'tipo_servico', 'label' => 'Serviço', 'valor' => Input::old('tipo_servico'), 'valores' => array(''=> '(todos)', '1,2'=>'Autorizações', '5'=>'Prorrogações', '8'=>'Cancelamentos') , 'help' => '', 'valores', array()))
                    @include('html/campo-data', array('id' => 'dt_protocolo', 'label' => 'Adicionar ao protocolo', 'valor' => $dt_protocolo_fmt, 'help' => '', array()))
                </div>
            </div>
        </div>
        {{ Form::close()}}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Ordens de serviço sem data de envio</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                @if(isset($registros))
                <table class="table table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 80px">Ação</th>
                            <th style="width: 80px; text-align:center;">Os</th>
                            <th style="width: 80px; text-align:center;">Aberta em</th>
                            <th style="width: auto;">Empresa<br/>Projeto/Emb.</th>
                            <th style="width: 200px">Candidatos</th>
                            <th style="width: auto;">Serviço</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $registro)
                        <tr>
                            <td>
                                @if ($dt_protocolo != '')
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/protocolobsb/adicionar/'.$dt_protocolo.'/'.$registro->id_os) }}" title="Adicionar essa OS ao protocolo de {{$dt_protocolo_fmt}}"><i class="icon-plus"></i> {{$dt_protocolo_fmt}}</a>
                                @endif
                            </td>
                            <td style="text-align: center;"><a href="{{ URL::to('/os/show/'.$registro->id_os) }}" target="_blank">{{ substr('000000'.$registro->numero, -6) }}</a></td>
                            <td>{{ $registro->dt_solicitacao}}</td>
                            <td>{{ $registro->apelido }}<br/><span style="color:#AAAAAA;font-style: italic;">{{ $registro->descricao_projeto }}</span></td>
                            <td>
                                <? $br = '';
                                $os = \Os::find($registro->id_os);
                                $candidatos = $os->candidatos;
                                ?>
                                @foreach($candidatos as $candidato)
                                {{$br}}<a href="{{URL::to('/candidato/show/'.$candidato->id_candidato)}}" target="_blank" title="Clique para ver o candidato em uma nova janela">{{ $candidato->nome_completo }}</a>
                                <? $br = '<br/>';?>
                                @endforeach
                            </td>
                            <td>
                               {{ $registro->descricao_servico  }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                Selecione uma empresa.
                @endif
            </div>
    </div>
    </div>
</div>
@stop
@section('scripts')
<script>
$( document ).ready(function(){
    VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajsonfiltro/', '(selecione a empresa)', false);
});


</script>
@stop
