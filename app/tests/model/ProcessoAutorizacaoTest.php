<?php

class ProcessoAutorizacaoTest extends TestCase {


	/**
     * @dataProvider provider
	 */
	public function testAtributo($atributo, $valor, $campo)
	{
		$obj = new ProcessoAutorizacao;
		$obj->$atributo = $valor;
		$obj->id_candidato = 1;
		$obj->id_tipoprocesso = 1;

		$this->assertEquals($valor, $obj->$atributo);
		$this->assertEquals($valor, $obj->$campo);

		$obj->save();

		$id = $obj->id_processo;
		unset($obj);

		$obj = ProcessoAutorizacao::find($id);
		$this->assertEquals($valor, $obj->$atributo);
		$this->assertEquals($valor, $obj->$campo);
//		$obj->destroy($id);
	}

	public function provider()
    {
        return array(
          array('numero_processo', 'sdsjdsjd', 'string_1'),
          array('data_requerimento', '2013-12-28', 'date_1'),
          array('data_deferimento', '2013-12-28', 'date_2'),
          array('numero_oficio', 'ewoei owiwe', 'string_2'),
          array('id_funcao', 10, 'integer_1'),
          array('id_reparticao', 20, 'integer_2'),
          array('prazo_solicitado', 'sd sdhsdkhjs dksjd ksj skd', 'string_3')
		);
    }
}