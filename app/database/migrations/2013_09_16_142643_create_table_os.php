<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableOs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('os', function( $table) {
            $table->increments('id_os');
			$table->integer('numero')->unsigned();
			$table->integer('id_empresa')->unsigned()->nullable();
			$table->integer('id_projeto')->unsigned()->nullable();
			$table->integer('id_empresa_armador')->unsigned()->nullable();
			$table->integer('id_centro_de_custo')->unsigned()->nullable();
			$table->integer('id_status_os')->unsigned()->nullable();
			$table->integer('id_servico')->unsigned()->nullable();
			$table->integer('id_usuario_abertura')->unsigned()->nullable();
			$table->integer('id_usuario_execucao')->unsigned()->nullable();
			$table->date('dt_solicitacao')->nullable();
			$table->date('dt_execucao')->nullable();
			$table->string('motorista')->nullable();
			$table->string('dados_do_voo')->nullable();
			$table->string('solicitante')->nullable();
			$table->text('observacao')->nullable();
			$table->timestamps();			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('os')){
			Schema::drop('os');
		}
	}

}