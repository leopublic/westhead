@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Acessos do perfil "'.$perfil->nome.'"', 'subTitulo' => ''))

@include('html/mensagens')


<div class="row">
        {{ Form::open(array("class"=>"form-horizontal", "id"=>'criterios')) }}
        {{ Form::hidden('id_perfil', $perfil->id_perfil) }}
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Funções</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width:200px;">Tela</th>
                            <th style="width:auto; ">Ação</th>
                        </tr>
                    </thead>
                    <? $id_tela_ant = ''; ?>
                    @if(isset($acoes))
                    <tbody>
                        @foreach($acoes as $acao)
                        @if ($acao->id_tela != $id_tela_ant)
                        <tr class="topo-borda">
                        @else
                        <tr>
                        @endif
                            @if ($acao->id_tela != $id_tela_ant)
                            <? $id_tela_ant = $acao->id_tela; ?>
                            <td style="font-weight:bold;">{{$acao->descricao_tela}}</td>
                            @else
                            <td>&nbsp;</td>
                            @endif
                            <td style="">
                                {{Form::checkbox('id_acao[]', $acao->id_acao, $acao->tem_acesso )}}
                                {{$acao->descricao_acao}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
                        <button type="submit" class="btn btn-primary">Salvar</button>                        
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
@stop
@section('scripts')
<script>

function geraExcel(){
    $('#criterios').attr('target', '_blank');
    $('#criterios').attr('action', '/invoice/excel');
    $("#criterios" ).submit() ;
    $('#criterios').attr('target', '_self');
    $('#criterios').attr('action', '/invoice');
}
    
</script>
@stop
