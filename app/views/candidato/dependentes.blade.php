@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="icon-male"></i>{{$obj->nome_completo}}</h1>
        @if ($obj->id_candidato_principal > 0)
        <h4>(
            @if ($obj->id_parentesco > 0)
            {{$obj->parentesco->descricao}} de
            @endif
            <a href="{{URL::to('/candidato/cadastro/'.$obj->id_candidato_principal)}}"  target="_blank" title="Clique para ir para o perfil do responsável" style="text-decoration: underline;">{{$obj->candidatoprincipal->nome_completo}}</a>)</h4>
        @endif
        <h4>@if (is_object($obj->empresa))
            {{$obj->empresa->razaosocial}}
            @endif
        </h4>
        <h4>@if (is_object($obj->projeto))
            {{$obj->projeto->descricao}}
            @endif
        </h4>
    </div>
</div>
<!-- END Page Title -->
@include('html.mensagens')
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                @include('candidato.show_abas', array('obj'=> $obj, 'ativa'=> 'Dependentes'))
                <h3>&nbsp;</h3>
            </div>
            <div class="box-content">
                <div class="row" style="margin-bottom:10px;">
                    <div class="col-md-12">
                        @if (is_object($obj->dependentes) && count($obj->dependentes) > 0)
                        <table class="table table-striped table-hover fill-head">
                            <thead>
                                <tr>
                                    <th style="width: 40px">#</th>
                                    <th style="width: 200px">Ação</th>
                                    <th style="width: auto">Nome</th>
                                    <th style="width: auto;">Parentesco</th>
                                    <th style="width: 150px;">Data nasc.</th>
                                    <th style="width: 80px;">Passaporte</th>
                                </tr>
                            </thead>
                            <tbody>
                                <? $i = 0; ?>
                                @foreach($obj->dependentes as $registro)
                                <? $i++; ?>
                                <tr>
                                    <td><? print $i; ?></td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" href="{{ URL::to('/candidato/show/'.$registro->id_candidato) }}" target="_blank" title="Ver detalhes desse candidato"><i class="icon-search"></i></a>
                                        @include('html.link_exclusao', array('url' => URL::to('/candidato/retiradependencia/'.$registro->id_candidato), 'msg_confirmacao' => 'Deseja mesmo tornar esse candidato independente?'))
                                    </td>
                                    <td>{{ $registro->nome_completo }}</td>
                                    <td>{{ Form::select('id_tipo_anexo', array(''=>'(n/d)') + Parentesco::orderBy('descricao')->lists('descricao', 'id_parentesco'), $registro->id_parentesco, array('onChange' => 'AtualizaParentesco(\''.$registro->id_candidato.'\', this)') ) }}</td>
                                    <td>{{ $registro->dt_nascimento }}</td>
                                    <td>{{ $registro->nu_passaporte }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        (nenhum dependente)<br/>
                        @endif

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::open(array("class"=>"form-horizontal", "id" => "buscaCandidato")) }}
                        {{ Form::hidden("id_candidato_principal", $obj->id_candidato) }}
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-group"></i>Adicionar novo dependente</h3>
                                <div class="box-tool">
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    @include('html/campo-texto', array('qtdCol' => 2, 'id' => 'nome_completo', 'label' => 'Nome completo', 'valor' => '',  'help' => '',  array()))
                                    @include('html/campo-texto', array('qtdCol' =>  2, 'id' => 'nome_mae', 'label' => 'Nome da mãe', 'valor' => '',  'help' => '',  array()))
                                    @include('html/campo-data', array('qtdCol' =>  2, 'id' => 'dt_nascimento', 'label' => 'Data de nascimento', 'valor' => '',  'help' => '',  array()))
                                    @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_parentesco', 'label' => 'Parentesco', 'valor' => '',  'help' => '', 'valores' => array(''=>'(n/d)') + Parentesco::orderBy('descricao')->lists('descricao', 'id_parentesco') , array()))
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">

    function AtualizaParentesco(id, id_parentesco)
    {
        $.ajax({
            type: 'POST'
            , url: '/candidato/atualizarparentesco/' + id
            , data: {"id_parentesco": $(id_parentesco).val()}
            , success:
                    function(data)
                    {
                        AtualizarDependentes();
                    }
        });
    }

    function AtualizarDependentes()
    {
        $('#containerDependentes').html('<div style="text-align:center; padding:20px;"><i class="icon-spinner icon-spin icon-4x"></i></div>');

        $.ajax({
            type: 'GET'
            , url: '/candidato/dependentes/' + $('#id_candidato').val()
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        $('#containerDependentes').html(data);
                    }
        });
    }

</script>

@stop
