<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdInvoiceToOsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('os', function(Blueprint $table) {
            $table->integer('id_invoice')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('os', function(Blueprint $table) {
            $table->dropColumn('id_invoice');
        });
    }

}
