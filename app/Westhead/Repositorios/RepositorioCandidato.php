<?php

namespace Westhead\Repositorios;

class RepositorioCandidato extends Repositorio {

    public function all($filtros = null, $orderBy = null) {
        $reg = new Candidato;
        if (isset($filtros['id_empresa']) && intval($filtros['id_empresa']) > 0) {
            $reg = $reg->daEmpresa($filtros['id_empresa']);
        }
        if (isset($filtros['id_projeto']) && intval($filtros['id_projeto']) > 0) {
            $reg = $reg->doProjeto($filtros['id_projeto']);
        }
        if (isset($filtros['nome_completo']) && $filtros['nome_completo'] != '') {
            $reg = $reg->doNome($filtros['nome_completo']);
        }
        return $reg->orderBy('nome_completo')->get();
    }

    /**
     * Cria um candidato incompleto
     * @param type $post
     * @return \Candidato
     */
    public function crie($post) {
        $post['nome_completo'] = $this->limpaInputString($post['nome_completo']);
        $post['nome_mae'] = $this->limpaInputString($post['nome_mae']);
        $this->validador = new \Westhead\Validadores\ValidadorCandidatoIncompleto($post);
        if ($this->validador->passes()) {
            $homonimo = \Candidato::where('nome_mae', '=', trim($post['nome_mae']))
                    ->where('nome_completo', '=', trim($post['nome_completo']))
                    ->where('dt_nascimento', '=', \DateTime::createFromFormat('d/m/Y', $post['dt_nascimento'])->format('Y-m-d'))
                    ->first();
            if (!is_object($homonimo) || \Auth::user()->temAcessoA('CAND', 'HOMO')) {
                // Cria o candidato
                $cand = new \Candidato;
                $cand->nome_mae = $post['nome_mae'];
                $cand->nome_completo = $post['nome_completo'];
                $cand->dt_nascimento = $post['dt_nascimento'];
                $cand->save();

                // Cria o visto inicial
                $visto = new \Visto();
                $visto->id_candidato = $cand->id_candidato;
                $visto->processo_atual = 1;
                $visto->save();

                return $cand;
            } else {
                $this->registreErro("Já existe um candidato com esse nome, mãe e data de nascimento. Use a pesquisa para encontrar o candidato.");
                return false;
            }
        } else {
            return false;
        }
    }

    public function verificaHomonimo(\Candidato $candidato) {
        if ($candidato->id_candidato > 0) {
            return false;
        } else {
            $cand = Candidato::where('nome_mae', '=', $candidato->nome_mae)
                    ->where('nome_completo', '=', $candidato->nome_completo)
                    ->where('dt_nascimento', '=', $candidato->dt_nascimento)
                    ->first();
            if (is_object($cand)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function exclua(\Candidato $cand) {
        \DB::beginTransaction();
        \Processo::where('id_candidato', '=', $cand->id_candidato)->delete();
        \Visto::where('id_candidato', '=', $cand->id_candidato)->delete();
        \OsCandidato::where('id_candidato', '=', $cand->id_candidato)->delete();
        $anexos = \Anexo::where('id_candidato', '=', $cand->id_candidato)->get();
        foreach($anexos as $anexo){
            $anexo->exclua();
            $anexo->delete();
        }

        $cand->delete();
        \DB::commit();
    }
    
    public function limpaInputString($texto){
        $ret = str_replace("\t", " ", $texto);
        while (str_contains($ret, "  ")){
            $ret = str_replace("  ", " ", $ret);            
        }
        return trim($ret);
    }

    public function relatorio_visa_control_excel($id_empresa, $id_projeto, $nome_completo, $id_funcao){
        set_time_limit(0);
        $sql = "
            select candidato.id_candidato
              , candidato.nome_completo
              , candidato.nu_passaporte
              , date_format(candidato.dt_emissao_passaporte, '%d/%m/%Y') dt_emissao_passaporte
              , date_format(candidato.dt_validade_passaporte, '%d/%m/%Y') dt_validade_passaporte
              , empresa.razaosocial
              , projeto.descricao descricao_projeto
              , visto.id_visto
              , coalesce(profissao.descricao_en, profissao.descricao) descricao_profissao
              , coalesce(funcao.descricao_en, funcao.descricao) descricao_funcao
              , aut.string_1 numero_processo
              ,  case aut.id_tipoprocesso when 1 then aut.string_3 when 5 then aut.string_2 else concat('(indefinido para ', aut.id_tipoprocesso,')') end prazo_solicitado
              , date_format(aut.date_2, '%d/%m/%Y') data_deferimento
              , date_format(date_add(aut.date_2, INTERVAL 180 DAY), '%d/%m/%Y') data_deadline
              , servico.descricao descricao_servico
              , case trim(coalesce(nacionalidade.descricao_en, '')) when '' then nacionalidade.descricao else nacionalidade.descricao_en end  nacionalidade
              , visto.observacao_cliente
              , reparticao.descricao descricao_reparticao
              , date_format(coleta.date_1, '%d/%m/%Y') data_emissao
              , date_format(date_add(coleta.date_1, INTERVAL 180 DAY), '%d/%m/%Y') data_deadline_entrada
              , date_format(reg.date_1, '%d/%m/%Y') data_requerimento_registro
              , date_format(date_add(reg.date_1, INTERVAL 180 DAY), '%d/%m/%Y') data_validade_protocolo
              , delegacia.nome nome_delegacia
              , date_format(candidato.data_ultima_entrada, '%d/%m/%Y') data_ultima_entrada
              , if(candidato.data_ultima_entrada is null, '--', datediff(NOW(), candidato.data_ultima_entrada)) dias_desde_data_ultima_entrada
            from candidato
            left join empresa on empresa.id_empresa = candidato.id_empresa
            left join projeto on projeto.id_projeto = candidato.id_projeto
            left join funcao on funcao.id_funcao = candidato.id_funcao
            left join profissao on profissao.id_profissao = candidato.id_profissao
            left join visto on visto.id_candidato = candidato.id_candidato and visto.processo_atual = 1
            left join processo  aut on aut.id_processo = visto.id_processo_principal
            left join servico on servico.id_servico = aut.id_servico
            left join reparticao on reparticao.id_reparticao = candidato.id_reparticao
            left join pais nacionalidade on nacionalidade.id_pais = candidato.id_pais_nacionalidade
            left join processo reg on reg.id_visto = visto.id_visto and reg.id_tipoprocesso = 3 and reg.fl_mais_recente = 1
            left join processo coleta on coleta.id_visto = visto.id_visto and coleta.id_tipoprocesso = 4 and coleta.fl_mais_recente = 1
            left join delegacia on delegacia.id_delegacia = reg.integer_1
            ";
        $where = ' where ';
        if ($id_empresa > 0){
            $sql .= $where." candidato.id_empresa = ".$id_empresa;
            $where = ' and ';
        }
        if ($id_projeto > 0){
            $sql .= $where." candidato.id_projeto = ".$id_projeto;
            $where = ' and ';
        }
        if ($nome_completo != ''){
            $sql .= $where." candidato.nome_completo like '%".$nome_completo."%'";
            $where = ' and ';
        }
        if ($id_funcao > 0){
            $sql .= $where." candidato.id_funcao = ".$id_funcao;
            $where = ' and ';
        }
        $sql .= " order by razaosocial, descricao_projeto, nome_completo";
        \Log::info($sql);
        $registros = \DB::select(\DB::raw($sql));


        $path = public_path().'/img/logo_ok.jpg';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $url_logo = 'data:image/' . $type . ';base64,' . base64_encode($data);


        return \View::make('relatorios.visa_control_excel')
                        ->with('registros', $registros)
                        ->with('logo', $url_logo);

    }
}
