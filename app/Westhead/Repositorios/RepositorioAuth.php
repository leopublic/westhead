<?php
namespace Westhead\Repositorios;

use Mailgun\Mailgun;

class RepositorioAuth extends Repositorio{

    /**
     * Verifica se o username /ip pode continuar a tentar acesso
     * @param type $username
     * @param type $ip
     */
    public function podeTentarAcesso($username, $ip){
        // Primeiro verifica se está tentando acessar um usuário válido
        $usuario = \Usuario::where('username', '=', $username)->first();
        if(is_object($usuario)){
            if ($usuario->qtd_tentativas_erradas <= 5){
                return true;
            } else {
                return false;
            }
        } else {
            //Se não é um usuário válido, então verifica se é DDOS pelo IP
            $tentativa = \TentativaAcesso::where('ip', '=', $ip)->first();
            if (is_object($tentativa)){
                if ($tentativa->qtd > 10){
                    return false;
                } else {
                    return true;
                }
            } else {
                $tentativa = new \TentativaAcesso();
                $tentativa->ip = $ip;
                $tentativa->qtd = 0;
                $tentativa->save();
                return true;
            }
            
        }
    }

    public function registraTentativaComSucesso(\Usuario $usuario, $ip){
        $tentativa = \TentativaAcesso::where('ip', '=', $ip)->first();
        if (is_object($tentativa)){
            $tentativa->qtd = 0;
            $tentativa->save();
        } else {
            $tentativa = new \TentativaAcesso();
            $tentativa->ip = $ip;
            $tentativa->qtd = 0;
            $tentativa->save();
        }
        $usuario->atualizaUltimoAcesso();
    }
    
    public function registraTentativaSemSucesso($username, $ip){
        $tentativa = \TentativaAcesso::where('ip', '=', $ip)->first();
        if (is_object($tentativa)){
            $tentativa->qtd = $tentativa->qtd+1;
            $tentativa->save();
        } else {
            $tentativa = new \TentativaAcesso();
            $tentativa->ip = $ip;
            $tentativa->qtd = 1;
            $tentativa->save();
        }

        $usuario = \Usuario::where('username', '=', $username)->first();
        if(is_object($usuario)){
            if ($usuario->dthr_primeiro_erro == ''){
                $usuario->dthr_primeiro_erro = date('Y-m-d H:i:s');
            }
            $usuario->qtd_tentativas_erradas = $usuario->qtd_tentativas_erradas + 1;
            $usuario->save();
        }
    }
     
    public function notificaExcessoTentativas($username, $ip){
        $data = array('username' => $username, 'ip' => $ip);

        $email = 'patricia.valente@lplaw.com.br';
        $nome = 'Patrícia';

        $mg = new Mailgun("key-2a01524c53cadc4520ea83dc53c83197");
        $domain = "mg.m2software.com.br";

        $html = \View::make('emails.usuario_bloqueado', $data);
        //Customise the email - self explanatory
        $mg->sendMessage($domain, array(
        'from'=>'patricia.valente@lplaw.com.br',
        'replyTo'=>'patricia.valente@lplaw.com.br',
        'to'=> $usuario->nome." <".$usuario->email.">",
        'subject' => 'WESTHEAD - Usuário bloqueado',
        'html' => $html
            )
        );

        
        \Mail::send('emails.usuario_bloqueado', $data, function($message) use($email, $nome) {
            $message->to($email, $nome)->subject('WESTHEAD - Usuário bloqueado');
        });
    }
   
}