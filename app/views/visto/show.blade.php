<!-- visto/show  -->
<? $qtdCol = 2;?>
<div class="row" name="visto" id="visto">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
            @if($visto->id_visto > 0)
                <div class="box-tool">
                    @if (Auth::user()->temAcessoA('PROC', 'INS'))
                    <div class="btn-group">
                        <a href="#" data-toggle="dropdown" class="btn btn-gray dropdown-toggle"><i class="icon-plus"></i> processo <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-gray pull-right">
                            <li><a href="/processo1/adicione/{{ $visto->id_visto}}">Autorização</a></li>
                            <li><a href="/processo4/adicione/{{ $visto->id_visto}}">Dados do Visto</a></li>
                            <li><a href="/processo3/adicione/{{ $visto->id_visto}}">Registro</a></li>
                            <li><a href="/processo9/adicione/{{ $visto->id_visto}}">Coleta de CIE</a></li>
                            <li><a href="/processo5/adicione/{{ $visto->id_visto}}">Prorrogação</a></li>
                        </ul>
                    </div>
                    @endif
                </div>
                <h3><i class="icon-folder-open"></i> Visto {{$visto->titulo}}</h3>
            @else
                <h3><i class="icon-folder-open"></i> Processos sem visto</h3>
            @endif
            </div>
            <div class="box-content">
                @if($visto->id_visto > 0)
                    <div class="row" name="visto" id="visto">
                        <div class="col-md-12">
                            <div class="box box-orange"  style="border:solid 1px #CCC;">
                                <div class="box-title">
                                    <h3><i class="icon-info-sign"></i> Situação do visto</h3>
                                    <div class="box-tool">
                                        <a data-action="collapse"><i class="icon-chevron-up"></i></a>
                                    </div>
                                </div>
                                <div class="box-content">
                                    {{Form::open(array('class'=>'form-horizontal'))}}
                                    {{Form::hidden('id_visto', $visto->id_visto)}}
                                    <div class="row">
                                        @include('html.campo-textarea', array('id'=>'observacao', 'label'=>'Observações internas', 'valor'=> $visto->observacao ,'help'=> 'Observações do visto importantes para a Westhead'))
                                        @include('html.campo-textarea', array('id'=>'observacao_cliente', 'label'=>'Observações para o cliente', 'valor'=> $visto->observacao_cliente, 'help'=> 'Observações do visto <strong>QUE SERÃO ENVIADAS PARA OS CLIENTES!!!</strong> Preencha com atenção e critério.', 'atributos' => array('style' => 'background-color:#dfdfdf; color:#ff863d; font-weight:bold;')))
                                        @include('html/campo-data', array('id' => 'data_ultima_entrada', 'label' => 'Data da última entrada', 'valor' => $obj->data_ultima_entrada,  'help' => '',  array()))
                                        @include('html/campo-texto', array('id' => 'dias_desde_ultima_entrada', 'label' => 'Dias desde última entrada', 'valor' => $obj->dias_desde_ultima_entrada,  'help' => '',   'atributos' => array('disabled') ))
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="col-md-12">
                                            <div class="form-group last">
                                                <div class="col-sm-8 col-sm-offset-4 col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2">
                                                    <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if(count($processos) > 0)
                    @foreach($processos as $processo)
                        @if ($processo->id_tipoprocesso == 1 || $processo->id_tipoprocesso == 2)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoAutorizacao::find($processo->id_processo) ) )
                        @endif
                        @if ($processo->id_tipoprocesso == 3)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoRegistro::find($processo->id_processo) ) )
                        @endif
                        @if ($processo->id_tipoprocesso == 4)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoColetaVisto::find($processo->id_processo) ) )
                        @endif
                        @if ($processo->id_tipoprocesso == 5)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoProrrogacao::find($processo->id_processo) ) )
                        @endif
                        @if ($processo->id_tipoprocesso == 6)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoOutros::find($processo->id_processo) ) )
                        @endif
                        @if ($processo->id_tipoprocesso == 7)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoAtendimento::find($processo->id_processo) ) )
                        @endif
                        @if ($processo->id_tipoprocesso == 8)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoCancelamento::find($processo->id_processo) ) )
                        @endif
                        @if ($processo->id_tipoprocesso == 9)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoColeta::find($processo->id_processo) ) )
                        @endif
                        @if ($processo->id_tipoprocesso == 10)
                            @include('processo.'.$processo->id_tipoprocesso, array('processo' => ProcessoTraducao::find($processo->id_processo) ) )
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
