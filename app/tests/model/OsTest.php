<?php

class OsTest extends TestCase {

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testQtdCandidatos() {
        $os = new \Os;
        $os->id_empresa = 10;
        $os->id_servico = 10;
        $os->save();
        $cand = new \Candidato;
        $cand->nome_completo = 'xxxx';
        $cand->save();

        $this->assertEquals(0, $os->qtdcandidatos());
        $os->candidatos()->attach($cand);
        $this->assertEquals(1, $os->qtdcandidatos());

        $cand = new \Candidato;
        $cand->nome_completo = 'yyyy';
        $cand->save();
        $os->candidatos()->attach($cand);
        $this->assertEquals(2, $os->qtdcandidatos());

        $cand = new \Candidato;
        $cand->nome_completo = 'zzz';
        $cand->save();
        $os->candidatos()->attach($cand);
        $this->assertEquals(3, $os->qtdcandidatos());


        $cand->delete();
        $os->delete();
    }

    public function testCotacao() {
        $cot = \Cotacao::where('data', '=', '2014-02-06')->first();
        $valorOk = $cot->compra;

        $empresa = new \Empresa;
        $empresa->id_tipo_cobranca = 1;
        $empresa->razaosocial = 'xxx';
        $empresa->save();

        $os = new \Os;
        $os->id_empresa = $empresa->id_empresa;
        $os->dt_execucao = '06/02/2014';
        $os->save();
        $this->assertNotEmpty($os->id_os);

        print $valorOk;
        $valor = $os->valorCotacao();
        $this->assertEquals($valorOk, $valor);
    }

}
