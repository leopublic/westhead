<html>
    <head>
        <style>
            *, body{font-family: Verdana, Arial, sans-serif;}
            h1{
                font-family: inherit;
                text-align: center;
                font-size: 20px;
                margin-top: 0px;
                padding-top: 0px;
            }
            div.titulo{
                font-family: inherit;
                font-size:14px;
                font-weight: bold;
                margin-top:30px;
                margin-bottom:5px;
            }
            table{
                font-family: inherit;
                font-size:12px;
                margin:0;
                padding:0;
                border-spacing: 0;
                border-collapse: collapse;
                width:100%;
            }
            table tr td{
                padding: 3px;
                font-family: inherit;
                font-size:inherit;
                border: solid 1px #000;
                vertical-align: bottom;
                page-break-inside: avoid;
                border-top: none;
            }
            table tr th{
                padding: 3px;
                font-family: inherit;
                font-size:inherit;
                font-weight: bold;
                border: solid 1px #000;
                border-bottom: none;
                vertical-align: top;
                text-align: left;
                padding-bottom: 10px;
            }
            table tr.deps td{
                border: solid 1px #000;
            }
            div.inseparavel{
                page-break-inside: avoid;
            }
            table.semLinhas tr td
            ,table.semLinhas tr th{
                border: none;
                font-family: inherit;
            }
            table tbody{
                page-break-inside: avoid;
            }
            @media all {
                .page-break	{ display: none; }
            }

            @media print {
                .page-break	{ display: block; page-break-before: always; }
            }
            div.topo{
                width:100%; 
                border: solid 1.5px black; 
                border-top-left-radius: 4px; 
                border-top-right-radius: 4px; 
                font-weight: bold; 
                margin:none; 
                text-align: center;
                padding: 3px;
            }
            div.redondo{
                width:48%;
                margin: 0;
                display: inline; 
                border: solid 1.5px black; 
                border-radius: 4px; 
                text-align: left;
                padding: 3px;
                height: 70px;
                font-size:8px;
            }
            div.meio{
                width:48%;
                margin: 0;
                display: inline; 
                padding: 3px;
                font-size:8px;
            }
            div.titulo{
                width:100%;
                border-left: solid 1.5px black;
                border-right: solid 1.5px black;
                font-weight: normal;
                margin:none;
                padding: 4px;
                font-size:8px;
            }
            div.linha{width:100%;}
            table.linha{
                width:100%;
            }
            table.linha2{
                table-layout:fixed;

                width:740px;
            }
            table.linha2 tr td{
                width:17px;
                border: solid 1.5px black;
                border-top: none;
                text-align:center;
                padding:0px;
                margin:0px;
            }
            table.linha tr td{
                width:18px;
                border: solid 1.5px black;
                border-top: none;
                text-align:center;
                padding:0px;
                margin:0px;
            }
            table.linha tr td.box{
                border: solid 1.5px black;
                font-size:9px; 
            }

            table.linha tr td.sep{
                border: none; 
                font-size:3px;
            }

            table.linha tr td.secao{
                font-size:14px; 
                text-align:center; 
                vertical-align: middle; 
                font-weight: bold; 
                text-align: center; 
                padding: 4px;
                border-top: solid 1.5px #000;
            }
            table.linha tr td.sel{
                background-repeat: no-repeat;
                background-image-resize: 6;
                background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAoCAIAAAAzED4bAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFVSURBVFhHxdBRVsMwDATA3v/SEMq0pKyV2G6Szg+v2ZXkx+3p69O8o0nlWm5v072Kqz1MnM+9IUZP40z4iXYbZ7A9iJ/PWkiC+Dj2BvHd64+C+Ag2BvFD46RiEL/BoiBeaR9TD+Jx5oM41EFBPMJkELdsZjWNDgaCuLB/wJogrukF8aa+UkHcohHEe7p7NY0VQRB3GKgurA/iO5+CuM9Ye+FI2I5GTc2MMDNocmzh7CbVcfOTC8cLSlPOepbGrDf+zx1Ux01OOtvBwKCZMQe7GRsxNuNOEG++WKPPQNv6IF4RBHGH3qrFQRzEQbynq2dlEBeUWjRqk6vFHQwEcWErtiCIuxkL4pYyMxrEgwy3aLwqvhbEs2wJ4pX/nxSD+G3WBfHDy2+VID6IpUF89/dDGMRHsz1I/Sn8pidxI2xE5z7oybFOhi7h5C71a7ld0foEL1jcbt81FHZr+JhSlQAAAABJRU5ErkJggg==);
            }

            table.linha tr td.peq{
                font-size:9px; 
                text-align:left;
                padding-left: 3px; 
                border-top:none;
            }
            table.linha tr.titulo td{
                border-bottom: none;
                font-size:8px; 
                text-align: left;
                padding:none; 
                margin:none;
                padding-left:4px; 
                padding-top:2px;
                padding-bottom:2px;
            }
            table.opcoescab{
                width: 400px;
                table-layout: fixed;
                border-collapse: collapse;
                border-spacing: 0px;
            }
            table.opcoescab tr td.quadrado{
                width:20px;
                border:none;
            }
            table.opcoescab tr td.quadrado div{
                width: 10px;
                height: 5px;
                border: solid 1.5px black;
            }
            table.check{
                font-size:12px;                
            }
            table.check tr td{
                border: solid 1.5px #000;
                padding:0px;
                margin:0px;
                width:8px;
                font-size:12px;
            }
            table.check tr.regua td{
                border: none;
                padding:0px;
                margin:0px;
                width:8px;
                font-size:5px;
            }
        </style>
    </head>

    <body>
        <div style="width:100%; margin-bottom: 4px; ">
            <div class="redondo" style="float: right; border:none;">
                <table style="width:100%;table-layout:fixed;">
                    <tr>
                        <td style="border:none;width: 20px; border-left: solid 1.5px black;border-top: solid 1.5px black;font-size:8px;"></td>
                        <td style="border:none; width: 320px;">&nbsp;</td>
                        <td style="border:none;width: 20px; border-right: solid 1.5px black;border-top: solid 1.5px black;font-size:8px;"></td>
                    </tr>
                    <tr>
                        <td style="border:none;font-size:10px;" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border:none;font-size:10px;" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border:none; text-align:center;" colspan="3">PROTOCOLO</td>
                    </tr>
                    <tr>
                        <td style="border:none;font-size:10px;" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border:none;font-size:10px;" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border:none;border-left: solid 1.5px black;border-bottom: solid 1.5px black;"></td>
                        <td style="border:none; ">&nbsp;</td>
                        <td style="border:none;border-right: solid 1.5px black;border-bottom: solid 1.5px black;"></td>
                    </tr>
                </table>                
            </div>
            <div class="redondo" style="float: left;border: none;">
                <table class="opcoescab">
                    <tr>
                        <td class="label" style="border:none;width:60px;"><img src="../public/img/brasao.jpg" /></td>
                        <td style="border:none; vertical-align: top; font-size:13px;"><strong>MINISTÉRIO DA JUSTIÇA</strong><br/>SECRETARIA DE JUSTIÇA<br/>DEPARTAMENTO DE ESTRANGEIROS</td>
                    </tr>
                </table>
                <table class="check" style="width:400px;">
                    <tr class="regua"><td colspan="60" style="text-align: center; font-weight: bold; font-size: 16px;">REQUERIMENTO</td></tr>
                    <tr class="regua">{{ str_repeat('<td>&nbsp;</td>', 60)}}</tr>
                    <tr>
                        <td colspan="27" style="border:none; font-size:16px; ">TRANSFORMAÇÃO DE VISTO</td><td colspan="29" style="border:none;border-bottom: solid 1.5px #000;">&nbsp;</td><td style="border:none;">&nbsp;</td><td colspan="3">&nbsp;</td>
                    </tr>
                    <tr class="regua">{{ str_repeat('<td>&nbsp;</td>', 60)}}</tr>
                    <tr>
                        <td colspan="42" style="border:none; font-size:16px;">PERMANÊNCIA DEFINITIVA-REUNIÃO FAMILIAR</td><td colspan="14" style="border:none;border-bottom: solid 1.5px #000;">&nbsp;</td><td style="border:none; font-size:3px;">&nbsp;</td><td colspan="3">&nbsp;</td>
                    </tr>
                    <tr class="regua">{{ str_repeat('<td>&nbsp;</td>', 60)}}</tr>
                    <tr>
                        <td colspan="38" style="border:none; font-size:16px;">PERMANÊNCIA DEFINITIVA-INEXPULSÁVEL</td><td colspan="18" style="border:none;border-bottom: solid 1.5px #000;">&nbsp;</td><td style="border:none; font-size:3px;">&nbsp;</td><td colspan="3">&nbsp;</td>
                    </tr>
                    <tr class="regua">{{ str_repeat('<td>&nbsp;</td>', 60)}}</tr>
                    <tr>
                        <td colspan="36" style="border:none; font-size:16px;">PRORROGAÇÃO DO PRAZO DE ESTADA</td><td colspan="20" style="border:none;border-bottom: solid 1.5px #000;">&nbsp;</td><td style="border:none; font-size:3px;">&nbsp;</td><td colspan="3">&nbsp;</td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="width:100%; margin-bottom: 4px;">
            <div style="width: 100px; display:inline; float: right; margin-right: 300px;font-size: 8px;">DPFDPMAT</div>
            <div style="width: 100px; display:inline; float: left; font-size: 8px;">SJ/DEEST</div>
        </div>
        <div style="width:100%; margin-bottom: 4px;">
            <div class="redondo" style="float: right;">USO DA SJ</div>
            <div class="redondo" style="float: left;">MICROFILMAGEM</div>
        </div>

        <div class="linha">
            <table class="linha">
                <tr>
                    <td colspan="37" class="sep">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="37" class="secao" >1 - IDENTIFICAÇÃO DO REQUERENTE</td>
                </tr>
                <tr class="titulo"><td colspan="37">NOME COMPLETO</td></tr>
                <tr>
                    <?
                    $cols = 0;
                    $lin = 1;
                    for ($index = 0; $index < strlen($candidato->nome_completo); $index++) {
                        $cols++;
                        print '<td>' . substr($candidato->nome_completo, $index, 1) . '</td>';
                        if ($cols == 37) {
                            print '</tr><tr class="titulo"><td colspan="29">&nbsp;</td><td colspan="8">R.N.E.</td></tr><tr>';
                            $cols = 1;
                            $lin++;
                        }
                    }
                    if ($lin == 1) {
                        while ($cols < 37) {
                            print '<td>&nbsp;</td>';
                            $cols++;
                        }
                        print '</tr><tr class="titulo"><td colspan="29">&nbsp;</td><td colspan="8">R.N.E.</td></tr><tr>';
                        $cols = 0;
                        while ($cols < 29) {
                            print '<td>&nbsp;</td>';
                            $cols++;
                        }
                    } else {
                        print str_repeat('<td>&nbsp;</td>', 30 - $cols);
                    }
                    ?>
                    {{ $candidato->getCaracteresTd('nu_rne', 8) }}
                </tr>
                <tr class="titulo"><td colspan="5">SEXO</td><td colspan="32">ESTADO CIVIL</td></tr>
                <tr>
                    <td class="box{{ $sexo['M'] }}">1</td><td class="peq">M</td><td>&nbsp;</td><td class="box{{ $sexo['F'] }}">2</td><td class="peq">F</td>
                    <td class="box{{ $estadocivil[1] }}">1</td><td colspan="4" class="peq">SOLTEIRO</td>
                    <td class="box{{ $estadocivil[2] }}">2</td><td colspan="3" class="peq">CASADO</td>
                    <td class="box{{ $estadocivil[3] }}">3</td><td colspan="3" class="peq">VIÚVO</td>
                    <td class="box{{ $estadocivil[4] }}">4</td><td colspan="8" class="peq">SEPARADO JUDICIALMENTE</td>
                    <td class="box{{ $estadocivil[5] }}">5</td><td colspan="5" class="peq">DIVORCIADO</td>
                    <td class="box{{ $estadocivil[6] }}">6</td><td colspan="3" class="peq">OUTROS</td>

                </tr>
                <tr class="titulo">
                    <td colspan="6">DATA DE NASCIMENTO</td>
                    <td colspan="31">CIDADE DE NASCIMENTO</td>
                </tr>
                <tr>
                    <?
                    print '<td>' . substr($candidato->dt_nascimento, 0, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_nascimento, 1, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_nascimento, 3, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_nascimento, 4, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_nascimento, 8, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_nascimento, 9, 1) . '</td>';
                    ?>
                    {{ $candidato->getCaracteresTd('local_nascimento', 31) }}
                </tr>
                <tr class="titulo">
                    <td colspan="18">PAÍS DE NASCIMENTO</td>
                    <td colspan="19">PAÍS DE NACIONALIDADE</td>
                </tr>
                <tr>
                    @if (is_object($candidato->nacionalidade))
                    {{ $candidato->nacionalidade->getCaracteresTd('descricao_resumida', 18) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 18); ?>
                    @endif

                    @if (is_object($candidato->nacionalidade))
                    {{ $candidato->nacionalidade->getCaracteresTd('descricao_resumida', 19) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 19); ?>
                    @endif
                </tr>
                <tr class="titulo">
                    <td colspan="26">PROFISSÃO</td>
                    <td colspan="11">CPF</td>
                </tr>
                <tr>
                    @if (is_object($candidato->profissao))
                    {{ $candidato->profissao->getCaracteresTd('descricao', 26) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 26); ?>
                    @endif
                    {{ $candidato->getCaracteresTd('nu_cpf_limpo', 11) }}
                </tr>
                <tr class="titulo">
                    <td colspan="37">NOME COMPLETO DO PAI</td>
                </tr>
                <tr>{{ $candidato->getCaracteresTd('nome_pai', 37) }}</tr>
                <tr class="titulo">
                    <td colspan="37">NOME COMPLETO DA MÃE</td>
                </tr>
                <tr>{{ $candidato->getCaracteresTd('nome_mae', 37) }}</tr>
                <tr class="titulo">
                    <td colspan="18">PAÍS DE NACIONALIDADE DO PAI</td>
                    <td colspan="19">PAÍS DE NACIONALIDADE DA MÃE</td>
                </tr>
                <tr>
                    @if (is_object($candidato->nacionalidadepai))
                    {{ $candidato->nacionalidadepai->getCaracteresTd('descricao_resumida', 18) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 18); ?>
                    @endif

                    @if (is_object($candidato->nacionalidademae))
                    {{ $candidato->nacionalidademae->getCaracteresTd('descricao_resumida', 19) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 19); ?>
                    @endif
                </tr>
                <tr class="titulo">
                    <td colspan="37">RESIDÊNCIA RUA, NÚMERO, ANDAR E COMPLEMENTO</td>
                </tr>
                <tr>
                    @if (is_object($candidato->empresa))
                    {{ $candidato->empresa->getCaracteresTd('end_form_trans', 37) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 37); ?>
                    @endif
                </tr>
                <tr class="titulo">
                    <td colspan="15">BAIRRO/DISTRITO</td>
                    <td colspan="20">CIDADE</td>
                    <td colspan="2">UF</td>
                </tr>
                <tr>
                    @if (is_object($candidato->empresa))
                    {{ $candidato->empresa->getCaracteresTd('end_bairro', 15) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 15); ?>
                    @endif
                    @if (is_object($candidato->empresa))
                    {{ $candidato->empresa->getCaracteresTd('end_municipio', 20) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 20); ?>
                    @endif
                    @if (is_object($candidato->empresa))
                    {{ $candidato->empresa->getCaracteresTd('end_uf', 2) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 2); ?>
                    @endif
                </tr>
                <tr class="titulo">
                    <td colspan="8">CEP</td>
                    <td colspan="29">TELEFONES: RESIDÊNCIA E TRABALHO</td>

                </tr>
                <tr>
                    @if (is_object($candidato->empresa))
                    {{ $candidato->empresa->getCaracteresTd('end_cep_limpo', 8) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 8); ?>
                    @endif
                    @if (is_object($candidato->empresa))
                    {{ $candidato->empresa->getCaracteresTd('tel_completo', 29) }}
                    @else
                    <? print str_repeat('<td>&nbsp;</td>', 29); ?>
                    @endif
                </tr>
                <tr>
                    <td colspan="37" class="sep">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="37" class="secao" >2 - DADOS DA ENTRADA NO PAÍS</td>
                </tr>
                <tr class="titulo"><td colspan="23">DOCUMENTO DE VIAGEM</td><td colspan="14">NÚMERO DOCUMENTO DE VIAGEM</td></tr>
                <tr>
                    <td class="">&nbsp;</td>
                    <td class="box sel">1</td><td colspan="5" class="peq">SOLTEIRO</td>
                    <td class="box">2</td><td colspan="9" class="peq">CARTEIRA DE IDENTIDADE</td>
                    <td class="box">3</td><td colspan="5" class="peq">OUTROS</td>

                    {{ $candidato->getCaracteresTd('nu_passaporte', 14) }}
                </tr>
                <tr class="titulo">
                    <td colspan="31">PAÍS EXPEDITOR DO DOCUMENTO DE VIAGEM</td>
                    <td colspan="6">VALIDADE DO DOCUMENTO</td>
                </tr>
                <tr>
                    @if (is_object($candidato->paispassaporte))
                    {{ $candidato->paispassaporte->getCaracteresTd('descricao', 31) }}
                    @else
                    {{ str_repeat('<td>&nbsp;</td>', 31) }}
                    @endif
                    <?
                    print '<td>' . substr($candidato->dt_validade_passaporte, 0, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_validade_passaporte, 1, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_validade_passaporte, 3, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_validade_passaporte, 4, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_validade_passaporte, 8, 1) . '</td>';
                    print '<td>' . substr($candidato->dt_validade_passaporte, 9, 1) . '</td>';
                    ?>
                </tr>
                <tr class="titulo">
                    <td colspan="31">PAÍS PROCEDÊNCIA</td>
                    <td colspan="6">DATA DO DESEMBARQUE</td>
                </tr>
                <tr>
                    {{ str_repeat('<td>&nbsp;</td>', 31) }}
                    <?
                    print '<td>' . substr($coleta->data_entrada, 0, 1) . '</td>';
                    print '<td>' . substr($coleta->data_entrada, 1, 1) . '</td>';
                    print '<td>' . substr($coleta->data_entrada, 3, 1) . '</td>';
                    print '<td>' . substr($coleta->data_entrada, 4, 1) . '</td>';
                    print '<td>' . substr($coleta->data_entrada, 8, 1) . '</td>';
                    print '<td>' . substr($coleta->data_entrada, 9, 1) . '</td>';
                    ?>
                </tr>
                <tr class="titulo">
                    <td colspan="37">LOCAL DE DESEMBARQUE</td>
                </tr>
                <tr>
                    {{ $candidato->getCaracteresTd('local_desembarque', 37) }}
                </tr>
                <tr class="titulo">
                    <td colspan="37">CLASSIFICAÇÃO DO VISTO DE ENTRADA NO PAÍS</td>
                </tr>
                <tr>
                    <td class="">&nbsp;</td>
                    <td class="box">1</td><td colspan="17" style="text-align:left;">TEMPORÁRIO- ITENS I(&nbsp;&nbsp;)&nbsp;&nbsp;II(&nbsp;&nbsp;)&nbsp;&nbsp;III(&nbsp;&nbsp;)&nbsp;&nbsp;IV(&nbsp;&nbsp;)&nbsp;&nbsp;V(&nbsp;&nbsp;)&nbsp;&nbsp;VI(&nbsp;&nbsp;)&nbsp;&nbsp;VII(&nbsp;&nbsp;)</td>
                    <td class="box">2</td><td colspan="4" style="text-align:left;">TURISTA</td>
                    <td class="box">3</td><td colspan="6" style="text-align:left;">DIPLOMÁTICO</td>
                    <td class="box">4</td><td colspan="5" style="text-align:left;">OFICIAL</td>
                </tr>
                <tr>
                    <td colspan="37" class="sep">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="37" class="secao" >3 - OBJETO DO REQUERIMENTO</td>
                </tr>
                <tr class="titulo"><td colspan="37">TIPO DE PEDIDO</td></tr>
                <tr>
                    <td style="border-bottom:none;">&nbsp;</td>
                    <td class="box" style="width:17px;">1</td><td colspan="16" class="peq" style="text-align:left;border-bottom:none; vertical-align: middle;">TRANSFORMAÇÃO DE VISTO <span style="text-decoration: underline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                    <td class="box" style="width:17px;">2</td><td colspan="18" class="peq" style="text-align:left;border-bottom:none; vertical-align: middle;">PRORROGAÇÃO DE PRAZO DE ESTADA ATÉ ______/______/______</td>
                </tr>
                <tr>
                    <td style="font-size: 4px; border: none;border-left: solid 1.5px #000;">&nbsp;</td>
                    {{ str_repeat('<td style="font-size: 4px; border: none;">&nbsp;</td>', 35)}}
                    <td style="font-size: 4px; border: none;border-right: solid 1.5px #000;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="border: none;border-left: solid 1.5px #000;">&nbsp;</td>
                    <td class="box">3</td><td colspan="35" class="peq" style="border: none; text-align:left; vertical-align: middle;border-right: solid 1.5px #000;">PERMANÊNCIA - REUNIÃO FAMILIAR - INEXPULSÁVEL (NESTE CASO É OBRIGATÓRIO PRESTAR A(S) DECLARAÇÃO(ÕES) - VERSO)</td>
                </tr>
                <tr>
                    <td style="font-size: 4px; border: none;border-left: solid 1.5px #000;border-bottom: solid 1.5px #000;">&nbsp;</td>
                    {{ str_repeat('<td style="font-size: 4px; border: none;border-bottom: solid 1.5px #000;">&nbsp;</td>', 35)}}
                    <td style="font-size: 4px; border: none;border-right: solid 1.5px #000;border-bottom: solid 1.5px #000;">&nbsp;</td>
                </tr>
                <tr class="titulo"><td colspan="37">JUSTIFICATIVA DO PEDIDO</td></tr>
                <tr>
                    <td colspan="37" style="padding-left:4px; font-size:10px; text-align: left; letter-spacing: -0.5px;">Em razão da continuidade do contrato de afretamento da embarcação, necessitamos manter o estrangeiro a bordo no exercício das suas atividades em AJB.</td>
                </tr>
                <tr>
                    <td colspan="37" class="peq" style="text-align: center;"><br/><br/><br/><br/><br/><br/><br/>________________________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__________/__________/__________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;____________________________________________________________________
                        <br/>CIDADE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSINATURA DO REQUERENTE
                    </td>
                </tr>
                <tr>
                    <td colspan="37" class="peq" style="border:none;">INSTRUÍDO PELA PORTARIA MINISTERIAL N&ordm;334, DE 7 DE JULHO DE 1988</td>
                </tr>
            </table>
        </div>
    </body>
</html>
