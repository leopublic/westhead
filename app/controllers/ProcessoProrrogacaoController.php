<?php
class ProcessoProrrogacaoController extends \ProcessoGenericoController {

    public function getShow($id){

    }

    public function postUpdate($id){
        $processo = ProcessoProrrogacao::find($id);
        $processo->fill(Input::all());

        $processo->save();

        if (Input::has('tem_andamento')){
            $this->repositorio()->atualiza_status_andamento($processo, Input::get('id_status_andamento'), Input::get('obs_andamento'), \Auth::user()->id_usuario);
        }

        if ($processo->dataRequerimento != '' && $processo->dataRequerimento != '00/00/0000' && is_object($processo->os)){
            $processo->os->concluiPeloProcesso($processo->dataRequerimento);
        }
        Session::flash($processo->id_processo."_success", "Processo atualizado com sucesso!");
        $proccontrol = new \ProcessoController();
        return $proccontrol->rotaParaVisualizarProcesso($processo);
    }

    public function getAdicione($id_visto){
        $rep = new Westhead\Repositorios\RepositorioProcesso;
        $processo = $rep->criaProcessoNoVisto(5, $id_visto);
        return Redirect::to('/candidato/show/'.$processo->id_candidato.'#processo'.$processo->id_processo);
    }
}