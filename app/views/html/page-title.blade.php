<!-- BEGIN Page Title -->
	<div class="page-title">
	    <div>
	        <h1><i class="icon-file-alt"></i>{{ $titulo }}</h1>
	        <h4>{{ $subTitulo }}</h4>
	    </div>
	</div>
<!-- END Page Title -->
