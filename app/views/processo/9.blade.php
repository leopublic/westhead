@extends('processo.comum')

<? $tipo_processo = 'coleta'; ?>
<? $nome_servico = 'Coleta'; ?>

@section('campos')
    <div class="row">
        @include('html/campo-texto', array('id' => 'numero_rne', 'label' => 'Número RNE', 'valor' => $processo->numero_rne,  'help' => '',  array()))
        @if (intval($processo->id_os) == 0)
            @if (Servico::where('id_tipo_servico', '=', 4)->count() > 1)
                @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => $processo->id_servico, 'valores' => array('0' => '(não informado)') +  Servico::where('id_tipo_servico', '=', 9)->lists('descricao', 'id_servico'),  'help' => '', array()))
            @else
                @include('html/campo-texto', array('id' => 'servico', 'label' => 'Serviço', 'valor' => $processo->descricao_servico,  'atributos'=> array( "disabled"=>"disabled")))        
            @endif
        @endif
        @include('html/campo-data', array('id' => 'data_expedicao', 'label' => 'Data expedição', 'valor' => $processo->data_expedicao,  'help' => '',  array()))
        @include('html/campo-data', array('id' => 'validade_cie', 'label' => 'Validade da CIE', 'valor' => $processo->validade_cie,  'help' => '', array()))
    </div>
@overwrite
