<?php
use Westhead\Repositorios;

class RepositorioProcessoTest extends TestCase {

    public function dadosCriaPeloTipoServico(){
        return array(
            array('1', 'ProcessoAutorizacao', '1'),
            array('2', 'ProcessoAutorizacao', '1'),
            array('3', 'ProcessoRegistro', '3'),
            array('4', 'ProcessoColetaVisto', '4'),
            array('5', 'ProcessoProrrogacao', '5'),
            array('9', 'ProcessoColeta', '9'),
        );
    }
    /**
     * @dataProvider dadosCriaPeloTipoServico
     */
    public function testCriaPeloTipoServico($id_tipo_servico, $nomeClasse, $id_tipoprocesso){
        $rep = new \Westhead\Repositorios\RepositorioProcesso;
        $proc = $rep->criaPeloTipoServico($id_tipo_servico);
        $this->assertInstanceOf($nomeClasse, $proc);
        $this->assertEquals($id_tipoprocesso, $proc->id_tipoprocesso);
    }

    public function dadosCriaProcessoDaOS(){
        return array(
            array('1','1'),
            array('2','1'),
            array('3','3'),
            array('4','4'),
            array('5','5'),
            array('9','9'),
        );
    }
    /**
     *
     * @dataProvider dadosCriaProcessoDaOS
     */
    public function testCriaProcessoDaOS($id_tipo_servico, $id_tipoprocesso){
        try{
            Auth::setUser(\Usuario::find(3));

            $repCand = new \Westhead\Repositorios\RepositorioCandidato;
            $post = array(
                "nome_completo" => 'xxx',
                "dt_nascimento" => '12/12/2012',
                "nome_mae"      => 'zzz'
            );
            $cand = \Candidato::where('nome_completo', '=', 'xxx')->first();
            if (!is_object($cand)){
                $cand = $repCand->crie($post);
            }

            $os = new \Os();
            $servico = \Servico::where('id_tipo_servico', '=', $id_tipo_servico)->first();
            if (!is_object($servico)){
                $servico = new \Servico;
                $servico->descricao = 'xxx';
                $servico->id_tipo_servico = $id_tipo_servico;
                $servico->save();
            }
            $os->id_servico = $servico->id_servico;
            $os->save();
            $repOs = new Repositorios\RepositorioOs();

            $repOs->adicionaCandidato($os, $cand);

            $visto = $cand->vistoAtual();

            $proc = \Processo::where('id_candidato', '=', $cand->id_candidato)
                        ->where('id_os', '=', $os->id_os)
                        ->where('id_tipoprocesso', '=', $id_tipoprocesso)
                        ->where('id_visto', '=', $visto->id_visto)
                        ->first();
            $this->assertNotEmpty($proc);

            $proc->delete();
            $os->delete();
        } catch (Exception $ex) {
            var_dump($ex->getMessage());
            var_dump($ex->getTraceAsString());
            if (isset($os)){
                $os->delete();
            }
            if (isset($proc)){
                $proc->delete();
            }
        }
    }
}
