@extends('base')

@section('content')
<!-- BEGIN Page Title -->
	<div class="page-title">
            <div class="box-tool">
                <a href="/protocolobsb/enviadosexcel/{{$dt_protocolo}}" class="btn btn-warning" target="_blank"><i class="icon-envelope"></i> Enviar por e-mail</a>
            </div>
            {{Form::open(array('id'=>'form_periodo'))}}
	        <h1>
                    <i class="icon-file-alt"></i>Protocolo de Brasília - Enviados 
                    de {{ Form::select('dt_protocolo_ini', $protocolos, $dt_protocolo_ini, $attributes = array('class' => '', 'id' => 'dt_protocolo_ini')); }} 
                    até {{ Form::select('dt_protocolo_fim', $protocolos, $dt_protocolo_fim, $attributes = array('class' => '', 'id' => 'dt_protocolo_fim')); }}
                    <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i></button>
                </h1>
            {{Form::close()}}
            <a data-action="collapse"><i class="icon-chevron-up"></i></a>
	</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
@if ($dt_protocolo != '')
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array( "class"=>"form-horizontal")) }}
        {{ Form::hidden('acao', 'salvar')}}
        {{ Form::hidden('dt_protocolo', $dt_protocolo)}}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Observações do protocolo de {{$dt_protocolo_fmt}}</h3>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        @include('html/campo-textarea', array('id' => 'observacao', 'label' => 'Observação', 'valor' => $protocolo->observacao,  'help' => '',  array()))
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close()}}
    </div>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Processos</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                @if(isset($registros))
                <table class="table table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 80px">Ação</th>
                            <th style="width: 80px">Os</th>
                            <th style="width: auto;">Empresa<br/>Projeto/Emb.</th>
                            <th style="width: 200px">Candidatos</th>
                            <th style="width: auto;">Serviço</th>
                            <th style="width: auto; text-align: center;">Dt. envio</th>
                            <th style="width: auto; text-align: center;">Processo</th>
                            <th style="width: auto; text-align: center;">Dt. req.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $registro)
                        <tr>
                            <td>
                                <a class="btn btn-danger btn-sm" href="{{ URL::to('/protocolobsb/retirar/'.$registro->id_os) }}" title="Retirar essa OS do protocolo"><i class="icon-minus"></i></a>
                            </td>
                            <td><a class="btn btn-primary btn-sm" href="{{ URL::to('/os/show/'.$registro->id_os) }}" target="_blank">{{ substr('000000'.$registro->numero, -6) }}</a></td>
                            <td>{{ $registro->razaosocial }}<br/>{{ $registro->descricao_projeto }}</td>
                            <td>
                                <? $br = '';
                                $os = \Os::find($registro->id_os);
                                $candidatos = $os->candidatos;
                                ?>
                                @foreach($candidatos as $candidato)
                                {{$br}}<a href="{{URL::to('/candidato/show/'.$candidato->id_candidato)}}" target="_blank" title="Clique para ver o candidato em uma nova janela">{{ $candidato->nome_completo }}</a>
                                <? $br = '<br/>';?>
                                @endforeach
                            </td>
                            <td>
                               {{ $registro->descricao_servico  }}
                            </td>
                            <td style="text-align: center;">
                                @if ($registro->dt_envio_bsb == '' || $registro->dt_envio_bsb == '0000-00-00')
                                <button>{{date('d/m/Y')}}</button>
                                @else
                                {{$registro->dt_envio_bsb_fmt}}
                                @endif
                            </td>
                            <td style="text-align: center;"><input type="text" value="{{$registro->string_1}}" class="input-block-level" name="processo" data-mask="99999.999999/9999-99" onblur="javascript:atualizaCampo(this, '9');" data-parameters='{"controller":"processo", "metodo":"atualizacampo", "campo_chave":"id_os", "valor_chave":"{{$registro->id_os}}", "campo":"string_1"  }' data-valor-original="{{$registro->string_1}}"/></td>
                            <td style="text-align: center;"><input type="text" value="{{$registro->date_1}}"  class="input-block-level" name="processo" data-mask="99/99/9999" onblur="javascript:atualizaCampo(this, 'D');" data-parameters='{"controller":"processo", "metodo":"atualizacampo", "campo_chave":"id_os", "valor_chave":"{{$registro->id_os}}", "campo":"date_1"  }' data-valor-original="{{$registro->date_1}}"/></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                Selecione uma empresa.
                @endif
            </div>
    </div>
    </div>
</div>
@stop
@section('scripts')
<script>
$( document ).ready(function(){
    VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajsonfiltro/', '(selecione a empresa)', false);
});


function atualizaCampo(pinput, ptipo){
    var valor = $(pinput).val();
    if (ptipo == '9'){
        var valor_limpo = valor.replace(/\D/g,'');        
    } else {
        var valor_limpo = valor;        
    }
    valor_original = $(pinput).attr('data-valor-original');
    if (valor_limpo != valor_original  && valor.indexOf("_") == -1){
        var pParametros = $(pinput).attr('data-parameters');
        var parm = jQuery.parseJSON(pParametros);
        var url = '{{URL::to('')}}/'+parm.controller+'/'+parm.metodo;
        var td = $(pinput).parent();
        parm['valor'] = valor;
        $.noty.closeAll();
        $.ajax({
            type: 'POST'
          , url: url
          , data: parm
          , dataType: 'json'
          , success:
                function(data) 
                {
                    if(data.msg.tipoMsg == 'success'){
                        $(pinput).attr('data-valor-original', valor);
                        td.animate({backgroundColor: "#8de580"}, 250 );
                        td.animate({backgroundColor: "transparent"}, 750 );                        
                        if(data.novoConteudo){
                            $(td).html(data.novoConteudo);
                        }
                    }
                    else{
                        if(data.msg.tipoMsg == 'nop'){
                            $.noty.closeAll();
                            data.msg.textoMsg = 'Não foi possível atualizar devido ao seguinte erro: '+data.msg.textoMsg;
                            var xx= noty({text: data.msg.textoMsg, timeout: false, type: 'error', closeWith: 'click', layout:'top' });
                        }
                    }
                }
        });        
    }
}

</script>
@stop
