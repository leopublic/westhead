@extends('base')
@section('styles')
 tr.vazia {background-color:#d9d9d9;}
@stop

@section('content')
@include('html/page-title', array('titulo' => 'Nova Invoice', 'subTitulo' => 'Indique o centro de custos'))

@include('html/mensagens')

<!-- BEGIN Main Content -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Centros de custos</h3>
            </div>
            <div class="box-content">
                <table class="table table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width:30px">#</th>
                            <th style="width:80px">Ação</th>
                            <th style="width: auto">Centro de custos</th>
                            <th style="width: auto">Qtd OS não faturadas</th>
                            <th style="width: auto">&nbsp;</th>
                        </tr>
                    </thead>
                    @if(isset($centros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($centros as $centro)
                        <? $i++; ?>
                        @if ($centro->qtdNaoFaturadas > 0)
                        <tr>
                        @else
                        <tr class="vazia">
                        @endif
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/invoice/create/'.$centro->id_centro_de_custo) }}" title="Clique para criar uma nova invoice para esse centro de custos"><i class="icon-plus"></i></a>
                            </td>
                            <td>{{ $centro->nome }}<br/><span style="font-size:11px; color:#999;">{{$centro->razaosocial}}</span></td>
                            <td>{{ $centro->qtdNaoFaturadas }}</td>
                            <td>&nbsp;</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop

