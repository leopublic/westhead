<table class="table table-striped table-hover fill-head">
    <thead>
        <tr>
            <th>#</th>
            <th style="width: 50px">ID</th>
            <th style="width: auto">Nome completo</th>
            <th style="width: auto;">Nome da mãe</th>
            <th style="width: 150px;">Data nasc.</th>
        </tr>
    </thead>
    @if(isset($registros))
    <tbody>
        <? $i = 0; ?>
        @foreach($registros as $registro)
        <? $i++; ?>
        <tr>
            <td><? print $i;?></td>
            <td>
                <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/show/'.$registro->id_os) }}"><i class="icon-plus"></i></a>
            </td>
            <td>{{ substr('000000'.$registro->numero, -6) }}</td>
            <td>{{ $registro->empresa->razaosocial }}</td>
            <td>{{ $registro->projeto->descricao }}</td>
            <td>@if(isset($registro->armador))
                {{ $registro->armador->razaosocial }}
                @else
                -
                @endif
            </td>
            <td>-</td>
            <td>{{ $registro->servico->descricao }}</td>
            <td>@include('html/data-fmt', array( 'data' => $registro->dt_solicitacao))
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
