<?php

class FuncaoController extends \CadastroController {

    const raiz = "funcao";
    const campoChave = 'id_funcao';
    const nomeSingular = 'Função';
    const nomePlural = 'Funções';

    public function RegistrosIndex() {
        $data = Funcao::orderBy('descricao')->get();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(
            array("titulo" => "Descrição", "campo" => "descricao")
            , array("titulo" => "Descrição (en)", "campo" => "descricao_en")
            , array("titulo" => "Código PF", "campo" => "codigo_pf")
        );
        return $data;
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = Funcao::find($id);
        } else {
            $this->obj = new Funcao;
        }
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = "Funções";
        $data['titulo_grid'] = "Lista";
        $data['subTitulo'] = "Administrar Funções";
        if (\Auth::user()->temAcessoA('TABS', 'ALT')){
            $data['urlEdit'] = 'funcao/show';
        }
        if (\Auth::user()->temAcessoA('TABS', 'EXCL')){
            $data['urlDel'] = 'funcao/del';
        }
        if (\Auth::user()->temAcessoA('TABS', 'INS')){
            $data['urlCreate'] = 'funcao/create';
        }
        $data['nomeCampoChave'] = 'id_funcao';
        $data['menu'] = 'menuTabelas';
        $data['item'] = 'itemFuncao';
        return $data;
   }

    public function get_ViewShow() {
        return 'funcao/show';
    }

    public function MontaCampos() {
        //	$this->campos[] = new
    }

    public function postUpdate($id = 0) {
        if ($id > 0) {
            $funcao = Funcao::find($id);
        } else {
            $funcao = new Funcao();
        }
        $funcao->fill(Input::all());
        if ($funcao->save()) {
            return Redirect::to('funcao')->with('flash_msg', 'Função atualizada com sucesso!');
        } else {
            Input::flash();
            return Redirect::to('funcao/show/' . $id)->withErrors($funcao->errors);
        }
    }

    public function getDel($id = 0) {
        try {
            Funcao::destroy($id);
            return Redirect::to(self::raiz)
                            ->with('flash_msg', self::nomeSingular . ' excluído com sucesso!');
        } catch (Exception $e) {
            return Redirect::to(self::raiz)
                            ->with('flash_error', $e->getMessage());
        }
    }

}
