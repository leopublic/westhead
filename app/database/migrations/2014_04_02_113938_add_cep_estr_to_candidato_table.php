<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCepEstrToCandidatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('candidato', function(Blueprint $table) {
			$table->integer('id_pais_nacionalidade_pai')->unsigned()->nullable();
			$table->integer('id_pais_nacionalidade_mae')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('candidato', function(Blueprint $table) {
			$table->dropColumn('id_pais_nacionalidade_pai');
			$table->dropColumn('id_pais_nacionalidade_mae');
		});
	}

}
