<?php
class Profissao extends Modelo {
	protected $table = 'profissao';
	protected $primaryKey = 'id_profissao';

	protected $guarded = array();

	public function assumeDefaults()
	{
		if($this->descricao_en == ''){
			$this->descricao_en = $this->descricao;
		}
	}

	public static function regras()
	{
		return array(
				'descricao' => 'required'
				);
	} 

	public static function mensagens()
	{
		return array(
				'descricao.required' => 'O nome é obrigatório!'
			);
	}
}
