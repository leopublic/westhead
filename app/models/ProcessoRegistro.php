<?php
/**
 * @property string $numero_processo Número do processo (string_1)
 * @property string $data_requerimento Data do requerimento do processo (date_1)
 * @property string $validade_protocolo Validade do protocolo (date_2)
 * @property string $prazo_estada Prazo de estada (string_2)
 * @property string $rne Número da RNE (string_3)
 */
class ProcessoRegistro extends Processo {

    protected $guarded = array('delegacia', 'id_status_andamento', 'obs_andamento', 'tem_andamento');

    public static function boot() {
        parent::boot();

        static::saving(function($modelo) {
            if (isset($modelo->numeroProcesso)){
                $codigo = substr($modelo->numeroProcesso, 0, 5);
                $delegacia = \Delegacia::where('codigo', '=', $codigo)->first();
                if (is_object($delegacia)){
                    $modelo->integer_1 = $delegacia->id_delegacia;
                } else {
                    $modelo->integer_1 = null;
                }
            }
        });

        static::saved(function($modelo) {
            // registra log
        });
    }

    public function assumeDefaults() {
        $this->id_tipoprocesso = 3;
    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

    public function delegacia(){
        return $this->belongsTo('Delegacia', 'integer_1');
    }

    public function nomeDelegacia(){
        if (is_object($this->delegacia)){
            return $this->delegacia->nome;
        } else {
            return '?';
        }
    }


    public function recupereDadosDaOs() {
        parent::recupereDadosDaOs();
    }

    public function getNumeroProcessoAttribute($valor) {
        if (isset($this->attributes['string_1'])){
            return $this->attributes['string_1'];
        } else {
            return '';
        }
    }

    public function setNumeroProcessoAttribute($valor) {
        $this->attributes['string_1'] = $valor;
    }

    public function getDataRequerimentoAttribute($valor) {
        return $this->dtModelParaView($this->attributes['date_1']);
    }

    public function setDataRequerimentoAttribute($valor) {
        $this->attributes['date_1'] = $this->dtViewParaModel($valor);
    }

    public function getValidadeProtocoloAttribute($valor) {
        return $this->dtModelParaView($this->attributes['date_2']);
    }

    public function setValidadeProtocoloAttribute($valor) {
        $this->attributes['date_2'] = $this->dtViewParaModel($valor);
    }

    public function getPrazoEstadaAttribute($valor) {
        if (isset($this->attributes['string_2'])){
            return $this->attributes['string_2'];
        } else {
            return '';
        }
    }

    public function setPrazoEstadaAttribute($valor) {
        $this->attributes['string_2'] = $valor;
    }

    public function getTituloAttribute(){
        return 'Registro';
    }

    public function getCampoOrdemMaisRecenteAttribute(){
        return 'date_1';
    }

}
