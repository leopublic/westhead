<?php

namespace Westhead\Repositorios;

class RepositorioInvoice extends Repositorio {

    public function adicioneOs($id_invoice, $id_os) {
        $os = \Os::find($id_os);
        if (!isset($os)) {
            throw new Exception('Ordem de serviço não encontrada para o ID informado (' . $id_os . ")");
        } else {
            $invoice = \Invoice::find($id_invoice);
            if (!isset($invoice)) {
                throw new Exception('Invoice não encontrada para o ID informado (' . $id_invoice . ")");
            } else {
                $os->id_invoice = $id_invoice;
                $os->save();
                $this->habilitaServicos($id_invoice);
                $this->atualizaDescricaoServico($id_invoice, $os->id_servico);
            }
        }
    }

    public function retireOs($id_invoice, $id_os) {
        $os = \Os::find($id_os);
        if (!isset($os)) {
            throw new Exception('Ordem de serviço não encontrada para o ID informado (' . $id_os . ")");
        } else {
            $os->id_invoice = null;
            $os->save();
            $this->habilitaServicos($id_invoice);
        }
    }

    public function habilitaServicos($id_invoice) {
        $sql = "insert into invoice_servico(id_invoice, id_servico) "
                . " select " . $id_invoice . ", id_servico "
                . " from os "
                . " where id_invoice = " . $id_invoice . " "
                . " and id_servico not in (select id_servico from invoice_servico where id_invoice = " . $id_invoice . ") ";
        \DB::insert(\DB::raw($sql));
        $sql = "delete from invoice_servico where id_invoice = ".$id_invoice." and id_servico not in (select id_servico from os where id_invoice= ".$id_invoice.")";
        \DB::delete(\DB::raw($sql));
    }

    public function atualizaDescricaoServico($id_invoice, $id_servico){
        $serv = \Servico::find($id_servico);
        $desc = $serv->descricao_invoice;

        if (str_contains($desc, '%candidatos%')){
            $candidatos = '<strong>'.$this->candidatos($id_invoice, $id_servico).'</strong>';
            $desc = str_replace('%candidatos%', $candidatos, $desc);
        }
        if (str_contains($desc, '%projeto%')){
            $projeto = '<strong>'.$this->projetos($id_invoice, $id_servico).'</strong>';
            $desc = str_replace('%projeto%', $projeto, $desc);
        }

        $descricao_invoice = $desc;
        $descricao_despesas = 'Reimbursable expenses for '.$desc;

        \DB::table('invoice_servico')
                ->where('id_invoice', '=', $id_invoice)
                ->where('id_servico', '=', $id_servico)
                ->update(array(
                    'descricao_invoice' => $descricao_invoice
                    , 'descricao_despesas' => $descricao_despesas
                   ));
    }

    public function listaOss($id_invoice) {
        $oss = \Os::where('id_invoice', '=', $id_invoice)
                ->orderBy('numero')
                ->get();
        $ret = '';
        $virg = '';
        foreach ($oss as $os) {
            $ret .= $virg . substr('0000' . $os->numero, -4);
            $virg = ', ';
        }
        if ($ret != '') {
            $ret = 'OS ' . $ret;
        }
        return $ret;
    }

    public function projetos($id_invoice, $id_servico) {
        $sql = "select distinct descricao "
                . " from os "
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " where os.id_invoice = " . $id_invoice
                . " and os.id_servico = " . $id_servico
                . " order by descricao";
        $projs = \DB::select(\DB::raw($sql));
        $ret = '';
        $virg = '';
        foreach ($projs as $proj) {
            $ret .= $virg . $proj->descricao;
            $virg = ', ';
        }
        return $ret;
    }

    public function candidatos($id_invoice, $id_servico) {
        $sql = "select nome_completo "
                . " from os "
                . " join os_candidato on os_candidato.id_os = os.id_os"
                . " join candidato on candidato.id_candidato = os_candidato.id_candidato"
                . " where os.id_invoice = " . $id_invoice
                . " and os.id_servico = " . $id_servico
                . " order by nome_completo";
        $cands = \DB::select(\DB::raw($sql));
        $ret = '';
        $virg = '';
        foreach ($cands as $cand) {
            $ret .= $virg . $cand->nome_completo;
            $virg = "\n";
        }
        return $ret;
    }

    public function atualizaServico($id_invoice, $id_servico, $descricao_invoice, $descricao_despesas, $val_despesas, $val_unitario, $val_total) {

        $val_total = str_replace(",", ".", str_replace('.', '', $val_total));
        $val_unitario = str_replace(",", ".", str_replace('.', '', $val_unitario));
        $val_despesas = str_replace(",", ".", str_replace('.', '', $val_despesas));

        // Calcula o valor total caso não seja informado
        if ($val_total =='' || floatval($val_total) == 0){
            $qtd = \Os::join('os_candidato', 'os_candidato.id_os', '=', 'os.id_os')
                    ->where('id_invoice', '=', $id_invoice)
                    ->where('id_servico', '=', $id_servico)->count();
            $val_total = $val_unitario * $qtd;
        }

        \DB::table('invoice_servico')
                ->where('id_invoice', '=', $id_invoice)
                ->where('id_servico', '=', $id_servico)
                ->update(array(
                    'descricao_invoice' => $descricao_invoice
                    , 'descricao_despesas' => $descricao_despesas
                    , 'val_despesas' => $val_despesas
                    , 'val_unitario' => $val_unitario
                    , 'val_total' => $val_total
                   ));
    }
    
    public function exclua(\Invoice $invoice){
        \DB::beginTransaction();
            \InvoiceServico::where('id_invoice', '=', $invoice->id_invoice)->delete();
            \Os::where('id_invoice', '=', $invoice->id_invoice)->update(array('id_invoice'=> null));
            $invoice->delete();
        \DB::commit();
    }
}
