@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>Análise de proporcionalidade</h1>
                        <h4>Verificar a proporcionalidade dos contratados das embarcações</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                @include('html.mensagens')
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-table"></i>Projetos</h3>
                                <div class="box-tool">
                                </div>
                            </div>
                            <div class="box-content">
                                <table class="table table-striped table-hover fill-head">
                                    <thead>
                                        <tr>
                                            <th>Ação</th>
                                            <th>Empresa</th>
                                            <th>Projeto</th>
                                            <th style="text-align:center;">Início operação</th>
                                            <th style="text-align:center;">Dias</th>
                                            <th style="text-align:center;">Brasileiros</th>
                                            <th style="text-align:center;">Estrangeiros</th>
                                            <th style="text-align:center;">Bras. / Estr.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <? $i = 0;?>
                                    @foreach($registros as $registro)
                                    <? $i++;?>
                                        <tr>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/projeto/show/'.$registro->id_projeto) }}"><i class="icon-edit"></i></a>                                                
                                            </td>
                                            <td>{{ $registro->Empresa->razaosocial }}</td>
                                            <td>{{ $registro->descricao }}</td>
                                            <td style="text-align:center;">{{ $registro->dt_inicio_operacao }}</td>
                                            <td style="text-align:center;">
                                                {{ $registro->dias_em_operacao }}
                                            </td>
                                            <td style="text-align:center;">{{ $registro->qtd_tripulantes - $registro->qtd_estrangeiros }}</td>
                                            <td style="text-align:center;">{{ $registro->qtd_estrangeiros }}</td>
                                            <td style="text-align:center;">{{ $registro->proporcionalidade }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
@stop