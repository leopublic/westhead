<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminCorrigeProcessosFaltando extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:corrigeprocessos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Corrige os processos faltando nas OS.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $sql = "select os.id_os, numero , os_candidato.id_candidato, os.id_servico, id_tipoprocesso, id_visto
                from os 
                join servico on servico.id_servico = os.id_servico
                join tipo_servico on tipo_servico.id_tipo_servico = servico.id_tipo_servico
                join os_candidato on os_candidato.id_os = os.id_os
                join candidato on candidato.id_candidato = os_candidato.id_candidato
                left join visto on visto.id_candidato = candidato.id_candidato and processo_atual = 1
                where not exists (select * from processo 
                                  where processo.id_os = os.id_os 
                                  and processo.id_candidato = os_candidato.id_candidato)
                order by numero, id_candidato
                ";
        $rs = \DB::select(\DB::raw($sql));
        $tot = 0;
        $repP = new \Westhead\Repositorios\RepositorioProcesso();
        foreach ($rs as $reg) {
            try{
                $os = \Os::find($reg->id_os);
                $candidato = \Candidato::find($reg->id_candidato);
                $processo = $repP->criaProcessoDaOs($os, $candidato);
                if(is_object($processo)){
                    $vistoAtual = \Visto::find($reg->id_visto);
                    $repP->vinculaProcessoAoVisto($processo, $vistoAtual);
                    $linha = "Processo corrigido: ID_OS ".substr('000000'.$reg->id_os, -6)." OS ".substr('000000'.$reg->numero, -6)." Cand:".substr('000000'.$reg->id_candidato, -6)." Servico:".substr('000'.$reg->id_servico, -3);
                    $this->info($linha);
                    $tot++;                
                } else {
                    $linha = "Processo ignorado: ID_OS ".substr('000000'.$reg->id_os, -6)." OS ".substr('000000'.$reg->numero, -6)." Cand:".substr('000000'.$reg->id_candidato, -6)." Servico:".substr('000'.$reg->id_servico, -3);
                    $this->error($linha);                    
                }
            } catch (Exception $ex) {
                $linha = $ex->getMessage();
                $this->error($linha);
            }
        }
        $this->error($tot." registros corrigidos");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }

}
