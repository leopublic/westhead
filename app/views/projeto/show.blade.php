@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-map-marker"></i>{{ $tela['titulo_tela'] }}</h1>
                        <h4>{{ $tela['subTitulo'] }}</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Projeto</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if(count($errors->all())> 0)
                <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-danger">
                        <ul class="errors">
                          @foreach($errors->all() as $message)
                          <li>{{ $message }}</li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Alterar projeto</h3>
                                <div class="box-tool">
                                </div>
                            </div>
                            <div class="box-content">

                                {{ Form::open(array('url' => 'projeto/update/'.$obj->id_projeto, "class"=>"form-horizontal")) }}
                                    <div class="form-group">
                                      {{Form::label('id_empresa', 'Empresa', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::select('id_empresa', Empresa::where('fl_armador','=',0)->orderBy('razaosocial')->lists('razaosocial', 'id_empresa'), Input::old('id_empresa', $obj->id_empresa), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Informe a empresa</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('id_empresa_armador', 'Armador', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::select('id_empresa_armador', array('' => '(idem empresa)') + Empresa::where('fl_armador','=',1)->orderBy('razaosocial')->lists('razaosocial', 'id_empresa'), Input::old('id_empresa_armador', $obj->id_empresa_armador), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Nome de fantasia</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('descricao', 'Projeto', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::text('descricao',  Input::old('descricao', $obj->descricao), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Nome do projeto</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('id_pais', 'Bandeira', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::select('id_pais', array('0' =>'(n/d)') + Pais::orderBy('descricao')->lists("descricao", 'id_pais'), Input::old('id_pais', $obj->id_pais), $attributes = array('class' => 'form-control')); }}
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('fl_embarcacao', 'É embarcação?', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-2 controls">
                                          <input type="checkbox" value="1" name="fl_embarcacao"
                                          @if(intval(Input::old('fl_embarcacao', $obj->fl_embarcacao)) == 1)
                                          checked="checked"
                                          @endif
                                          />
                                          <br/>
                                          <span class="help-inline">Indica se o projeto sai na declaração de embarcações ou não</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('fl_ativa', 'Ativo?', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-2 controls">
                                          <input type="checkbox" value="1" name="fl_ativa"
                                          @if(intval(Input::old('fl_ativa', $obj->fl_ativa)) == 1)
                                          checked="checked"
                                          @endif
                                          />
                                          <br/>
                                          <span class="help-inline">Indica se o projeto está ativo ou não</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('dt_inicio_operacao', 'Início de operação', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-2 controls">
                                          {{ Form::text('dt_inicio_operacao', Input::old('dt_inicio_operacao', $obj->dt_inicio_operacao), $attributes = array('class' => 'form-control', 'data-mask' => '99/99/9999')); }}
                                          <span class="help-inline">Data de início de operação para o controle de dilação</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('dt_vencimento_contrato', 'Vencimento do contrato', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-2 controls">
                                          {{ Form::text('dt_vencimento_contrato', Input::old('dt_vencimento_contrato', $obj->dt_vencimento_contrato), $attributes = array('class' => 'form-control', 'data-mask' => '99/99/9999')); }}
                                          <span class="help-inline">Data de vencimento do contrato</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('qtd_tripulantes', 'Quantidade total de tripulantes', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-2 controls">
                                          {{ Form::text('qtd_tripulantes', Input::old('qtd_tripulantes', $obj->qtd_tripulantes), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Quantidade total de tripulantes, brasileiros e estrangeiros</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('qtd_estrangeiros', 'Quantidade de tripulantes estrangeiros', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-2 controls">
                                          {{ Form::text('qtd_estrangeiros', Input::old('qtd_estrangeiros', $obj->qtd_estrangeiros), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Quantidade de tripulantes estrangeiros</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('justificativa', 'Justificativa', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::textarea('justificativa', Input::old('justificativa', $obj->justificativa), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Texto padrão da justificativa</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                           {{ HTML::link('empresa', 'Cancelar', array('class' => 'btn')) }}
                                        </div>
                                    </div>
                                 {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

             </div>
@stop