<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAtributosToOsCandidatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('os_candidato', function(Blueprint $table) {
			$table->integer('id_reparticao')->unsigned()->nullable();
            $table->text('justificativa')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('os_candidato', function(Blueprint $table) {
            $table->dropColumn('id_reparticao');
            $table->dropColumn('justificativa');
		});
	}

}
