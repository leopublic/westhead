<?php
/**
 * @property int $id_delegacia Chave
 * @property string $codigo
 * @property string $nome
 * @property string $uf
 */
class Delegacia extends Modelo {
	protected $table = 'delegacia';
	protected $primaryKey = 'id_delegacia';

}
