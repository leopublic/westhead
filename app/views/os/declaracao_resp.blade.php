
AO
<br/>MINISTÉRIO DA JUSTIÇA
<br/>DEPARTAMENTO DE ESTRANGEIROS
<br/>BRASÍLIA-DF
<br/>
<br/>
<br/>
<div style="text-align: center; width:100%; font-weight: bold;">
TERMO DE DECLARAÇÃO GERAL DE RESPONSABILIDADE
</div>
<br/>
<br/>
<br/>
<br/>
<br/><strong>{{$os->empresa->razaosocial}}</strong>, com sede na {{$os->empresa->enderecoCompleto()}}, inscrita no CNPJ sob o nº {{$os->empresa->cnpj}} {{$os->empresa->filial}}, representado por seu procurador abaixo assinado, DECLARA, sob as penas da Lei, em relação ao estrangeiro {{$cand->tratamento()}} <strong>{{$cand->nome_completo}}</strong>, portador do passaporte <strong>Nº {{$cand->nu_passaporte}}</strong>, durante a sua permanência em Território Nacional, que:
<br/>
<br/>
<br/>
<br/>a)	Assume a responsabilidade por todas e quaisquer despesas médicas e/ou hospitalares do estrangeiro;
<br/>b)	Assume a responsabilidade pela repatriação do estrangeiro ao país de origem;
<br/>c)	O estrangeiro exercerá suas funções no(s) seguinte(s) endereço(s):
<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.	A bordo da embarcação <strong>{{ $os->projeto->descricao}}</strong>.
<br/>
<br/>
<br/>
<div style="text-align: center; width:100%">
Rio de Janeiro, {{ date('d')}} de {{$mes}} de {{ date('Y')}}.<br/><br/>
__________________________________<br/>
<strong>{{ $os->empresa->razaosocial }}</strong><br/>
{{ $os->empresa->procurador->nome}}<br/>
{{ $os->empresa->procurador->titulo}}<br/>
CPF {{ $os->empresa->procurador->cpf }}<br/>
</div>
