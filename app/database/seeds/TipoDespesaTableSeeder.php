<?php

class TipoDespesaTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('tipo_despesa')->truncate();

		$tipodespesa = array('nome_pt_br' => 'Malote', 'nome_en_us' => 'Courier');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Taxa de registro / restabelecimento', 'nome_en_us' => 'Registration Fee');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Taxa de Visto', 'nome_en_us' => 'Brazilian Working Visa Governamental Fee');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Autenticações Cartorárias', 'nome_en_us' => 'Notarized copies');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Reconhecimento de firma', 'nome_en_us' => 'Notarized Signature');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Transporte', 'nome_en_us' => 'Transportation');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Ônibus', 'nome_en_us' => 'Bus');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Mêtro', 'nome_en_us' => 'Subway');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Táxi', 'nome_en_us' => 'Taxi');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Xerox', 'nome_en_us' => 'Photocopies');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Internet', 'nome_en_us' => 'Internet');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Fotos', 'nome_en_us' => 'Photographs');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('nome_pt_br' => 'Taxa ASO Panamenho', 'nome_en_us' => 'Panama Medical fee');
        DB::table('tipo_despesa')->insert($tipodespesa);
	}

}
