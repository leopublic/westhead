<?php

/**
 * Controla a verificação de acessos
 */
class AuthController extends \BaseController {

    public function getLogin() {
        return View::make('login');
    }

    public function postLogin() {
        $username = Input::get('username');
        $password = Input::get('password');
        if (trim($username) == ''){
            return Redirect::to('auth/login')->with('login_errors', true)->with('msg', 'Informe o login do usuário.');
        }
        if (trim($password) == ''){
            return Redirect::to('auth/login')->with('login_errors', true)->with('msg', 'Informe a senha do usuário.');
        }
        $usuario = \Usuario::where('username', '=', $username)->first();
        $ip = Request::getClientIp();
        $rep = new \Westhead\Repositorios\RepositorioAuth;
        // Testa o limite de tentativas erradas
        if ($rep->podeTentarAcesso($username, $ip)){
            if (Auth::attempt(array('username' => $username, 'password' => $password), false)) {
                $rep->registraTentativaComSucesso(Auth::user(), $ip);
                return Redirect::to('/');
            } else {
                $rep->registraTentativaSemSucesso($username, $ip);
                return Redirect::to('auth/login')->with('login_errors', true)->with('msg', 'Usuário ou senha inválidos..');
            }            
        } else {
            //Notifica responsáveis
            $rep->notificaExcessoTentativas($username, $ip);
            return Redirect::to('auth/login')->with('login_errors', true)->with('msg', 'Você excedeu o número permitido de tentativas e seu acesso foi bloqueado. Por favor, solicite uma nova senha.');
        }
    }

    public function getEsquecisenha() {
        return View::make('usuario.esquecisenha');
    }

    public function postEsquecisenha() {
        if(Input::has('email')){
            $usuario = \Usuario::where('email', '=', Input::get('email'))->first();
            if (is_object($usuario)){
                $rep = new \Westhead\Repositorios\RepositorioUsuario;
                $rep->enviaNovaSenha($usuario);
                Session::flash('login_errors', true);
                Session::flash('msg', 'Nova senha enviada para o e-mail "'. Input::get('email') . '". Verifique na sua caixa de entrada.');
                return Redirect::to('/auth/login');
            } else {
                Session::flash('login_errors', true);
                Session::flash('msg', 'Nenhum usuário encontrado com o e-mail informado');
                return Redirect::back();
            }
        } else {
                Session::flash('login_errors', true);
            Session::flash('msg', 'Informe o e-mail');
            return Redirect::back();            
        }
    }

    public function getLogout() {
        Session::clear();
        Auth::logout();
        return Redirect::to('/');
    }
    
    public function getAltereminhasenha() {
        $usuario = Auth::user();
        return View::make('usuario.altereminhasenha')
                        ->with('model', $usuario)
        ;
    }
    public function postAltereminhasenha() {
        try{
            Input::flash();
            $rep = new \Westhead\Repositorios\RepositorioUsuario();
            if($rep->altereMinhaSenha(Input::all())){
                $msg = 'Senha alterada com sucesso!';
                Session::flash('flash_msg', $msg);
                return Redirect::to('/home/bemvindo');
            } else {
                return Redirect::back()->withErrors($rep->getErrors());
            }
        } catch (Exception $ex) {
            Session::flash('flash_error', $ex->getMessage());
            return Redirect::back();
        }
    }

}
