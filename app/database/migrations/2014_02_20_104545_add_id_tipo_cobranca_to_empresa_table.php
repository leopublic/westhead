<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdTipoCobrancaToEmpresaTable extends Migration {

    public function up() {
        Schema::table('empresa', function(Blueprint $table) {
            $table->integer('id_tipo_cobranca')->unsigned()->nullable();
            $table->integer('id_tipo_calc_desp')->unsigned()->nullable();
            $table->decimal('valor_cotacao_propria', 10,2)->unsigned()->nullable();
        });

        DB::table('empresa')->update(array('id_tipo_cobranca' => 1));
        DB::table('empresa')->update(array('id_tipo_calc_desp' => 1));
    }

    public function down() {
        Schema::table('empresa', function(Blueprint $table) {
            $table->dropColumn('id_tipo_cobranca');
            $table->dropColumn('id_tipo_calc_desp');
            $table->dropColumn('valor_cotacao_propria');
        });
    }

}
