DELIMITER ;
drop trigger if exists status_processo_insert  ;
DELIMITER $$
CREATE TRIGGER  status_processo_insert  AFTER INSERT ON  status_processo  FOR EACH ROW
BEGIN
  	DECLARE msg varchar(2000);
    
  	SET msg = '';
	call varchar_alterado (null, NEW.descricao, 'descrição alterada de ', msg);

    IF msg <> '' THEN
		SET msg = concat('Alterações: ', msg);
		insert into log_status_processo(id_status_processo, id_usuario, controller, metodo, created_at, updated_at) 
		    values (NEW.id_status_processo, NEW.id_usuario, NEW.controller, NEW.metodo, msg, now(), now());
    END IF;
END
$$
DELIMITER ;