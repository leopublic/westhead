<?php

class HomeController extends BaseController {

    public function getBemvindo() {
        $rep = new \Westhead\Repositorios\RepositorioOs;
        $registros = $rep->all(null, null, 10);

        return View::make('home')->with('registros', $registros);
    }

    public function getFaq() {
        return View::make('faq');
    }

}
