<?php
/**
 * @property int $id_empresa Chave
 * @property string $razaosocial 
 * @property string $nomefantasia
 * @property string $cnpj
 * @property string $insc_estadual
 * @property string $insc_municipal
 * @property int $id_endereco_corr Description
 * @property int $id_endereco_cobr Description
 * 
 */
class Empresa extends Eloquent {
	protected $table = 'empresa';
	protected $primaryKey = 'id_empresa';
	
	public function __construct()
	{
		$this->fl_armador = 0;
	}

	public function scopeTodos($query)
	{
		return $query->where('fl_armador', '=', 0);
	}

}
