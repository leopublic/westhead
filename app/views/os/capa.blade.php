<style>

    table tr th{text-align: left; font-weight:bold;}
    table tr.subtitulo {margin-top: 25px;}
    table tr.subtitulo {margin-top:15px;margin-bottom:5px;}
    table tr.subtitulo th{text-align:left;color:white;font-size:16px; padding:5px;margin-top:15px; margin-bottom:5px;background-color: #ccc;}
</style>
<body>
    <table style="border-bottom:solid 4px #ccc; width:100%;">
        <tr>
            <td style="widht:60%;font-weight:bold;font-size:40px;">OS {{substr('000000'.$os->numero, -6)}}</td>
            <td style="text-align:right;widht:40%;font-weight:bold;font-size:40px;">WESTHEAD</td>
        </tr>
    </table>
    <br/>
    <span style="font-weight:bold;font-size:24px; text-decoration: underline;">{{ $os->servico->descricao }}</span>
    <br/>Responsável: {{ $os->usuarioabertura->nome }}
    <br/>
    <br/>
    <div style="width:100%;">
        <table style="width:100%;">
            <tr class="subtitulo"><th colspan="2">Cliente</th></tr>
            <tr><th width="280px">Cliente:</th><td>{{ $os->empresa->razaosocial }}</td></tr>
            <tr><th>Projeto:</th><td>{{ $os->projeto->descricao }}</td></tr>
            <tr><th>Centro de custo:</th><td>{{ $os->centrodecusto->nome }}</td></tr>
            <tr ><td colspan="2">&nbsp;</td></tr>
            <tr class="subtitulo"><th colspan="2">Candidatos</th></tr>
        </table>
            <? $i = 1; ?>
            @foreach($os->candidatos as $candidato)
            <?=$i;?>. {{ $candidato->nome_completo }}<br/><? $i++; ?>
            @endforeach
        <table style="width:100%;table-layout:fixed;">
            <tr class="subtitulo"><th colspan="2">Execução</th></tr>
            <tr><th width="280px">Data de entrada da solicitação:</th><td>{{ $os->dt_solicitacao }}</td></tr>
            <tr><th>Data do envio para assinatura:</th><td>{{ $os->dt_assinatura }}</td></tr>
            <tr><th>Data da prest. do serviço/protocolo:</th><td>{{ $os->dt_execucao }}</td></tr>
            <tr><th>Solicitante:</th><td>{{ $os->solicitante }}</td></tr>
            <tr class="subtitulo"><th colspan="2">Observações</th></tr>
            <tr><td colspan="2">{{ nl2br($os->observacao) }}</td></tr>
            <tr ><td colspan="2">&nbsp;</td></tr>
        </table>

    </div>
</body>