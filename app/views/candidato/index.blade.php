@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Candidatos', 'subTitulo' => ''))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
                {{ Form::open(array("class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                    <a data-action="collapse"><i class="icon-chevron-up"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => Input::old('id_empresa', Session::get('id_empresa')), 'valores' => $empresas , 'help' => '', array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Emb./Proj.', 'valor' => Input::old('id_projeto', Session::get('id_projeto')), 'valores' => $projetos , 'help' => '', array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'nome_completo', 'label' => 'Nome', 'valor' => Input::old('nome_completo', Session::get('nome_completo')), 'help' => '', 'atributos' => array('autocomplete' => 'off') ,  array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_funcao', 'label' => 'Função', 'valor' => Input::old('id_funcao', Session::get('id_funcao')), 'help' => '', 'valores' => array(''=> '(todas)') + Funcao::orderBy('descricao')->lists('descricao', 'id_funcao'), array()))
                    </div>
                </div>
            </div>
        </div>
                {{ Form::close()}}
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Candidatos</h3>
                <div class="box-tool">
                @if (Auth::user()->temAcessoA('CAND', 'INS'))
                    <a href="{{ URL::to('/candidato/create') }}" class="show-tooltip" data-placement="bottom" data-original-title="Clique para adicionar um novo candidato"><i class="icon-plus"></i></a>
                @endif
                </div>
            </div>
            <div class="box-content">
                @if(isset($registros))
                    {{$registros->links()}}
                @endif
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width:30px">#</th>
                            <th style="width:100px">Ação</th>
                            <th style="width: auto">Nome</th>
                            <th style="width: auto;">Nome mãe</th>
                            <th style="width: auto;">Data nasc.</th>
                            <th style="width: auto;">Passaporte</th>
                            <th style="width: auto;">Função</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $registro)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/candidato/cadastro/'.$registro->id_candidato) }}"><i class="icon-edit"></i></a>
                                @if (Auth::user()->temAcessoA('CAND', 'EXCL'))
                                @include('html.link_exclusao', array('url' => '/candidato/del/'.$registro->id_candidato, 'msg_confirmacao' => 'Clique para confirmar a exclusão do candidato.<br/><strong>ATENÇÃO: ESSA OPERAÇÃO NÃO PODE SER DESFEITA!</strong>'))
                                @endif
                            </td>
                            <td>{{ $registro->nome_completo }}</td>
                            <td>{{ $registro->nome_mae }}</td>
                            <td>{{ $registro->dt_nascimento }}</td>
                            <td>{{ $registro->nu_passaporte }}</td>
                            <td>
                                @if (is_object( $registro->funcao))
                                {{ $registro->funcao->descricao }}
                                @else
                                --
                                @endif</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
                @if(isset($registros))
                    {{$registros->links()}}
                @endif
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script>
    $(document).ready(function() {
        VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajsonfiltro/', '(selecione a empresa)', false);
    });
    
</script>
@stop
