@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Ordens de serviço', 'subTitulo' => ''))

@include('html/mensagens')
<? $qtdCol=2;?>
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
                {{ Form::open(array('url' => '/os', "class"=>"form-horizontal", "id"=>"criterios")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                    <a data-action="collapse"><i class="icon-chevron-up"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          {{Form::label('dt_solicitacao', 'Entrada em', array('id' => 'dt_solicitacao', 'class' => 'col-sm-4 col-md-4 col-lg-4 control-label')) }}
                            <div class="col-sm-8 col-md-8 col-lg-8 controls">
                               <div class="input-group">
                                <span class="input-group-addon">de</span>
                                {{ Form::text('dt_solicitacao_ini', Input::old('dt_solicitacao_ini', Session::get('dt_solicitacao_ini')), $attributes = array('class' => 'form-control', 'data-mask' => '99/99/9999')); }}
                                <span class="input-group-addon">até</span>
                                {{ Form::text('dt_solicitacao_fim', Input::old('dt_solicitacao_fim', Session::get('dt_solicitacao_fim')), $attributes = array('class' => 'form-control', 'data-mask' => '99/99/9999')); }}
                               </div>
                           </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          {{Form::label('dt_execucao', 'Executada em', array('id' => 'dt_execucao', 'class' => 'col-sm-4 col-md-4 col-lg-4 control-label')) }}
                            <div class="col-sm-8 col-md-8 col-lg-8 controls">
                               <div class="input-group">
                                <span class="input-group-addon">de</span>
                                {{ Form::text('dt_execucao_ini', Input::old('dt_execucao_ini', Session::get('dt_execucao_ini')), $attributes = array('class' => 'form-control', 'data-mask' => '99/99/9999')); }}
                                <span class="input-group-addon">até</span>
                                {{ Form::text('dt_execucao_fim', Input::old('dt_execucao_fim', Session::get('dt_execucao_fim')), $attributes = array('class' => 'form-control', 'data-mask' => '99/99/9999')); }}
                               </div>
                           </div>
                        </div>
                    </div>
                    @include('html/campo-texto', array('id' => 'numero', 'label' => 'Número', 'valor' => Input::old('numero', Session::get('numero')),  'help' => '',  array()))
                    @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => Input::old('id_empresa', Session::get('id_empresa')), 'valores' => $empresas , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Emb./Proj.', 'valor' => Input::old('id_projeto', Session::get('id_projeto')), 'valores' => $projetos , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_centro_de_custo', 'label' => 'Centro c.', 'valor' => Input::old('id_centro_de_custo', Session::get('id_centro_de_custo')), 'valores' => $centros , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_empresa_armador', 'label' => 'Armador', 'valor' => Input::old('id_armador', Session::get('id_armador')), 'valores' => $armadores , 'help' => '', 'valores', array()))
                    @include('html/campo-texto', array('id' => 'nome_completo', 'label' => 'Candidato', 'valor' => Input::old('nome_completo', Session::get('nome_completo')),  'help' => '',  array()))
                    @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => Input::old('id_servico', Session::get('id_servico')), 'valores' => $servicos , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_funcao', 'label' => 'Função', 'valor' => Input::old('id_funcao', Session::get('id_funcao')), 'help' => '', 'valores' => array(''=> '(todas)') + Funcao::orderBy('descricao')->lists('descricao', 'id_funcao'), array()))
                    @include('html/campo-select', array('id' => 'id_status_os', 'label' => 'Status', 'valor' => Input::old('id_status_os', Session::get('id_status_os')), 'help' => '', 'valores' => $statusos, array()))
                </div>
            </div>
        </div>
            {{ Form::close()}}
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Ordens de serviço</h3>
                <div class="box-tool">
                @if (Auth::user()->temAcessoA('ORDS', 'INS'))                    
                    <a href="{{ URL::to('/os/create') }}" class="show-tooltip" data-placement="bottom" data-original-title="Clique para adicionar uma nova OS"><i class="icon-plus"></i></a>
                @endif
                @if (Auth::user()->temAcessoA('ORDS', 'EXCE'))                    
                    <button onclick="javascript:geraExcel();" class="show-tooltip" data-placement="bottom" data-original-title="Clique para exportar essa consulta em excel"><i class="icon-table"></i></button>
                @endif
                </div>
            </div>
            <div class="box-content">
                @if(isset($registros))
                    {{$registros->links()}}
                @endif
                <table class="table table-hover fill-head">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 150px">Núm.</th>
                            <th style="width: auto;">Empresa</th>
                            <th style="width: auto;">Projeto/Emb.</th>
                            <th style="width: auto;">Armador</th>
                            <th style="width: 200px">Candidatos</th>
                            <th style="width: auto;">Serviço</th>
                            <th style="width: auto; text-align: center;">Entrada</th>
                            <th style="width: auto; text-align: center;">Execução</th>
                            <th style="width: auto; text-align: center;">Status</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $registro)
                        <? $i++; ?>
                        @if ($registro->id_status_os == 2)
                        <tr style="background-color: #36c77b;" name="os{{$registro->id_os}}" id="os{{$registro->id_os}}">
                        @elseif ($registro->id_status_os == 3)
                        <tr style="background-color: #d3d3d3; color:#666;" name="os{{$registro->id_os}}" id="os{{$registro->id_os}}">
                        @else
                        <tr name="os{{$registro->id_os}}" id="os{{$registro->id_os}}">
                        @endif
                            <td><? print $i; ?></td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{URL::to('/os/show/'.$registro->id_os)}}" class="btn btn-sm" title="Clique para ver os dados da OS" target="_blank">{{ substr('000000'.$registro->numero, -6) }}</a>
                                    <a href="#" data-toggle="dropdown" class="btn btn-sm dropdown-toggle"><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ URL::to('/os/candidatos/'.$registro->id_os) }}"><i class="icon-group"></i> Candidatos</a></li>
                                        <li><a href="{{ URL::to('/os/anexos/'.$registro->id_os) }}"><i class="icon-folder-open"></i>Anexos</a></li>
                                        <li><a href="{{ URL::to('/os/despesas/'.$registro->id_os) }}"><i class="icon-dollar"></i>Despesas</a></li>
                                    </ul>
                                    @if (Auth::user()->temAcessoA('ORDS', 'EXCL'))
                                        @include('html.link_exclusao', array('url'=> '/os/cancelar/'.$registro->id_os,  'msg_confirmacao'=>'Clique para confirmar a exclusão da Ordem de Serviço'))
                                    @endif                                    
                                </div>
                                
                            </td>
                            <td>{{ $registro['razaosocial'] }}</td>
                            <td>{{ $registro->descricao_projeto }}</td>
                            <td>{{ $registro->razaosocial_armador }}</td>
                            <td>
                                <? $br = '';?>
                                @foreach($registro->candidatos as $candidato)
                                <a href="{{URL::to('/candidato/cadastro/'.$candidato->id_candidato)}}" target="_blank" title="Clique para ver o candidato em uma nova janela">{{ $br.$candidato->nome_completo }}</a>
                                <? $br = '<br/>';?>
                                @endforeach
                            </td>
                            <td>
                               {{ $registro->descricao_servico  }}
                            </td>
                            <td style="text-align: center;">{{$registro->dt_solicitacao}}
                            <td style="text-align: center;">
                                @if ($registro->dt_execucao == '' )
                                    <div class="btn-group">
                                        <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-warning btn-sm" title="Clique para indicar que essa OS foi executada HOJE!"><i class="icon-ok"></i></a>
                                        <ul class="dropdown-menu btn-danger pull-right">
                                            <li><a href="{{ URL::to('/os/executadahoje/'.$registro->id_os) }}">Clique para confirmar que essa essa OS foi executada hoje ({{date('d/m/Y')}})</a></li>
                                        </ul>
                                    </div>
                                @else
                                {{$registro->dt_execucao}}
                                @endif
                            </td>
                            <td style="text-align: center;">
                                {{$registro->nome_status != '' ? $registro->nome_status : '--'}}
                                @if($registro->id_status_os == 1)
                                    <br/><a href="{{ URL::to('/os/concluida/'.$registro->id_os) }}" class="btn btn-xs" title="Clique para indicar que essa OS está concluída">concluída</a>
                                @endif
                                @if($registro->id_status_os == 1 || $registro->id_status_os == 2 )
                                    <br/><a href="{{ URL::to('/os/liberada/'.$registro->id_os) }}" class="btn btn-xs" title="Clique para indicar que essa OS está liberada para faturamento">liberada p/ fat.</a>
                                @endif
                                @if(($registro->id_status_os == 1 || $registro->id_status_os == 2 || $registro->id_status_os == 4) && $financeiro)
                                    <br/><a href="{{ URL::to('/os/faturada/'.$registro->id_os) }}" class="btn btn-xs" title="Clique para indicar que essa OS está faturada">faturada</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
                @if(isset($registros))
                    {{$registros->links()}}
                @endif
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script>

function geraExcel(){
    $('#criterios').attr('target', '_blank');
    $('#criterios').attr('action', '/os/excel');
    $("#criterios" ).submit() ;
    $('#criterios').attr('target', '_self');
    $('#criterios').attr('action', '/os');
}
    
$( document ).ready(function(){
    VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajsonfiltro/', '(selecione a empresa)', false);
    VinculaCombos('id_empresa', 'id_centro_de_custo', '/centrodecusto/daempresajsonfiltro/', '(selecione a empresa)', false);
});
</script>
@stop
