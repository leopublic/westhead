@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-map-marker"></i>{{ $tela['titulo_tela'] }}</h1>
                        <h4>{{ $tela['subTitulo'] }}</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Estado civil</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if(count($errors->all())> 0)
                <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-danger">
                        <ul class="errors">
                          @foreach($errors->all() as $message)
                          <li>{{ $message }}</li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Criar/Alterar estado civil</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">

                                {{ Form::open(array('url' => 'estadocivil/update/'.$obj->id_estadocivil, "class"=>"form-horizontal")) }}
                              @include('html/campo-texto', array('id' => 'descricao', 'label' => 'Descrição', 'valor' => $obj->descricao,  'help' => '',  array()))
                              @include('html/campo-texto', array('id' => 'descricao_en', 'label' => 'Descrição (inglês)', 'valor' => $obj->descricao_en,  'help' => '',  array()))
                              @include('html/campo-texto', array('id' => 'cod_pf', 'label' => 'Código no formulário da PF', 'valor' => $obj->cod_pf,  'help' => 'Código desse valor no formulário de transformação em permanente da Polícia Federal',  array()))

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                           {{ HTML::link('estadocivil', 'Cancelar', array('class' => 'btn')) }}
                                        </div>
                                    </div>
                                 {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

             </div>
@stop