<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnexoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('anexo', function(Blueprint $table) {
			$table->increments('id_anexo');
			$table->integer('id_tipo_anexo')->unsigned()->nullable();
			$table->integer('id_candidato')->unsigned()->nullable();
			$table->string('nome_original', 500);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('anexo');
	}

}
