<?php

class AnexoController extends BaseController {

    public function postNovoos($id_os_chave) {
        $anexoRep = App::make('RepositorioAnexo');
        $anexoRep->criarParaOs($id_os_chave);
    }

    public function postNovocandidato($id_candidato) {
        $anexoRep = App::make('RepositorioAnexo');
        $anexoRep->criarParaCandidato($id_candidato);
    }

    public function getDaos($id_os) {
        $anexos = Os::find($id_os)->anexos;
        $tipoanexo = array('' => '(não informado)') + TipoAnexo::orderBy('nome_pt_br')->lists('nome_pt_br', 'id_tipo_anexo');

        return View::make('anexos')
                        ->with('anexos', $anexos)
                        ->with('tipoanexo', $tipoanexo)
        ;
    }

    public function getDocandidato($id_candidato) {
        $anexos = Candidato::find($id_candidato)->anexos;
        $tipoanexo =  array('' => '(não informado)') + TipoAnexo::orderBy('nome_pt_br')->lists('nome_pt_br', 'id_tipo_anexo');

        return View::make('anexos')
                        ->with('anexos', $anexos)
                        ->with('tipoanexo', $tipoanexo)
        ;
    }

    public function getRemover($id_anexo) {
        try{
            $anexoRep = App::make('RepositorioAnexo');
            $anexoRep->excluir($id_anexo);
        } catch (Exception $ex) {

        }
        return Redirect::back()->with('flash_msg', 'Anexo excluído com sucesso');
    }

    public function postAtualizartipo($id_anexo) {
        $anexo = Anexo::find($id_anexo);
        $anexo->id_tipo_anexo = Input::get('id_tipo_anexo');
        $anexo->save();
    }

    public function getDownload($id_anexo) {
        $anexo = Anexo::find($id_anexo);
        $headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => $anexo->extensao->mime,
            'Content-Disposition' => 'attachment; filename="' . $anexo->nome_original . '"',
            'Content-Transfer-Encoding' => 'binary',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public',
            'Content-Length' => $anexo->tamanho()
        );

        return Response::download($anexo->caminho(), $anexo->nome_original, $headers);
    }

    public function getVerifica(){
        $i = 0;
        $ret = '';
        $anexos = \Anexo::chunk(200, function($anexos) use(&$i, &$ret){
            foreach($anexos as $anexo){
                if (!file_exists($anexo->caminho())){
                    $i++;
                    if ($anexo->id_candidato > 0){
                        $link = 'http://westhead.m2software.com.br/candidato/anexos/'.$anexo->id_candidato;
                    } elseif($anexo->id_os > 0) {
                        $link = 'http://westhead.m2software.com.br/os/anexos/'.$anexo->id_os;
                    }
                    $ret .= '<tr><td>'.$i.'</td><td>Arquivo '.$anexo->id_anexo. '</td><td>'. $anexo->created_at. '</td><td>'. $anexo->nome_original. '</td><td><a href="'.$link.'" target="_blank">Link</a></td</tr>';
                }
            }
        });
        return '<table>'.$ret.'</table>';
    }

}
