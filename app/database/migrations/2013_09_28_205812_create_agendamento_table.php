<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgendamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agendamento', function(Blueprint $table) {
			$table->increments('id_agendamento');
			$table->integer('id_os')->unsigned()->nullable();
			$table->date('dt_agendamento')->nullable();
			$table->boolean('id_status_agendamento')->unsigned()->nullable();
			$table->text('observacao')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agendamento');
	}

}
