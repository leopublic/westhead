<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableHistoricoStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('historico_andamento', function(Blueprint $table) {
            $table->increments('id_historico_andamento');
            $table->integer('id_processo')->unsigned();
            $table->datetime('datahora');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_status_andamento')->unsigned();
            $table->text('obs_andamento')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('historico_andamento');
	}

}