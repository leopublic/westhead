<?php
class ProfissaoController extends \CadastroController {

	public function RegistrosIndex()
	{
		$data = Profissao::orderBy('descricao')->get();
		return $data;
	}

	public function ColunasIndex()
	{
		$data = array(
				array("titulo" => "Descrição", "campo" => "descricao")
			, 	array("titulo" => "Descrição (en)", "campo" => "descricao_en")
			, 	array("titulo" => "CBO", "campo" => "cbo")
			);
		return $data;
	}
	
	public function ObtemInstancia($id = 0)
	{
		if($id > 0){
			$this->obj = Profissao::find($id);
		}
		else{
			$this->obj = new Profissao;
		}
	}
	
	public function TelaIndex()
	{
		$data = array();
		$data['titulo_tela'] = "Profissões";
		$data['titulo_grid'] = "Lista";
		$data['subTitulo'] = "Administrar Profissões";
		$data['urlEdit'] = 'profissao/show';
		$data['urlDel'] = 'profissao/del';
		$data['urlCreate'] = 'profissao/create';
		$data['nomeCampoChave'] = 'id_profissao';
        $data['menu'] = 'menuTabelas';
        $data['item'] = 'itemProfissao';
		return $data;
	}

	public function get_ViewShow()
	{
		return 'profissao/show';
	}

	public function MontaCampos()
	{
	//	$this->campos[] = new 
	}

	public function postUpdate($id = 0)
	{
		if($id > 0){
			$profissao = Profissao::find($id);
		}
		else{
			$profissao = new Profissao();
		}
		$profissao->fill(Input::all());
		if($profissao->save()){
			return Redirect::to('profissao')->with('flash_msg', 'Profissão atualizada com sucesso!');		
		}
		else{
			Input::flash();
			return Redirect::to('profissao/show/'.$id)->withErrors($profissao->errors);			
		}
	}

	public function getDel($id = 0)
	{
		try{
			Profissao::destroy($id);			
			return Redirect::to(self::raiz)
					->with('flash_msg', self::nomeSingular.' excluído com sucesso!');		
		}
		catch(Exception $e){
			return Redirect::to(self::raiz)
					->with('flash_error', $e->getMessage());		
		}
	}
}