<?php

class ArmadorController extends \CadastroController {

    public function RegistrosIndex() {
        $data = Armador::todos()->get();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(
            array("titulo" => "Razão social", "campo" => "razaosocial")
            , array("titulo" => "CNPJ", "campo" => "cnpj")
            , array("titulo" => "Ativa", "campo" => "fl_ativo")
        );
        return $data;
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = Armador::find($id);
        } else {
            $this->obj = new Armador();
        }
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = "Armadores";
        $data['titulo_grid'] = "Clique na ação ao lado do armador para alterar ou excluir";
        $data['subTitulo'] = "Administrar armadores ";
        $data['urlEdit'] = 'armador/show';
        $data['urlDel'] = 'armador/del';
        $data['urlCreate'] = 'armador/create';
        $data['menu'] = 'menuCadastros';
        $data['item'] = 'itemArmadores';
        $data['nomeCampoChave'] = 'id_empresa';
        return $data;
    }

    public function get_ViewShow() {
        return 'armador/show';
    }

    public function MontaCampos() {
        //	$this->campos[] = new 
    }

    public function postUpdate($id = 0) {
        if ($id > 0) {
            $armador = Armador::find($id);
        } else {
            $armador = new Armador();
        }
        $armador->razaosocial = Input::get('razaosocial');
        $armador->nomefantasia = Input::get('nomefantasia');
        $armador->insc_estadual = Input::get('insc_estadual');
        $armador->insc_municipal = Input::get('insc_municipal');
        $armador->cnpj = Input::get('cnpj');
        $armador->fl_ativo = Input::get('fl_ativo');
        // Validações
        $messages = array(
            'razaosocial.unique' => 'O campo razão social não pode assumir valores iguais em outra empresa ou armador.',
            'required' => 'O campo :attribute é obrigatório.',
            'regex' => 'Formato do campo :attribute inválido.'
        );
        $data = Input::all();
        $validator = Validator::make(
                        $data, array(
                    'razaosocial' => 'required|unique:empresa,razaosocial,' . $id . ',id_empresa',
                    'cnpj' => 'regex:/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/'
                        ), $messages);
        if ($validator->passes()) {
            $armador->save();
            return Redirect::to('armador')->with('flash_msg', 'Armador atualizado com sucesso!');
        } else {
            return Redirect::to('armador/show/' . $id)->withErrors($validator);
        }
    }

}
