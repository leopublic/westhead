<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableStatusAndamento extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('status_andamento', function(Blueprint $table) {
			$table->increments('id_status_andamento');
			$table->boolean('fl_encerra')->nullable();
			$table->integer('id_status_os')->unsigned()->nullable();
			$table->string('nome', 50);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('status_andamento');
	}

}