<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCotacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cotacao', function(Blueprint $table) {
			$table->increments('id_cotacao');
			$table->date('data');
            $table->decimal('venda', 10, 2)->unsigned()->nullable();
            $table->decimal('compra', 10, 2)->unsigned()->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cotacao');
	}

}
