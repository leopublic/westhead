<?php
class PaisController extends \CadastroController {
	const raiz = "pais";
	const campoChave = 'id_pais';
	const nomeSingular = 'País';
	const nomePlural = 'Países';
	const artigo = 'o';
	public function RegistrosIndex()
	{
		$data = Pais::orderBy('descricao')->get();
		return $data;
	}

	public function ColunasIndex()
	{
		$data = array(
				array("titulo" => "Nome", "campo" => "descricao"),
				array("titulo" => "Nacionalidade", "campo" => "nacionalidade"),
				array("titulo" => "Nome (inglês)", "campo" => "descricao_en"),
				array("titulo" => "Nacionalidade (inglês)", "campo" => "nacionalidade_en")
			);
		return $data;
	}
	
	public function ObtemInstancia($id = 0)
	{
		if($id > 0){
			$this->obj = Pais::find($id);
		}
		else{
			$this->obj = new Pais;
		}
	}
	
	public function TelaIndex()
	{
		$data = array();
		$data['titulo_tela'] = self::nomeSingular;
		$data['titulo_grid'] = "Lista";
		$data['subTitulo'] = "Administrar ".self::nomePlural;
		$data['urlEdit'] = self::raiz.'/show';
		$data['urlDel'] = self::raiz.'/del';
		$data['urlCreate'] = self::raiz.'/create';
		$data['nomeCampoChave'] = self::campoChave;
        $data['menu'] = 'menuTabelas';
        $data['item'] = 'itemPais';
		return $data;
	}

	public function get_ViewShow()
	{
		return self::raiz.'/show';
	}

	public function MontaCampos()
	{
	//	$this->campos[] = new 
	}

	public function getDel($id = 0)
	{
		try{
			Pais::destroy($id);			
			return Redirect::to(self::raiz)
					->with('flash_msg', self::nomeSingular.' excluíd'.self::artigo.' com sucesso!');		
		}
		catch(Exception $e){
			return Redirect::to(self::raiz)
					->with('flash_error', $e->getMessage());		
		}
	}

	public function postUpdate($id = 0)
	{
		if($id > 0){
			$obj = Pais::find($id);
		}
		else{
			$obj = new Pais();
		}
		$obj->fill(Input::all());

		if($obj->save()){
			return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular.' atualizad'.self::artigo.' com sucesso!');		
		}
		else{
			Input::flash();
			return Redirect::to(self::raiz.'/show/'.$id)->withErrors($obj->errors);			
		}
	}
}