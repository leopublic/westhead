<style>
    table tr th{text-align: left;}
    table tr td {vertical-align: top;}
    table.check{border-spacing: 5px; font-size:12px}
    p.check {border: solid 1px black; width:100%;}
    div.box{padding:10px; border:solid 1px black;}
    div.subtitulo{width:100%; text-align:left;color:white;font-size:16px; padding:5px;margin-top:15px; margin-bottom:5px;background-color: #ccc;}
</style>
<body>
    <div style="width:100%; text-align:center;font-size:30px;border-bottom:solid 2px #999;">CHECK LIST – Prorrogação RN 72/06</div>
    <div class="subtitulo">OS {{substr('000000'.$os->numero , -6)}}</div>
    <table>
        <tr><th>Data de elaboração</th><th>:</th><td>___________________</td></tr>
        <tr><th>Cliente</th><th>:</th><td>{{ $os->empresa->razaosocial}}</td></tr>
        <tr><th>Embarcação</th><th>:</th><td>{{ $os->projeto->descricao }}</td></tr>
        <tr><th>Centro de custo</th><th>:</th><td>{{ $os->centrodecusto->nome }}</td></tr>
    </table>

    <div class="subtitulo">Itens a serem verificados</div>
    <table class="check">
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Formulário de Prorrogação assinado pelo estrangeiro (1344)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Justificativa para Prorrogação / Descrição de Atividades Exercidas</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Declaração Geral de Responsabilidade</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Relação das Embarcações Afretadas atualizada</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia da Procuração personalíssima - (firma reconhecida)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia da Crew List contendo o nome do estrangeiro – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do protocolo RNE/CIE do estrangeiro – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia Completa do Passaporte (incluindo páginas em branco) – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do DOU constando deferimento do visto inicial</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>GRU Cód.140090 Original Paga (R$110,44)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>GRU Cód.140490 Original Paga (R$165,55) – multa somente para protocolo nos 30 dias antes do vencimento</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia da Procuração - (Autenticada e com firma reconhecida)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do Contrato Social ou Estatuto com carimbo Jucerja - (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do CNPJ</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do Contrato de Afretamento Inglês/Português (com Aditivo se necessário) - (Autenticada)</td></tr>
    </table>

    <div class="subtitulo">Candidato (s)</div>
    <div class="box">
    @foreach( $candidatos as $candidato)
        {{$candidato->nome_completo}}<br/>
    @endforeach
    </div>
    <div class="subtitulo">Processo elaborado por</div>
    <div class="box">{{$os->usuarioabertura->nome}}</div>

</body>
