<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoiceServicoTable extends Migration {

    public function up() {
        Schema::create('invoice_servico', function(Blueprint $table) {
            $table->increments('id_invoice_servico');
            $table->integer('id_invoice')->unsigned()->nullable();
            $table->integer('id_servico')->unsigned()->nullable();
            $table->decimal('val_unitario', 10, 2)->unsigned()->nullable();
            $table->decimal('val_total', 10, 2)->unsigned()->nullable();
            $table->decimal('val_despesas', 10, 2)->unsigned()->nullable();
            $table->text('descricao_despesas')->nullable();
            $table->text('descricao_invoice')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('invoice_servico');
    }

}
