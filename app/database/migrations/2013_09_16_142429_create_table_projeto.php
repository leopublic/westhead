<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableProjeto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('projeto', function( $table) {
            $table->increments('id_projeto');
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->integer('id_empresa_armador')->unsigned()->nullable();
			$table->string('descricao',500);
			$table->date('dt_inicio_operacao')->nullable();
			$table->date('dt_vencimento_contrato')->nullable();
			$table->text('justificativa')->nullable();
			$table->boolean('fl_ativa')->nullable();
			$table->integer('qtd_tripulantes')->nullable();
			$table->integer('qtd_estrangeiros')->nullable();
			$table->timestamps();
		});
		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projeto');
	}

}