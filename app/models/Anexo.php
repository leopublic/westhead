<?php
class Anexo extends Modelo {
	protected $table = 'anexo';
	protected $primaryKey = 'id_anexo';
	protected $guarded = array();


	public function assumeDefaults()
	{
	}

	public static function regras()
	{
		return array(
				);
	} 

	public static function mensagens()
	{
		return array(
			);
	}

	public static function raiz(){
		return '../public/arquivos/anexos/';
	}

	public function os()
	{
		return $this->belongsTo('Os', 'id_os');
	}

	public function candidato()
	{
		return $this->belongsTo('Candidato', 'id_candidato');
	}

	public function extensao()
	{
		return $this->belongsTo('Extensao', 'id_extensao');
	}

	public function caminho()
	{
		return $this->raiz().$this->id_anexo;
	}

	public function exclua(){
		try{
			unlink($this->caminho());
		} catch(\Exception $e){
			
		}
	}

	public function tamanho()
	{
		filesize($this->caminho());
	}

	public function existe(){
		if(file_exists($this->caminho())){
			return true;
		} else {
			return false;
		}
	}
}
