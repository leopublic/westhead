<?php
        $presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>
        <ul class="pagination pagination-xs pagination-colory">
            <?php echo $presenter->render(); ?>
        </ul>
<?php endif; ?>