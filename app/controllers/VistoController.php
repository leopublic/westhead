<?php
class VistoController extends BaseController {

	protected $campos;
	protected $obj;

	public function __construct()
	{
	}

	public function getIndex()
	{
	}

	public function postIndex()
	{
		return Redirect::to('os')->withInput();
	}

	public function getCreate()
	{
		$obj = new Os;

		$empresas = Empresa::where('fl_armador','=',0)->lists('razaosocial', 'id_empresa');
		$armadores = Armador::where('fl_armador','=',1)->lists('razaosocial', 'id_empresa');
		$projetos = Projeto::all()->lists('descricao', 'id_projeto');
		$centros = Centrodecusto::all()->lists('nome', 'id_centro_de_custo');
		$servicos = Servico::all()->lists('descricao', 'id_servico');

		return View::make('os/create')
					->with('empresas', $empresas)
					->with('armadores', $armadores)
					->with('projetos', $projetos)
					->with('centros', $centros)
					->with('servicos', $servicos)
					->with('obj', $obj)
		;
	}

	public function postStore()
	{
		$campos = Input::all();
		if(isset($campos['id_os'])){
			$os = Os::find($campos['id_os']);
			$os->fill($campos);
			$msg = "Ordem de serviço atualizada com sucesso.";
		}
		else{
			$os = new Os($campos);
			$msg = "Ordem de serviço criada com sucesso.";
		}

		if($os->save()){
			return Redirect::to('/os')->with('flash_msg', $msg);
		}
		else{
			return Redirect::back()
					->withInput()
					->withErrors($os->errors)
					;
		}
	}

	public function getShow($id_visto)
	{
		$visto = Visto::find($id_visto);

		return View::make('visto/show')
					->with('visto', $visto)
					;

	}

	public function getAdicionarcandidato($id_os, $id_candidato)
	{
		$os = Os::find($id_os);
		$os->candidatos()->attach($id_candidato);

		$visto = Visto::where('id_candidato','=', $id_candidato)
					->where('processo_atual', '=', 1)
					->first();
		if(!isset($visto)){
			$visto = new Visto;
			$visto->processo_atual = 1;
			$visto->id_candidato = $id_candidato;
			$visto->save();
		}

		$servico = Os::find($id_os)->servico();
		if($servico->id_tipo_servico == 2){
			$processo = ProcessoAutorizacao::where('id_os', '=', $id_os)
						->where('id_candidato', '=', $id_candidato)
						->first()
						;
			if(!isset($processo)){
				$processo = new ProcessoAutorizacao;
				$processo->id_os = $id_os;
				$processo->id_candidato = $id_candidato;
				$processo->id_visto = $visto->id_visto;
				$processo->id_servico = $servico->id_servico;
				$processo->save();
			}
		}

		return Redirect::to('/os/show/'.$id_os)->with('flash_msg', 'Candidato adicionado com sucesso!');
	}

	public function getRemovercandidato($id_os, $id_candidato)
	{
		$os = Os::find($id_os);
		$os->candidatos()->detach($id_candidato);
		return Redirect::to('/os/show/'.$id_os)->with('flash_msg', 'Candidato removido com sucesso!');
	}

}