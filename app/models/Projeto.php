<?php

/**
 * Projetos
 * @property int $id_projeto
 * @property int $id_empresa
 * @property int $id_empresa_armador
 * @property string $descricao
 * @property date $inicio_operacao
 * @property date $vencimento_contrato
 * @property text $justificativa
 * @property boolean $fl_ativa
 * @property int $qtd_tripulantes
 * @property int $qtd_estrangeiros
 * @property int $id_pais
 *
 */
class Projeto extends Modelo {

    protected $table = 'projeto';
    protected $primaryKey = 'id_projeto';

    public function scopeTodos($query) {
        return Projeto::with('Empresa')->with('Armador');
    }

    public function empresa() {
        return $this->belongsTo('Empresa', 'id_empresa');
    }

    public function armador() {
        return $this->belongsTo('Empresa', 'id_empresa_armador');
    }

    public function pais(){
        return $this->belongsTo('Pais', 'id_pais');
    }

    public function setDtInicioOperacaoAttribute($valor) {
        if ($valor != '') {
            $this->attributes['dt_inicio_operacao'] = $this->dtViewParaModel($valor);
        }
    }

    public function setDtVencimentoContratoAttribute($valor) {
        if ($valor != '') {
            $this->attributes['dt_vencimento_contrato'] = $this->dtViewParaModel($valor);
        }
    }

    public function getDtInicioOperacaoAttribute() {
        if (isset($this->attributes['dt_inicio_operacao'])) {
            return $this->dtModelParaView($this->attributes['dt_inicio_operacao']);
        } else {
            return null;
        }
    }

    public function getDtVencimentoContratoAttribute() {
        if (isset($this->attributes['dt_vencimento_contrato'])) {
            return $this->dtModelParaView($this->attributes['dt_vencimento_contrato']);
        } else {
            return null;
        }
    }
	public function oss(){
        return $this->hasMany('Os', 'id_projeto');
    }

    public function candidatos(){
        return $this->hasMany('Candidato', 'id_projeto');
    }

    public function scopeEmbarcacoes($query, $id_empresa = ''){
        $query = $query->where('fl_ativa', '=', 1)
                    ->where('fl_embarcacao', '=', 1);
        if($id_empresa != ''){
            $query = $query->where('id_empresa', '=', $id_empresa);
        }
        return $query;
    }

    public function scopeDaEmpresa($query, $id_empresa){
        return $query->where('id_empresa', '=', $id_empresa)->orderBy('descricao');
    }
    
    public function getDiasEmOperacaoAttribute($data){
        if ($this->attributes['dt_inicio_operacao'] != '' && $this->attributes['dt_inicio_operacao'] != '00/00/0000'){
            $inicio = DateTime::createFromFormat('Y-m-d', $this->attributes['dt_inicio_operacao']);
            $hoje = new DateTime(date('Y-m-d'));
            $dias = $hoje->diff($inicio)->days;
            return $dias;
        } else {
            return '-'.$this->attributes['dt_inicio_operacao'];
        }
    }
    
    public function getProporcionalidadeAttribute($data){
        if ($this->qtd_estrangeiros > 0 ){
            $prop = round(( ($this->qtd_tripulantes - $this->qtd_estrangeiros) / $this->qtd_estrangeiros) * 100, 1).'%';
            return $prop;
        } else {
            return '-';
        }
    }    
}
