<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCotacaoToInvoice extends Migration {

    public function up() {
        Schema::table('invoice', function(Blueprint $table) {
            $table->decimal('cotacao', 10, 2)->unsigned()->nullable();
        });
    }

    public function down() {
        Schema::table('invoice', function(Blueprint $table) {
            $table->dropColumn('cotacao');
        });
    }

}
