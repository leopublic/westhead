<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ProcessoAddStatusAndamento extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('processo', function(Blueprint $table) {
			$table->boolean('id_status_andamento')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasColumn('processo', 'id_status_andamento'))
		{
			Schema::table('processo', function(Blueprint $table) {
	            $table->dropColumn('id_status_andamento');
			});
		}
	}

}