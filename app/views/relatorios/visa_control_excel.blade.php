    <style>
         table { border: thin solid black; color:#162143;}
         .xltd{color:#162143;   color:#162143; vertical-align:middle; height: 80pt; text-align: center; font-family: Calibri;}
    </style>
    <table class="table table-hover fill-head">
        <thead>
            <tr heigth="97">
                <td colspan="24" style="font-size:42px; font-weight: bold; background:#0F243E; color:#162143;">WESTHEAD Visas & Solutions</td>
            </tr>
            <tr heigth="97" style="mso-height-source:userset;height:72.75pt; background:#808080;color:white; vertical-align: middle; font-family: Calibri; font-weight: bold;">
                <th style="border: thin solid #A6A6A6; font-size:14px;">#</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">ID</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Link </th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Company </th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Current Vessel</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Candidate</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Rank</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Visa's Deadline (Date/month/year)</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Visa Process / Visa Extension Process nr:</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Visa Type</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Brazilian Consulate</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Nationality</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Passport #</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Passport validity (Date/month/year)</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Visa Process Approval date (Date/month/year)</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Deadline for visa collection (Date/month/year)</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Visa Issued on (Date/month/year)</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Entry Brazil deadline (Date/month/year)</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Registration Date (Date/month/year)</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Validity of the RNE Protocol / Brazilian ID Card (CIE) (Date/month/year)</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Local that the candidate perform his registration:</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Last entry</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Days from last entry</th>
                <th style="border: thin solid #A6A6A6; font-size:14px;">Status</th>
            </tr>
        </thead>
        @if(isset($registros))
        <tbody>
            <? $i = 0; ?>
            @foreach($registros as $registro)
            <? $i++; ?>
            <tr>
                <td class="xltd"><? print $i; ?></td>
                <td class="xltd">{{ $registro->id_candidato }}</td>
                <td class="xltd"><a href="{{URL::to('/candidato/vistoatual/'.$registro->id_candidato) }}" >Ver no sistema</a></td>
                <td class="xltd">{{ $registro->razaosocial }}</td>
                <td class="xltd">{{ $registro->descricao_projeto }}</td>
                <td class="xltd">{{ $registro->nome_completo }}</td>
                <td class="xltd">{{ $registro->descricao_funcao }}</td>
                <td class="xltd">{{ $registro->prazo_solicitado }}</td>
                <td class="xltd">{{ $registro->numero_processo }}</td>
                <td class="xltd">{{ $registro->descricao_servico }}</td>
                <td class="xltd">{{ $registro->descricao_reparticao }}</td>
                <td class="xltd">{{ $registro->nacionalidade }}</td>
                <td class="xltd">{{ $registro->nu_passaporte }}</td>
                <td class="xltd">{{ $registro->dt_validade_passaporte }}</td>
                <td class="xltd">{{ $registro->data_deferimento }}</td>
                <td class="xltd">{{ $registro->data_deadline }}</td>
                <td class="xltd">{{ $registro->data_emissao }}</td>
                <td class="xltd">{{ $registro->data_deadline_entrada }}</td>
                <td class="xltd">{{ $registro->data_requerimento_registro }}</td>
                <td class="xltd">{{ $registro->data_validade_protocolo }}</td>
                <td class="xltd">{{ $registro->nome_delegacia }}</td>
                <td class="xltd">{{ $registro->data_ultima_entrada }}</td>
                <td class="xltd">{{ $registro->dias_desde_data_ultima_entrada }}</td>
                <td class="xltd">{{ $registro->observacao_cliente }}</td>
            </tr>
            @endforeach
        </tbody>
        @endif
    </table>
