@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="icon-male"></i>{{$obj->nome_completo}}</h1>
        @if ($obj->id_candidato_principal > 0)
        <h4>(
            @if ($obj->id_parentesco > 0)
            {{$obj->parentesco->descricao}} de
            @endif
            <a href="{{URL::to('/candidato/show/'.$obj->id_candidato_principal)}}"  target="_blank" title="Clique para ir para o perfil do responsável" style="text-decoration: underline;">{{$obj->candidatoprincipal->nome_completo}}</a>)</h4>
        @endif
        <h4>@if (is_object($obj->empresa))
            {{$obj->empresa->razaosocial}}
            @endif
        </h4>
        <h4>@if (is_object($obj->projeto))
            {{$obj->projeto->descricao}}
            @endif
        </h4>
    </div>
</div>
<!-- END Page Title -->
@include('html.mensagens')
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                @include('candidato.show_abas', array('obj'=> $obj, 'ativa'=> 'Anexos'))
                <h3>&nbsp;</h3>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12"  id="containerAnexos">
                        @include('anexos', array('anexos' => $obj->anexos) )
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-paper-clip"></i> Adicione novos anexos</h3>
                            </div>
                            <div class="box-content">
                                {{ Form::hidden('id_candidato', $obj->id_candidato, array('id' => 'id_candidato'))}}
                                <form action="/anexo/novocandidato/{{$obj->id_candidato}}" class=" dropzone hidden-xs dz-clickable" id="my-awesome-dropzone">
                                    <div class="dz-message" style="text-align:center;">
                                        Arraste os arquivos para cá ou clique para selecionar
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop


@section('scripts')
<script src="/assets/dropzone-3.8.3/downloads/dropzone.min.js"></script>
<script type="text/javascript">

function ExcluirAnexo(id)
{
    $.ajax({
        type: 'GET'
        , url: '/anexo/excluir/' + id
        , dataType: 'html'
        , success:
                function(data)
                {
                    AtualizarAnexos();
                }
    });
}

function AtualizaTipoAnexo(id, id_tipo_anexo)
{
    $.ajax({
        type: 'POST'
        , url: '/anexo/atualizartipo/' + id
        , data: {"id_tipo_anexo": $(id_tipo_anexo).val()}
        , success:
                function(data)
                {
                    AtualizarAnexos();
                }
    });
}

function AtualizarAnexos()
{
    $('#containerAnexos').html('<div style="text-align:center; padding:20px;"><i class="icon-spinner icon-spin icon-4x"></i></div>');

    $.ajax({
        type: 'GET'
        , url: '/anexo/docandidato/' + $('#id_candidato').val()
        , dataType: 'html'
        , success:
                function(data)
                {
                    $('#containerAnexos').html(data);
                }
    });
}

$(document).ready(function() {
    Dropzone.options.myAwesomeDropzone = {
        paramName: "file", // The name that will be used to transfer the file
        complete: function(file, done) {
            AtualizarAnexos();
        }
    }
})


</script>

@stop
