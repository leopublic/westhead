@extends('processo.comum')

<? $tipo_processo = 'tradução juramentada'; ?>
<? $nome_servico = 'Tradução juramentada'; ?>

@section('titulo')
    @if(is_object($processo->servico))
        {{$processo->servico->descricao}}
    @else
        Tradução juramentada: (?)
    @endif
@overwrite

@section('campos')
    <div class="row">
        @if (intval($processo->id_os) == 0)
        @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => $processo->id_servico, 'valores' => array('0' => '(não informado)') +  Servico::where('id_tipo_servico', '=', 1)->orWhere('id_tipo_servico', '=', 10)->lists('descricao', 'id_servico'),  'help' => '', array()))
        @endif
        @include('html/campo-data', array('id' => 'data_requerimento', 'label' => 'Data requerimento', 'valor' => $processo->data_requerimento,  'help' => '',  array()))
    </div>
@overwrite
