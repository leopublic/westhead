<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableParentesco extends Migration {

    public function up() {
        Schema::create('parentesco', function(Blueprint $table) {
            $table->increments('id_parentesco');
            $table->string('descricao', 100)->nullable();
            $table->string('descricao_en', 100)->nullable();
        });

        DB::table('parentesco')->truncate();

        $obj = array("descricao" => "MÃE", "descricao_en" => "MOTHER");
        DB::table('parentesco')->insert($obj);
        $obj = array("descricao" => "PAI", "descricao_en" => "FATHER");
        DB::table('parentesco')->insert($obj);
        $obj = array("descricao" => "FILHO", "descricao_en" => "SON");
        DB::table('parentesco')->insert($obj);
        $obj = array("descricao" => "FILHA", "descricao_en" => "DAUGHTER");
        DB::table('parentesco')->insert($obj);
        $obj = array("descricao" => "ESPOSA", "descricao_en" => "WIFE");
        DB::table('parentesco')->insert($obj);
        $obj = array("descricao" => "MARIDO", "descricao_en" => "HUSBAND");
        DB::table('parentesco')->insert($obj);
        $obj = array("descricao" => "TIA", "descricao_en" => "AUNTIE");
        DB::table('parentesco')->insert($obj);
    }

    public function down() {
        Schema::drop('parentesco');
    }

}
