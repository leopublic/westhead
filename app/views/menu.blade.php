<!-- BEGIN Navlist -->
<ul class="nav nav-list">
    <!-- BEGIN Search Form -->
    <!--                    <li>
                            <form target="#" method="GET" class="search-form">
                                <span class="search-pan">
                                    <button type="submit">
                                        <i class="icon-search"></i>
                                    </button>
                                    <input type="text" name="search" placeholder="Search ..." autocomplete="off" />
                                </span>
                            </form>
                        </li>-->
    <!-- END Search Form -->
    <li class="active"><a href="{{ URL::to('/') }}"><i class="glyphicon glyphicon-home"></i><span>Home</span></a></li>
    <li id="itemEstrangeiros"><a href="{{ URL::to('candidato') }}"><i class="icon-group"></i><span>Estrangeiros</span></a></li>
    <li id="itemHomonimos"><a href="{{ URL::to('candidato/homonimos') }}"><i class="icon-group"></i><span>Homônimos!!!</span></a></li>
    <li id="itemOrdensDeServico"><a href="{{ URL::to('/os')}}"><i class="icon-folder-open"></i><span>Ordens de serviço</span></a></li>
    <li id="menuAcompanhamento"><a href="#" class="dropdown-toggle"><i class="icon-eye-open"></i><span>Acompanhamento</span> <b class="arrow icon-angle-right"></b></a>
        <ul class="submenu">
            <li id="itemProcessosNovos"><a href="{{ URL::to('processo/semnumero') }}"> Processos novos</a></li>
            <li id="itemPrecadastro"><a href="{{ URL::to('processo/precadastro') }}"> Pré-cadastro prorr.</a></li>
            <li id="itemRegistros"><a href="{{ URL::to('processo3/emaberto') }}"> Registros</a></li>
        </ul>
    </li>
    <li id="menuAndamento"><a href="#" class="dropdown-toggle"><i class="icon-random"></i><span>Acompanhamento</span> <b class="arrow icon-angle-right"></b></a>
        <ul class="submenu">
            <li id="itemAutorizacoes"><a href="{{ URL::to('andamento') }}"> Autorizações</a></li>
        </ul>
    </li>
    <li id="menuProtocoloBsb"><a href="#" class="dropdown-toggle"><i class="icon-inbox"></i><span>Protocolo BSB</span> <b class="arrow icon-angle-right"></b></a>
        <ul class="submenu">
            <li id="itemNaoEnviados"><a href="{{ URL::to('protocolobsb/pendentes') }}"> Não enviados</a></li>
            <li id="itemEnviados"><a href="{{ URL::to('protocolobsb/enviados') }}"> Enviados</a></li>
        </ul>
    </li>

    <li id="menuCadastros"><a href="#" class="dropdown-toggle"><i class="icon-tags"></i><span>Cadastros</span> <b class="arrow icon-angle-right"></b></a>
        <ul class="submenu">
            <li id="itemArmadores"><a href="{{ URL::to('armador') }}"><i class="icon-shield"></i> Armadores</a></li>
            <li id="itemCentros"><a href="{{ URL::to('centrodecusto') }}"><i class="icon-sitemap"></i> Centros de custo</a></li>
            <li id="itemEmpresas"><a href="{{ URL::to('empresa') }}"><i class="icon-building"></i> Empresas</a></li>
            <li id="itemProcuradores"><a href="{{ URL::to('procurador') }}"><i class="icon-male"></i> Procuradores</a></li>
            <li id="itemProjetos"><a href="{{ URL::to('projeto') }}"><i class="icon-map-marker"></i> Emb./Projetos</a></li>
            <li id="itemServicos"><a href="{{ URL::to('servico') }}"><i class="icon-gift"></i> Serviços</a></li>
        </ul>
    </li>

    <li id="menuTabelas"><a href="#" class="dropdown-toggle"><i class="icon-gears"></i><span>Tabelas</span> <b class="arrow icon-angle-right"></b></a>
        <ul class="submenu">
            <li id="itemEscolaridade"><a href="{{ URL::to('escolaridade') }}">Escolaridade</a></li>
            <li id="itemEstadoCivil"><a href="{{ URL::to('estadocivil') }}">Estado civil</a></li>
            <li id="itemFuncao"><a href="{{ URL::to('funcao') }}">Funções</a></li>
            <li id="itemPais"><a href="{{ URL::to('pais') }}">País</a></li>
            <li id="itemProfissao"><a href="{{ URL::to('profissao') }}">Profissão</a></li>
            <li id="itemReparticao"><a href="{{ URL::to('reparticao') }}">Repartição consular</a></li>
            <li id="itemStatusAndamento"><a href="{{ URL::to('statusandamento') }}">Status andamento</a></li>
            <li id="itemTipoAnexo"><a href="{{ URL::to('tipoanexo') }}">Tipos de Anexo</a></li>
            <li id="itemTipoAutorizacao"><a href="{{ URL::to('tipoautorizacao') }}">Tipos de Autorização</a></li>
            <li id="itemTipoDespesa"><a href="{{ URL::to('tipodespesa') }}">Tipos de Despesa</a></li>
            <li id="itemTipoServico"><a href="{{ URL::to('tiposervico') }}">Tipos de Serviço</a></li>
        </ul>
    </li>

    <li>
        <a href="#" class="dropdown-toggle">
            <i class="icon-key"></i><span>Controle de acesso</span>
            <b class="arrow icon-angle-right"></b>
        </a>

        <ul class="submenu">
            @if (Auth::user()->temAcessoA(\Tela::tlPERFIS, 'CONS'))
            <li><a href="{{ URL::to('perfil') }}">Perfis</a></li>
            @endif
            @if (Auth::user()->temAcessoA(\Tela::tlUSUARIOS, 'CONS'))
            <li><a href="{{ URL::to('usuario') }}">Usuários</a></li>
            @endif
        </ul>
    </li>

    <li>
        <a href="#" class="dropdown-toggle">
            <i class="icon-usd"></i><span>Financeiro</span>
            <b class="arrow icon-angle-right"></b>
        </a>

        <!-- BEGIN Submenu -->
        <ul class="submenu">
            @if (Auth::user()->temAcessoA(\Tela::tlINVOICE, 'CONS'))
            <li><a href="{{ URL::to('invoice') }}">Invoices</a></li>
            @endif
            @if (Auth::user()->temAcessoA(\Tela::tlCOTACOES, 'CONS'))
            <li><a href="{{ URL::to('cotacao') }}">Cotações US$</a></li>
            @endif
        </ul>
        <!-- END Submenu -->
    </li>

    <li>
        <a href="#" class="dropdown-toggle">
            <i class="icon-print"></i>
            <span>Relatórios</span>
            <b class="arrow icon-angle-right"></b>
        </a>

        <!-- BEGIN Submenu -->
        <ul class="submenu">
            <li><a href="{{URL::to('/projeto/proporcionalidade')}}">Proporcionalidade</a></li>
            <li><a href="{{URL::to('/candidato/visacontrol')}}">Visa Control</a></li>
        </ul>
        <!-- END Submenu -->
    </li>

</ul>
<!-- END Navlist -->
