<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class MigrationBase extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('extensao', function(Blueprint $table) {
			$table->increments('id_extensao');
			$table->string('extensao');
			$table->string('mime', 500);
			$table->timestamps();
		});
	}

	public function dropColumnIfExists($nomeTabela, $nomeColuna)
	{
		if(Schema::hasColumn($nomeTabela, $nomeColuna)){
			Schema::table($nomeTabela, function(Blueprint $table) {
				$table->dropColumn($nomeColuna);
			});
		}
	}

	public function dropForeignKeyIfExists($nomeTabela, $nomeColuna)
	{
		$nomeForeign = $nomeTabela."_".$nomeColuna."_foreign";
		if(Schema::hasColumn($nomeTabela, $nomeColuna)){
			Schema::table($nomeTabela, function(Blueprint $table) {
				$table->dropForeign($nomeForeign);
			});
		}
	}
}
