<?php
class TipoAnexo extends Modelo {
	protected $table = 'tipo_anexo';
	protected $primaryKey = 'id_tipo_anexo';

	protected $guarded = array();

	public function assumeDefaults()
	{
	}

	public static function regras()
	{
		return array(
				'nome_pt_br' => 'required'
				,'nome_en_us' => 'required'
				);
	} 

	public static function mensagens()
	{
		return array(
				'nome_pt_br.required' => 'A descrição é obrigatória!'
				,'nome_en_us.required' => 'A descrição em inglês é obrigatória!'
			);
	}
}
