<div class="row" name="processo{{$processo->id_processo}}" id="processo{{$processo->id_processo}}">
    <div class="col-md-12">
        <div class="box" style="border:solid 1px #CCC;">
            <div class="box-title">
                @if (!isset($exemplo))
                    @include('processo.toolbar', array('processo'=>$processo))
                @endif
                <h3>
                    @if ($processo->eh_principal)
                        <i class="icon-star orange" ></i>
                    @elseif ($processo->fl_mais_recente)
                        <i class="icon-star"></i>
                    @else
                        <i class="icon-edit"></i>
                    @endif
                @if (!isset($exemplo))
                    @include('processo.titulo', array('processo' => $processo))
                @else
                    {{$processo->titulo}}
                @endif
                </h3>
            </div>
            <div class="box-content">
                @include('html.msg-processo', array('id'=> $processo->id_processo))
                {{ Form::open(array('url' => 'processo'.$processo->id_tipoprocesso.'/update/'.$processo->id_processo, "class"=>"form-horizontal")) }}
                {{ Form::hidden('id_tipoprocesso', $processo->id_tipoprocesso) }}
                
                @yield('campos')

                @if($processo->id_servico > 0)
                    {{Form::hidden('tem_andamento', 1)}}
                    @if($processo->servico->fl_andamento == '1')
                        <div class="row">
                            @include('html/campo-select', array('id' => 'id_status_andamento', 'label' => 'Andamento', 'valor' => $processo->id_status_andamento, 'valores' => array('0' => '(não informado)') +  StatusAndamento::orderBy('nome')->lists('nome', 'id_status_andamento'),  'help' => '', array()))
                            @include('html/campo-texto', array('id' => 'obs_andamento', 'label' => 'Observação andamento', 'valor' => $processo->obs_andamento,  'help' => ''))
                        </div>
                    @endif
                @endif
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        @include('html/campo-textarea', array('qtdCol' => 1, 'id' => 'observacao', 'label' => 'Observações', 'valor' => $processo->observacao,  'help' => '', array()))
                    </div>
                </div>
                @if (!isset($exemplo))
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4 col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                {{ Form::close() }}
                @if (!isset($exemplo))
                    @if($processo->id_servico > 0)
                        @if($processo->servico->fl_andamento == '1')
                            @include('/processo.historico_andamento')
                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>
