<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdCandidatoToAnexo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('anexo', function(Blueprint $table) {
			$table->foreign('id_candidato')->references('id_candidato')->on('candidato');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('anexo', function(Blueprint $table) {
			$table->dropForeign('anexo_id_candidato_foreign');
		});
	}

}
