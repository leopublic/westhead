<?php

class ProcessoCancelamento extends Processo {

    protected $guarded = array('id_status_andamento', 'obs_andamento', 'tem_andamento');


    public function assumeDefaults() {
        $this->id_tipoprocesso = 8;
    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

    public function getNumeroProcessoAttribute() {
        return $this->attributes['string_1'];
    }

    public function setNumeroProcessoAttribute($valor) {
        $this->attributes['string_1'] = $valor;
    }

    public function getDataRequerimentoAttribute($valor) {
        return $this->dtModelParaView($this->attributes['date_1']);
    }

    public function setDataRequerimentoAttribute($valor) {
        $this->attributes['date_1'] = $this->dtViewParaModel($valor);
    }

    public function getNumeroOficioAttribute($valor) {
        return $this->attributes['string_2'];
    }

    public function setNumeroOficioAttribute($valor) {
        $this->attributes['string_2'] = $valor;
    }

    public function getDataDeferimentoAttribute($valor) {
        return $this->dtModelParaView($this->attributes['date_2']);
    }

    public function setDataDeferimentoAttribute($valor) {
        $this->attributes['date_2'] = $this->dtViewParaModel($valor);
    }
    
    public function getDtEnvioBsbFmtAttribute($valor){
        return $this->dtModelParaView($this->attributes['dt_envio_bsb']);
    }
    public function setDtEnvioBsbFmtAttribute($valor){
        if ($valor != '' && $valor != '00/00/0000'){
            $this->attributes['dt_envio_bsb'] = $this->dtViewParaModel($valor);
        } else {
            $this->attributes['dt_envio_bsb'] = null;
        }
    }

    public function getTituloAttribute(){
        return 'Cancelamento';
    }
}
