<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminAtualizaDelegaciasRegistroCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:delegacia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza delegacias dos processos de registro';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $sql = "update processo 
                    set integer_1 = (select id_delegacia from delegacia where codigo = substring(processo.string_1, 1, 5))
                    where id_tipoprocesso = 3";
        $qtd = \DB::update(\DB::raw($sql));
        $this->info(number_format($qtd, 0, ",", ".")." processos atualizados com sucesso!");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }
}
