<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCobrancaToCentrodecustos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('centro_de_custo', function(Blueprint $table) {
			$table->text('end_cobr')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('centro_de_custo', function(Blueprint $table) {
            $table->dropColumn('end_cobr');
		});
	}

}
