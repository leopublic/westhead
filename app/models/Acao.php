<?php
/**
 * @property string $id_acao Id da acao
 * @property string $id_tela Id do tela
 * @property string $descricao Nome do acao
 * @property datetime $created_at Data em que o tela foi cadastrado
 * @property datetime $updated_at Data em que foi atualizado
 */
class Acao extends Modelo {
    protected $table = 'acao';
    protected $primaryKey = 'id_acao';
    protected $guarded = array();
    
    public static function acoes(){
        return array(
             array('codigoTela'=>'INVO', 'descricao' =>'Consulta'  , 'codigo'=>'CONS')
            ,array('codigoTela'=>'INVO', 'descricao' =>'Criação'  , 'codigo'=>'INS')
            ,array('codigoTela'=>'INVO', 'descricao' =>'Alteração'  , 'codigo'=>'ALT')
            ,array('codigoTela'=>'INVO', 'descricao' =>'Exclusão'  , 'codigo'=>'EXCL')
            ,array('codigoTela'=>'INVO', 'descricao' =>'Excel'  , 'codigo'=>'EXCE')
            
            ,array('codigoTela'=>'COTA', 'descricao' =>'Consulta'  , 'codigo'=>'CONS')
            ,array('codigoTela'=>'COTA', 'descricao' =>'Criação'  , 'codigo'=>'INS')
            ,array('codigoTela'=>'COTA', 'descricao' =>'Alteração'  , 'codigo'=>'ALT')
            ,array('codigoTela'=>'COTA', 'descricao' =>'Exclusão'  , 'codigo'=>'EXCL')

            ,array('codigoTela'=>'PERF', 'descricao' =>'Consulta'  , 'codigo'=>'CONS')
            ,array('codigoTela'=>'PERF', 'descricao' =>'Criação'  , 'codigo'=>'INS')
            ,array('codigoTela'=>'PERF', 'descricao' =>'Alteração'  , 'codigo'=>'ALT')
            ,array('codigoTela'=>'PERF', 'descricao' =>'Exclusão'  , 'codigo'=>'EXCL')

            ,array('codigoTela'=>'USUA', 'descricao' =>'Consulta'  , 'codigo'=>'CONS')
            ,array('codigoTela'=>'USUA', 'descricao' =>'Criação'  , 'codigo'=>'INS')
            ,array('codigoTela'=>'USUA', 'descricao' =>'Alteração'  , 'codigo'=>'ALT')
            ,array('codigoTela'=>'USUA', 'descricao' =>'Exclusão'  , 'codigo'=>'EXCL')
            ,array('codigoTela'=>'USUA', 'descricao' =>'Nova senha'  , 'codigo'=>'SENH')        

            ,array('codigoTela'=>'ORDS', 'descricao' =>'Consulta'  , 'codigo'=>'CONS')
            ,array('codigoTela'=>'ORDS', 'descricao' =>'Criação'  , 'codigo'=>'INS')
            ,array('codigoTela'=>'ORDS', 'descricao' =>'Alteração'  , 'codigo'=>'ALT')
            ,array('codigoTela'=>'ORDS', 'descricao' =>'Exclusão'  , 'codigo'=>'EXCL')
            ,array('codigoTela'=>'ORDS', 'descricao' =>'Exportar lista para excel'  , 'codigo'=>'EXCE')

            ,array('codigoTela'=>'CAND', 'descricao' =>'Consulta'  , 'codigo'=>'CONS')
            ,array('codigoTela'=>'CAND', 'descricao' =>'Criação'  , 'codigo'=>'INS')
            ,array('codigoTela'=>'CAND', 'descricao' =>'Criação de homônimo'  , 'codigo'=>'HOMO')
            ,array('codigoTela'=>'CAND', 'descricao' =>'Alteração'  , 'codigo'=>'ALT')
            ,array('codigoTela'=>'CAND', 'descricao' =>'Exclusão'  , 'codigo'=>'EXCL')

            ,array('codigoTela'=>'TABS', 'descricao' =>'Criação'  , 'codigo'=>'INS')
            ,array('codigoTela'=>'TABS', 'descricao' =>'Alteração'  , 'codigo'=>'ALT')
            ,array('codigoTela'=>'TABS', 'descricao' =>'Exclusão'  , 'codigo'=>'EXCL')
        );
        
    }
    
    public static function carregaAcoes(){
        $cadastradas = self::acoesCadastradas();
        $acoes = self::acoes();
        foreach ($acoes as $acao){
            if (!str_contains($cadastradas, '#'.$acao['codigoTela'].'_'.$acao['codigo'])){
                $tela = \Tela::where('codigo', '=', $acao['codigoTela'])->first();
                $nova = \Acao::where('id_tela', '=', $tela->id_tela)->where('codigo', '=', $acao['codigo'])->first();
                if (!is_object($nova )){
                    $nova = new \Acao;
                    $nova->id_tela = $tela->id_tela;
                    $nova->codigo = $acao['codigo'];
                    $nova->descricao = $acao['descricao'];
                    $nova->save();
                    print "Ação '".$acao['codigoTela']."_".$acao['codigo']."' =>'".$acao['descricao']."' adicionada \n";
                }
            }
        }
    }
    
    public static function acoesCadastradas(){
        $sql = "select concat(tela.codigo,'_', acao.codigo ) codigo
                from acesso, acao, tela
                where acao.id_acao = acesso.id_acao
                and tela.id_tela = acao.id_tela";
        $acoes = \DB::select(\DB::raw($sql));
        $ret = '';
        foreach($acoes as $acao){
            $ret .= '#'.$acao->codigo.'#';
        }
        return $ret;
    }
    
    public static function recuperaPeloCodigo($codigoTela, $codigo){
        $tela = \Tela::recuperaPeloCodigo($codigoTela);
        if (is_object($tela)){
            $acao = \Acao::where('id_tela', '=', $tela->id_tela)->where('codigo', '=', $codigo)->first();
            if (is_object($acao)){
                return $acao;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
