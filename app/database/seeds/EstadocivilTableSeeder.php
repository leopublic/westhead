<?php

class EstadocivilTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
        DB::table('estadocivil')->truncate();

        $obj = array("descricao" => "Solteiro", "descricao_en" => "Single");
        DB::table('estadocivil')->insert($obj);
        $obj = array("descricao" => "Casado", "descricao_en" => "Married");
        DB::table('estadocivil')->insert($obj);
        $obj = array("descricao" => "Divorciado", "descricao_en" => "Divorced");
        DB::table('estadocivil')->insert($obj);
    }

}