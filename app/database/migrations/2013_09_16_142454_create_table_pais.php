<?php

use Illuminate\Database\Migrations\Migration;

class CreateTablePais extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pais', function( $table) {
            $table->increments('id_pais');
			$table->string('descricao',500);
			$table->string('descricao_en', 500)->nullable();
			$table->string('nacionalidade', 500)->nullable();
			$table->string('nacionalidade_en', 500)->nullable();
			$table->string('codigo_pf', 10)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pais');
	}

}