<?php
class Procurador extends Eloquent {
	protected $table = 'procurador';
	protected $primaryKey = 'id_procurador';


	protected $guarded = array();

	public function assumeDefaults()
	{
	}

	public static function regras()
	{
		return array(
				'nome' => 'required',
				'titulo' => 'required',
				'cpf' => 'required'
				);
	}

	public static function mensagens()
	{
		return array(
				'nome.required' => 'O nome é obrigatório!',
				'titulo.required' => 'O título é obrigatório!',
				'cpf.required' => 'O cpf é obrigatório!'
			);
	}

}
