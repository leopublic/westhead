<?php

use Westhead\Repositorios;

class RepositorioAuthTest extends TestCase {

    public function testPodeTentarAcesso_usuarioBloqueado() {
        $agora = date('Y-m-d H:i:s');
        $usuario = new \Usuario;
        $usuario->username = 'Teste';
        $usuario->password = Hash::make('teste');
        $usuario->dthr_primeiro_erro = $agora;
        $usuario->qtd_tentativas_erradas = 6;
        $usuario->save();

        $rep = new \Westhead\Repositorios\RepositorioAuth;

        $this->assertFalse($rep->podeTentarAcesso('Teste', '192.168.0.1'));

        $usuario->delete();
    }

    public function testPodeTentarAcesso_usuarioLiberado() {
        $agora = date('Y-m-d H:i:s');
        $usuario = new \Usuario;
        $usuario->username = 'Teste';
        $usuario->password = Hash::make('teste');
        $usuario->dthr_primeiro_erro = $agora;
        $usuario->qtd_tentativas_erradas = 5;
        $usuario->save();

        $rep = new \Westhead\Repositorios\RepositorioAuth;

        $this->assertTrue($rep->podeTentarAcesso('Teste', '192.168.0.1'));

        $usuario->delete();
    }

    public function testPodeTentarAcesso_usuarioZerado() {
        $agora = date('Y-m-d H:i:s');
        $usuario = new \Usuario;
        $usuario->username = 'Teste';
        $usuario->password = Hash::make('teste');
        $usuario->dthr_primeiro_erro = $agora;
        $usuario->save();

        $rep = new \Westhead\Repositorios\RepositorioAuth;

        $this->assertTrue($rep->podeTentarAcesso('Teste', '192.168.0.1'));

        $usuario->delete();
    }

    public function testPodeTentarAcesso_usuarioInexistenteIpBloqueado() {
        \TentativaAcesso::where('ip', '=', '192.168.0.1')->delete();

        $tent = new \TentativaAcesso;
        $tent->ip = '192.168.0.1';
        $tent->qtd = 11;
        $tent->save();

        $rep = new \Westhead\Repositorios\RepositorioAuth;

        $this->assertFalse($rep->podeTentarAcesso('sd sdlk sdlfj sdlkfjsdlfj', '192.168.0.1'));

        $tent->delete();
    }

    public function testPodeTentarAcesso_usuarioInexistenteIpLiberado() {
        \TentativaAcesso::where('ip', '=', '192.168.0.1')->delete();
 
        $tent = new \TentativaAcesso;
        $tent->ip = '192.168.0.1';
        $tent->qtd = 10;
        $tent->save();

        $rep = new \Westhead\Repositorios\RepositorioAuth;

        $this->assertTrue($rep->podeTentarAcesso('sd sdlk sdlfj sdlkfjsdlfj', '192.168.0.1'));

        $tent->delete();
    }

    public function testRegistraTentativaComSucesso() {
        \TentativaAcesso::where('ip', '=', '192.168.0.1')->delete();
        \Usuario::where('username', '=', 'Teste')->delete();

        $rep = new \Westhead\Repositorios\RepositorioAuth;
        $tent = new \TentativaAcesso;
        $tent->ip = '192.168.0.1';
        $tent->qtd = 10;
        $tent->save();
        $agora = date('Y-m-d H:i:s');
        $usuario = new \Usuario;
        $usuario->username = 'Teste';
        $usuario->password = Hash::make('teste');
        $usuario->dthr_primeiro_erro = $agora;
        $usuario->qtd_tentativas_erradas = 5;
        $usuario->save();

        $rep->registraTentativaComSucesso($usuario, '192.168.0.1');

        $usu = \Usuario::find($usuario->id_usuario);
        $tent2 = \TentativaAcesso::find($tent->id_tentativa);

        $this->assertEmpty($usu->qtd_tentativas_erradas);
        $this->assertEquals(0, $tent2->qtd);
    }

    public function testRegistraTentativaSemSucesso() {
        \TentativaAcesso::where('ip', '=', '192.168.0.1')->delete();
        \Usuario::where('username', '=', 'Teste')->delete();

        $rep = new \Westhead\Repositorios\RepositorioAuth;
        $tent = new \TentativaAcesso;
        $tent->ip = '192.168.0.1';
        $tent->qtd = 20;
        $tent->save();
        $agora = date('Y-m-d H:i:s');
        $usuario = new \Usuario;
        $usuario->username = 'Teste';
        $usuario->password = Hash::make('teste');
        $usuario->dthr_primeiro_erro = $agora;
        $usuario->qtd_tentativas_erradas = 5;
        $usuario->save();

        $rep->registraTentativaSemSucesso('Teste', '192.168.0.1');

        $usu = \Usuario::find($usuario->id_usuario);
        $tent2 = \TentativaAcesso::find($tent->id_tentativa);

        $this->assertEquals(6, $usu->qtd_tentativas_erradas);
        $this->assertEquals(11, $tent2->qtd);
    }

    public function testRegistraTentativaSemSucesso_semTentativa() {
        \TentativaAcesso::where('ip', '=', '192.168.0.1')->delete();
        \Usuario::where('username', '=', 'Teste')->delete();

        $rep = new \Westhead\Repositorios\RepositorioAuth;
        $agora = date('Y-m-d H:i:s');
        $usuario = new \Usuario;
        $usuario->username = 'Teste';
        $usuario->password = Hash::make('teste');
        $usuario->dthr_primeiro_erro = $agora;
        $usuario->qtd_tentativas_erradas = 5;
        $usuario->save();

        $rep->registraTentativaSemSucesso('Teste', '192.168.0.1');

        $usu = \Usuario::find($usuario->id_usuario);
        $tent2 = \TentativaAcesso::where('ip', '=', '192.168.0.1')->first();

        $this->assertEquals(6, $usu->qtd_tentativas_erradas);
        $this->assertEquals(1, $tent2->qtd);
    }

    public function tearDown() {
        \Usuario::where('username', '=', 'Teste')->delete();
        \TentativaAcesso::where('ip', '=', '192.168.0.1')->delete();
    }

}
