<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ServicoAddFlAndamento extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('servico', function(Blueprint $table) {
			$table->boolean('fl_andamento')->nullable();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasColumn('servico', 'fl_andamento'))
		{
			Schema::table('servico', function(Blueprint $table) {
	            $table->dropColumn('fl_andamento');
			});
		}
	}
}