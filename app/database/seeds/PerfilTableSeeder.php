<?php

class PerfilTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('perfil')->truncate();

		$perfil = array("nome" => "administrador");
        DB::table('perfil')->insert($perfil);
        $perfil = array("nome" => "coordenador");
        DB::table('perfil')->insert($perfil);
        $perfil = array("nome" => "operacional");
        DB::table('perfil')->insert($perfil);
        $perfil = array("nome" => "financeiro");
        DB::table('perfil')->insert($perfil);

		// Uncomment the below to run the seeder
	}

}
