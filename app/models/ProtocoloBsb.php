<?php
/**
 * @property int $id_protocolo_bsb Chave
 * @property date $dt_protocolo Data do protocolo
 * @property text $observacao Observacoes 
 */
class ProtocoloBsb extends Modelo {

    protected $table = 'protocolo_bsb';
    protected $primaryKey = 'id_protocolo_bsb';
    protected $guarded = array();

    /**
     * Recupera o protocolo do dia informado ou cria um novo caso não exista
     * @param type $dt_protocolo Data no formato Y-m-d
     */
    public static function recuperaOuCria($dt_protocolo){
        $protocolo = \ProtocoloBsb::where('dt_protocolo', '=', $dt_protocolo)->first();
        if (!is_object($protocolo)){
            $protocolo = new \ProtocoloBsb;
            $protocolo->dt_protocolo = $dt_protocolo;
            $protocolo->save();
        }
        return $protocolo;
    }

}
