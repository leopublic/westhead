<?php
class ValidadorCandidato implements InterfaceValidadorModelo{
	public function novoOk($data)
	{
		return Validator::make($data, $this->regrasNovo(), $this->mensagensNovo());
	}

	public function editaOk($data)
	{
		return Validator::make($data, $this->regrasEdita(), $this->mensagensEdita());
	}

	public function regrasNovo()
	{
		return $this->regrasPadrao();
	}

	public function mensagensNovo()
	{
		return $this->mensagensPadrao();
	}

	public function regrasEdita()
	{
		return $this->regrasPadrao();
	}

	public function mensagensEdita()
	{
		return $this->mensagensPadrao();
	}

	public function regrasPadrao()
	{
		return array(
				'nome_completo' => 'required',
				'nome_mae' => 'required',
				'dt_nascimento' => 'required',
				);
	}

	public function mensagensPadrao()
	{
		return array(
				'nome_completo.required' => 'O nome completo é obrigatório!',
				'nome_mae.required' => 'O nome da mãe é obrigatório!',
				'dt_nascimento.required' => 'A data de nascimento é obrigatória!'
			);
	}
}
