<?php
/**
 * @property int $id_status_andamento
 * @property tinyint $fl_encerra
 * @property int $id_status_os
 * @property varchar $nome
 * @property timestamp $created_at
 * @property timestamp $updated_at
 */

class StatusAndamento extends Modelo{
	protected $table = 'status_andamento';
	protected $primaryKey = 'id_status_andamento';
    protected $guarded = array();


} 