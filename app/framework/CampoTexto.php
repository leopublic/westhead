<?php
class CampoTexto extends Campo{
	/**
	 * 
	 * @param type $nome
	 * @param type $atributos
	 * @param type $valor Passar o oldinput se for o caso
	 */
	public function __construct($label, $nome, $valor = null, $dica = null, $atributos = array())
	{
		$this->label = $label;
		$this->nome = $nome;
		$this->atributos = $atributos;
		$this->valor;
	}
	/**
	 * Renderiza o campo usando o formbuilder
	 * @param array $patributos
	 * @return type
	 */
	public function html(array $patributos)
	{
		//Unifica os atributos com os atributos passados
		$this->atributos = array_merge($this->atributos , $patributos);
		return Form::text($this->nome, $this->valor, $this->atributos);
	}
}