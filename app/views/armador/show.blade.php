@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>{{ $tela['titulo_tela'] }}</h1>
                        <h4>{{ $tela['subTitulo'] }}</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Layout</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if(count($errors->all())> 0)
                <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-danger">
                        <ul class="errors">
                          @foreach($errors->all() as $message)
                          <li>{{ $message }}</li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-shield"></i> Alterar armador</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">

                                {{ Form::open(array('url' => 'armador/update/'.$obj->id_empresa, "class"=>"form-horizontal")) }}
                                    <div class="form-group">
                                      {{Form::label('razaosocial', 'Razão social', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::text('razaosocial', Input::old('razao_social', $obj->razaosocial), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Razão social</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('nomefantasia', 'Nome fantasia', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::text('nomefantasia', Input::old('nomefantasia', $obj->nomefantasia), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Nome de fantasia</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('cnpj', 'CNPJ', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                          {{ Form::text('cnpj', Input::old('cnpj', $obj->cnpj), $attributes = array('class' => 'form-control', 'data-mask' => '99.999.999/9999-99')); }}
                                          <span class="help-inline">CNPJ</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('insc_estadual', 'Inscrição estadual', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-3 col-lg-10 controls">
                                          {{ Form::text('insc_estadual', Input::old('insc_estadual', $obj->insc_estadual), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Inscrição estadual</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('insc_municipal', 'Inscrição municipal', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-md-3 col-lg-10 controls">
                                          {{ Form::text('insc_municipal', Input::old('insc_municipal', $obj->insc_municipal), $attributes = array('class' => 'form-control')); }}
                                          <span class="help-inline">Inscrição municipal</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                           {{ HTML::link('empresa', 'Cancelar', array('class' => 'btn')) }}
                                        </div>
                                    </div>
                                 {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

             </div>
@stop
@section('scripts')
<script>
$( document ).ready(function(){
    $('#menuCadastros').addClass('active');
    $('#itemArmadores').addClass('active');
});
</script>
@stop
