<?php
class Funcao extends Modelo {
	protected $table = 'funcao';
	protected $primaryKey = 'id_funcao';

	protected $guarded = array();

	public function assumeDefaults()
	{
		if($this->descricao_en == ''){
			$this->descricao_en = $this->descricao;
		}
	}

	public static function regras()
	{
		return array(
				'descricao' => 'required',
				'codigo_pf' => 'required'
				);
	} 

	public static function mensagens()
	{
		return array(
				'descricao.required' => 'O nome é obrigatório!',
				'codigo_pf.required' => 'O código da PF é obrigatório!'
			);
	}
}
