@if (isset($id))
    @if (Session::has($id.'_success'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                {{ Session::get($id."_success") }}
            </div>
        </div>
    </div>
    @endif
    @if (Session::has($id.'_error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ Session::get($id.'_error') }}
            </div>
        </div>
    </div>
    @endif
@endif