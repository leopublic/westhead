<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('tipo_servico')){
			Schema::create('tipo_servico', function(Blueprint $table) {
				$table->increments('id_tipo_servico');
				$table->string('nome');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('tipo_servico')){
			Schema::drop('tipo_servico');
		}
	}
}
