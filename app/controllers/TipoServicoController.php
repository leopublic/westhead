<?php
class TipoServicoController extends \CadastroController {
	const raiz = "tiposervico";
	const campoChave = 'id_tipo_servico';
	const nomeSingular = 'Tipo de serviço';
	const nomePlural = 'Tipos de serviço';

	public function RegistrosIndex()
	{
		$data = TipoServico::all();
		return $data;
	}

	public function ColunasIndex()
	{
		$data = array(
				array("titulo" => "Descrição", "campo" => "nome")
			);
		return $data;
	}
	
	public function ObtemInstancia($id = 0)
	{
		if($id > 0){
			$this->obj = TipoServico::find($id);
		}
		else{
			$this->obj = new TipoServico;
		}
	}
	
	public function TelaIndex()
	{
		$data = array();
		$data['titulo_tela'] = self::nomeSingular;
		$data['titulo_grid'] = "Lista";
		$data['subTitulo'] = "Administrar ".self::nomePlural;
		$data['urlEdit'] = self::raiz.'/show';
		$data['urlDel'] = self::raiz.'/del';
		$data['urlCreate'] = self::raiz.'/create';
		$data['nomeCampoChave'] = self::campoChave;
        $data['menu'] = 'menuTabelas';
        $data['item'] = 'itemTipoServico';
		return $data;
	}

	public function get_ViewShow()
	{
		return self::raiz.'/show';
	}

	public function MontaCampos()
	{
	//	$this->campos[] = new 
	}

	public function getDel($id = 0)
	{
		try{
			TipoServico::destroy($id);			
			return Redirect::to(self::raiz)
					->with('flash_msg', self::nomeSingular.' excluído com sucesso!');		
		}
		catch(Exception $e){
			return Redirect::to(self::raiz)
					->with('flash_error', $e->getMessage());		
		}
	}

	public function postUpdate($id = 0)
	{
		if($id > 0){
			$obj = TipoServico::find($id);
		}
		else{
			$obj = new TipoServico();
		}
		$obj->fill(Input::all());

		if($obj->save()){
			return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular.' atualizado com sucesso!');		
		}
		else{
			Input::flash();
			return Redirect::to(self::raiz.'/show/'.$id)->withErrors($obj->errors);			
		}
	}
}