<?php

class ProcessoProrrogacao extends Processo {

    protected $guarded = array('id_status_andamento', 'obs_andamento', 'tem_andamento');

    public function assumeDefaults() {
        $this->id_tipoprocesso = 5;
    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

    public function getNumeroProcessoAttribute($valor) {
        return $this->attributes['string_1'];
    }

    public function setNumeroProcessoAttribute($valor) {
        $this->attributes['string_1'] = $valor;
    }

    public function getDataRequerimentoAttribute($valor) {
        if(isset($this->attributes['date_1']) && $this->attributes['date_1'] != ''){
            return $this->dtModelParaView($this->attributes['date_1']);
        } else {
            return null;
        }
    }

    public function setDataRequerimentoAttribute($valor) {
        if($valor != '' && $valor != '0000-00-00'){
            $this->attributes['date_1'] = $this->dtViewParaModel($valor);
        } else {
            $this->attributes['date_1'] = null;
        }
    }

    public function getPrazoPretendidoAttribute($valor) {
        return $this->attributes['string_2'];
    }

    public function setPrazoPretendidoAttribute($valor) {
        $this->attributes['string_2'] = $valor;
    }

    public function getDataPublicacaoDouAttribute($valor) {
        if(isset($this->attributes['date_2']) && $this->attributes['date_2'] != ''){
            return $this->dtModelParaView($this->attributes['date_2']);
        } else {
            return null;
        }
    }

    public function setDataPublicacaoDouAttribute($valor) {
        if($valor != '' && $valor != '0000-00-00' && $valor != '00/00/0000'){
            $this->attributes['date_2'] = $this->dtViewParaModel($valor);
        } else {
            $this->attributes['date_2'] = null;
        }
    }

    public function getPaginaDouAttribute($valor) {
        return $this->attributes['string_3'];
    }

    public function setPaginaDouAttribute($valor) {
        $this->attributes['string_3'] = $valor;
    }

    public function getNumeroPrecadastroAttribute($valor) {
        return $this->attributes['string_4'];
    }

    public function setNumeroPrecadastroAttribute($valor) {
        $this->attributes['string_4'] = $valor;
    }

    public function getDataPreCadastroAttribute($valor) {
        if(isset($this->attributes['date_3']) && $this->attributes['date_3'] != ''){
            return $this->dtModelParaView($this->attributes['date_3']);
        } else {
            return null;
        }
    }

    public function setDataPreCadastroAttribute($valor) {
        if($valor != '' && $valor != '0000-00-00' && $valor != '00/00/0000'){
            $this->attributes['date_3'] = $this->dtViewParaModel($valor);
        } else {
            $this->attributes['date_3'] = null;
        }
    }

    public function recupereDadosDaOs() {
        parent::recupereDadosDaOs();
        $this->prazo_pretendido = $this->os->prazo_solicitado;
    }
    
    public function getDtEnvioBsbFmtAttribute($valor){
        return $this->dtModelParaView($this->attributes['dt_envio_bsb']);
    }
    public function setDtEnvioBsbFmtAttribute($valor){
        if ($valor != '' && $valor != '00/00/0000'){
            $this->attributes['dt_envio_bsb'] = $this->dtViewParaModel($valor);
        } else {
            $this->attributes['dt_envio_bsb'] = null;
        }
    }

    public function getTituloAttribute(){
        return 'Prorrogação';
    }

}
