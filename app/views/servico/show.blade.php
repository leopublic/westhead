@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-map-marker"></i>{{ $tela['titulo_tela'] }}</h1>
                        <h4>{{ $tela['subTitulo'] }}</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Serviço</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if(count($errors->all())> 0)
                <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-danger">
                        <ul class="errors">
                          @foreach($errors->all() as $message)
                          <li>{{ $message }}</li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Criar/Alterar serviços</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                            {{ Form::open(array('url' => 'servico/update/'.$obj->id_servico, "class"=>"form-horizontal")) }}
                              @include('html/campo-select', array('id' => 'id_tipo_servico', 'label' => 'Tipo de serviço', 'valor' => $obj->id_tipo_servico,  'help' => '', 'valores' => array('' =>'(selecione)') + TipoServico::orderBy('nome')->lists('nome', 'id_tipo_servico') , array()))
                              @include('html/campo-select', array('id' => 'id_tipo_autorizacao', 'label' => 'Tipo de autorização', 'valor' => $obj->id_tipo_autorizacao,  'help' => '', 'valores' => (array('' =>'(selecione)') + TipoAutorizacao::orderBy('descricao')->lists('descricao', 'id_tipo_autorizacao')) , array()))
                              @include('html/campo-select', array('id' => 'id_classificacao_visto', 'label' => 'Classificação do visto', 'valor' => $obj->id_classificacao_visto,  'help' => '', 'valores' => (array('' =>'(n/a)') + ClassificacaoVisto::orderBy('classificacao')->lists('classificacao', 'id_classificacao_visto')) , array()))
                              @include('html/campo-texto', array('id' => 'descricao', 'label' => 'Descrição', 'valor' => $obj->descricao,  'help' => '',  array()))
                              @include('html/campo-texto', array('id' => 'descricao_en', 'label' => 'Descrição (inglês)', 'valor' => $obj->descricao_en,  'help' => '',  array()))
                              @include('html/campo-textarea', array('id' => 'descricao_invoice', 'label' => 'Descrição invoice', 'valor' => $obj->descricao_invoice,  'help' => 'Informe como a descrição deve constar na invoice. Use os códigos "%projeto%", "%candidatos%" e "%cotacao%" para indicar onde o sistema deve inserir o nome do projeto, os nomes dos candidatos e a cotação utilizada. Ex: "Brazilian Working Visa (RN72) for %projeto%. Crewmember(s): %candidatos% (Dolar rate R$ %cotacao%)."',  array()))
                              @include('html/campo-checkbox', array('id' => 'disponivel', 'label' => 'Ativo?', 'valor' => $obj->disponivel,  'help' => '',  array()))
                              @include('html/campo-checkbox', array('id' => 'multi_candidatos', 'label' => 'Multi-candidatos?', 'valor' => $obj->multi_candidatos,  'help' => '',  array()))
                              @include('html/campo-checkbox', array('id' => 'fl_bsb', 'label' => 'Vai para BSB?', 'valor' => $obj->fl_bsb,  'help' => '',  array()))
                              @include('html/campo-checkbox', array('id' => 'fl_abre_servico', 'label' => 'Cria novo visto?', 'valor' => $obj->fl_abre_servico,  'help' => '',  array()))
                              @include('html/campo-checkbox', array('id' => 'fl_andamento', 'label' => 'Aparece no andamento?', 'valor' => $obj->fl_andamento,  'help' => '',  array()))
                              @include('html/campo-textarea', array('id' => 'observacao', 'label' => 'Observação', 'valor' => $obj->observacao,  'help' => '', array()))

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                           {{ HTML::link('servico', 'Cancelar', array('class' => 'btn')) }}
                                        </div>
                                    </div>
                                 {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

             </div>
@stop