@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Ordem de serviço {{substr('000000'.$obj->numero, -6)}}</h1>
    @if (is_object($obj->empresa))
    <h4>{{$obj->empresa->razaosocial}}</h4>
    @endif
    @if (is_object($obj->projeto))
    <h6>{{$obj->projeto->descricao}}</h6>
    @endif
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>&nbsp;</h3>
                <ul class="nav nav-tabs pull-left">
                    <li class=""><a href="/os/show/{{$obj->id_os}}" ><i class="icon-tags"></i> Dados da OS</a></li>
                    <li class=""><a href="/os/candidatos/{{$obj->id_os}}" ><i class="icon-group"></i> Candidatos</a></li>
                    <li class=""><a href="/os/anexos/{{$obj->id_os}}" ><i class="icon-folder-open"></i> Arquivos</a></li>
                    <li class=""><a href="/os/despesas/{{$obj->id_os}}" ><i class="icon-dollar"></i> Despesas</a></li>
                    <li class="active"><a href="#" ><i class="icon-exchange"></i> Desmembrar</a></li>
                </ul>
                <div class="box-tool">
                    @include('os.menu_os', array('obj', $obj))
                </div>
            </div>

            <div class="box-content">
                <div class="row">
                    <!-- coluna candidatos -->
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-group"></i>Candidatos da OS</h3>
                            </div>
                            {{Form::open()}}
                            {{Form::hidden('id_os', $obj->id_os)}}
                            <div class="box-content">
                                @if(isset($candidatos))
                                <table class="table table-striped table-hover fill-head">
                                    <thead>
                                        <tr>
                                            <th style="width: 40px; text-align: center;">Sel</th>
                                            <th style="width: 80px; text-align: center;">Ação</th>
                                            <th style="width: auto">Nome</th>
                                            <th style="width: auto;">Nome mãe</th>
                                            <th style="width: 150px;">Data nasc.</th>
                                            <th style="width: 80px;">Passaporte</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? $i = 0; ?>
                                        @foreach($candidatos as $registro)
                                        <? $i++; ?>
                                        <tr>
                                            <td style="text-align: center;">
                                                {{Form::checkbox('id_candidato[]', $registro->id_candidato)}}
                                            </td>
                                            <td style="text-align: center;">
                                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/candidato/show/'.$registro->id_candidato) }}" target="_blank"><i class="icon-search"></i></a>
                                                <a class="btn btn-danger btn-sm" href="{{ URL::to('/os/removercandidato/'.$obj->id_os.'/'.$registro->id_candidato) }}"><i class="icon-trash"></i></a>
                                            </td>
                                            <td>{{ $registro->nome_completo }}
                                                @if ($registro->id_candidato_principal > 0)
                                                    <br/>(
                                                    @if ($registro->id_parentesco > 0)
                                                    {{$registro->parentesco->descricao}}&nbsp;de&nbsp;
                                                    @endif
                                                    <a href="{{URL::to('/candidato/show/'.$registro->id_candidato_principal)}}"  target="_blank" title="Clique para ir para o perfil do responsável" style="text-decoration: underline;">{{$registro->candidatoprincipal->nome_completo}}</a>)
                                                @else
                                                    @if (is_object($registro->dependentes) && count($registro->dependentesforadaos($obj->id_os)) > 0)
                                                    <div class="btn-group">
                                                        <a href="#" class="btn btn-xs">adicionar dependentes</a><a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-xs"><span class="caret"></span></a>
                                                        <ul class="dropdown-menu pull-left">
                                                            @foreach($registro->dependentesforadaos($obj->id_os) as $dep)
                                                            <li class="pull-left"><a class="" href="{{ URL::to('/os/adicionarcandidato/'.$obj->id_os.'/'.$dep->id_candidato) }}" title="Clique para incluir esse dependente na OS"><i class="icon-plus"></i>&nbsp;{{$dep->nome_completo}} ({{$dep->parentesco->descricao}})</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    @endif
                                                    @if (is_object($registro->dependentes) && count($registro->dependentesnaos($obj->id_os)) > 0)
                                                    <br/><ul class="iconic">
                                                        @foreach($registro->dependentesnaos($obj->id_os) as $dep)
                                                        <li>
                                                            <a class="btn btn-xs" href="{{ URL::to('/candidato/show/'.$dep->id_candidato) }}"" title="Clique para ver os detalhes desse dependente" target="_blank"><i class="icon-search"></i></a>
                                                            <a class="btn btn-xs" href="{{ URL::to('/os/formtrans/'.$obj->id_os.'/'.$dep->id_candidato) }}"" title="Clique para gerar o formulário de transformação para esse dependente" target="_blank"><i class="icon-print"></i></a>
                                                            <a class="btn btn-xs" href="{{ URL::to('/os/removercandidato/'.$obj->id_os.'/'.$dep->id_candidato) }}"" title="Clique para retirar esse dependente na OS"><i class="icon-trash"></i></a>
                                                            &nbsp;{{$dep->nome_completo}} ({{$dep->parentesco->descricao}})
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                    @endif
                                                @endif
                                            </td>
                                            <td>{{ $registro->nome_mae }}</td>
                                            <td>{{ $registro->dt_nascimento }}</td>
                                            <td>{{ $registro->nu_passaporte }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="col-12">
                                        <button type="submit" class="btn btn-primary center"><i class="icon-exchange"></i> Mover candidatos selecionados para uma nova OS</button>
                                </div>
                                @else
                                (nenhum candidato na OS)
                                @endif
                            </div>
                            {{Form::close()}}
                            <!-- /coluna candidatos -->
                        </div>
                        <!-- /row candidatos -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>




@stop

@section('scripts')
<script>
    function ChamaRota(form, rota, retorno)
    {
        var xdata = $(form).serialize();
        $(retorno).html('<div style="text-align:center"><i class="icon-spinner icon-spin icon-3x"></i> carregando...</div>');
        $.ajax({
            type: 'POST'
            , url: rota
            , data: xdata
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        $(retorno).html(data);
                    }
        });
    }


    $(document).ready(function() {
        $(window).keydown(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });

    $("#nome_completo").keyup(function(e) {
        if (e.keyCode == 13) {
            ChamaRota('#buscaCandidato', '/candidato/foradaos', '#candidatosEncontrados');
            return false;
        }
    });



</script>

@stop
