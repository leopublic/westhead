@if (isset($tela['urlEdit']))
    <a class="btn btn-primary btn-sm" href="{{ URL::to($tela['urlEdit'], array($registro->$tela['nomeCampoChave']) ) }}"><i class="icon-edit"></i></a>
@endif
@if (isset($tela['urlDel']))
    <a class="btn btn-danger btn-sm" href="{{ URL::to($tela['urlDel'], array($registro->$tela['nomeCampoChave']) ) }}"><i class="icon-trash"></i></a>
@endif
