<?php
namespace Westhead\Validadores;

class ValidadorInvoice extends Validador {
    public static $rules = array(
        'dataFmt'                       => 'required|datavalida:dd/mm/yyyy',
        'vencimentoFmt'                 => 'required|datavalida:dd/mm/yyyy',
        'cotacao'                       => 'numeric',
        'sequencial'                    => 'required|numeric|min:1',
    );
    public static $messages = array(
        'dataFmt.required'              => 'A data da invoice é obrigatória',
        'dataFmt.datavalida'            => 'Informe uma data da invoice válida (dd/mm/aaaa)',
        'vencimentoFmt.required'        => 'A data do vencimento é obrigatória',
        'vencimentoFmt.datavalida'      => 'Informe uma data de vencimento válida (dd/mm/aaaa)',
        'cotacao.numeric'               => 'Cotação inválida',
        'sequencial.required'           => 'O sequencial da invoice é obrigatório',
        'sequencial.numeric'            => 'Informe um sequencial válido (número maior que zero)',
        'sequencial.min'                => 'Informe um sequencial válido (número maior que zero)',
    );
}
