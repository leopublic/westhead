<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCampos104ToEmpresaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('empresa', function(Blueprint $table) {
            $table->string('atividade_economica', 100)->nullable();
            $table->text('objeto_social')->nullable();
            $table->string('valor_capital_inicial')->nullable();
            $table->string('valor_capital_atual')->nullable();
            $table->date('dt_constituicao')->nullable();
            $table->date('dt_cadastro_bc')->nullable();
            $table->date('dt_alteracao_contratual')->nullable();
            $table->string('nome_empresa_estrangeira', 1000)->nullable();
            $table->string('valor_investimento_estrangeiro', 100)->nullable();
            $table->date('dt_investimento_estrangeiro')->nullable();
            $table->integer('qtd_empregados')->nullable();
            $table->integer('qtd_empregados_estrangeiros')->nullable();
            $table->string('nome_administrador')->nullable();
            $table->string('cargo_administrador')->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('empresa', function(Blueprint $table) {
			if(Schema::hasColumn('empresa', 'atividade_economica')) { $table->dropColumn('atividade_economica');}
			if(Schema::hasColumn('empresa', 'objeto_social'))  { $table->dropColumn('objeto_social');}
			if(Schema::hasColumn('empresa', 'valor_capital_inicial'))  { $table->dropColumn('valor_capital_inicial');}
			if(Schema::hasColumn('empresa', 'valor_capital_atual'))  { $table->dropColumn('valor_capital_atual');}
			if(Schema::hasColumn('empresa', 'dt_constituicao'))  { $table->dropColumn('dt_constituicao');}
			if(Schema::hasColumn('empresa', 'dt_cadastro_bc'))  { $table->dropColumn('dt_cadastro_bc');}
			if(Schema::hasColumn('empresa', 'dt_alteracao_contratual'))  { $table->dropColumn('dt_alteracao_contratual');}
			if(Schema::hasColumn('empresa', 'nome_empresa_estrangeira'))  { $table->dropColumn('nome_empresa_estrangeira');}
			if(Schema::hasColumn('empresa', 'valor_investimento_estrangeiro'))  { $table->dropColumn('valor_investimento_estrangeiro');}
			if(Schema::hasColumn('empresa', 'dt_investimento_estrangeiro'))  { $table->dropColumn('dt_investimento_estrangeiro');}
			if(Schema::hasColumn('empresa', 'qtd_empregados'))  { $table->dropColumn('qtd_empregados');}
			if(Schema::hasColumn('empresa', 'qtd_empregados_estrangeiros'))  { $table->dropColumn('qtd_empregados_estrangeiros');}
			if(Schema::hasColumn('empresa', 'nome_administrador'))  { $table->dropColumn('nome_administrador');}
            if(Schema::hasColumn('empresa', 'cargo_administrador')) {  $table->dropColumn('cargo_administrador');}
		});
	}

}
