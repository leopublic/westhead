<?php
use Westhead\Repositorios;

class RepositorioVistoTest extends TestCase {

    
    public function testRecuperaVistoAtual(){
        $rep = new Repositorios\RepositorioVisto();
        $candidato = \Candidato::first();

        $visto = $rep->recuperaVistoAtual($candidato);
        $this->assertNotEmpty($visto->id_visto);

        $id = $visto->id_visto;
        $visto_x = \Visto::find($id);

        $this->assertEquals($id, $visto_x->id_visto);
        $this->assertEquals($candidato->id_candidato, $visto_x->id_candidato);
        $this->assertEquals(true, $visto_x->processo_atual);

    }

}