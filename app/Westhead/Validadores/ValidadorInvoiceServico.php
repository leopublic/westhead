<?php
namespace Westhead\Validadores;

class ValidadorInvoiceServico extends Validador {
    public static $rules = array(
        'id_os'                         => 'required|numeric|min:1',
        'dt_ocorrencia'                 => 'required|datavalida:dd/mm/yyyy',
        'id_tipo_despesa'               => 'required|numeric|min:1',
        'qtd'                           => 'required|numeric|max:99999999.99',
        'valor'                         => 'required|numeric|max:99999999.99',
    );
    public static $messages = array(
        'id_os.required'                => 'Não foi possível saber em qual ordem de serviço é para cadastrar a despesa',
        'id_os.min'                => 'Não foi possível saber em qual ordem de serviço é para cadastrar a despesa',
        'id_tipo_despesa.required'      => 'O tipo de despesa é obrigatório',
        'id_tipo_despesa.min'      => 'O tipo de despesa é obrigatório',
        'dt_ocorrencia.required'        => 'A data em que a despesa ocorreu é obrigatória',
        'dt_ocorrencia.datavalida'      => 'Informe uma data de ocorrência válida (dd/mm/aaaa)',
        'qtd.required'                  => 'A quantidade é obrigatória',
        'qtd.numeric'                   => 'Quantidade inválida',
        'qtd.max'                       => 'A quantidade máxima é 99.999.999,99',
        'valor.required'                => 'O valor é obrigatório',
        'valor.numeric'                 => 'Valor inválido',
        'valor.max'                     => 'O valor máximo é R$ 99.999.999,99',
    );
}
