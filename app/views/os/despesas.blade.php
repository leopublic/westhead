@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Ordem de serviço {{substr('000000'.$obj->numero, -6)}}</h1>
    @if (is_object($obj->empresa))
    <h4>{{$obj->empresa->razaosocial}}</h4>
    @endif
    @if (is_object($obj->projeto))
    <h6>{{$obj->projeto->descricao}}</h6>
    @endif
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>&nbsp;</h3>
                <ul class="nav nav-tabs pull-left">
                    <li class=""><a href="/os/show/{{$obj->id_os}}" ><i class="icon-tags"></i> Dados da OS</a></li>
                    <li class=""><a href="/os/candidatos/{{$obj->id_os}}" ><i class="icon-group"></i> Candidatos</a></li>
                    <li class=""><a href="/os/anexos/{{$obj->id_os}}" ><i class="icon-folder-open"></i> Arquivos</a></li>
                    <li class="active"><a href="#" ><i class="icon-dollar"></i> Despesas</a></li>
                    <li class=""><a href="/os/desmembrar/{{$obj->id_os}}" ><i class="icon-exchange"></i> Desmembrar</a></li>
                </ul>
                <div class="box-tool">
                    @include('os.menu_os', array('obj', $obj))
                </div>
            </div>

            <div class="box-content">

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-dollar"></i> 
                                    @if (intval($desp->id_despesa) == 0)
                                    Adicionar despesa
                                    @else
                                    Alterar despesa
                                    @endif
                                </h3>
                            </div>
                        {{ Form::open(array('url' => 'os/adicionedespesa', "class"=>"form-horizontal")) }}
                            <div class="box-content">
                                {{ Form::hidden('id_os', $obj->id_os, array('id' => 'id_os')) }}
                                {{ Form::hidden('id_despesa', $desp->id_despesa, array('id' => 'id_despesa')) }}
                                @include('html/campo-data', array('id' => 'dt_ocorrencia', 'label' => 'Data', 'valor' => Input::old('dt_ocorrencia', $desp->dt_ocorrencia),  'help' => '', 'valores', array()))
                                @include('html/campo-select', array('id' => 'id_tipo_despesa', 'label' => 'Tipo', 'valor' => Input::old('id_tipo_despesa', $desp->id_tipo_despesa), 'valores' => $tipodespesas , 'help' => '',  array()))
                                @include('html/campo-texto', array('id' => 'qtd', 'label' => 'Quantidade', 'valor' => Input::old('qtd', str_replace(".", ",",str_replace(",","",$desp->qtd))),  'help' => '', 'valores', array()))
                                @include('html/campo-texto', array('id' => 'valor', 'label' => 'Valor unitário', 'valor' => Input::old('valor', str_replace(".", ",",str_replace(",","",$desp->valor))),  'help' => '', 'valores', array()))
                                @include('html/campo-textarea', array('id' => 'observacao', 'label' => 'Observação', 'valor' => Input::old('observacao', $desp->observacao),  'help' => '', 'valores', array()))
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                        {{ HTML::link('os', 'Cancelar', array('class' => 'btn')) }}
                                    </div>
                                </div>
                            </div>
                        {{Form::close()}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- coluna candidatos -->
                    <div class="col-md-12" id="containerGrid">
                        @include('/os/griddespesas', array('despesas' => $despesas) )
                    </div>
                    <!-- /coluna candidatos -->
                </div>

            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script>
    function ChamaRota(form, rota, retorno)
    {
        var xdata = $(form).serialize();
        $(retorno).html('<div style="text-align:center"><i class="icon-spinner icon-spin icon-3x"></i> carregando...</div>');
        $.ajax({
            type: 'POST'
            , url: rota
            , data: xdata
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        $(retorno).html(data);
                    }
        });
    }

    function ExcluirAnexo(id)
    {
        $.ajax({
            type: 'GET'
            , url: '/anexo/excluir/' + id
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        AtualizarGrid();
                    }
        });

    }

    function AtualizarGrid()
    {
        $('#containerGrid').html('<div style="text-align:center; padding:20px;"><i class="icon-spinner icon-spin icon-4x"></i></div>');

        $.ajax({
            type: 'GET'
            , url: '/os/griddespesas/' + $('#id_os').val()
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        $('#containerGrid').html(data);
                    }
        });

    }

    $(document).ready(function() {
        $(window).keydown(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });

</script>

@stop
