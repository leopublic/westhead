@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="icon-file-alt"></i>Home</h1>
        <h4>Sistema de Gestão de Estrangeiros Westhead</h4>
    </div>
</div>
<!-- END Page Title -->
@include('html.mensagens')
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-2">
        <a class="tile tile-lime" href="{{ URL::to('os/create') }}">
            <div class="img">
                <i class="icon-plus"></i>
            </div>
            <div class="content">
                <p class="title">Abrir OS</p>
            </div>
        </a>
    </div>
    <div class="col-md-2">
        <a class="tile tile-orange" href="{{ URL::to('os') }}">
            <div class="img">
                <i class="icon-folder-open"></i>
            </div>
            <div class="content">
                <p class="title">Listar OS</p>
            </div>
        </a>
    </div>
    <div class="col-md-3">
        <a class="tile tile-light-blue" href="{{ URL::to('candidato') }}">
            <div class="img">
                <i class="icon-group"></i>
            </div>
            <div class="content">
                <p class="title">Estrangeiros</p>
            </div>
        </a>
    </div>
    <div class="col-md-2">
        <a class="tile tile-magenta" href="{{ URL::to('empresa') }}">
            <div class="img">
                <i class="icon-building"></i>
            </div>
            <div class="content">
                <p class="title">Empresas</p>
            </div>
        </a>
    </div>
    <div class="col-md-2">
        <a class="tile tile-pink" href="{{ URL::to('projeto') }}">
            <div class="img">
                <i class="icon-map-marker"></i>
            </div>
            <div class="content">
                <p class="title">Projetos</p>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Ordens de serviço recentes</h3>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 200px">Ação</th>
                            <th style="width: 80px">Núm.</th>
                            <th style="width: auto;">Empresa</th>
                            <th style="width: auto;">Projeto/Emb.</th>
                            <th style="width: auto;">Armador</th>
                            <th style="width: 200px">Candidatos</th>
                            <th style="width: auto;">Serviço</th>
                            <th style="width: auto;">Entrada</th>
                            <th style="width: auto;">Execução</th>
                            <th style="width: auto;">Status</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $registro)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/show/'.$registro->id_os) }}"><i class="icon-tags"></i></a>
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/candidatos/'.$registro->id_os) }}"><i class="icon-group"></i></a>
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/anexos/'.$registro->id_os) }}"><i class="icon-folder-open"></i></a>
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/despesas/'.$registro->id_os) }}"><i class="icon-dollar"></i></a>
                                @include('html.link_exclusao', array('url' => '/os/cancelar/'.$registro->id_os, 'msg_confirmacao' => 'Clique para confirmar a exclusão da Ordem de Serviço'))
                            </td>
                            <td>{{ substr('000000'.$registro->numero, -6) }}</td>
                            <td>{{ $registro['razaosocial'] }}</td>
                            <td>{{ $registro->descricao_projeto }}</td>
                            <td>{{ $registro->razaosocial_armador }}</td>
                            <td>
                                <? $br = '';?>
                                @foreach($registro->candidatos as $candidato)
                                {{ $br.$candidato->nome_completo }}
                                <? $br = '<br/>';?>
                                @endforeach
                            </td>
                            <td>
                               {{ $registro->descricao_servico  }}
                            </td>
                            <td>{{$registro->dt_solicitacao}}
                            <td>{{$registro->dt_execucao}}</td>
                            <td>{{is_object($registro->statusos) ? $registro->statusos->nome : '--'}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>


@stop