@extends('base')

@section('content')
    <!-- BEGIN Page Title -->
    <div class="page-title">
        <div>
            <h1><i class="icon-male"></i>{{$obj->nome_completo}}</h1>
            @if ($obj->id_candidato_principal > 0)
            <h4>(
                @if ($obj->id_parentesco > 0)
                {{$obj->parentesco->descricao}} de
                @endif
                <a href="{{URL::to('/candidato/cadastro/'.$obj->id_candidato_principal)}}"  target="_blank" title="Clique para ir para o perfil do responsável" style="text-decoration: underline;">{{$obj->candidatoprincipal->nome_completo}}</a>)
            </h4>
            @endif
            <h4>@if (is_object($obj->empresa))
                {{$obj->empresa->razaosocial}}
                @endif
            </h4>
            <h4>@if (is_object($obj->projeto))
                {{$obj->projeto->descricao}}
                @endif
            </h4>
        </div>
    </div>
    
    @include('html.mensagens')
    <!-- BEGIN Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    @include('candidato.show_abas', array('obj'=> $obj, 'ativa'=> 'Cadastro'))
                    <h3>&nbsp;</h3>
                </div>
                <div class="box-content">
                    {{ Form::open(array("class"=>"form-horizontal", "url"=>'/candidato/cadastro')) }}
                    {{ Form::hidden('id_candidato', $obj->id_candidato, array('id' => 'id_candidato'))}}
                        <h3>Empresa e projeto/embarcação</h3>
                        <div class="row">
                        @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => $obj->id_empresa, 'valores' => array('0' => '(n/a)') + Empresa::where('fl_armador', '=', 0)->orderBy('razaosocial')->lists('razaosocial', 'id_empresa') , 'help' => '', 'valores', array()))
                        @if ($obj->id_empresa > 0)
                            @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Emb./Proj.', 'valor' => $obj->id_projeto, 'valores' => array('' => '(n/a)') + Projeto::where('id_empresa', '=', $obj->id_empresa)->orderBy('descricao')->lists('descricao', 'id_projeto') , 'help' => '', 'valores', array()))
                        @else
                            @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Emb./Proj.', 'valor' => $obj->id_projeto, 'valores' => array('' => '(selecione a empresa)') , 'help' => '', 'valores', array()))
                        @endif
                            @include('html/campo-data', array('id' => 'data_ultima_entrada', 'label' => 'Data da última entrada', 'valor' => $obj->data_ultima_entrada,  'help' => '',  array()))
                            @include('html/campo-texto', array('id' => 'dias_desde_ultima_entrada', 'label' => 'Dias desde última entrada', 'valor' => $obj->dias_desde_ultima_entrada,  'help' => '',  'atributos' => array('disabled') ))
                        </div>
                        <h3>Dados pessoais</h3>
                        <div class="row">
                            @include('html/campo-texto', array('qtdCol' => $qtdCol, 'id' => 'nome_completo', 'label' => 'Nome completo', 'valor' => $obj->nome_completo,  'help' => '',  array()))
                            @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_pais_nacionalidade', 'label' => 'Nacionalidade', 'valor' => $obj->id_pais_nacionalidade,  'help' => '', 'valores' => array(''=>'(n/d)') + Pais::orderBy('descricao')->lists('descricao', 'id_pais') , array()))
                            @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'nome_mae', 'label' => 'Nome da mãe', 'valor' => $obj->nome_mae,  'help' => '',  array()))
                            @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_pais_nacionalidade_mae', 'label' => 'Nacionalidade mãe', 'valor' => $obj->id_pais_nacionalidade_mae,  'help' => '', 'valores' => array(''=>'(n/d)') + Pais::orderBy('descricao')->lists('descricao', 'id_pais') , array()))
                            @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'nome_pai', 'label' => 'Nome do pai', 'valor' => $obj->nome_pai,  'help' => '',  array()))
                            @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_pais_nacionalidade_pai', 'label' => 'Nacionalidade pai', 'valor' => $obj->id_pais_nacionalidade_pai,  'help' => '', 'valores' => array(''=>'(n/d)') + Pais::orderBy('descricao')->lists('descricao', 'id_pais') , array()))
                            @include('html/campo-data', array('qtdCol' =>  $qtdCol, 'id' => 'dt_nascimento', 'label' => 'Data de nascimento', 'valor' => $obj->dt_nascimento,  'help' => '',  array()))
                            @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'email', 'label' => 'Email', 'valor' => $obj->email,  'help' => '',  array()))
                            @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'local_nascimento', 'label' => 'Local de nascimento', 'valor' => $obj->local_nascimento,  'help' => '',  array()))
                            @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'sexo', 'label' => 'Sexo', 'valor' => $obj->sexo,  'help' => '', 'valores' => array( ''=>'(n/d)', 'F' => 'Feminino', 'M' => 'Masculino'), array()))
                            @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_estadocivil', 'label' => 'Estado civil', 'valor' => $obj->id_estadocivil,  'help' => '', 'valores' => array( ''=>'(n/d)') + Estadocivil::orderBy('descricao')->lists('descricao', 'id_estadocivil') , array()))
                            @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_escolaridade', 'label' => 'Escolaridade', 'valor' => $obj->id_escolaridade,  'help' => '', 'valores' => array( ''=>'(n/d)') + Escolaridade::orderBy('descricao')->lists('descricao', 'id_escolaridade') , array()))
                        </div>
                        <h3 >Endereço no exterior</h3>
                        <div class="row">
                                        @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'endereco_estr', 'label' => 'Endereço', 'valor' => $obj->endereco_estr,  'help' => '',  array()))
                                        @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'cidade_estr', 'label' => 'Cidade', 'valor' => $obj->cidade_estr,  'help' => '',  array()))
                                        @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'telefone_estr', 'label' => 'Telefone', 'valor' => $obj->telefone_estr,  'help' => '',  array()))
                                        @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_pais_estr', 'label' => 'País', 'valor' => $obj->id_pais_estr,  'help' => '', 'valores' => array(''=> '(n/d)') + Pais::orderBy('descricao')->lists('descricao', 'id_pais') , array()))
                        </div>
                        <h3>Documentação</h3>
                        <div class="row">
                                        @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'nu_passaporte', 'label' => 'Núm passaporte', 'valor' => $obj->nu_passaporte,  'help' => '',  array()))
                                        @include('html/campo-data', array('qtdCol' =>  $qtdCol, 'id' => 'dt_emissao_passaporte', 'label' => 'Data emissão pass.', 'valor' => $obj->dt_emissao_passaporte,  'help' => '',  array()))
                                        @include('html/campo-data', array('qtdCol' =>  $qtdCol, 'id' => 'dt_validade_passaporte', 'label' => 'Data validade pass.', 'valor' => $obj->dt_validade_passaporte,  'help' => '',  array()))
                                        @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_pais_passaporte', 'label' => 'País emissor', 'valor' => $obj->id_pais_passaporte,  'help' => '', 'valores' => array(''=>'(n/d)') + Pais::orderBy('descricao')->lists('descricao', 'id_pais') , array()))
                                        @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'nu_cpf', 'label' => 'CPF', 'valor' => $obj->nu_cpf,  'help' => '',  array()))
                                        @include('html/campo-data', array('qtdCol' =>  $qtdCol, 'id' => 'dt_emissao_cpf', 'label' => 'Data de emissão do CPF', 'valor' => $obj->dt_emissao_cpf,  'help' => '',  array()))
                                        @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'nu_rne', 'label' => 'RNE', 'valor' => $obj->nu_rne,  'help' => '',  array()))
                                        @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'nu_ctps', 'label' => 'CTPS', 'valor' => $obj->nu_ctps,  'help' => '',  array()))
                                        @include('html/campo-data', array('qtdCol' =>  $qtdCol, 'id' => 'dt_validade_ctps', 'label' => 'Data validade CTPS', 'valor' => $obj->dt_validade_ctps,  'help' => '',  array()))
                                        @include('html/campo-texto', array('qtdCol' =>  $qtdCol, 'id' => 'nu_cnh', 'label' => 'CNH', 'valor' => $obj->nu_cnh,  'help' => '',  array()))
                                        @include('html/campo-data', array('qtdCol' =>  $qtdCol, 'id' => 'dt_validade_cnh', 'label' => 'Data validade CNH', 'valor' => $obj->dt_validade_cnh,  'help' => '',  array()))
                                        @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_reparticao', 'label' => 'Repartição consular', 'valor' => $obj->id_reparticao, 'valores' => array(''=>'(n/d)') + Reparticao::orderBy('descricao')->lists('descricao', 'id_reparticao'),  'help' => '',  array()))
                        </div>
                        <h3>Perfil profissional</h3>
                        <div class="row">
                                        @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_funcao', 'label' => 'Função', 'valor' => $obj->id_funcao,  'help' => '', 'valores' => array(''=>'(n/d)') + Funcao::orderBy('descricao')->lists('descricao', 'id_funcao') , array()))
                                        @include('html/campo-select', array('qtdCol' =>  $qtdCol, 'id' => 'id_profissao', 'label' => 'Profissão', 'valor' => $obj->id_profissao,  'help' => '', 'valores' => array(''=>'(n/d)') + Profissao::orderBy('descricao')->lists('descricao', 'id_profissao') , array()))
                                        @include('html/campo-textarea', array('qtdCol' =>  $qtdCol, 'id' => 'remuneracao_br', 'label' => 'Remuneração no Brasil', 'valor' => $obj->remuneracao_br,  'help' => '',  array()))
                                        @include('html/campo-textarea', array('qtdCol' =>  $qtdCol, 'id' => 'remuneracao_ext', 'label' => 'Remuneração no exterior', 'valor' => $obj->remuneracao_ext,  'help' => '',  array()))
                                        @include('html/campo-textarea', array('qtdCol' =>  $qtdCol, 'id' => 'trabalho_anterior', 'label' => 'Experiência anterior', 'valor' => $obj->trabalho_anterior,  'help' => '',  array()))
                                        @include('html/campo-textarea', array('qtdCol' =>  $qtdCol, 'id' => 'descricao_atividades', 'label' => 'Descrição das atividades', 'valor' => $obj->descricao_atividades,  'help' => '',  array()))
                                        @include('html/campo-textarea', array('qtdCol' =>  $qtdCol, 'id' => 'justificativa', 'label' => 'Justificativa de contratação', 'valor' => $obj->justificativa,  'help' => '',  array()))
                        </div>

                        <div class="form-group" style="margin-top:10px">
                            <div class="col-sm-10 col-sm-offset-2 col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2 controls">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                {{ HTML::link('empresa', 'Cancelar', array('class' => 'btn')) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajson/', '(selecione a empresa)', false);
});

</script>

@stop
