@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Cotações', 'subTitulo' => ''))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
                {{ Form::open(array('url' => '/cotacao', "class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Escolha o mês e o ano</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'mes', 'label' => 'Mês', 'valor' => $mes, 'valores' => array("1"=> 'Janeiro', "2"=>'Fevereiro', "3"=>'Março', "4"=>'Abril', "5"=>'Maio', "6"=>'Junho', "7"=>'Julho', "8"=>'Agosto', "9"=>'Setembro', "10"=>'Outubro', "11"=>'Novembro', "12"=>'Dezembro') , 'help' => '', 'valores', array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'ano', 'label' => 'Ano', 'valor' => $ano,  'help' => '',  array()))
                    </div>
                </div>
            </div>
        </div>
            {{ Form::close()}}
    </div>

</div>
<div class="row">
    <div class="col-md-12">
                    {{ Form::open(array('url' => '/cotacao/atualizar', "class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Cotações por dia</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-save"></i></button>
                </div>
            </div>
            <div class="box-content">
                {{Form::hidden('mes', $mes)}}
                {{Form::hidden('ano', $ano)}}
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width:100px;">Dia</th>
                            <th style="width: 100px; text-align:right;">Valor</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 1; $i< 32; $i++)
                        <? $data = substr('00'.$i, -2).'/'.substr('00'.$mes, -2).'/'.$ano; ?>
                        <tr>
                            <td>{{ $data}}</td>
                            <td>{{ Form::text($i, number_format($cotacao[$i], 4, ",", "."), array('class'=> 'input-small', 'style'=>'text-align:right'))}}</td>
                            <td>&nbsp;</td>
                        </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
            {{ Form::close()}}
    </div>
</div>
@stop
