<?php

class UsuarioTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('usuario')->truncate();

		$usuario = array(
			'username' => 'patricia', 'password' => Hash::make('westhead'), 'nome' => 'Patricia Valente', 'fl_interno' => '1', 'created_at' => date('Y-m-d h:i:s')
		);
		DB::table('usuario')->insert($usuario);

		$usuario = array(
			'username' => 'leonardo', 'password' => Hash::make('westhead'), 'nome' => 'Leonardo Medeiros', 'fl_interno' => '1', 'created_at' => date('Y-m-d h:i:s')
		);
		DB::table('usuario')->insert($usuario);

		// Uncomment the below to run the seeder
		DB::table('usuario')->insert($usuario);
	}

}
