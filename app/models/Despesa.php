<?php
/**
 * @property int $id_despesa Chave
 * @property int $id_os Chave da OS
 * @property int $id_tipo_despesa Chave do tipo de despesa
 * @property decimal $qtd Quantidade
 * @property decimal $valor Valor unitário
 */
class Despesa extends Modelo {
	protected $table = 'despesa';
	protected $primaryKey = 'id_despesa';
    protected $fillable = array('id_os', 'dt_ocorrencia', 'observacao', 'qtd', 'valor', 'id_tipo_despesa');

    /**
     * Relacionamentos
     *
     */
    public function os(){
        return $this->belongsTo('Os', 'id_os');
    }

    public function tipodespesa(){
        return $this->belongsTo('TipoDespesa', 'id_tipo_despesa');
    }

    /**
     * Atributos especiais
     */
    public function getDtOcorrenciaAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtOcorrenciaAttribute($data) {
        $this->attributes['dt_ocorrencia'] = $this->dtViewParaModel($data);
    }

    public function valor_total(){
        return $this->qtd * $this->valor;
    }

    public function valor_total_usd(){
        return $this->qtd * $this->valor_usd() ;
    }

    public function valor_usd(){
        if ($this->os->valorCotacao() > 0){
            return $this->valor / $this->os->valorCotacao();
        } else {
            return 0;
        }
    }


}
