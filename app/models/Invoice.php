<?php

/**
 * @
 * Invoice é uma fatura. 
 * Pode agrupar várias OS.
 * 
 * @property int $id_invoice Chave da invoice
 * @property int $id_centro_de_custo Centro de custos da invoice
 * @property date $data
 * @property string $data_fmt Data da Invoice formatada para exibição
 * @property string $mes Mês da data da invoice
 * @property string $ano Ano da data da invoice
 * @property date $vencimento Data do vencimento original
 * @property string $vencimento_fmt Data do vencimento formatada para exibição
 * @property int $numero Número da invoice
 * @property string $numero_fmt Número da invoice formatado com zeros à frente para exibição
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property text $observacao 
 * @property decimal $cotacao Valor da cotação do dólar que deve ser usada pela invoice
 * @property string $cotacao_fmt Valor da cotação do dólar formatado para exibição
 * @property string $end_cobr Endereço de cobrança do centro de custos da invoice.
 * @property int $id_usuario_cadastro ID do usuário que criou a invoice
 * @property \Usuario $usuariocadastro Instancia do usuário que cadastrou a Invoice
 */
class Invoice extends Modelo {

    protected $table = 'invoice';
    protected $primaryKey = 'id_invoice';
    protected $guarded = array();

    public function getVencimentoFmtAttribute() {
        return $this->dtModelParaView($this->vencimento);
    }

    public function setVencimentoFmtAttribute($data) {
        $this->attributes['vencimento'] = $this->dtViewParaModel($data);
    }

    public function getDataFmtAttribute() {
        return $this->dtModelParaView($this->data);
    }

    public function setDataFmtAttribute($data) {
        $this->attributes['data'] = $this->dtViewParaModel($data);
    }

    public function getCotacaoFmtAttribute() {
        return number_format($this->attributes['cotacao'], 4, ",", ".");
    }

    public function setCotacaoFmtAttribute($data) {
        $this->attributes['cotacao'] = str_replace(',', '.', str_replace('.', '', $data));
    }

    public function getEndCobrAttribute() {
        if (is_object($this->centrodecusto)) {
            return $this->centrodecusto->end_cobr;
        } else {
            return '(n/a)';
        }
    }

    /**
     * 
     * @return Centrodecusto
     */
    public function centrodecusto() {
        return $this->belongsTo('Centrodecusto', 'id_centro_de_custo');
    }

    public function getMesAttribute() {
        $xdata = \DateTime::createFromFormat('Y-m-d', $this->data);
        if (is_object($xdata)) {
            return $xdata->format('m');
        } else {
            return '--';
        }
    }

    public function getAnoAttribute() {
        $xdata = \DateTime::createFromFormat('Y-m-d', $this->data);
        if (is_object($xdata)) {
            return $xdata->format('Y');
        } else {
            return '----';
        }
    }

    public function getNumeroFmtAttribute() {
        return substr('000' . $this->numero, -3) . 'W' . substr('00' . $this->mes, -3) . '/' . $this->ano;
    }

    public function getValorTotalAttribute($val) {
        return \InvoiceServico::where('id_invoice', '=', $this->id_invoice)->sum('val_total');
    }

    public function getValorTotalFmtAttribute($val) {
        return number_format($this->valor_total, 2, ",", ".");
    }

    public function servicos() {
        return $this->hasMany('InvoiceServico', 'id_invoice');
    }

    public function getListaProjetosAttribute($val) {
        $sql = "select distinct descricao from projeto, os where projeto.id_projeto = os.id_projeto and os.id_invoice = " . $this->id_invoice;
        $projs = \DB::select(\DB::raw($sql));
        $ret = '';
        $virg = '';
        foreach ($projs as $proj) {
            $ret .= $virg . $proj->descricao;
            $virg = ', ';
        }
        return $ret;
    }

    public function getListaOsAttribute($val) {
        $oss = \Os::where('id_invoice', '=', $this->id_invoice)->orderBy('numero')->get();
        $ret = '';
        $virg = '';
        foreach ($oss as $os) {
            $ret .= $virg . '<a href="/os/show/' . $os->id_os . '" target="_blank">OS ' . substr('000000' . $os->numero, -6) . '</a>';
            $virg = ', ';
        }
        return $ret;
    }

    public function getListaOsPorTipoAttribute($val) {
        $sql = "select tipo_servico.nome,tipo_servico.nome_plural, count(*) qtd"
                . " from os, servico, tipo_servico"
                . " where os.id_invoice = " . $this->id_invoice
                . " and servico.id_servico = os.id_servico"
                . " and tipo_servico.id_tipo_servico = servico.id_tipo_servico"
                . " group by tipo_servico.nome, tipo_servico.nome_plural";
        $oss = \DB::select(\DB::raw($sql));
        $ret = '';
        $virg = '';
        foreach ($oss as $os) {
            if ($os->qtd == 1) {
                $ret .= $virg . $os->qtd . ' ' . $os->nome;
            } else {
                $ret .= $virg . $os->qtd . ' ' . $os->nome_plural;
            }
            $virg = '<br/>';
        }
        return $ret;
    }

    /**
     * 
     * @return \Usuario
     */
    public function usuariocadastro(){
        return $this->belongsTo('Usuario', 'id_usuario_cadastro');
    }
    
    public function getNomeUsuarioCadastroAttribute(){
        if (is_object($this->usuariocadastro)){
            return $this->usuariocadastro->nome;
        } else {
            return '--';
        }
    }
    
    public function scopeDocentrodecusto($query, $id_centrodecusto) {
        if ($id_centrodecusto != '' && intval($id_centrodecusto) > 0) {
            return $query->where('id_centro_de_custo', '=', $id_centrodecusto);
        } else {
            return $query;
        }
    }

    public function getDadosAttribute() {
        return 'Criada por '.$this->nome_usuario_cadastro." em ".$this->data_fmt;
    }

    public function scopeDataEntre($query, $data_ini, $data_fim) {
        if ($data_ini != '' && $data_ini != '__/__/____') {
            $query = $query->where('data', '>=', DateTime::createFromFormat('d/m/Y', $data_ini)->format('Y-m-d'));
        }
        if ($data_fim != '' && $data_fim != '__/__/____') {
            $query = $query->where('data', '<=', DateTime::createFromFormat('d/m/Y', $data_fim)->format('Y-m-d'));
        }
        return $query;
    }

}
