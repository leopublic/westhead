<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableDelegacia extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('delegacia', function(Blueprint $table) {
			$table->increments('id_delegacia');
			$table->string('codigo', 10);
            $table->string('nome', 500);
            $table->string('uf', 2);
		});
        DB::table('delegacia')->truncate();
        $delegacia = array("codigo" =>"08507", "nome" =>"São Sebastião", "uf" =>"SP");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08458", "nome" =>"Niterói", "uf" =>"RJ");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08460", "nome" =>"Galeão", "uf" =>"RJ");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08240", "nome" =>"Manaus", "uf" =>"AM");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08364", "nome" =>"Belém", "uf" =>"PA");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08102", "nome" =>"Suape", "uf" =>"PE");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08504", "nome" =>"Santos", "uf" =>"SP");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08260", "nome" =>"Salvador", "uf" =>"BA");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08387", "nome" =>"Paranaguá", "uf" =>"PR");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08097", "nome" =>"Angra dos Reis", "uf" =>"RJ");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08270", "nome" =>"Fortaleza", "uf" =>"CE");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08310", "nome" =>"São Luis", "uf" =>"MA");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08434", "nome" =>"Rio Grande", "uf" =>"RS");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08286", "nome" =>"Espírito Santo", "uf" =>"ES");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08461", "nome" =>"Macaé", "uf" =>"RJ");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08444", "nome" =>"Tramandaí", "uf" =>"RS");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08101", "nome" =>"Macapá", "uf" =>"AP");
        DB::table('delegacia')->insert($delegacia);
        $delegacia = array("codigo" =>"08230", "nome" =>"Maceió", "uf" =>"AL");
        DB::table('delegacia')->insert($delegacia);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('table_delegacia');
	}

}
