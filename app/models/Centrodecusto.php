<?php
class Centrodecusto extends Modelo {
	protected $table = 'centro_de_custo';
	protected $primaryKey = 'id_centro_de_custo';
	protected $guarded = array();

	public function assumeDefaults()
	{
	}

	public static function regras()
	{
		return array(
				'id_empresa' => 'required',
				'nome' => 'required'
				);
	}

	public static function mensagens()
	{
		return array(
				'id_empresa.required' => 'A empresa é obrigatória!',
				'nome.required' => 'O nome é obrigatório!'
			);
	}

	public function empresa()
	{
		return $this->belongsTo('Empresa', 'id_empresa');
	}

    public function scopeDaEmpresa($query, $id_empresa){
        return $query->where('id_empresa', '=', $id_empresa)->orderBy('nome');
    }
}
