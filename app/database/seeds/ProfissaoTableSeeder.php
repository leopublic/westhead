<?php

class ProfissaoTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('profissao')->truncate();
		
		$rep = array("descricao" =>"Engenheiro Subsea", "descricao_en"=>"Subsea Engineer", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Perfuração", "descricao_en"=>"Drilling Supervisor", "cbo"=>"7101");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Contador", "descricao_en"=>" Accountant", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Marítimo", "descricao_en"=>"Marítimo", "cbo"=>"469");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Segurança do Trabalho", "descricao_en"=>"Safety Tech", "cbo"=>"5173");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro Naval", "descricao_en"=>"Marine Engineer", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Inspetor hidrográfico", "descricao_en"=>"Inspetor hidrográfico", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Aviação", "descricao_en"=>"Técnico em Aviação", "cbo"=>"2153-15");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro", "descricao_en"=>"Engineer", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Eletrônica", "descricao_en"=>"Técnico em Eletrônica", "cbo"=>"3132");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Oceanógrafo/Oceanólogo", "descricao_en"=>"Oceanographer", "cbo"=>"2134-15");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"técnico em oceanografia", "descricao_en"=>"oceanographer technical", "cbo"=>"2134-15");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"técnico em soldagem ", "descricao_en"=>"welder", "cbo"=>"3146-20");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"chapeador", "descricao_en"=>"plater", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Mestre/Contramestre", "descricao_en"=>"Foreman", "cbo"=>"7202");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"técnico em caldeiraria", "descricao_en"=>"boiler", "cbo"=>"3146");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Advogado", "descricao_en"=>"Lawyer", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Inspetor de Segurança", "descricao_en"=>"Safety Inspector", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Bacharel em Direito", "descricao_en"=>"Bacharel em Direito", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Eletricista de Embarcações", "descricao_en"=>"Electrician", "cbo"=>"9531-10");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de Operações", "descricao_en"=>"Operations Manager", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Serviços", "descricao_en"=>"Services Engineer", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Operador de Posicionamento Dinâmico", "descricao_en"=>"Dinamic Position Operator", "cbo"=>"2151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Téc. de Plan. de Projetos", "descricao_en"=>"Téc. de Plan. de Projetos", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Qualidade", "descricao_en"=>"Supervisor de Qualidade", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Piloto de helicóptero", "descricao_en"=>"Helicopter Pilot", "cbo"=>"2153-05");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Encarregado de Sonda Senior", "descricao_en"=>"Senior Toolpusher", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro Mecanico", "descricao_en"=>"Engenheiro Mecanico", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de Produção", "descricao_en"=>"Gerente de Produção", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Empresário", "descricao_en"=>"Empresário", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro Geomático", "descricao_en"=>"Engenheiro Geomático", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Cozinheira", "descricao_en"=>"Cook", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Hidráulica", "descricao_en"=>"Técnico em Hidráulica", "cbo"=>"7241");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Oficial de Convés", "descricao_en"=>"Deck officer", "cbo"=>"2151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Eng. de Manut. de Helicóp.", "descricao_en"=>"Eng. de Manut. de Helicóp.", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Inspetor e Vistoriador Naval", "descricao_en"=>"Inspetor e Vistoriador Naval", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Capitão de longo curso (comandante) ", "descricao_en"=>"Capitão de longo curso (comandante) ", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Perfuração", "descricao_en"=>"Técnico de Perfuração", "cbo"=>"3144");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Manutenção", "descricao_en"=>"Supervisor de Manutenção", "cbo"=>"9503");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico Eletricista", "descricao_en"=>"Técnico Eletricista", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Analista Quimico", "descricao_en"=>"Analista Quimico", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"engenheiro de produção", "descricao_en"=>"engenheiro de produção", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"gerente de produção e marketing", "descricao_en"=>"gerente de produção e marketing", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de Marketing/Vendas", "descricao_en"=>"Gerente de Marketing/Vendas", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Eletricista", "descricao_en"=>"Eletricista", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"1º Operador de escavação", "descricao_en"=>"1º Operador de escavação", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Operador de escavação hidráulica", "descricao_en"=>"Operador de escavação hidráulica", "cbo"=>"7151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Topógrafo", "descricao_en"=>"Topógrafo", "cbo"=>"3123");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Inspetor Naval", "descricao_en"=>"Inspetor Naval", "cbo"=>"2151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico Superintendente Naval", "descricao_en"=>"Técnico Superintendente Naval", "cbo"=>"2151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro Metalurgico", "descricao_en"=>"Engenheiro Metalurgico", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Serviços", "descricao_en"=>"Técnico de Serviços", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Inspetor de controle dimensional", "descricao_en"=>"Inspetor de controle dimensional", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Projetos", "descricao_en"=>"Engenheiro de Projetos", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"engenheiro de sistemas", "descricao_en"=>"engenheiro de sistemas", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Inspetor Supervisor de Soldagem", "descricao_en"=>"Inspetor Supervisor de Soldagem", "cbo"=>"3146");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro Eletricista", "descricao_en"=>"Engenheiro Eletricista", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Economista", "descricao_en"=>"Economista", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Bacharel em Ciências", "descricao_en"=>"Bacharel em Ciências", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Administrador", "descricao_en"=>"Administrador", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro Químico", "descricao_en"=>"Engenheiro Químico", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Mecânico de Instrumentos de Precisão", "descricao_en"=>"Mecânico de Instrumentos de Precisão", "cbo"=>"7411");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico Instr. de Precisão", "descricao_en"=>"Técnico Instr. de Precisão", "cbo"=>"7411");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de Processos", "descricao_en"=>"Gerente de Processos", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Sistemas", "descricao_en"=>"Técnico de Sistemas", "cbo"=>"3112");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Comissionamento", "descricao_en"=>"Engenheiro de Comissionamento", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Eletrônica de Telecomunicação", "descricao_en"=>"Técnico em Eletrônica de Telecomunicação", "cbo"=>"2143");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Perfurador", "descricao_en"=>"Perfurador", "cbo"=>"7113-20");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Perfuração", "descricao_en"=>"Técnico de Perfuração", "cbo"=>"7113-20");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro Eletromecânico", "descricao_en"=>"Engenheiro Eletromecânico", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Serviços de Campo", "descricao_en"=>"Engenheiro de Serviços de Campo", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Engenharia Eletrônica", "descricao_en"=>"Técnico em Engenharia Eletrônica", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico Mecânico", "descricao_en"=>"Técnico Mecânico", "cbo"=>"3141-10");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Geologia ", "descricao_en"=>"Técnico em Geologia ", "cbo"=>"3161-10");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Sócio - Administrador", "descricao_en"=>"Sócio - Administrador", "cbo"=>"2521");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Elétrica", "descricao_en"=>"Técnico em Elétrica", "cbo"=>"3131");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Mecânico de Motores", "descricao_en"=>"Mecânico de Motores", "cbo"=>"7254");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Téc. em Levant. Hidrográficos", "descricao_en"=>"Téc. em Levant. Hidrográficos", "cbo"=>"3123-15");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Soldagem", "descricao_en"=>"Técnico em Soldagem", "cbo"=>"3146-20 ");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Equipamentos Navais", "descricao_en"=>"Técnico em Equipamentos Navais", "cbo"=>"3143");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Instrutor de Segurança de Aeronaves", "descricao_en"=>"Instrutor de Segurança de Aeronaves", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Especialista em Bombas (Embarcações e Lastreamento)", "descricao_en"=>"Especialista em Bombas (Embarcações e Lastreamento)", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Serviços Elétricos", "descricao_en"=>"Técnico em Serviços Elétricos", "cbo"=>"3131");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Téc. de Serviços de Campo", "descricao_en"=>"Téc. de Serviços de Campo", "cbo"=>"3146");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Operador de Plataforma", "descricao_en"=>"Operador de Plataforma", "cbo"=>"7113-25");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Construção Naval", "descricao_en"=>"Técnico de Construção Naval", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico Naval", "descricao_en"=>"Técnico Naval", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Tecnico em estruturas metálicas", "descricao_en"=>"Rigbuilder", "cbo"=>"3146 ");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Operador de Turismo", "descricao_en"=>"Operador de Turismo", "cbo"=>"3548");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Especialista Téc. de Garantia da Qualidade", "descricao_en"=>"Especialista Téc. de Garantia da Qualidade", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Proc. de Dados", "descricao_en"=>"Técnico em Proc. de Dados", "cbo"=>"3172-05 ");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Soldador", "descricao_en"=>"Soldador", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Pesquisador de Levantamentos de Quantidade", "descricao_en"=>"Pesquisador de Levantamentos de Quantidade", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Contador Público", "descricao_en"=>"Contador Público", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Contratos", "descricao_en"=>"Supervisor de Contratos", "cbo"=>"4102");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Sistemas", "descricao_en"=>"Engenheiro de Sistemas", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro Chefe", "descricao_en"=>"Engenheiro Chefe", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Engenharia", "descricao_en"=>"Técnico em Engenharia", "cbo"=>"2144");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Inspetor Técnico de Operações", "descricao_en"=>"Inspetor Técnico de Operações", "cbo"=>"2151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Calibração e Ins.", "descricao_en"=>"Técnico em Calibração e Ins.", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Tecnico em Inspeção Hidrográfica", "descricao_en"=>"Tecnico em Inspeção Hidrográfica", "cbo"=>"3161");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Pintor", "descricao_en"=>"Painter", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Pintura", "descricao_en"=>"Painter Supervisor", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Segurança no Trabalho", "descricao_en"=>"Técnico em Segurança no Trabalho", "cbo"=>"3516");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Garantia da Qualidade ", "descricao_en"=>"Técnico de Garantia da Qualidade ", "cbo"=>"3912");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Tecnico Controle de qualidade e Higienização", "descricao_en"=>"Tecnico Controle de qualidade e Higienização", "cbo"=>"3912");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Higiene ambiental", "descricao_en"=>"Supervisor de Higiene ambiental", "cbo"=>"3516");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Ajustador Técnico Naval", "descricao_en"=>"Ajustador Técnico Naval", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Construção Naval", "descricao_en"=>"Supervisor de Construção Naval", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Turno", "descricao_en"=>"shift supervisor", "cbo"=>"7101-20");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Geólogo de Engenharia", "descricao_en"=>"Geólogo de Engenharia", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Geólogo", "descricao_en"=>"Geólogo", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Coordenador de Materiais", "descricao_en"=>"Coordenador de Materiais", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Almoxarife", "descricao_en"=>"Almoxarife", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Professor de Inglês", "descricao_en"=>"Professor de Inglês", "cbo"=>"2346-16");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Professor(A)", "descricao_en"=>"Professor(A)", "cbo"=>"3331");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Téc. de Serviços de Man. e Instalação", "descricao_en"=>"Téc. de Serviços de Man. e Instalação", "cbo"=>"3146");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Téc. de Serviços de Man. e Instalação", "descricao_en"=>"Téc. de Serviços de Man. e Instalação", "cbo"=>"3146");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Consultor de Perfuração", "descricao_en"=>"Consultor de Perfuração", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor Técnico de Construção Naval", "descricao_en"=>"Supervisor Técnico de Construção Naval", "cbo"=>"7202");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Construção", "descricao_en"=>"Engenheiro de Construção", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Especialista em Projetos", "descricao_en"=>"Especialista em Projetos", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Revestimento", "descricao_en"=>"Técnico em Revestimento", "cbo"=>"7210");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Vendas", "descricao_en"=>"Engenheiro de Vendas", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Téc. de Planej. de Projetos", "descricao_en"=>"Téc. de Planej. de Projetos", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Pesquisadora em Saúde Pública", "descricao_en"=>"Pesquisadora em Saúde Pública", "cbo"=>"2033");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Superintendente Técnico ", "descricao_en"=>"Superintendente Técnico ", "cbo"=>"2152-20");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de ROV", "descricao_en"=>"ROC Operations Manager", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Médico", "descricao_en"=>"Médico", "cbo"=>"2231-18");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Logística", "descricao_en"=>"Supervisor de Logística", "cbo"=>"1224");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Suporte (Software)", "descricao_en"=>"Engenheiro de Suporte (Software)", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Sistemas Sênior", "descricao_en"=>"Técnico de Sistemas Sênior", "cbo"=>"3132");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Engenheiro de Telecomunicações ", "descricao_en"=>"Engenheiro de Telecomunicações ", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Superintendente de Plataforma", "descricao_en"=>"Superintendente de Plataforma", "cbo"=>"7113");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Vistoriador", "descricao_en"=>"Vistoriador", "cbo"=>"2151-50");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Processador de Dados Sísmicos", "descricao_en"=>"Seismic Data Processor", "cbo"=>"3172-05");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Designer", "descricao_en"=>"Designer", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente Financeiro", "descricao_en"=>"Gerente Financeiro", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico Analista (Contábil)", "descricao_en"=>"Técnico Analista (Contábil)", "cbo"=>"2522-10");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Empresário", "descricao_en"=>"Empresário", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor de Analise Financeiras", "descricao_en"=>"Supervisor de Analise Financeiras", "cbo"=>"1227");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Assistente de Lubrificação", "descricao_en"=>"Assistente de Lubrificação", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico de Manutenção de Helicópteros", "descricao_en"=>"Técnico de Manutenção de Helicópteros", "cbo"=>"3143-10");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Superintendente de Projetos", "descricao_en"=>"Superintendente de Projetos", "cbo"=>"1427");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Consultor de Organização", "descricao_en"=>"Consultor de Organização", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente Comercial", "descricao_en"=>"Sales Manager", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor Técnico em Segurança", "descricao_en"=>"Safety Training Supervisor", "cbo"=>"3516");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor Técnico de Segurança", "descricao_en"=>"Supervisor Técnico de Segurança", "cbo"=>"2151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Analista de Dados Master II", "descricao_en"=>"Analista de Dados Master II", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Auditor Financeiro", "descricao_en"=>"Auditor Financeiro", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Hidrografia", "descricao_en"=>"Técnico em Hidrografia", "cbo"=>"3123");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Consultor de Garantia de Qualidade", "descricao_en"=>"Quality Assurance Consultant", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Técnico em Manutenção", "descricao_en"=>"Maintenance Technician", "cbo"=>"3131-20");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Consultor Técnico", "descricao_en"=>"Technical Consultant", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Tradutor", "descricao_en"=>"Translator", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Arquiteto", "descricao_en"=>"Arquiteto", "cbo"=>"10");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Assistente de Subsea", "descricao_en"=>"Assistente de Subsea", "cbo"=>"3143");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>" Supervisor Técnico de Inspeção Naval", "descricao_en"=>" Supervisor Técnico de Inspeção Naval", "cbo"=>"2151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Supervisor Técnico em Inspeção Naval", "descricao_en"=>"Supervisor Técnico em Inspeção Naval", "cbo"=>" Supervisor Técnico ");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de Oficina", "descricao_en"=>"Gerente de Oficina", "cbo"=>"1414-20 ");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Operador de Posicionamento Dinâmico", "descricao_en"=>"Operador de Posicionamento Dinâmico", "cbo"=>"2151");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Operador de Draga", "descricao_en"=>"Operador de Draga", "cbo"=>"7821-05");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Camareiro(a) de Embarcações Chefe", "descricao_en"=>"Camareiro(a) de Embarcações Chefe", "cbo"=>"5133-20");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Analista de Investimentos", "descricao_en"=>"Investment Analyst", "cbo"=>"2144");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Médico de embarcações", "descricao_en"=>"Médico de embarcações", "cbo"=>"2231-15");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Segundo Oficial de Náutica", "descricao_en"=>"Segundo Oficial de Náutica", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Analista de sistemas", "descricao_en"=>"Analista de sistemas", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Bacharel em Ciências Sociais e Recursos Humanos", "descricao_en"=>"Bacharel em Ciências Sociais e Recursos Humanos", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de Sonda", "descricao_en"=>"Rig Manager", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Dentista", "descricao_en"=>"Dentist", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Geofísico Senior", "descricao_en"=>"Senior Geophysicist", "cbo"=>"2134-15");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de Recursos Humanos", "descricao_en"=>"Human Resources Manager", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Gerente de Recursos Humanos", "descricao_en"=>"Human Resources Manager", "cbo"=>"");
		DB::table('profissao')->insert($rep);
		$rep = array("descricao" =>"Artista", "descricao_en"=>"Artist", "cbo"=>"");
		DB::table('profissao')->insert($rep);
	}

}
