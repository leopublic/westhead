<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
Route::controller('auth', 'AuthController');

// Rotas autorizadas somente a usuários logados
Route::group(array('before' => 'autenticado|senha_atualizada'), function() {

    Route::controller('home', 'HomeController');
    Route::get('/', function() {
        return Redirect::to('home/bemvindo');
    });

    Route::group(array('prefix' => 'publico'), function() {

    });

    // Rotas autorizadas somente aos usuários nivel > 10 (internos)
    Route::group(array('before' => 'backoffice'), function() {

        App::bind('RepositorioAnexo', function($app) {
            return new \Westhead\Repositorios\RepositorioAnexo;
        });
        App::bind('OsController', function($app) {
            return new OsController(new \Westhead\Repositorios\RepositorioOs);
        });
        App::bind('CandidatoController', function($app) {
            return new CandidatoController(new \Westhead\Repositorios\RepositorioCandidato);
        });

        Route::controller('layout', 'LayoutController');

        Route::controller('anexo', 'AnexoController');
        Route::controller('andamento', 'AndamentoController');
        Route::controller('armador', 'ArmadorController');
        Route::controller('candidato', 'CandidatoController');
        Route::controller('centrodecusto', 'CentrodecustoController');
        Route::controller('checklist', 'ChecklistController');
        Route::controller('cotacao', 'CotacaoController');
        Route::controller('empresa', 'EmpresaController');
        Route::controller('estadocivil', 'EstadocivilController');
        Route::controller('escolaridade', 'EscolaridadeController');
        Route::controller('funcao', 'FuncaoController');
        Route::controller('invoice', 'InvoiceController');
        Route::controller('os', 'OsController');
        Route::controller('pais', 'PaisController');
        Route::controller('perfil', 'PerfilController');
        Route::controller('preco', 'PrecoController');
        Route::controller('processo', 'ProcessoController');
        Route::controller('processo1', 'ProcessoAutorizacaoController');
        Route::controller('processo3', 'ProcessoRegistroController');
        Route::controller('processo4', 'ProcessoColetaVistoController');
        Route::controller('processo5', 'ProcessoProrrogacaoController');
        Route::controller('processo6', 'ProcessoOutrosController');
        Route::controller('processo7', 'ProcessoAtendimentoController');
        Route::controller('processo8', 'ProcessoCancelamentoController');
        Route::controller('processo9', 'ProcessoColetaController');
        Route::controller('processo10', 'ProcessoTraducaoController');
        Route::controller('procurador', 'ProcuradorController');
        Route::controller('profissao', 'ProfissaoController');
        Route::controller('projeto', 'ProjetoController');
        Route::controller('protocolobsb', 'ProtocolobsbController');
        Route::controller('reparticao', 'ReparticaoController');
        Route::controller('servico', 'ServicoController');
        Route::controller('statusandamento', 'StatusAndamentoController');
        Route::controller('tabelapreco', 'TabelaPrecoController');
        Route::controller('tipoanexo', 'TipoAnexoController');
        Route::controller('tipoautorizacao', 'TipoAutorizacaoController');
        Route::controller('tipodespesa', 'TipoDespesaController');
        Route::controller('tiposervico', 'TipoServicoController');
        Route::controller('visto', 'VistoController');
        Route::controller('usuario', 'UsuarioController');
    });
});
