@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="icon-file-alt"></i>{{ $tela['titulo_tela'] }}</h1>
        <h4>{{ $tela['subTitulo'] }}</h4>
    </div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<!--                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Basic Tables</li>
                    </ul>
                </div>-->
<!-- END Breadcrumb -->
@if (Session::has('flash_error'))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            {{ Session::get('flash_error') }}
        </div>
    </div>
</div>
@endif
@if (Session::has('flash_msg'))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success">
            {{ Session::get('flash_msg') }}
        </div>
    </div>
</div>
@endif

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>{{ $tela['titulo_grid'] }}</h3>
                <div class="box-tool">
                    <a href="{{ $tela['urlCreate'] }}"><i class="icon-plus"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width:30px;">#</th>
                            <th style="width: 150px">Ação</th>
                            <th>Descrição</th>
                        </tr>
                    </thead>
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $registro)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ URL::to($tela['urlEdit'], array($registro->$tela['nomeCampoChave']) ) }}"><i class="icon-edit"></i></a>
                                <a class="btn btn-lime btn-sm" href="{{ URL::to('/perfil/acessos/'.$registro->id_perfil) }}" title="Clique para administrar os acessos desse perfil"><i class="icon-key"></i></a>
                                <a class="btn btn-danger btn-sm" href="{{ URL::to($tela['urlDel'], array($registro->$tela['nomeCampoChave']) ) }}"><i class="icon-trash"></i></a>
                            </td>
                            <td>{{ $registro->nome }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop