<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProtocoloBsbTable extends Migration {

    public function up() {
        Schema::create('protocolo_bsb', function(Blueprint $table) {
            $table->increments('id_protocolo_bsb');
            $table->date('dt_protocolo');
            $table->text('observacao')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('protocolo_bsb');
    }

}
