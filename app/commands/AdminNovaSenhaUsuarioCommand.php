<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminNovaSenhaUsuarioCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:novasenha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gera uma nova senha para um usuário específico';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $username = $this->argument('username');
        $password = $this->argument('password');
        $confirma = $this->argument('confirma');
        if ($password != $confirma) {
            $this->error("As senhas informadas não batem. Tente novamente.");
        } else {
            $usuario = \Usuario::where('username', '=', $username)->first();
            if (!is_object($usuario)){
                $this->error('Não foi encontrado um usuário com esse login:"' . $username .'"');
            } else {
                if ($this->confirm('Tem certeza que deseja alterar a senha do usuário "'.$usuario->nome.'" para "'.$password.'"? [yes|no]')) {
                    $usuario->password = \Hash::make($password);
                    $usuario->save();
                    $this->info('Senha alterada com sucesso!');
                } else{
                    $this->info('Senha não alterada.');                    
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
            array('username', InputArgument::REQUIRED, 'Login'),
            array('password', InputArgument::REQUIRED, 'Nova senha'),
            array('confirma', InputArgument::REQUIRED, 'Confirmação'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }

}
