
<div class="row">
    <div class="col-md-12">
        <div class="form-wizard" >
            <ul class="row steps steps-fill nav ">
                <? $i = 1;?>
                @foreach($vistos as $visto)
                    @if($visto->id_visto == $id_visto_ativo)
                        <li class="col-md-3 active">
                    @else
                        <li class="col-md-3">
                    @endif
                    <a href="/candidato/vistosinativos/{{$cand->id_candidato}}/{{$visto->id_visto}}" class="step">
                        <span class="number">{{$i}}</span>
                        <span class="desc" style="font-size:12px; line-height: 100%;">{{$visto->id_visto}}</span>
                    </a>
                </li>
                <? $i++;?>
                @endforeach

                @if(0 == $id_visto_ativo)
                    <li class="col-md-3 active">
                @else
                    <li class="col-md-3">
                @endif
                <a href="/candidato/vistosinativos/{{$cand->id_candidato}}/0" class="step">
                    <span class="number">{{$i}}</span>
                    <span class="desc" style="font-size:12px; line-height: 100%;">(processos sem visto)</span>
                </a>

                @if (Auth::user()->temAcessoA('VIST', 'INS'))                
                    <li class="col-md-3">
                        <a href="/candidato/novovisto/{{$cand->id_candidato}}" class="step">
                            <span class="number"><i class="icon-plus" style="display: inline;"></i></span>
                            <span class="desc">Criar novo visto</span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>