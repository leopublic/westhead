<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoAutorizacaoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tipo_autorizacao', function(Blueprint $table) {
            $table->increments('id_tipo_autorizacao');
            $table->string('descricao', 1000)->nullable();
            $table->string('descricao_res', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tipo_autorizacao');
    }

}
