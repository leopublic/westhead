<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminRessetaSenhaUsuariosCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:renovarsenhas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obriga todos os usuários a informarem uma nova senha';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $sql = "update usuario set fl_alterar_senha = 1";
        $qtd = \DB::update(\DB::raw($sql));
        $this->info(number_format($qtd, "0", ",", ".")." usuários atualizados com sucesso");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }
}
