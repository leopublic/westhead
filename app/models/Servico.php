<?php
/**
 * @property int $id_servico
 * @property varchar $descricao
 * @property varchar $descricao_en
 * @property text $observacao
 * @property tinyint $disponivel
 * @property tinyint $afeta_historico_visto
 * @property tinyint $multi_candidatos
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property int $id_status_os_inicial
 * @property int $id_tipo_servico
 * @property tinyint $fl_abre_servico
 * @property timestamp $deleted_at
 * @property int $id_tipo_autorizacao
 * @property int $id_classificacao_visto
 * @property varchar $descricao_invoice
 * @property tinyint $fl_bsb 
 * @property tinyint $fl_andamento Indica se o serviço está sujeito ao andamento
 */
class Servico extends Eloquent {

    protected $table = 'servico';
    protected $primaryKey = 'id_servico';
    protected $guarded = array();

    public function assumeDefaults() {

    }

    public static function regras() {
        return array(
            'descricao' => 'required'
        );
    }

    public static function mensagens() {
        return array(
            'descricao.required' => 'A descrição é obrigatória!'
        );
    }

    public function tipoautorizacao(){
        return $this->belongsTo('TipoAutorizacao', 'id_tipo_autorizacao');
    }

    public function tiposervico(){
        return $this->belongsTo('TipoServico', 'id_tipo_servico');
    }

    public function classificacao(){
        return $this->belongsTo('ClassificacaoVisto', 'id_classificacao_visto');
    }
}
