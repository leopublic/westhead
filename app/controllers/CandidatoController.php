<?php

class CandidatoController extends BaseController {

    public function __construct($repositorio) {
        $this->repositorio = $repositorio;
    }

    public function getShow($id_candidato){
        if (\Auth::user()->temAcessoA('CAND', 'CAD')){
            return Redirect::to('/candidato/cadastro/'.$id_candidato);
        } elseif (\Auth::user()->temAcessoA('CAND', 'DEP')){
            return Redirect::to('/candidato/dependentes/'.$id_candidato);
        } elseif (\Auth::user()->temAcessoA('CAND', 'ANEX')){
            return Redirect::to('/candidato/anexos/'.$id_candidato);
        } elseif (\Auth::user()->temAcessoA('CAND', 'VISA')){
            return Redirect::to('/candidato/vistoatual/'.$id_candidato);
        } elseif (\Auth::user()->temAcessoA('CAND', 'VISI')){
            return Redirect::to('/candidato/vistosinativos/'.$id_candidato);
        } else{
            return Redirect::to('/')->with('flash_msg', 'Seu perfil não tem acesso a essa função.');
        }
    }
    
    public function getIndex() {
        $obj = new Candidato;
        $filtros = array();
        $post = array('id_empresa', 'id_projeto', 'nome_completo', 'id_funcao');
        foreach($post as $key ){
            $filtros[$key] = Input::old($key, Session::get($key));
        }

        $empresas = Empresa::where('fl_armador', '=', 0)->orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        if (isset($filtros['id_empresa'])){
            $id_empresa = $filtros['id_empresa'];
        } else {
            $id_empresa = '';
        }
        if ($id_empresa > 0){
            $projetos = Projeto::daEmpresa($id_empresa)->lists('descricao', 'id_projeto');
            $projetos = array('' => '(todas)') + $projetos;
        } else {
            $projetos = array('' => '(selecione a empresa)');
        }
        $empresas = array('0' => '(todas)') +  $empresas;

        $reg =  new Candidato;
        if (intval($filtros['id_empresa']) > 0) {
            $reg = $reg->daEmpresa($filtros['id_empresa']);
        }
        if (intval($filtros['id_projeto']) > 0) {
            $reg = $reg->doProjeto($filtros['id_projeto']);
        }
        if ($filtros['nome_completo'] != '') {
            $reg = $reg->doNome($filtros['nome_completo']);
        }
        if ($filtros['id_funcao'] != '') {
            $reg = $reg->where('id_funcao', '=', $filtros['id_funcao']);
        }
        $registros =  $reg->orderBy('nome_completo')->paginate(20);

        return View::make('candidato/index')
                        ->with('registros', $registros)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
        ;
    }

    public function postIndex() {
        Input::flash();
        $post = Input::all();
        foreach($post as $key => $value){
            Session::set($key, $value);
        }
        return Redirect::to('candidato')->withInput();
    }
    public function getHomonimos() {
        $filtros = array();
        $post = array('nome_completo');
        foreach($post as $key ){
            $filtros[$key] = Input::old($key, Session::get($key));
        }
        
        $sql = "select distinct c1.id_candidato, c1.nome_completo, c1.nome_mae, date_format(c1.dt_nascimento, '%d/%m/%Y') dt_nascimento, c1.nome_mae, c1.nu_passaporte
                from candidato c1 join candidato c2
                where c1.nome_completo = c2.nome_completo
                and c1.id_candidato <> c2.id_candidato
                order by nome_completo";
        
        $registros =  \DB::select(\DB::raw($sql));

        return View::make('candidato/homonimos')
                        ->with('registros', $registros)
        ;
    }

    public function postHomonimos() {
        Input::flash();
        $post = Input::all();
        foreach($post as $key => $value){
            Session::set($key, $value);
        }
        return Redirect::to('candidato')->withInput();
    }

    public function getCreate() {
        $obj = new Candidato;
        return View::make('candidato/cadastro')
                        ->with('obj', $obj)
                        ->with('qtdCol', '2')
        ;
    }
    
    public function getCadastro($id) {
        if (\Auth::user()->temAcessoA('CAND', 'CAD')){
            $obj = Candidato::find($id);
            return View::make('candidato/cadastro')
                            ->with('obj', $obj)
                            ->with('qtdCol', '2')
            ;
        } elseif (\Auth::user()->temAcessoA('CAND', 'DEP')){
            return Redirect::to('/candidato/dependentes/'.$id);
        } elseif (\Auth::user()->temAcessoA('CAND', 'ANEX')){
            return Redirect::to('/candidato/anexos/'.$id);
        } elseif (\Auth::user()->temAcessoA('CAND', 'VISA')){
            return Redirect::to('/candidato/vistoatual/'.$id);
        } elseif (\Auth::user()->temAcessoA('CAND', 'VISI')){
            return Redirect::to('/candidato/vistosinativos/'.$id);
        } else{
            return Redirect::to('/')->with('flash_msg', 'Seu perfil não tem acesso a essa função.');
        }
    }

    public function postCadastro($id = 0) {

        $campos = Input::all();
        $id_candidato = $campos['id_candidato'];

        $msg = "";
        if (!isset($campos['nome_completo']) || $campos['nome_completo'] == ''){
            $msg .= "<br>O nome do candidato é obrigatório";
        }
        if (!isset($campos['nome_mae']) || $campos['nome_mae'] == ''){
            $msg .= "<br>O nome da mãe é obrigatório";
        }
        
        
        if (isset($campos['id_candidato']) && $campos['id_candidato'] > 0) {
            $cand = Candidato::find($campos['id_candidato']);
            $cand->fill($campos);
            $msg = "Candidato atualizado com sucesso.";
        } else {
            $cand = new Candidato($campos);
            $msg = "Candidato criado com sucesso.";
        }
        if ($cand->dt_validade_cnh == '') {
            $cand->dt_validade_cnh = null;
        }
        if ($cand->dt_validade_ctps == '') {
            $cand->dt_validade_ctps = null;
        }
        if ($cand->dt_emissao_passaporte == '') {
            $cand->dt_emissao_passaporte = null;
        }
        if ($cand->dt_validade_passaporte == '') {
            $cand->dt_validade_passaporte = null;
        }
        if ($cand->dt_nascimento == '') {
            $cand->dt_nascimento = null;
        }
        if ($cand->data_ultima_entrada == '') {
            $cand->data_ultima_entrada = null;
        }
        
        if ($cand->save()) {
            if(!is_object($cand->vistoAtual())){
                $visto = new Visto();
                $visto->id_candidato = $cand->id_candidato;
                $visto->processo_atual = 1;
                $visto->save();
            }
            return Redirect::to('/candidato/cadastro/'.$cand->id_candidato)->with('flash_msg', $msg);
        } else {
            return Redirect::back()
                            ->withInput()
                            ->withErrors($cand->errors)
            ;
        }
    }
    
    public function getDependentes($id) {
        if (\Auth::user()->temAcessoA('CAND', 'DEP')){
            $obj = Candidato::find($id);
            return View::make('candidato/dependentes')
                            ->with('obj', $obj)
                            ->with('qtdCol', '2')
            ;
        } else {
            return Redirect::to('/')->with('flash_msg', 'Seu perfil não tem acesso a essa função.');
        }
    }

    public function postDependentes(){
        $id_candidato_principal = Input::get('id_candidato_principal');
        $repcand = new \Westhead\Repositorios\RepositorioCandidato;
        try{
            $post = Input::all();
            $cand = $repcand->crie($post);
            if ($cand){
                $cand->id_candidato_principal = Input::get('id_candidato_principal');
                $cand->id_parentesco = Input::get('id_parentesco');
                $cand->save();                
                return Redirect::to('/candidato/dependentes/'.$id_candidato_principal)->with('flash_msg', 'Dependente adicionado com sucesso!');
            } else {
                return Redirect::to('/candidato/dependentes/'.$id_candidato_principal)->with('errors', $repcand->getErrors());
            }
        } catch (Exception $ex) {
            return Redirect::to('/candidato/dependentes/'.$id_candidato_principal)->with('flash_msg', $ex->getMessage().$ex->getTraceAsString());
        }
    }

    public function getAnexos($id) {
        if (!\Auth::user()->temAcessoA('CAND', 'ANEX')){
            return Redirect::to('/')->with('flash_msg', 'Seu perfil não tem acesso a essa função.');
        }

        $obj = Candidato::find($id);
        $tipoanexo = array('' => '(não informado)') + TipoAnexo::orderBy('nome_pt_br')->lists('nome_pt_br', 'id_tipo_anexo');
        return View::make('candidato/anexos')
                        ->with('obj', $obj)
                        ->with('qtdCol', '2')
                        ->with('tipoanexo', $tipoanexo)
        ;
    }

    public function getVistoatual($id_candidato, $id_visto = '') {

        if (!\Auth::user()->temAcessoA('CAND', 'VISA')){
            return Redirect::to('/')->with('flash_msg', 'Seu perfil não tem acesso a essa função.');
        }

        $obj = Candidato::find($id_candidato);
        $vistos = \Visto::where('id_candidato', '=', $id_candidato)->get();
        $visto_atual = $obj->vistoAtual();
        if (count($visto_atual) > 0){
            $processos = $visto_atual->processosemordem;
        }
        if ($id_visto == ''){
            if(is_object($visto_atual) && $visto_atual->id_visto > 0){
                $id_visto = $visto_atual->id_visto ;
            }
        }

        return View::make('candidato/vistoatual')
                    ->with('obj', $obj)
                    ->with('qtdCol', '2')
                    ->with('visto_atual', $visto_atual)
                    ->with('vistos', $vistos)
                    ->with('id_visto', $id_visto)
                    ->with('processos', $processos)
        ;
    }

    public function postVistoatual(){
        $visto = \Visto::find(Input::get('id_visto'));
        $visto->observacao = Input::get('observacao');
        $visto->observacao_cliente = Input::get('observacao_cliente');
        $visto->save();
        $cand = \Candidato::find($visto->id_candidato);
        $cand->data_ultima_entrada = Input::get('data_ultima_entrada');
        if ($cand->data_ultima_entrada == ''){
            $cand->data_ultima_entrada = null;
        }
        $cand->save();
        Session::flash('flash_msg', 'Situação do visto atualizada com sucesso');
        return $this->getVistoatual($visto->id_candidato, $visto->id_visto);
    }

    public function getVistosinativos($id_candidato, $id_visto = '') {
        if (!\Auth::user()->temAcessoA('CAND', 'VISI')){
            return Redirect::to('/')->with('flash_msg', 'Seu perfil não tem acesso a essa função.');
        }

        $obj = Candidato::find($id_candidato);
        $vistos = $obj->vistosinativos();
                
        if ($id_visto > 0){
            $visto = \Visto::where('id_visto', '=', $id_visto)->first();
            $processos = $visto->processosemordem;
        } else {
            $visto = new \Visto;
            $visto->id_candidato = $id_candidato;
            $processos = \Processo::where('id_candidato','=', $id_candidato)->whereNull('id_visto')->orderBy('ordem', 'desc')->get();
        }
        return View::make('candidato/vistosinativos')
                        ->with('obj', $obj)
                        ->with('qtdCol', '2')
                        ->with('vistos', $obj->vistosinativos())
                        ->with('visto', $visto)
                        ->with('id_visto', $id_visto)
                        ->with('processos', $processos)
        ;
    }
    
    public function postVistosinativos(){
        $visto = \Visto::find(Input::get('id_visto'));
        $visto->observacao = Input::get('observacao');
        $visto->observacao_cliente = Input::get('observacao_cliente');
        $visto->save();
        Session::flash('flash_msg', 'Situação do visto atualizada com sucesso');
        return $this->getVistosinativos($visto->id_candidato, $visto->id_visto);
    }
    

    public function getDel($id){
        try{
            $cand = \Candidato::find($id);
            if (is_object($cand)){
                $nome_completo = $cand->nome_completo;
                $rep = new \Westhead\Repositorios\RepositorioCandidato();
                $rep->exclua($cand);
                return Redirect::to('candidato')->with('flash_msg', 'Candidato '.$nome_completo.' excluído com sucesso!');
            } else {
                return Redirect::to('candidato')->withErrors('Não foi possível excluir porque o candidato não foi encontrado.');
            }
        } catch (Exception $ex) {
            return Redirect::to('candidato')->withErrors($ex->getMessage());
        }
    }

    public function postStoreos(){
        $repCand = new \Westhead\Repositorios\RepositorioCandidato;
        try{
            $post = Input::all();
            $cand = $repCand->crie($post);
            if ($cand){
                $repOs = new \Westhead\Repositorios\RepositorioOs;
                $os = \Os::findOrFail(Input::get('id_os'));
                $repOs->adicionaCandidato($os, $cand);
                return Redirect::to('/os/candidatos/'.Input::get('id_os'))->with('flash_msg', 'Candidato adicionado com sucesso!');
            } else {
                Input::flash();
                return Redirect::to('/os/candidatos/'.Input::get('id_os'))->with('errors', $repCand->getErrors());
            }
        } catch (Exception $ex) {
            Input::flash();
            return Redirect::back()
                          ->withInput()
                          ->withErrors($ex->getMessage())
            ;
        }

    }


    public function ColunasIndex() {
        $data = array(array("titulo" => "Nome completo", "campo" => "nome_completo")
            , array("titulo" => "Data de nasc.", "campo" => "dt_nascimento")
            , array("titulo" => "# passaporte", "campo" => "nu_passaporte")
            , array("titulo" => "Validade passaporte", "campo" => "dt_validade_passaporte")
        );
        return $data;
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = "Candidatos";
        $data['titulo_grid'] = "Lista";
        $data['subTitulo'] = "Acompanhamento de estrangeiros";
        $data['urlEdit'] = 'candidato/show';
        $data['nomeCampoChave'] = 'id_candidato';
        $data['urlDel'] = 'candidato/del';
        $data['urlCreate'] = 'candidato/create';
        return $data;
    }

    public function postForadaos() {
        $registros = new Candidato;
        $id_os = Input::get('id_os');
        $naOs = Os::find($id_os)->candidatos()->get()->lists('id_candidato');
        if (is_array($naOs) && count($naOs) > 0) {
            $registros = $registros->whereNotIn('id_candidato', $naOs);
        }
        $input = Input::all();
//        if (isset($input['id_empresa']) && $input['id_empresa'] > 0) {
//            $registros = $registros->where('id_empresa', '=', $input['id_empresa']);
//        }
//
//        if (isset($input['id_projeto']) && $input['id_projeto'] > 0) {
//            $registros = $registros->where('id_projeto', '=', $input['id_projeto']);
//        }
//
        if (isset($input['nome_completo']) && $input['nome_completo'] != '') {
            $registros = $registros->where('nome_completo', 'like', '%' . $input['nome_completo'] . '%');
        }

        $registros = $registros->orderBy('nome_completo')
                ->get();
        return View::make('candidato/tabelaBusca')
                        ->with('registros', $registros)
                        ->with('id_os', $id_os)
        ;
    }

    public function postDaos($id_os) {
        $registros = Os::find($id_os)->candidatos()->get();
        return View::make('candidato/tabelaBusca')
                        ->with('modo', 'exclusao')
                        ->with('registros', $registros)
                        ->with('id_os', $id_os)
        ;
    }

    public function postAtualizarparentesco($id_candidato){
        $cand = \Candidato::find($id_candidato);
        $cand->id_parentesco = Input::get('id_parentesco');
        $cand->save();
    }

    public function getRetiradependencia($id_candidato){
        $cand = \Candidato::find($id_candidato);
        $id_candidato_principal = $cand->id_candidato_principal;
        $cand->id_parentesco = null;
        $cand->id_candidato_principal = null;
        $cand->save();
        return Redirect::to('/candidato/show/'.$id_candidato_principal);
    }

    public function getNomes(){
        $nome = Input::get('q');
        $nomes = \Candidato::where('nome_completo', 'like', '%'.str_replace('', '%', $nome).'%')->select(array('nome_completo'))->get()->toJson();
        return $nomes;
    }
    
    public function getNovovisto($id_candidato){
        $cand = \Candidato::find($id_candidato);
        $rep = new \Westhead\Repositorios\RepositorioVisto;
        $visto = $rep->criaNovoVisto($cand);
        Session::flash('flash_msg', 'Visto criado com sucesso!');
        return Redirect::to('/candidato/vistosinativos/'.$id_candidato.'/'.$visto->id_visto);
    }
    /**
     * Atualiza o número da RNE
     * - No processo e no candidato
     */
    public function postAtualizanurne(){
        $ret = array();
        try {
            $campo_chave = Input::get('campo_chave');
            $valor_chave = Input::get('valor_chave');
            $valor = Input::get('valor');
            
            $cand = \Candidato::where($campo_chave, '=', $valor_chave)->first();
            $cand->nu_rne = $valor;
            $cand->update();
            
            $ret["msg"] = array(
                "tipoMsg" => "success"
            );
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
            $ret["msg"] = array(
                "tipoMsg" => "nop"
                , "textoMsg" => $msg
            );
        }
        return json_encode($ret);
    }

    public function getVisacontrol() {
        return $this->visa_control();
    }

    public function postVisacontrol(){
        Input::flash();
        if (Input::get('modo') == 'excel'){
            return $this->getVisacontrolexcel();
        } else {
            return $this->visa_control();
        }
    }

    public function visa_control(){
        $obj = new Candidato;
        $filtros = array();
        $post = array('id_empresa', 'id_projeto', 'nome_completo', 'id_funcao');
        foreach($post as $key ){
            $filtros[$key] = Input::old($key, Session::get($key));
        }

        $empresas = Empresa::where('fl_armador', '=', 0)->orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        if (isset($filtros['id_empresa'])){
            $id_empresa = $filtros['id_empresa'];
        } else {
            $id_empresa = '';
        }
        if ($id_empresa > 0){
            $projetos = Projeto::daEmpresa($id_empresa)->lists('descricao', 'id_projeto');
            $projetos = array('' => '(todas)') + $projetos;
        } else {
            $projetos = array('' => '(selecione a empresa)');
        }
        $empresas = array('0' => '(todas)') +  $empresas;

        $registros = \Candidato::daEmpresa($filtros['id_empresa'])
            ->doProjeto($filtros['id_projeto'])
            ->doNome($filtros['nome_completo'])
            ->daFuncao($filtros['id_funcao'])
            ->orderBy('nome_completo')
            ->paginate(20)
        ;

        return View::make('candidato/visacontrol')
                        ->with('registros', $registros)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
        ;
    }

    public function getVisacontrolexcel(){
        $rep = new \Westhead\Repositorios\RepositorioCandidato;

        $tabela = $rep->relatorio_visa_control_excel(Input::get('id_empresa'), Input::get('id_projeto'), Input::get('nome_completo'), Input::get('id_funcao'));
        $ret = '<style> th{border:solid thin #000; font-weight: bold;} td{border:solid thin #000;} </style>'.$tabela;
        $ret = iconv('UTF-8', 'ISO-8859-1', $ret);

        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel; charset=utf-8',
            'Content-Disposition' => 'attachment; filename="Visacontrol.xls"',
        );
        return Response::make($ret, 200, $headers);
    }
}