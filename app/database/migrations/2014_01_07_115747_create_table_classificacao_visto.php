<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableClassificacaoVisto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('classificacao_visto', function(Blueprint $table) {
			$table->increments('id_classificacao_visto');
            $table->string('sigla');
            $table->string('classificacao');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('classificacao_visto');
	}

}
