<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdOsToProcesso extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('processo', function(Blueprint $table) {
			$table->foreign('id_os')->references('id_os')->on('os');			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('processo', function(Blueprint $table) {
			$table->dropForeign('processo_id_os_foreign');
		});
	}

}
