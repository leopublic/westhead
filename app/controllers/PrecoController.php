<?php
class PrecoController extends \CadastroController {
	const raiz = "tabelapreco";
	const campoChave = 'id_tabela_preco';
	const nomeSingular = 'Tabela de preços';
	const nomePlural = 'Tabelas de preço';

	public function RegistrosIndex()
	{
		$data = TabelaPreco::all();
		return $data;
	}

	public function ColunasIndex()
	{
		$data = array(
				array("titulo" => "Descrição", "campo" => "nome")
			);
		return $data;
	}
	
	public function ObtemInstancia($id = 0)
	{
		if($id > 0){
			$this->obj = TabelaPreco::find($id);
		}
		else{
			$this->obj = new TabelaPreco;
		}
	}
	
	public function TelaIndex()
	{
		$data = array();
		$data['titulo_tela'] = self::nomeSingular;
		$data['titulo_grid'] = "Lista";
		$data['subTitulo'] = "Administrar ".self::nomePlural;
		$data['urlEdit'] = self::raiz.'/show';
		$data['urlDel'] = self::raiz.'/del';
		$data['urlCreate'] = self::raiz.'/create';
		$data['nomeCampoChave'] = self::campoChave;
		return $data;
	}

	public function get_ViewShow()
	{
		return self::raiz.'/show';
	}

	public function MontaCampos()
	{
	//	$this->campos[] = new 
	}

	public function postUpdate($id = 0)
	{
		if($id > 0){
			$obj = TabelaPreco::find($id);
		}
		else{
			$obj = new TabelaPreco();
		}
		$obj->fill(Input::all());

		if($obj->save()){
			return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular.' atualizada com sucesso!');		
		}
		else{
			Input::flash();
			return Redirect::to(self::raiz.'/show/'.$id)->withErrors($obj->errors);			
		}
	}
}