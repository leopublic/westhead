@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Candidatos', 'subTitulo' => ''))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
                {{ Form::open(array("class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'nome_completo', 'label' => 'Nome', 'valor' => Input::old('nome_completo', Session::get('nome_completo')), 'help' => '', 'atributos' => array('autocomplete' => 'off') ,  array()))
                    </div>
                </div>
            </div>
        </div>
                {{ Form::close()}}
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Candidatos</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width:30px">#</th>
                            <th style="width:100px">Ação</th>
                            <th style="width: auto">Nome</th>
                            <th style="width: auto;">Nome mãe</th>
                            <th style="width: auto;">Data nasc.</th>
                            <th style="width: auto;">Passaporte</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $registro)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/candidato/show/'.$registro->id_candidato) }}"><i class="icon-edit"></i></a>
                                @include('html.link_exclusao', array('url' => '/candidato/del/'.$registro->id_candidato, 'msg_confirmacao' => 'Clique para confirmar a exclusão do candidato.<br/><strong>ATENÇÃO: ESSA OPERAÇÃO NÃO PODE SER DESFEITA!</strong>'))
                            </td>
                            <td>{{ $registro->nome_completo }}</td>
                            <td>{{ $registro->nome_mae }}</td>
                            <td>{{ $registro->dt_nascimento }}</td>
                            <td>{{ $registro->nu_passaporte }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script>
    $(document).ready(function() {
        VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajsonfiltro/', '(selecione a empresa)', false);
    });
    
</script>
@stop
