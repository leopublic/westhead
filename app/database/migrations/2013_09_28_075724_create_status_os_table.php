<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatusOsTable extends Migration {
	public function up()
	{
		Schema::create('status_os', function(Blueprint $table) {
			$table->increments('id_status_os');
			$table->string('nome',50);
			$table->string('sigla',50);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('status_os');
	}

}
