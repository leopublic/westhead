<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableReparticaoConsular extends Migration {
	public function up()
	{
        Schema::create('reparticao', function( $table) {
            $table->increments('id_reparticao');
			$table->string('descricao',500);
			$table->string('endereco', 500)->nullable();
			$table->string('cidade', 500)->nullable();
			$table->integer('id_pais')->unsigned()->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reparticao');
	}

}