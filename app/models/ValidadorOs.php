<?php
class ValidadorOs implements InterfaceValidadorModelo{
	public function novoOk($data)
	{
		return Validator::make($data, $this->regrasNovo(), $this->mensagensNovo());
	}

	public function editaOk($data)
	{
		return Validator::make($data, $this->regrasEdita(), $this->mensagensEdita());
	}

	public function regrasNovo()
	{
		return $this->regrasPadrao();
	}

	public function mensagensNovo()
	{
		return $this->mensagensPadrao();
	}

	public function regrasEdita()
	{
		return $this->regrasPadrao();
	}

	public function mensagensEdita()
	{
		return $this->mensagensPadrao();
	}

	public function regrasPadrao()
	{
	}

	public function mensagensPadrao()
	{
	}
}
