<?php
class AnexoTest extends TestCase 
{
    public function testCaminho()
    {
        $anexo = new Anexo;
        $anexo->id_anexo = 1;
        $this->assertEquals('../public/arquivos/anexos/1', $anexo->caminho());
    }

    public function testTamanho()
    {
        $anexo = new Anexo;
        $anexo->id_anexo = 10;
        $this->assertEquals(20, $anexo->tamanho());

    }
}
