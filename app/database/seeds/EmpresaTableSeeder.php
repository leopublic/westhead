<?php

class EmpresaTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
        DB::table('empresa')->truncate();

        $obj = array("razaosocial" => "Acamin Navegação e Serviços Maritimos LTDA.", "fl_armador" => 0);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Asgaard Navegação S/A", "fl_armador" => 0);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Astro Internacional S/A", "fl_armador" => 0);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Astromaritima Navegação S/A", "fl_armador" => 0);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Brasbunker Participações S.A.", "fl_armador" => 0);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Deep Sea Supply Navegação Marítima LTDA.", "fl_armador" => 0);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Galaxia Maritima LTDA.", "fl_armador" => 0);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Geoquasar Energy Solutions Participações LTDA.", "fl_armador" => 0);
        DB::table('empresa')->insert($obj);

        $obj = array("razaosocial" => "Havila Shipping", "fl_armador" => 1);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Emas Marine", "fl_armador" => 1);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Hartmann", "fl_armador" => 1);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Hornbeck", "fl_armador" => 1);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Maridive", "fl_armador" => 1);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Varada", "fl_armador" => 1);
        DB::table('empresa')->insert($obj);
        $obj = array("razaosocial" => "Vega Offshore", "fl_armador" => 1);
        DB::table('empresa')->insert($obj);
    }

}