<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('status_os')->truncate();
        DB::table('projeto')->delete();

		$this->call('EstadocivilTableSeeder');
		Eloquent::unguard();
		$this->call('EmpresaTableSeeder');
		Eloquent::unguard();
		$this->call('CandidatoTableSeeder');
		Eloquent::unguard();
		$this->call('ProjetoTableSeeder');
		// $this->call('UserTableSeeder');
		$this->call('TipoServicoTableSeeder');
		$this->call('ServicoTableSeeder');
		$this->call('PaisTableSeeder');
		$this->call('UsuarioTableSeeder');
		$this->call('ReparticaoTableSeeder');
		$this->call('FuncaoTableSeeder');
		$this->call('ProfissaoTableSeeder');
		$this->call('EscolaridadeTableSeeder');
		$this->call('TipoDespesaTableSeeder');
		$this->call('ExtensaoTableSeeder');
		$this->call('TipoAnexoTableSeeder');
		$this->call('PerfilTableSeeder');

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}