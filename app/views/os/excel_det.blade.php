@if ($registro->id_status_os == 2)
<tr style="background-color: #36c77b;" name="os{{$registro->id_os}}">
    @elseif ($registro->id_status_os == 3)
<tr style="background-color: #d3d3d3; color:#666;" name="os{{$registro->id_os}}">
    @else
<tr name="os{{$registro->id_os}}">
    @endif
    <td style="vertical-align: top; border:solid 1px #000;"><? print $i; ?></td>
    <td style="vertical-align: top; border:solid 1px #000;">{{ substr('000000'.$registro->numero, -6) }}</td>
    <td style="vertical-align: top; border:solid 1px #000;"> 
    @if (is_object($registro->empresa))
        {{$registro->empresa->razaosocial }}
    @else
        --
    @endif
    </td>
    <td style="vertical-align: top; border:solid 1px #000;">
    @if (is_object($registro->projeto))
        {{ $registro->projeto->descricao }}
    @else
        --
    @endif
    </td>
    <td style="vertical-align: top; border:solid 1px #000;">
    @if (is_object($registro->armador))
        {{ $registro->armador->razaosocial }}
    @else
        --
    @endif
    </td>
    <td style="vertical-align: top; border:solid 1px #000;">
        {{$registro->getNomesCandidatos()}}
    </td>
    <td style="vertical-align: top; border:solid 1px #000;">
    @if (is_object($registro->servico))
        {{ $registro->servico->descricao  }}
    @else
        --
    @endif
    </td>
    <td style="vertical-align: top; border:solid 1px #000;">{{$registro->dt_solicitacao}}</td>
    <td style="vertical-align: top; border:solid 1px #000;">
        {{$registro->dt_execucao}}
    </td>
    <td style="vertical-align: top; border:solid 1px #000;">
    @if (is_object($registro->statusos))
        {{$registro->statusos->nome}}
    @else
        --
    @endif
    </td>
</tr>
