<?php

class StatusAndamentoController extends \CadastroController {

    const raiz = "statusandamento";
    const campoChave = 'id_status_andamento';
    const nomeSingular = 'Status andamento';
    const nomePlural = 'Status andamento';
    const artigo = 'o';

    public function RegistrosIndex() {
        $data = StatusAndamento::orderBy('nome')
                ->get();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(
            array("titulo" => "Descrição", "campo" => "nome"),
            array("titulo" => "Encerra OS?", "campo" => "fl_encerra"),
        );
        return $data;
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = StatusAndamento::find($id);
        } else {
            $this->obj = new StatusAndamento;
        }
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = self::nomeSingular;
        $data['titulo_grid'] = "Lista";
        $data['subTitulo'] = "Administrar " . self::nomePlural;
        $data['urlEdit'] = self::raiz . '/show';
        $data['urlDel'] = self::raiz . '/del';
        $data['urlCreate'] = self::raiz . '/create';
        $data['nomeCampoChave'] = self::campoChave;
        $data['menu'] = 'menuTabelas';
        $data['item'] = 'itemStatusAndamento';
        return $data;
    }

    public function get_ViewShow() {
        return self::raiz . '/show';
    }

    public function MontaCampos() {
        //	$this->campos[] = new
    }

    public function getDel($id = 0) {
        try {
            StatusAndamento::destroy($id);
            return Redirect::to(self::raiz)
                            ->with('flash_msg', self::nomeSingular . ' excluíd' . self::artigo . ' com sucesso!');
        } catch (Exception $e) {
            return Redirect::to(self::raiz)
                            ->with('flash_error', $e->getMessage());
        }
    }

    public function postUpdate($id = 0) {
        if ($id > 0) {
            $obj = StatusAndamento::find($id);
        } else {
            $obj = new StatusAndamento();
        }
        $obj->fill(Input::all());
        if ($obj->nome == '') {
            Input::flash();
            return Redirect::to(self::raiz . '/show/' . $id)->withErrors('Informe a descrição do status');
        } else {
            if ($obj->save()) {
                return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular . ' atualizad' . self::artigo . ' com sucesso!');
            } else {
                Input::flash();
                return Redirect::to(self::raiz . '/show/' . $id)->withErrors($obj->errors);
            }
        }
    }

}
