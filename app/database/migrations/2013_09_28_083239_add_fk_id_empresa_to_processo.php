<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdEmpresaToProcesso extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('processo', function(Blueprint $table) {
			$table->foreign('id_empresa')->references('id_empresa')->on('empresa');			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('processo', function(Blueprint $table) {
			$table->dropForeign('processo_id_empresa_foreign');
		});
	}

}
