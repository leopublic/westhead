<?php

/**
 * @property string $descricao Nome do país
 * @property string $descricao_en Nome do país em ingles
 * @property string $descricao_resumida Nome do país resumido 
 * @property string $descricao_resumida_en Nome do país resumido em ingles
 * @property string $nacionalidade Nacionalidade do país
 * @property string $nacionalidade_en Nacionalidade do país em ingles
 * @property string $codigo_pf Código do país no formulário da PF
 * @property datetime $created_at Data em que o país foi cadastrado
 * @property datetime $updated_at Data em que foi atualizado
 */
class Pais extends Modelo {

    protected $table = 'pais';
    protected $primaryKey = 'id_pais';
    protected $guarded = array();

    public function assumeDefaults() {
        
    }

    public static function regras() {
        return array(
            'descricao' => 'required'
        );
    }

    public static function mensagens() {
        return array(
            'descricao.required' => 'A descrição é obrigatória!'
        );
    }

}
