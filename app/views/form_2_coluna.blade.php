@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Layout</h1>
                        <h4>Simple form element, griding and layout</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Layout</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-green">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Tow Column Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="#" class="form-horizontal">
                                    <div class="row">
                                       <div class="col-md-6 ">
                                          <!-- BEGIN Left Side -->
                                            <div class="form-group">
                                                <label for="textfield1" class="col-xs-3 col-lg-2 control-label">Text input</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                    <input type="text" name="textfield1" id="textfield1" placeholder="Text input" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="password1" class="col-xs-3 col-lg-2 control-label">Password</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                    <input type="password" name="password1" id="password1" placeholder="Password input" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-lg-2 control-label">Checkboxes</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                    <label class='checkbox'>
                                                        <input type="checkbox" name="checkbox"> Checkbox 1
                                                    </label>
                                                    <label class='checkbox'>
                                                        <input type="checkbox" name="checkbox"> Checkbox 2
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textarea1" class="col-xs-3 col-lg-2 control-label">Textarea</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                    <textarea name="textarea1" id="textarea1" rows="5" class="form-control"></textarea>
                                                </div>
                                            </div>
                                          <!-- END Left Side -->
                                       </div>
                                       <div class="col-md-6 ">
                                          <!-- BEGIN Right Side -->
                                            <div class="form-group">
                                                <label for="textfield2" class="col-xs-3 col-lg-2 control-label">Text input</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                    <input type="text" name="textfield2" id="textfield2" placeholder="Text input" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="password2" class="col-xs-3 col-lg-2 control-label">Password</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                    <input type="password" name="password2" id="password2" placeholder="Password input" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-lg-2 control-label">Checkboxes</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                    <label class='checkbox'>
                                                        <input type="checkbox" name="checkbox"> Checkbox 1
                                                    </label>
                                                    <label class='checkbox'>
                                                        <input type="checkbox" name="checkbox"> Checkbox 2
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textarea2" class="col-xs-3 col-lg-2 control-label">Textarea</label>
                                                <div class="col-sm-9 col-lg-10 controls">
                                                    <textarea name="textarea2" id="textarea2" rows="5" class="form-control"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                                   <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Save</button>
                                                   <button type="button" class="btn">Cancel</button>
                                                </div>
                                            </div>
                                          <!-- END Right Side -->
                                       </div>
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>
@stop