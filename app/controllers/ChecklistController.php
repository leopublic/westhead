<?php

class ChecklistController extends BaseController {

    protected $campos;
    protected $obj;

    public function getGere($nome, $id_os){
        $os = Os::find($id_os);
        $candidato = $os->candidatos;
        $html = View::make('checklist.'.$nome)
                        ->with('os', $os)
                        ->with('candidatos', $candidato)
        ;

        $mpdf =new mPDF('utf-8', 'A4', '','','20','20','20','20','0','0');		// largura x altura
        $mpdf->allow_charset_conversion = false;
        $html = iconv("UTF-8","UTF-8//IGNORE",$html);
        $mpdf->WriteHTML($html);
		return $mpdf->Output();
    }
}