<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdCandidatoToProcesso extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('processo', function(Blueprint $table) {
			$table->foreign('id_candidato')->references('id_candidato')->on('candidato');			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('processo', function(Blueprint $table) {
			$table->dropForeign('processo_id_candidato_foreign');
		});
	}

}
