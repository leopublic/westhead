<table class="table table-striped table-hover fill-head">
    <thead>
        <tr>
            <th style="width: 40px">#</th>
            <th style="width: 80px">Ação</th>
            <th style="width: auto">Nome</th>
            <th style="width: auto;">Tipo</th>
        </tr>
    </thead>
    <tbody>
    @if(count($anexos) > 0)
        <? $i = 0; ?>
        @foreach($anexos as $anexo)
        <? $i++; ?>
        <tr>
            <td><? print $i; ?></td>
            <td>
                @include('html.link_exclusao', array('url' => '/anexo/remover/'.$anexo->id_anexo, 'msg_confirmacao' => 'Clique para confirmar a exclusão do anexo'))
                </div>
            </td>
            <td><a href="{{ URL::to('/anexo/download/'.$anexo->id_anexo)}}" @if(!$anexo->existe()) style="color:red;text-decoration: line-through; " title="Arquivo não encontrado" @endif>{{ $anexo->nome_original }}</a></td>
            <td>{{ Form::select('id_tipo_anexo', $tipoanexo, $anexo->id_tipo_anexo, array('onChange' => 'AtualizaTipoAnexo(\''.$anexo->id_anexo.'\', this)') ) }}</td>
        </tr>
        @endforeach
    @else
    <tr><td colspan="4">(nenhum anexo informado)</td></tr>
    @endif
    </tbody>
</table>