<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableEmpresa extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('empresa', function($table) {
            $table->increments('id_empresa');
			$table->string('razaosocial',500);
			$table->string('nomefantasia', 500)->nullable();
			$table->string('cnpj', 30)->nullable();
			$table->string('insc_estadual', 20)->nullable();
			$table->string('insc_municipal', 20)->nullable();			
			$table->boolean('fl_ativo')->nullable();
			$table->boolean('fl_armador')->nullable();	
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empresa');
	}

}