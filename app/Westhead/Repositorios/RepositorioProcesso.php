<?php

namespace Westhead\Repositorios;

class RepositorioProcesso {

    public function recuperaProcessoDaOs(\Os $os, \Candidato $candidato) {
        $processo = Processo::where('id_os', '=', $os->id_os)
                        ->where('id_candidato', '=', $candidato->id_candidato)
                        ->first()
                        ;

        if (is_object($processo)){
            return $processo;
        } else {
            return  $this->criaProcessoDaOs($os, $candidato);
        }
    }
    /**
     * Cria ou recupera o processo 
     * 
     * @param  \Os        $os        [description]
     * @param  \Candidato $candidato [description]
     * @return [type]                [description]
     */
    public function criaProcessoDaOs(\Os $os, \Candidato $candidato){
        // Verifica se já existe um processo da OS
        $processo = \Processo::where('id_os', '=', $os->id_os)
                        ->where('id_candidato', '=', $candidato->id_candidato)
                        ->first();
        if (is_object($processo)){
            $processo->id_tipoprocesso = $os->servico->id_tipo_servico;
            if ($processo->id_tipoprocesso == 2){
                $processo->id_tipoprocesso = 1;
            }
            $processo->id_servico = $os->id_servico;
            $processo->save();
        } else {
            if (is_object($os->servico)){
                $repV = new RepositorioVisto;
                $processo = $this->criaPeloTipoServico($os->servico->id_tipo_servico);
                if(isset($processo)){
                    $processo->os()->associate($os);
                    $processo->candidato()->associate($candidato);
                    $processo->recupereDadosDaOs();
                    $processo->save();

                    if ($os->servico->tiposervico->fl_cria_novo_visto){
                        $visto = $repV->abre_novo_visto($candidato, "Abertura da OS ".$os->numero);
                        $visto->id_processo_principal = $processo->id_processo;
                    } else {
                        $visto = $repV->recuperaVistoAtual($candidato);
                    }
                    $processo->id_visto = $visto->id_visto;
                    $visto->save();
                    $processo->save();
                }
            } else {
                return null;
            }
        }
        return $processo;
    }

    public function criaProcessoNoVisto($id_tipo_servico, $id_visto){
        $processo = $this->criaPeloTipoServico($id_tipo_servico);
        if(isset($processo)){
            $visto = \Visto::find($id_visto);
            $processo->candidato()->associate($visto->candidato);
            $processo->visto()->associate($visto);
            $processo->save();
            return $processo;
        } else {
            return null;
        }
    }

    public function criaPeloTipoServico($id_tipo_servico){
        switch ($id_tipo_servico){
            case 1:
            case 2:
                $processo = new \ProcessoAutorizacao();
                $processo->id_tipoprocesso = 1;
                break;
            case 3:
                $processo = new \ProcessoRegistro();
                $processo->id_tipoprocesso = 3;
                break;
            case 4:
                $processo = new \ProcessoColetaVisto();
                $processo->id_tipoprocesso = 4;
                break;
            case 5:
                $processo = new \ProcessoProrrogacao();
                $processo->id_tipoprocesso = 5;
                break;
            case 6:
                $processo = new \ProcessoOutros();
                $processo->id_tipoprocesso = 6;
                break;
            case 7:
                $processo = new \ProcessoAtendimento();
                $processo->id_tipoprocesso = 7;
                break;
            case 8:
                $processo = new \ProcessoCancelamento();
                $processo->id_tipoprocesso = 8;
                break;
            case 9:
                $processo = new \ProcessoColeta();
                $processo->id_tipoprocesso = 9;
                break;
            case 10:
                $processo = new \ProcessoTraducao();
                $processo->id_tipoprocesso = 10;
                break;
            default:
                $processo = null;
        }
        if(isset($processo)){
            return $processo;
        } else {
            return null;
        }
    }

    public function vinculaProcessoAoVisto(\Processo $processo, \Visto $visto){
        $processo->visto()->associate($visto);
        $processo->save();
    }

    /**
     * Manda um processo para cima (aumenta a ordem)
     * @param type $processo
     */
    public function paraCima($processo){
        $processo->ordem = $processo->ordem + 15;
        $processo->save();
        $processo->visto->reordeneProcessos();        
    }

    /**
     * Manda um processo para baixo (diminui a ordem)
     * @param type $processo
     */
    public function paraBaixo($processo){
        $processo->ordem = $processo->ordem - 15;
        $processo->save();
        $processo->visto->reordeneProcessos();
    }
    
    public function reordenaDefault(){
        $vistos = \Visto::all();
        
        foreach($vistos as $visto){
            $processos = \Processo::where('id_visto', '=', $visto->id_visto)
                        ->orderBy('id_processo')
                        ->get();
            $ordem = 10;
            foreach($processos as $processo){
                $processo->ordem = $ordem;
                $processo->save();
                $ordem = $ordem + 10;
            }
        }
    }

    public function atualiza_status_andamento($processo, $id_status_andamento, $obs_andamento, $id_usuario, $data = ''){
        if ($processo->servico->fl_andamento == 1){
            if ($processo->id_status_andamento != $id_status_andamento){

                $hist = new \HistoricoAndamento;
                $hist->id_processo = $processo->id_processo;
                $hist->id_usuario = $id_usuario;
                $hist->obs_andamento = $obs_andamento;

                $tz_object = new \DateTimeZone('America/Sao_Paulo');
                $datetime = new \DateTime();
                $datetime->setTimezone($tz_object);
                if ($data == ''){
                    $hist->datahora = $datetime->format('Y-m-d H:i:s');
                } else {
                    $hist->datahora = Carbon::createFromFormat('d/m/Y', $data)->format('Y-m-d');
                }

                \Log::info($hist->datahora);
                $hist->id_status_andamento = $id_status_andamento;
                $hist->save();

                $processo->id_status_andamento = $id_status_andamento;
                $processo->id_historico_andamento_atual = $hist->id_historico_andamento;
                $processo->save();
            }
        }
    }

    public function atualiza_status_andamento_os($id_os, $id_status_andamento, $obs_andamento, $id_usuario, $data = ''){
        $processos = \Processo::where('id_os', '=', $id_os)->get();
        \log::info('Processos encontrados: '.$processos->count());
        foreach($processos as $processo){
            \log::info('processando processo 1 '.$processo->id_processo);
            $hist = new \HistoricoAndamento;
            $hist->id_processo = $processo->id_processo;
            $hist->id_usuario = $id_usuario;
            $hist->obs_andamento = $obs_andamento;

            $tz_object = new \DateTimeZone('America/Sao_Paulo');
            $datetime = new \DateTime();
            $datetime->setTimezone($tz_object);
            if ($data == ''){
                $hist->datahora = $datetime->format('Y-m-d H:i:s');
            } else {
                $hist->datahora = \Carbon::createFromFormat('d/m/Y', $data)->format('Y-m-d');
            }

            \Log::info($hist->datahora);
            $hist->id_status_andamento = $id_status_andamento;
            $hist->save();


            $processo->id_status_andamento = $id_status_andamento;
            $processo->id_historico_andamento_atual = $hist->id_historico_andamento;
            $processo->save();
        }
    }

    public function corrige_vinculos_todos_vistos(){
        set_time_limit(0);
        $vistos = \Visto::get();
        foreach($vistos as $visto){
            $this->corrige_vinculos_visto($visto);
        }
    }

    public function corrige_vinculos_visto($visto){
        $processos = \Processo::where('id_visto', '=', $visto->id_visto)->orderBy('ordem')->get();
        $id = null;
        foreach($processos as $processo){
            if ($processo->id_tipoprocesso == 1){
                $id = $processo->id_processo;
            } else {
                if ($id){
                    $processo->id_processo_pai = $id;
                    $processo->save();
                    print "<br/>Processo corrigido ".$processo->id_processo." (tipo ".$processo->id_tipoprocesso.")";
                }
            }
        }

        $processos = \Processo::where('id_visto', '=', $visto->id_visto)->orderBy('ordem', 'desc')->get();
        $tipos = array();
        foreach($processos as $processo){
            if ($processo->id_tipoprocesso != 1 && !array_key_exists($processo->id_tipoprocesso, $tipos)){
                $tipos[$processo->id_tipoprocesso] = 'ok';
                $processo->fl_mais_recente = 1;
                $processo->save();
            }
        }
    }

    public function corrige_vinculos_todos_processos(){
        set_time_limit(0);
        // Seleciona os candidatos que tem processos.
        $sql = "select distinct id_candidato from processo";
        $id_candidatos = \DB::select(\DB::raw($sql));
        foreach($id_candidatos as $id_candidato){
            $this->corrige_processos_do_candidato($id_candidato->id_candidato);
        }

    }

    public function corrige_processos_do_candidato($id_candidato){
        print "<br/>Processando candidato ".$id_candidato;
        $cand = \Candidato::find($id_candidato);
        //
        // Deleta vistos do candidato
        \Visto::where('id_candidato', '=', $id_candidato)->delete();
        //
        // Zera vistos dos processos
        \Processo::where('id_candidato', '=', $id_candidato)->update(array('id_visto' => null));
        //
        // Cria o visto atual
        $visto_atual = new \Visto;
        $visto_atual->id_candidato = $id_candidato;
        $visto_atual->processo_atual = 1;
        $visto_atual->observacao = 'Visto atual criado pelo sistema ('.date('Y-m-d H:i:s').')';
        $visto_atual->save();
        //
        // Cria o visto histórico
        $visto_historico = new \Visto;
        $visto_historico->id_candidato = $id_candidato;
        $visto_historico->observacao = 'Visto historico criado pelo sistema ('.date('Y-m-d H:i:s').')';
        $visto_historico->save();
        //
        // Seleciona os processos de autorização pela data de solicitação da OS.
        $sql = "select id_processo, dt_solicitacao from processo, os where (processo.id_tipoprocesso=1 or processo.id_tipoprocesso=5) and  os.id_os = processo.id_os and processo.id_candidato = ".$id_candidato." order by os.dt_solicitacao desc";
        $processos = \DB::select(\DB::raw($sql));
        if (count($processos) > 0){
            $id_processo = $processos[0]->id_processo;
            $dt_solicitacao = $processos[0]->dt_solicitacao;
            $processo_pai = \Processo::find($id_processo);
            // Associa o processo ao visto
            $processo_pai->id_visto = $visto_atual->id_visto;
            $processo_pai->ordem = 10;
            $visto_atual->id_processo_principal = $processo_pai->id_processo;
            $processo_pai->save();

            $visto_atual->save();
            // Associa todos os outros ao visto antigo. 
            \Processo::where('id_candidato', '=', $id_candidato)
                     ->where('id_tipoprocesso', '=', 1)
                     ->where('id_processo', '<>', $processo_pai->id_processo)
                     ->update(array('id_visto' => $visto_historico->id_visto));
            // Associa os processos que não forem de autorização, posteriores à autorização ao visto atual
            $sql = "select id_processo, dt_solicitacao 
                      from processo, os
                     where os.id_os = processo.id_os
                       and processo.id_candidato = ".$id_candidato."
                       and dt_solicitacao >= '".$dt_solicitacao."' order by dt_solicitacao ";
            $processos = \DB::select(\DB::raw($sql));
            $ordem = 10;
            foreach($processos as $processo){
                $ordem = $ordem + 10;
                \Processo::where('id_processo', '=', $processo->id_processo)->update(array('id_visto' => $visto_atual->id_visto, 'ordem' => $ordem));
            }
            // define os atuais
            $sql = "select id_processo, id_tipoprocesso
                      from processo, os
                     where os.id_os = processo.id_os
                       and processo.id_candidato = ".$id_candidato."
                       and dt_solicitacao >= '".$dt_solicitacao."'
                     order by dt_solicitacao desc";
            $processos = \DB::select(\DB::raw($sql));
            $tipos = array();
            foreach($processos as $processo){
                if ($processo->id_tipoprocesso != 1 && !array_key_exists($processo->id_tipoprocesso, $tipos)){
                    \Processo::where('id_processo', '=', $processo->id_processo)->update(array('fl_mais_recente' => 1, 'id_processo_pai' => $processo_pai->id_processo));
                    $tipos[$processo->id_tipoprocesso] = 'ok';
                }
            }
            // Associa os processos anteriores ao visto histórico
            $where = " id_candidato = ".$id_candidato." and id_os in (select id_os from os where dt_solicitacao < '".$dt_solicitacao."')";
            \Processo::whereRaw($where)
                     ->update(array('id_visto' => $visto_historico->id_visto));
        }
    }

    public function corrige_mais_recente_do_tipo($processo){

    }
}
