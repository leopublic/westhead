@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Criar nova ordem de serviço', 'subTitulo' => ''))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-reorder"></i> Dados da OS</h3>
            </div>
            <div class="box-content">
                {{ Form::open(array('url' => 'os/store', "class"=>"form-horizontal")) }}
                
                <div class="form-group">
                    {{Form::label('tipopessoa', 'Tipo pessoa', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label')) }}
                    <div class="col-sm-8 col-md-10 col-lg-10 controls">
                        {{ Form::select('tipopessoa', array('F' => 'Física', 'J' => 'Jurídica'), Input::old('tipopessoa', 'J'), $attributes = array('id' => 'tipopessoa', 'class' => 'form-control')); }}
                    </div>
                </div>
                <div id="J">
                    @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => $obj->id_empresa, 'valores' => $empresas , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Projeto', 'valor' => $obj->id_projeto, 'valores' => $projetos , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_empresa_armador', 'label' => 'Armador', 'valor' => $obj->id_armador, 'valores' => $armadores , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_centro_de_custo', 'label' => 'Centro de custo', 'valor' => $obj->id_centro_de_custo, 'valores' => $centros , 'help' => '', 'valores', array()))
                    @include('html/campo-select', array('id' => 'id_empresa_intermediaria', 'label' => 'Empresa intermediária', 'valor' => $obj->id_empresa_intermediaria, 'valores' => $intermediarias , 'help' => '', 'valores', array()))
                </div>

                <div id="F">
                    @include('html/campo-texto', array('id' => 'nomecompleto', 'label' => 'Nome completo', 'valor' => $obj->nomecompleto,  'help' => '', 'valores', array()))
                    @include('html/campo-texto', array('id' => 'cpf', 'label' => 'CPF', 'valor' => $obj->cpf,  'help' => '', 'valores', array()))
                </div>
                @include('html/campo-data', array('id' => 'dt_solicitacao', 'label' => 'Solicitada em', 'valor' => $obj->dt_solicitacao, 'help' => '', array()))
                @include('html/campo-data', array('id' => 'dt_execucao', 'label' => 'Executada em', 'valor' => $obj->dt_execucao, 'help' => '', array()))
                @include('html/campo-data', array('id' => 'dt_assinatura', 'label' => 'Assinada em', 'valor' => $obj->dt_assinatura, 'help' => '', array()))
                    <div class="form-group">
                        {{Form::label('id_servico', 'Serviço', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label')) }}
                        <div class="col-sm-8 col-md-10 col-lg-10 controls">
                            {{ Form::select('id_servico',$servicos , Input::old('id_servico', $obj->id_servico), $attributes = array('class' => 'form-control chosen')); }}
                            @if(isset($help))
                            <span class="help-inline">{{ $help }}</span>
                            @endif
                        </div>
                    </div>
                @include('html/campo-texto', array('id' => 'prazo_solicitado', 'label' => 'Prazo solicitado', 'valor' => $obj->prazo_solicitado, 'help' => 'O prazo solicitado, conforme deve aparecer no formulário', array()))
                @include('html/campo-texto', array('id' => 'solicitante', 'label' => 'Solicitante', 'valor' => $obj->solicitante,  'help' => '', 'valores', array()))
                @include('html/campo-texto', array('id' => 'motorista', 'label' => 'Motorista', 'valor' => $obj->motorista,  'help' => '', 'valores', array()))
                @include('html/campo-texto', array('id' => 'dados_do_voo', 'label' => 'Dados do vôo', 'valor' => $obj->dados_do_voo,  'help' => '', 'valores', array()))
                @include('html/campo-texto', array('id' => 'local_trabalho', 'label' => 'Local de trabalho', 'valor' => $obj->local_trabalho,  'help' => '', array()))
                @include('html/campo-textarea', array('id' => 'observacao', 'label' => 'Observação', 'valor' => $obj->observacao,  'help' => '', 'valores', array()))
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4 col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                        {{ HTML::link('os', 'Cancelar', array('class' => 'btn')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tipopessoa')
            .change(function() {
                var id = $('#tipopessoa').val();
                if (id == 'J') {
                    $('#J').show();
                    $('#F').hide();
                } else {
                    $('#J').hide();
                    $('#F').show();
                }
            })
            .change();
    });
    
    $(document).ready(function() {
        $(document).ready(function() {
            VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajson/', '(selecione a empresa)', true);
            VinculaCombos('id_empresa', 'id_centro_de_custo', '/centrodecusto/daempresajson/', '(selecione a empresa)', true);
        });
    });


</script>

@stop