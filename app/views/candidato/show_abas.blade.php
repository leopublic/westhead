<ul class="nav nav-tabs " style="float:left">
    @if(\Auth::user()->temAcessoA('CAND', 'CAD'))
        @if ($ativa == 'Cadastro')
            <li class="active"><a href="#" ><i class="icon-tags"></i> Cadastro</a></li>
        @else
            <li class=""><a href="/candidato/cadastro/{{$obj->id_candidato}}" ><i class="icon-tags"></i> Cadastro</a></li>
        @endif
    @endif
    @if($obj->id_candidato > 0)
        @if(\Auth::user()->temAcessoA('CAND', 'DEP'))
            @if (intval($obj->id_candidato_principal) == 0)
                @if ($ativa == 'Dependentes')
                    <li class="active"><a href="#" ><i class="icon-group"></i> Dependentes ({{$obj->qtd_dependentes}})</a></li>
                @else
                    <li class=""><a href="/candidato/dependentes/{{$obj->id_candidato}}" ><i class="icon-group"></i> Dependentes ({{$obj->qtd_dependentes}})</a></li>
                @endif
            @endif
        @endif

        @if(\Auth::user()->temAcessoA('CAND', 'ANEX'))
            @if ($ativa == 'Anexos')
                <li class="active"><a href="#" ><i class="icon-paper-clip"></i> Anexos ({{$obj->qtd_anexos}})</a></li>
            @else
                <li class=""><a href="/candidato/anexos/{{$obj->id_candidato}}" ><i class="icon-paper-clip"></i> Anexos ({{$obj->qtd_anexos}})</a></li>
            @endif
        @endif

        @if(\Auth::user()->temAcessoA('CAND', 'VISA'))
            @if ($ativa == 'Visto atual')
                <li class="active"><a href="#" ><i class="icon-folder-open"></i> Visto atual</a></li>
            @else
                <li class=""><a href="/candidato/vistoatual/{{$obj->id_candidato}}" ><i class="icon-folder-open"></i> Visto atual</a></li>
            @endif
        @endif

        @if(\Auth::user()->temAcessoA('CAND', 'VISI'))
            @if ($ativa == 'Vistos inativos')
                <li class="active"><a href="#" ><i class="icon-time"></i> Vistos inativos ({{$obj->qtd_vistos_inativos}})</a></li>
            @else
                <li class=""><a href="/candidato/vistosinativos/{{$obj->id_candidato}}" ><i class="icon-time"></i> Vistos inativos ({{$obj->qtd_vistos_inativos}})</a></li>
            @endif
        @endif

    @endif

</ul>
