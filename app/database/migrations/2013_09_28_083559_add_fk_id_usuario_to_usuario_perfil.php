<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdUsuarioToUsuarioPerfil extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuario_perfil', function(Blueprint $table) {
			$table->foreign('id_usuario')->references('id_usuario')->on('usuario');			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuario_perfil', function(Blueprint $table) {
			$table->dropForeign('usuario_perfil_id_usuario_foreign');
		});
	}

}
