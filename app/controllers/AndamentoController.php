<?php

class AndamentoController extends \ProcessoGenericoController {

    public function getIndex() {
        return $this->listaAndamento();
    }

    public function postIndex() {
        Input::flash();
        return $this->listaAndamento();
    }

    public function listaAndamento($id_os = '') {
        set_time_limit(0);
        $pag = Input::get('pag', 0);
        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", empresa.razaosocial"
                . ", empresa.apelido"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.string_1"
                . ", date_format(processo.date_1, '%d/%m/%Y') date_1"
                . ", date_format(historico_andamento.datahora, '%d/%m/%Y') datahora"
                . ", status_andamento.nome nome_status_andamento";
        $from = " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join processo on processo.id_os = os.id_os "
                . " left join status_andamento on status_andamento.id_status_andamento = processo.id_status_andamento"
                . " left join historico_andamento on historico_andamento.id_historico_andamento = processo.id_historico_andamento_atual"
                . " where servico.fl_andamento = 1"
                . " and coalesce(processo.string_1, '') <> '' "
                . " and coalesce(status_andamento.fl_encerra, 0) = 0 ";
        $filtrou = false;
        $where = '';
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $where .= " and os.id_empresa = " . Input::get('id_empresa', Input::old('id_empresa'));
            $filtrou = true;
        }
        if (Input::get('id_projeto', Input::old('id_projeto')) > 0) {
            $where .= " and os.id_projeto = " . Input::get('id_projeto', Input::old('id_projeto'));
            $filtrou = true;
        }
        if (Input::get('id_status_andamento', Input::old('id_status_andamento')) > 0) {
            $where .= " and processo.id_status_andamento = " . Input::get('id_status_andamento', Input::old('id_status_andamento'));
            $filtrou = true;
        }
        if (Input::get('numero', Input::old('numero')) > 0) {
            $where .= " and os.numero = " . Input::get('numero', Input::old('numero'));
            $filtrou = true;
        }
        if ($id_os > 0) {
            $where .= " and os.id_os= " . $id_os;
            $filtrou = true;
        }

        $sql_qtd = "select count(*) qtd ".$from.$where;
        $qtd_reg = \DB::select(\DB::raw($sql_qtd));
        $qtd = $qtd_reg[0]->qtd;
        $qtd_pag = $qtd/ 20;
                $offset = $pag * 20;
        $sql_Raw = $sql.$from.$where.' order by id_os desc limit '.$offset.' , 20';
        if ($filtrou) {
            $registros = \DB::select(\DB::raw($sql_Raw));
        } else {
            $registros = null;
        }

        $empresas = array('' => '(todas)') + \Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        $servicos = array('' => '(todos)') + \Servico::where('fl_andamento', '=', '1')->orderBy('descricao')->lists('descricao', 'id_servico');
        $status_andamento = array('' => '(todos)') + \StatusAndamento::orderBy('nome')->lists('nome', 'id_status_andamento');
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $projetos = \Projeto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))
                    ->orderBy('descricao')
                    ->lists('descricao', 'id_projeto');
            $projetos = array('' => '(todos)') + $projetos;
        } else {
            $projetos = array();
        }
        return View::make('processo.andamento')
                        ->with('registros', $registros)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
                        ->with('servicos', $servicos)
                        ->with('status_andamento', $status_andamento)
                        ->with('pag', $pag)
                        ->with('qtd_pag', $qtd_pag)
        ;
    }


}
