<?php

namespace Westhead\Repositorios;

class RepositorioVisto {

    /**
     * Recupera ou cria um visto atual para o candidato informado
     * @param type $candidato
     * @return \Visto
     */
    public function recuperaVistoAtual($candidato) {
        $visto = \Visto::where('id_candidato', '=', $candidato->id_candidato)
                ->where('processo_atual', '=', 1)
                ->first()
        ;
        if (isset($visto)) {
            return $visto;
        } else {
            $visto = $this->criaNovoVisto($candidato);
            $visto->processo_atual = true;
            $visto->save();
            return $visto;
        }
    }
    /**
     * 
     * @param  [type] $candidato [description]
     * @return [type]            [description]
     */
    public function criaNovoVisto($candidato){
        $visto = new \Visto;
        $visto->id_candidato = $candidato->id_candidato;
        $visto->processo_atual = false;
        $visto->save();
        return $visto;
    }

    /**
     * Arquiva o visto atual e cria um novo vazio no candidato
     * @param  [type] $candidato [description]
     * @return [type]            [description]
     */
    public function abre_novo_visto($candidato, $obs = '(n/a)'){
        $vistoAtual = \Visto::where('id_candidato', '=', $candidato->id_candidato)
                ->where('processo_atual', '=', 1)
                ->first()
        ;
        // Se encontrou algum, arquiva
        if (count($vistoAtual) > 0) {
            $vistoAtual->processo_atual = false;
            $vistoAtual->observacao .= "\n(Visto arquivado: ".$obs.")";
            $vistoAtual->save();
        }
        $visto = new \Visto;
        $visto->id_candidato = $candidato->id_candidato;
        $visto->processo_atual = true;
        $visto->observacao = "(Visto criado: ".$obs.")";
        $visto->save();
        return $visto;
    }

    public function atualiza_mais_recente($processo){
        $mais_recente = '';
        if ($processo->campo_ordem_mais_recente != ''){
            $processos = \Processo::where('id_visto', '=', $processo->id_visto)
                    ->where('id_tipoprocesso', '=', $processo->id_tipoprocesso)
                    ->whereNotNull($processo->campo_ordem_mais_recente)
                    ->orderBy($processo->campo_ordem_mais_recente, 'desc')
                    ->get();
            $primeiro = true;
            foreach($processos as $processo){
                \log::info('Data: '.$processo->date_1);
                if ($primeiro){
                    $processo->fl_mais_recente = 1;
                    $mais_recente = $processo->id_processo;
                    $primeiro = false;
                } else {
                    $processo->fl_mais_recente = 0;
                }
                $processo->save();
            }
        }
        return $mais_recente;
    }

}