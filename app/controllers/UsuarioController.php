<?php

class UsuarioController extends \CadastroController {

    const raiz = "usuario";
    const campoChave = 'id_usuario';
    const nomeSingular = 'Usuário';
    const nomePlural = 'Usuários';
    const artigo = 'o';

    public function RegistrosIndex() {
        $data = Usuario::orderBy('nome')->get();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(
            array("titulo" => "Nome", "campo" => "nome"),
            array("titulo" => "Login", "campo" => "username"),
            array("titulo" => "Email", "campo" => "email"),
            array("titulo" => "Perfis", "campo" => "lista_nome_perfis"),
            array("titulo" => "Criado em", "campo" => "created_at"),
            array("titulo" => "Último acesso", "campo" => "dt_ult_acesso")
        );
        return $data;
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = Usuario::find($id);
        } else {
            $this->obj = new Usuario;
        }
    }

    public function getShow($id) {
        $this->ObtemInstancia($id);
        $tela = $this->TelaIndex();
        return View::make($this->get_ViewShow())
                        ->with('tela', $tela)
                        ->with('obj', $this->obj)
        ;
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = self::nomeSingular;
        $data['titulo_grid'] = "Lista";
        $data['subTitulo'] = "Administrar " . self::nomePlural;
        $data['urlEdit'] = self::raiz . '/show';
        $data['urlDel'] = self::raiz . '/del';
        $data['urlCreate'] = self::raiz . '/create';
        $data['nomeCampoChave'] = self::campoChave;
        return $data;
    }

    public function get_ViewShow() {
        return self::raiz . '/show';
    }

    public function get_ViewIndex() {
        return self::raiz . '/table';
    }

    public function getSenha($id) {
        $this->ObtemInstancia($id);
        $tela = $this->TelaIndex();
        return View::make('usuario/senha')
                        ->with('tela', $tela)
                        ->with('obj', $this->obj)
        ;
    }

    public function MontaCampos() {
        //	$this->campos[] = new
    }

    public function getDel($id = 0) {
        try {
            $usuario = Usuario::find($id);
            $usuario->perfis()->detach();
            $usuario->delete();
            return Redirect::to(self::raiz)
                            ->with('flash_msg', self::nomeSingular . ' excluíd' . self::artigo . ' com sucesso!');
        } catch (Exception $e) {
            return Redirect::to(self::raiz)
                            ->with('flash_error', $e->getMessage());
        }
    }

    public function postUpdate($id = 0) {
        if ($id > 0) {
            $obj = Usuario::find($id);
        } else {
            $obj = new Usuario();
        }
        $obj->fill(Input::all());

        if ($id == 0) {
            $valido = Validator::make(Input::all(), array(
                        'password' => 'required|Confirmed',
                        'password_confirmation' => 'required',
                        'username' => 'unique:usuario,username,' . $id . ',id_usuario'
                            ), array(
                        'password.required' => 'A senha é obrigatória!',
                        'password_confirmation.required' => 'O campo repetição de senha é obrigatório',
                        'password.confirmed' => 'Repita a senha para verificação no campo correspondente',
                        'username.unique' => 'O login informado já está sendo usado por outro usuário'
                            )
            );
            if ($valido->fails()) {
                Input::flash();
                return Redirect::to(self::raiz . '/show/' . $id)->withErrors($valido->messages());
            } else {
                $obj->password = Hash::make(Input::get('password'));
            }
        }
        if ($obj->save()) {
            $perfis_selecionados = Input::get('id_perfil');
            if (is_array($perfis_selecionados) || $perfis_selecionados > 0) {
                $obj->perfis()->sync(Input::get('id_perfil'));
            }
            return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular . ' atualizad' . self::artigo . ' com sucesso!');
        } else {
            Input::flash();
            return Redirect::to(self::raiz . '/show/' . $id)->withErrors($obj->errors);
        }
    }

    public function postSenha($id = 0) {
        $obj = Usuario::find($id);

        $valido = Validator::make(Input::all(), array(
                    'password' => 'required|Confirmed',
                    'password_confirmation' => 'required',
                    'username' => 'unique:usuario,username,' . $id . ',id_usuario'
                        ), array(
                    'password.required' => 'A senha é obrigatória!',
                    'password_confirmation.required' => 'O campo repetição de senha é obrigatório',
                    'password.confirmed' => 'Repita a senha para verificação no campo correspondente',
                    'username.unique' => 'O login informado já está sendo usado por outro usuário'
                        )
        );
        if ($valido->passes()) {
            $obj->password = Hash::make(Input::get('password'));
            if ($obj->save()) {
                return Redirect::to(self::raiz)->with('flash_msg', 'Senha do usuário alterada com sucesso!');
            } else {
                Input::flash();
                return Redirect::to(self::raiz . '/senha/' . $id)
                                ->withErrors($obj->errors)
                ;
            }
        } else {
            Input::flash();
            return Redirect::to(self::raiz . '/senha/' . $id)
                            ->withErrors($valido->messages())
            ;
        }
    }

    public function getTestemail(){
        
    }

}
