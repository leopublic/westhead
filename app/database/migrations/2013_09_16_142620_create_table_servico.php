<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableServico extends Migration {
	public function up()
	{
        Schema::create('servico', function( $table) {
            $table->increments('id_servico');
			$table->string('descricao', 500);
			$table->string('descricao_en', 500)->nullable();
			$table->text('observacao')->nullable();
			$table->boolean('disponivel')->nullable();
			$table->boolean('afeta_historico_visto')->nullable();
			$table->boolean('multi_candidatos')->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('servico');
	}
}