<?php

use Westhead\Repositorios;

class RepositorioDespesaTest extends TestCase {

    public function despesasComErros() {
        return array(
            array('', 10, '12/12/2012', 10, 10, 'Não foi possível saber em qual ordem de serviço é para cadastrar a despesa'),
            array(0, 10, '12/12/2012', 10, 10, 'Não foi possível saber em qual ordem de serviço é para cadastrar a despesa'),
            array(10, '', '12/12/2012', 10, 10, 'O tipo de despesa é obrigatório'),
            array(10, 10, '', 10, 10, 'A data em que a despesa ocorreu é obrigatória'),
            array(10, 10, 'dsdlsdk', 10, 10, 'Informe uma data de ocorrência válida (dd/mm/aaaa)'),
            array(10, 10, '2012-10-31', 10, 10, 'Informe uma data de ocorrência válida (dd/mm/aaaa)'),
            array(10, 10, '10/31/2012', 10, 10, 'Informe uma data de ocorrência válida (dd/mm/aaaa)'),
            array(10, 10, '30/02/2012', 10, 10, 'Informe uma data de ocorrência válida (dd/mm/aaaa)'),
            array(10, 10, '31/11/2012', 10, 10, 'Informe uma data de ocorrência válida (dd/mm/aaaa)'),
            array(10, 10, '12/12/2012', '', 10, 'A quantidade é obrigatória'),
            array(10, 10, '12/12/2012', 'xx', 10, 'Quantidade inválida'),
            array(10, 10, '12/12/2012', 'zz', 10, 'Quantidade inválida'),
            array(10, 10, '12/12/2012', 999999999, 10, 'A quantidade máxima é 99.999.999,99'),
            array(10, 10, '12/12/2012', 10, 999999999, 'O valor máximo é R$ 99.999.999,99'),
        );
    }

    public function despesasSemErros() {
        return array(
            array(1, 10, '28/02/1999', 10, 10),
            array(1, 10, '12/12/2012', 10, 10),
            array(1, 10, '12/12/2012', '10', '10'),
            array(1, 10, '12/12/2012', '10.2', 10),
            array(1, 10, '12/12/2012', '10,2', 10),
            array(1, 10, '12/12/2012', '1.000,20', 10),
            array(1, 10, '12/12/2012', 10, 10),
            array(1, 10, '12/12/2012', 10, '10'),
            array(1, 10, '12/12/2012', 10, '10.2'),
            array(1, 10, '12/12/2012', 10, '10,2'),
            array(1, 10, '12/12/2012', 10, '1.000,20'),
        );
    }

    public function despesasValores() {
        return array(
            array(10, 10, 20, 20),
            array('10.2', 102, '20.2', 202),
            array('1002,02', 1002.02, '2003,04', 2003.04),
            array('1004', 1004, '2005', 2005),
            array('123456', 123456, '234567', 234567),
            array('23.456.789,1', 23456789.1, '34.567.890,2', 34567890.2),
        );
    }

    /**
     * @dataProvider despesasComErros
     */
    public function testCriar_erros($id_os, $id_tipo_despesa, $dt_ocorrencia, $qtd, $valor, $msg = '') {
        $post = array(
            'id_os' => $id_os,
            'id_tipo_despesa' => $id_tipo_despesa,
            'dt_ocorrencia' => $dt_ocorrencia,
            'qtd' => $qtd,
            'valor' => $valor,
        );
        $rep = new \Westhead\Repositorios\RepositorioDespesa();
        $desp = $rep->crie($post, 1450);
        $errors = $rep->getErrors();
        $this->assertGreaterThan(0, count($errors));
        if ($msg != '' && count($errors) == 1) {
            $this->assertEquals($msg, $errors[0]);
        }
        if ($desp) {
            $desp->delete();
        }
    }

    /**
     * @dataProvider despesasSemErros
     */
    public function testCriar_Ok($id_os, $id_tipo_despesa, $dt_ocorrencia, $qtd, $valor) {
        $post = array(
            'id_os' => $id_os,
            'id_tipo_despesa' => $id_tipo_despesa,
            'dt_ocorrencia' => $dt_ocorrencia,
            'qtd' => $qtd,
            'valor' => $valor,
        );
        $rep = new \Westhead\Repositorios\RepositorioDespesa();
        $desp = $rep->crie($post, 1450);
        $errors = $rep->getErrors();
        $this->assertEquals(0, count($errors));
        if ($desp) {
            $desp->delete();
        }
    }

    /**
     * @dataProvider despesasValores
     */
    public function testValores_Ok($qtd, $qtdBanco, $valor, $valorBanco) {
        $post = array(
            'id_os' => 10,
            'id_tipo_despesa' => 10,
            'dt_ocorrencia' => '12/12/2001',
            'qtd' => $qtd,
            'valor' => $valor,
        );
        $rep = new \Westhead\Repositorios\RepositorioDespesa();
        $desp = $rep->crie($post, 1450);
        $errors = $rep->getErrors();
        $this->assertEquals(0, count($errors));

        $x = \Despesa::find($desp->id_despesa);

        $this->assertEquals($qtdBanco, $x->qtd);
        $this->assertEquals($valorBanco, $x->valor);

        if ($desp) {
            $desp->delete();
        }
    }


}
