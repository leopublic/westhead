<?php

class ProcessoTraducaoController extends \ProcessoGenericoController {
    const FILTRO_MSG = "processotraducao";
    public function getShow($id) {
        
    }

    public function postUpdate($id) {
        $processo = ProcessoTraducao::find($id);
        try {
            $msg = '';
            if ($msg != '') {
                Session::flash($processo->id_processo."_error", "O processo não foi atualizado. Verifique o preenchimento:".$msg);
                return Redirect::to('/candidato/show/' . $processo->id_candidato . '#processo' . $id);
            } else {
                $processo->fill(Input::all());
                $processo->save();
                $this->repositorio()->atualiza_status_andamento($processo, Input::get('id_status_andamento'), Input::get('obs_andamento'), \Auth::user()->id_usuario);
                Session::flash($processo->id_processo."_success", "Processo atualizado com sucesso!");
            }
        } catch (Exception $ex) {
            Session::flash($processo->id_processo."_error", $ex->getMessage());
        }
        $proccontrol = new \ProcessoController();
        return $proccontrol->rotaParaVisualizarProcesso($processo);
    }

    public function getAdicione($id_visto) {
        $rep = new Westhead\Repositorios\RepositorioProcesso;
        $processo = $rep->criaProcessoNoVisto(10, $id_visto);
        return Redirect::to('/candidato/show/' . $processo->id_candidato . '#processo' . $processo->id_processo);
    }

}
