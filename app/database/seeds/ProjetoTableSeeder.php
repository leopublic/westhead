<?php

class ProjetoTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
        DB::table('projeto')->truncate();

        $empresa = Empresa::where('razaosocial', 'like', 'Acamin%')->first();
        $armador = Armador::where('razaosocial', 'like', 'Havila%')->first();

        $obj = array("descricao" => "Havila Faith", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Havila Fortress", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Havila Princess", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);

        $empresa = Empresa::where('razaosocial', 'like', 'Astro Internacional %')->first();
        $armador = Armador::where('razaosocial', 'like', 'Emas Marine%')->first();

        $obj = array("descricao" => "Lewek Fulmar", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $armador = Armador::where('razaosocial', 'like', 'Hartmann%')->first();
        $obj = array("descricao" => "UOS Navigator", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);

        $empresa = Empresa::where('razaosocial', 'like', 'Astromar%')->first();
        $obj = array("descricao" => "UOS Challenger", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "UOS Liberty", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "UOS Voyager", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);


        $armador = Armador::where('razaosocial', 'like', 'Havila%')->first();
//        $obj = array("descricao" => "Havila Faith", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
 //       DB::table('projeto')->insert($obj);

        $obj = array("descricao" => "Havila Favour", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Havila Fortress", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Havila Princess", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);

        $armador = Armador::where('razaosocial', 'like', 'Hornbeck%')->first();
        $obj = array("descricao" => "HOS Navegante", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "HOS Pinnacle", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "HOS Resolution", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "HOS Wildwing", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "HOS Windancer", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);

        $empresa = Empresa::where('razaosocial', 'like', 'Bras%')->first();
        $armador = Armador::where('razaosocial', 'like', 'Maridive%')->first();
        $obj = array("descricao" => "Maridive 208", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Maridive 212", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Maridive 231", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Maridive 601", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Maridive 602", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);

        $empresa = Empresa::where('razaosocial', 'like', 'Deep%')->first();
        $armador = new Armador();
        $obj = array("descricao" => "Sea Cheetah", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Sea Fox", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Sea Jaguar", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Sea Stoat", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Sea Vixen", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);


        $empresa = Empresa::where('razaosocial', 'like', 'Galaxia%')->first();
        $armador = Armador::where('razaosocial', 'like', 'Varada%')->first();
        $obj = array("descricao" => "Varada Buzios", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Varada Ibiza", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Varada Ilheus", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Varada Ipanema", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Varada Santos", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);

        $armador = Armador::where('razaosocial', 'like', 'Vega%')->first();
        $obj = array("descricao" => "Vega Corona", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Vega Crusader", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Vega Emtoli", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Vega Inruda", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Vega Jaanca", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);
        $obj = array("descricao" => "Vega Juniz", "id_empresa" => $empresa->id_empresa, "id_empresa_armador" => $armador->id_empresa, "fl_ativa"=> 1);
        DB::table('projeto')->insert($obj);

    }

}