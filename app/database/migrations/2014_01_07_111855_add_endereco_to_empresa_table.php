<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddEnderecoToEmpresaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('empresa', function(Blueprint $table) {
			$table->string('endereco', 1000)->nullable();
            $table->string('end_complemento', 1000)->nullable();
            $table->string('end_bairro', 1000)->nullable();
            $table->string('end_municipio', 1000)->nullable();
            $table->string('end_uf', 2)->nullable();
            $table->string('end_cep', 10)->nullable();
            $table->string('tel_ddd', 50)->nullable();
            $table->string('tel_num', 50)->nullable();
            $table->string('email', 1000)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('empresa', function(Blueprint $table) {
			if(Schema::hasColumn('empresa', 'endereco')) { $table->dropColumn('endereco');}
			if(Schema::hasColumn('empresa', 'end_complemento')) { $table->dropColumn('end_complemento');}
			if(Schema::hasColumn('empresa', 'end_bairro')) { $table->dropColumn('end_bairro');}
			if(Schema::hasColumn('empresa', 'end_municipio')) { $table->dropColumn('end_municipio');}
			if(Schema::hasColumn('empresa', 'end_uf')) { $table->dropColumn('end_uf');}
			if(Schema::hasColumn('empresa', 'end_cep')) { $table->dropColumn('end_cep');}
			if(Schema::hasColumn('empresa', 'tel_ddd')) { $table->dropColumn('tel_ddd');}
			if(Schema::hasColumn('empresa', 'email')) { $table->dropColumn('email');}
			if(Schema::hasColumn('empresa', 'tel_num')) { $table->dropColumn('tel_num');}
		});
	}

}
