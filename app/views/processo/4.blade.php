@extends('processo.comum')

<? $tipo_processo = 'coletavisto'; ?>
<? $nome_servico = 'Coleta de visto'; ?>

@section('campos')
    <div class="row">
        @include('html/campo-texto', array('id' => 'numero_visto', 'label' => 'Número visto', 'valor' => $processo->numero_visto,  'help' => '',  array()))
        @if (intval($processo->id_os) == 0)
            @if (Servico::where('id_tipo_servico', '=', 4)->count() > 1)
                @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => $processo->id_servico, 'valores' => array('0' => '(não informado)') +  Servico::where('id_tipo_servico', '=', 4)->lists('descricao', 'id_servico'),  'help' => '', array()))
            @else
                @include('html/campo-texto', array('id' => 'servico', 'label' => 'Serviço', 'valor' => $processo->descricao_servico,  'atributos'=> array( "disabled"=>"disabled")))        
            @endif
        @endif
        @include('html/campo-data', array('id' => 'data_expedicao', 'label' => 'Data emissão', 'valor' => $processo->data_expedicao,  'help' => '',  array()))
        @include('html/campo-select', array('id' => 'id_reparticao', 'label' => 'Local de emissão', 'valor' => $processo->id_reparticao, 'valores'=>array('0' => '(não informado)') +  Reparticao::orderBy('descricao')->lists('descricao', 'id_reparticao'),  'help' => '', array()))
        @include('html/campo-texto', array('id' => 'prazo_estada', 'label' => 'Prazo de estada', 'valor' => $processo->prazo_estada,  'help' => '',  array()))
    </div>
@overwrite
