<style>
    table tr th{text-align: left;}
    table tr td {vertical-align: top;}
    table.check{border-spacing: 5px; font-size:12px}
    p.check {border: solid 1px black; width:100%;}
    div.box{padding:10px; border:solid 1px black;}
    div.subtitulo{width:100%; text-align:left;color:white;font-size:16px; padding:5px;margin-top:15px; margin-bottom:5px;background-color: #ccc;}
</style>
<body>
    <div style="width:100%; text-align:center;font-size:30px;border-bottom:solid 2px #999;">CHECK LIST – Transformação de Visto<br/><span style="font-size:18px;">(RN 99/12 em Permanente)</span>
</div>
    <div class="subtitulo">OS {{substr('000000'.$os->numero , -6)}}</div>
    <table>
        <tr><th>Data de elaboração</th><th>:</th><td>___________________</td></tr>
        <tr><th>Cliente</th><th>:</th><td>{{ $os->empresa->razaosocial}}</td></tr>
        <tr><th>Embarcação</th><th>:</th><td>{{ $os->projeto->descricao }}</td></tr>
        <tr><th>Centro de custo</th><th>:</th><td>{{ $os->centrodecusto->nome }}</td></tr>
    </table>

    <div class="subtitulo">Itens a serem verificados</div>
    <table class="check">
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Formulário de Transformação assinado pelo estrangeiro (1344)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia Completa do Passaporte (incluindo páginas em branco) – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Declaração de Boa Conduta  assinada pelo estrangeiro</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia Completa da CTPS (incluindo páginas em branco) – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do contrato de trabalho que deu ensejo à prorrogação assinado pela empresa requerente e pelo estrangeiro – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do DOU constando deferimento da autorização de trabalho inicial</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do contrato de trabalho por prazo indeterminado assinado pela empresa requerente e pelo estrangeiro – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do protocolo RNE/CIE do estrangeiro – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia da Procuração – (Autenticada)</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Documento hábil, de que o signatário do novo contrato tem poderes para contratar em nome da empresa empregadora (contrato social; estatuto; ata de assembleia ou procuração lavrada em cartório).</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Cópia do Cartão de CNPJ </td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Currículo do estrangeiro em português ou espanhol</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>Justificativa para Transformação / Descrição de Atividades Exercidas assinado pela empresa requerente</td></tr>
        <tr><td class="check"><p class="check">&nbsp;&nbsp;&nbsp;&nbsp;</p></td><td>GRU Cód. 140074 Original Paga (R$102,00)</td></tr>
    </table>

    <div class="subtitulo">Candidato(s)</div>
    <div class="box">
        <? $br = '';?>
        @foreach($os->candidatos as $candidato)
        {{ $br.$candidato->nome_completo }}
        <? $br = '<br/>';?>
        @endforeach
    </div>
    <div class="subtitulo">Processo elaborado por</div>
    <div class="box">{{$os->usuarioabertura->nome}}</div>
    <div class="subtitulo">Número do pré-cadastro no MTE</div>
    <div class="box">&nbsp;</div>

</body>
