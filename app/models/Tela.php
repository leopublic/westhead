<?php
/**
 * @property string $id_tela Id do tela
 * @property string $id_menu Id do menu da tela
 * @property string $descricao Nome do tela
 * @property datetime $created_at Data em que o tela foi cadastrado
 * @property datetime $updated_at Data em que foi atualizado
 */
class Tela extends Modelo {
    protected $table = 'tela';
    protected $primaryKey = 'id_tela';
    protected $guarded = array();

    const tlINVOICE = 'INVO';
    const tlCOTACOES = 'COTA';
    const tlPERFIS = 'PERF';
    const tlUSUARIOS = 'USUA';
    const tlOS = 'ORDS';
    const tlPROCESSOS = 'PROC';
    const tlCANDIDATOS = 'CAND';
    
    public function acoes(){
        return $this->hasMany('Acao', 'id_tela');
    }
    
    public static function telas(){
        return array(
             array('descricao' =>'Invoice'  , 'codigo'=>'INVO')
            ,array('descricao' =>'Cotações' , 'codigo'=>'COTA')
            ,array('descricao' =>'Perfis'   , 'codigo'=>'PERF')
            ,array('descricao' =>'Usuarios' , 'codigo'=>'USUA')
            ,array('descricao' =>'Ordens de Serviço' , 'codigo'=>'ORDS')
            ,array('descricao' =>'Processos' , 'codigo'=>'PROC')
            ,array('descricao' =>'Candidatos' , 'codigo'=>'CAND')
            ,array('descricao' =>'Tabelas' , 'codigo'=>'TABS')
        );
        
    }
    
    public static function carregaTelas(){
        $cadastradas = self::telasCadastradas();
        $telas = self::telas();
        foreach ($telas as $tela){
            if (!str_contains($cadastradas, $tela['codigo'])){
                $nova = new \Tela;
                $nova->codigo = $tela['codigo'];
                $nova->descricao = $tela['descricao'];
                $nova->save();
                print "Tela '".$tela['codigo']."' =>'".$tela['descricao']."' adicionada \n";
            }
        }
    }
    
    public static function telasCadastradas(){
        $telas = \Tela::all();
        $ret = '';
        foreach($telas as $tela){
            $ret .= '#'.$tela->codigo.'#';
        }
        return $ret;
    }
    
    public static function recuperaPeloCodigo($codigo){
        $tela = \Tela::where('codigo', '=', $codigo)->first();
        if (is_object($tela)){
            return $tela;
        } else {
            return false;
        }
    }
}
