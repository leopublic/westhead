@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="icon-male"></i>{{$obj->nome_completo}}</h1>
        @if ($obj->id_candidato_principal > 0)
        <h4>(
            @if ($obj->id_parentesco > 0)
            {{$obj->parentesco->descricao}} de
            @endif
            <a href="{{URL::to('/candidato/cadastro/'.$obj->id_candidato_principal)}}"  target="_blank" title="Clique para ir para o perfil do responsável" style="text-decoration: underline;">{{$obj->candidatoprincipal->nome_completo}}</a>)</h4>
        @endif
        <h4>@if (is_object($obj->empresa))
            {{$obj->empresa->razaosocial}}
            @endif
        </h4>
        <h4>@if (is_object($obj->projeto))
            {{$obj->projeto->descricao}}
            @endif
        </h4>
    </div>
</div>
<!-- END Page Title -->
@include('html.mensagens')
<!-- BEGIN Main Content -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                @include('candidato.show_abas', array('obj'=> $obj, 'ativa'=> 'Visto atual'))
                <h3>&nbsp;</h3>
            </div>
            <div class="box-content">
                @include('visto.show', array('visto' => $visto_atual, 'processos' => $processos) )
            </div>
        </div>
    </div>
</div>


@stop
