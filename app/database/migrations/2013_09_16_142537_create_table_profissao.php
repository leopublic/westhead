<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableProfissao extends Migration {
	public function up()
	{
        Schema::create('profissao', function( $table) {
            $table->increments('id_profissao');
			$table->string('descricao',500);
			$table->string('descricao_en', 500)->nullable();
			$table->string('cbo', 10)->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('profissao');
	}
}