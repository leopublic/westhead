<?php
// Copie esse arquivo para database.php e altere para os dados da sua conexao local
return array(
	'default' => 'mysql',

	'connections' => array(
		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'westhead',
			'username'  => 'root',
			'password'  => 'mysql',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),
	),


);
