<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdStatusSolToServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasColumn('servico', 'id_status_os_inicial')){
			Schema::table('servico', function(Blueprint $table) {
				$table->integer('id_status_os_inicial')->unsigned()->nullable();
			});			
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasColumn('servico', 'id_status_os_inicial')){
			Schema::table('servico', function(Blueprint $table) {
				 $table->dropColumn('id_status_os_inicial');
			});
		}
	}

}
