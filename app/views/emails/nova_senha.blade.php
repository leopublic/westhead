<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
    Olá {{$usuario->nome}}<br/>
    <br/>
    <br/>Esse e-mail confirma a geração de uma nova senha para acesso à aplicação Westhead<br/>
    <br/>
    <br/>Seu login: {{$usuario->username}}
    <br/>Sua senha: {{$novasenha}}
    <br/>
    <br/>Acesse <a href="http://westhead.m2software.com.br">http://westhead.m2software.com.br</a> e informe a senha fornecida.
    <br/>
    <br/>Atenciosamente,
    <br/>Westhead
    </body>
</html>