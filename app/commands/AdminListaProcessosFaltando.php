<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminListaProcessosFaltando extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:listaprocessos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica se existem processos que não foram criados pelas OS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $sql = "select os.id_os, numero , os_candidato.id_candidato, os.id_servico, id_tipoprocesso
                from os , servico, tipo_servico, os_candidato
                where 
                servico.id_servico = os.id_servico
                and os_candidato.id_os = os.id_os
                and tipo_servico.id_tipo_servico = servico.id_tipo_servico
                and not exists (select * from processo where processo.id_os = os.id_os and processo.id_candidato = os_candidato.id_candidato)
                order by numero, id_candidato";
        $rs = \DB::select(\DB::raw($sql));
        $tot = 0;
        foreach ($rs as $reg) {
            $linha = "OS ".substr('000000'.$reg->numero, -6)." Cand:".substr('000000'.$reg->id_candidato, -6)." Servico:".substr('000'.$reg->id_servico, -3);
            $this->info($linha);
            $tot++;
        }
        $this->error($tot." registros encontrados");
        
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }

}
