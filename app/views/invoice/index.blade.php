@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Invoices', 'subTitulo' => ''))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array("class"=>"form-horizontal", "id"=>'criterios')) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          {{Form::label('data', 'Data', array('id' => 'data', 'class' => 'col-sm-3 col-lg-2 control-label')) }}
                            <div class="col-sm-9 col-lg-10 controls">
                               <div class="input-group">
                                <span class="input-group-addon">de</span>
                                {{ Form::text('data_ini', Input::old('data_ini', Session::get('data_ini')), $attributes = array('class' => 'form-control', 'data-mask' => '99/99/9999')); }}
                                <span class="input-group-addon">até</span>
                                {{ Form::text('data_fim', Input::old('data_fim', Session::get('data_fim')), $attributes = array('class' => 'form-control', 'data-mask' => '99/99/9999')); }}
                               </div>
                           </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_centro_de_custo', 'label' => 'Centro de custos', 'valor' => $id_centro_de_custo, 'valores' => $centros , 'help' => '', array()))
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close()}}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Invoices</h3>
                <div class="box-tool">
                    <a href="{{ URL::to('/invoice/createlist') }}" class="show-tooltip" data-placement="bottom" data-original-title="Clique para adicionar uma nova invoice"><i class="icon-plus"></i></a>
                    <button onclick="javascript:geraExcel();" class="show-tooltip" data-placement="bottom" data-original-title="Clique para exportar essa consulta em excel"><i class="icon-table"></i></button>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width:30px">#</th>
                            <th style="width:100px; text-align: center;">Ação</th>
                            <th style="width: auto">Número</th>
                            <th style="width: auto">Centro de custos</th>
                            <th style="width: auto">Projeto</th>
                            <th style="width: auto; text-align: center;">Data</th>
                            <th style="width: auto; text-align: center;">Vencimento</th>
                            <th style="width: auto; text-align: left;">Ordens de serviço</th>
                            <th style="text-align: right;">Valor total R$</th>
                        </tr>
                    </thead>
                    @if(isset($invoices))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($invoices as $invoice)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-primary btn-sm" href="{{ URL::to('/invoice/show/'.$invoice->id_invoice) }}"><i class="icon-edit"></i></a>
                                @include('html.link_exclusao', array('url' => '/invoice/del/'.$invoice->id_invoice, 'msg_confirmacao' => 'Clique para confirmar a exclusão dessa invoice.<br/><strong>ATENÇÃO: ESSA OPERAÇÃO NÃO PODE SER DESFEITA!</strong>'))
                            </td>
                            <td>{{ $invoice->numeroFmt }}</td>
                            <td>{{ $invoice->centrodecusto->nome }}</td>
                            <td>{{ $invoice->listaProjetos }}</td>
                            <td style="text-align: center;">{{ $invoice->dataFmt }}</td>
                            <td style="text-align: center;">{{ $invoice->vencimentoFmt}}</td>
                            <td style="text-align: left;">{{ $invoice->listaOsPorTipo}}</td>                            
                            <td style="text-align: right;">{{ $invoice->valorTotalFmt}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script>

function geraExcel(){
    $('#criterios').attr('target', '_blank');
    $('#criterios').attr('action', '/invoice/excel');
    $("#criterios" ).submit() ;
    $('#criterios').attr('target', '_self');
    $('#criterios').attr('action', '/invoice');
}
    
</script>
@stop
