<?php
namespace Westhead\Repositorios;
class RepositorioAnexo {

    public function excluir($id_anexo){
        $anexo = \Anexo::find($id_anexo);
        if(!isset($anexo)){
            $anexo->id_anexo = $id_anexo;
        }
        if(file_exists($anexo->caminho())){
            unlink($anexo->caminho());
        }
        $anexo->delete();
    }

    public function criarParaOs($id_os_chave) {
        if (!empty($_FILES)) {
            $anexo = $this->capturarFile($_FILES["file"]);
            $anexo->id_os = $id_os_chave;
            $anexo->save();
            $this->mover($_FILES["file"]['tmp_name'], $anexo->caminho());
        }
    }

    public function criarParaCandidato($id_candidato) {
        if (!empty($_FILES)) {
            $anexo = $this->capturarFile($_FILES["file"]);
            $anexo->id_candidato = $id_candidato;
            $anexo->save();
            try{
                $this->mover($_FILES["file"]['tmp_name'], $anexo->caminho());
            } catch(\Exception $e){
                $anexo->delete();
                return "Impossível adicionar: ";$e->getMessage();
            }
        }
    }

    /**
     * [capturarFile description]
     * @return $anexo
     */
    public function capturarFile($arquivo) {
        $path_parts = pathinfo($arquivo["name"]);
        $extensao = \Extensao::where('extensao', '=', $path_parts['extension'])->first();
        if(count($extensao) > 0 ){
            $anexo = new \Anexo();
            $anexo->nome_original = $arquivo["name"];
            $anexo->extensao()->associate($extensao);
            return $anexo;
        } else {
            throw new \Exception('Extensão não autorizada');
        }
    }

    public function mover($origem, $destino) {
        if (file_exists($destino)) {
            unlink($destino);
        }
        move_uploaded_file($origem, $destino); //6
    }

}
