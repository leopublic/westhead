<?php

/**
 * @property int $id_status_os
 * @property varchar $nome
 * @property varchar $sigla
 * @property timestamp $created_at
 * @property timestamp $updated_at
 */

class StatusOs extends Eloquent{
	protected $table = 'status_os';
	protected $primaryKey = 'id_status_os';

	const stNOVA = 1;
	const stCONCLUIDA = 2;
	const stFATURADA = 3;
	const stLIBERADA = 4;

}