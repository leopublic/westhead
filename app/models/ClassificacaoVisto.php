<?php
class ClassificacaoVisto extends Modelo {
	protected $table = 'classificacao_visto';
	protected $primaryKey = 'id_classificacao_visto';

	protected $guarded = array();

	public function assumeDefaults()
	{
	}

	public static function regras()
	{
		return array(
				'classificacao' => 'required'
				);
	}

	public static function mensagens()
	{
		return array(
				'classificacao.required' => 'O nome é obrigatório!'
			);
	}
}
