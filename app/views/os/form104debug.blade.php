<html>
    <head>
        <style>
            *, body{font-family: Verdana, Arial, sans-serif;}
            h1{
                font-family: inherit;
                text-align: center;
                font-size: 20px;
                margin-top: 0px;
                padding-top: 0px;
            }
            div.titulo{
                font-family: inherit;
                font-size:14px;
                font-weight: bold;
                margin-top:30px;
                margin-bottom:5px;
            }
            table{
                font-family: inherit;
                font-size:12px;
                margin:0;
                padding:0;
                border-spacing: 0;
                border-collapse: collapse;
                width:100%;
            }
            table tr td{
                padding: 3px;
                font-family: inherit;
                font-size:inherit;
                border: solid 1px #000;
                vertical-align: bottom;
                page-break-inside: avoid;
                border-top: none;
            }
            table tr th{
                padding: 3px;
                font-family: inherit;
                font-size:inherit;
                font-weight: bold;
                border: solid 1px #000;
                border-bottom: none;
                vertical-align: top;
                text-align: left;
                padding-bottom: 10px;
            }
            table tr.deps td{
                border: solid 1px #000;
            }
            div.inseparavel{
                page-break-inside: avoid;
            }
            table.semLinhas tr td
            ,table.semLinhas tr th{
                border: none;
                font-family: inherit;
            }
            table tbody{
                page-break-inside: avoid;
            }
            @media all {
                .page-break	{ display: none; }
            }

            @media print {
                .page-break	{ display: block; page-break-before: always; }
            }
        </style>
    </head>
    <body>
        <h1>Formulário de Requerimento de Autorização de Trabalho</h1>
        <div class="titulo">1. REQUERIMENTO, COM FUNDAMENTO LEGAL:</div>
        <table>
            <tbody>
                <tr><th>1. Lei/Decreto/Resolução:</th></tr>
                <tr><td>{{ $os->servico->tipoautorizacao->descricao }}</td></tr>
            </tbody>
        </table>
        <div class="titulo">2. DO REQUERENTE</div>
        <table>
            <tr>
                <th colspan="4">2. Requerente</th>
                <th style="width:35%">3. Ativ. Econômica (CNAE)</th>
            </tr>
            <tr>
                <td colspan="4">{{ $os->empresa->razaosocial }}</td>
                <td>{{ $os->empresa->atividade_economica }}</td>
            </tr>
            <tr>
                <th colspan="4">4. Endereço</th>
                <th>5. Cidade</th>
            </tr>
            <tr>
                <td colspan="4">{{ $os->empresa->endereco }} {{ $os->empresa->end_complemento }} - {{ $os->empresa->end_bairro }}</td>
                <td>{{ $os->empresa->end_municipio }}</td>
            </tr>
            <tr>
                <th colspan="2">6. UF</th>
                <th colspan="2">7. CEP</th>
                <th>8. Telefone</th>
            </tr>
            <tr>
                <td colspan="2">{{ $os->empresa->end_uf }}</td>
                <td colspan="2">{{ $os->empresa->end_cep }}</td>
                <td>({{ $os->empresa->tel_ddd }}){{ $os->empresa->tel_num }}</td>
            </tr>
            <tr>
                <th colspan="4">9. Correio Eletrônico</th>
                <th>10. CNPJ/CPF</th>
            </tr>
            <tr>
                <td colspan="4">{{ $os->empresa->email }}</td>
                <td>{{ $os->empresa->cnpj }}</td>
            </tr>
        </table>

        <div class="titulo">2.1. DADOS ESPECÍFICOS DA EMPRESA</div>
        <table>
            <tr>
                <th colspan="4">11. Objeto Social (resumo):</th>
            </tr>
            <tr>
                <td colspan="4" style="font-size:10px;">{{ nl2br($os->empresa->objeto_social) }}</td>
            </tr>
            <tr>
                <th colspan="2">12. Capital Social inicial:</th>
                <th colspan="2">13. Capital Social atual:</th>
            </tr>
            <tr>
                <td colspan="2">{{ $os->empresa->valor_capital_inicial }}</td>
                <td colspan="2">{{ $os->empresa->valor_capital_atual }}</td>
            </tr>
            <tr>
                <th colspan="2">14. Data da constituição:</th>
                <th colspan="2">15. Data da última alteração contratual:</th>
            </tr>
            <tr>
                <td colspan="2">{{ $os->empresa->dt_constituicao }}</td>
                <td colspan="2">{{ $os->empresa->dt_alteracao_contratual }}</td>
            </tr>
            <tr>
                <th colspan="4">16. Pessoa(s) jurídica(s) estrangeira(s) associada(s):</th>
            </tr>
            <tr>
                <td colspan="4">
                    @if ( $os->empresa->nome_empresa_estrangeira  != '')
                    {{ $os->empresa->nome_empresa_estrangeira }}
                    @else
                    Não aplicável.
                    @endif
                </td>
            </tr>
            <tr>
                <th colspan="4">17. Relação das principais associadas, quando se tratar de Sociedade Anônima:</th>
            </tr>
            <tr>
                <td colspan="4">
                    Não aplicável
                </td>
            </tr>
            <tr>
                <th colspan="4">18. Valor do investimento de capital estrangeiro:</th>
            </tr>
            <tr>
                <td colspan="4">
                    @if ( $os->empresa->valor_investimento_estrangeiro  != '' &&  $os->empresa->valor_investimento_estrangeiro  != '0.00')
                    {{ $os->empresa->dt_investimento_estrangeiro }}
                    @else
                    Não aplicável.
                    @endif
                </td>
            </tr>
            <tr>
                <th colspan="2">19. Data do último investimento:</th>
                <th colspan="2">20. Data de registro no Banco Central do Brasil:</th>
            </tr>
            <tr>
                <td colspan="2">
                    @if ($os->empresa->dt_investimento_estrangeiro != '' && $os->empresa->dt_investimento_estrangeiro!= '00/00/0000')
                    {{ $os->empresa->dt_investimento_estrangeiro }}
                    @else
                    Não aplicável.
                    @endif
                </td>
                <td colspan="2">
                    @if ($os->empresa->dt_cadastro_bc != '' && $os->empresa->dt_cadastro_bc != '00/00/0000')
                    {{ $os->empresa->dt_cadastro_bc }}
                    @else
                    Não aplicável.
                    @endif
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <th colspan="4">21. Administrador (es) - Nome e cargo:</td>
            </tr>
            <tr>
                <td colspan="4">{{ nl2br($os->empresa->administradores)}}</td>
            </tr>
            <tr>
                <th colspan="4">22 Número total de empregados:</th>
            </tr>
            <tr>
                <td colspan="4">{{ $os->empresa->qtd_empregados  }}</td>
            </tr>
            <tr>
                <th colspan="2">22.1 Número atual de empregados brasileiros:</th>
                <th colspan="2">22.2 Número atual de empregados estrangeiros:</th>
            </tr>
            <tr>
                <td colspan="2">{{ $os->empresa->qtd_empregados - $os->empresa->qtd_empregados_estrangeiros }}</td>
                <td colspan="2">{{ $os->empresa->qtd_empregados_estrangeiros }}</td>
            </tr>
            <tr>
                <th colspan="4">23. Justificativa para a contratação do estrangeiro:</th>
            </tr>
            <tr>
                <td colspan="4" style="font-size:{{ tamanho_letra }}">
                    @if ($candidato->justificativa != '')
                    {{ nl2br($candidato->justificativa) }}
                    @else
                    {{ nl2br($os->projeto->justificativa) }}
                    @endif
                    <br/><br/><b>Descrição das atividades:</b>
                    <br/>
                    @if($candidato->descricao_atividades != '')
                    {{ nl2br($candidato->descricao_atividades) }}
                    @else
                    {{ nl2br($candidato->funcao->atividades) }}
                    @endif
                </td>
            </tr>
        </table>
        <div class="page-break"></div>
        <div class="titulo"> 3. DO ESTRANGEIRO</div>
        <table>
            <tr>
                <th colspan="6">24. Nome </th>
            </tr>
            <tr>
                <td colspan="6">{{ $candidato->nome_completo }}</td>
            </tr>
            <tr>
                <th colspan="6">25 Filiação:</th>
            </tr>
            <tr>
                <td colspan="6"><b>Pai:</b> {{ $candidato->nome_pai }}<br/><b>Mãe:</b> {{ $candidato->nome_mae }}</td>
            </tr>
            <tr>
                <th colspan="3">26. Sexo</th>
                <th colspan="3">27. Estado civil</th>
            </tr>
            <tr>
                <td colspan="3">{{ $candidato->sexo }}</td>
                <td colspan="3">{{ $candidato->estadocivil->descricao}}</td>
            </tr>
            <tr>
                <th colspan="3">28. Data de nascimento</th>
                <th colspan="3">29. Escolaridade</th>
            </tr>
            <tr>
                <td colspan="3">{{$candidato->dt_nascimento}}</th>
                <td colspan="3">{{ $candidato->escolaridade->descricao }}</td>
            </tr>
            <tr>
                <th colspan="3">30. Profissão</th>
                <th colspan="3">31. Nacionalidade</th>
            </tr>
            <tr>
                <td colspan="3">{{ $candidato->profissao->descricao }}</td>
                <td colspan="3">{{ $candidato->nacionalidade->descricao }}</td>
            </tr>
            <tr>
                <th colspan="3">32. Documento de viagem - Validade</th>
                <th colspan="3">33. Função no Brasil</th>
            </tr>
            <tr>
                <td colspan="3">Passaporte n&ordm; {{ $candidato->nu_passaporte }} - {{$candidato->dt_validade_passaporte}}</td>
                <td colspan="3">{{ $candidato->funcao->descricao}}</td>
            </tr>
            <tr>
                <th colspan="3">34. CBO</th>
                <th colspan="3">35. Local de trabalho</th>
            </tr>
            <tr>
                <td colspan="3">{{ $candidato->funcao->codigo_pf }}</td>
                <td colspan="3">{{ $os->local_trabalho }}</td>
            </tr>
            <tr>
                <th colspan="6">36. Informar última remuneração percebida pelo estrangeiro no exterior:</th>
            </tr>
            <tr>
                <td colspan="6">{{ nl2br($candidato->remuneracao_ext) }}</td>
            </tr>
            <tr>
                <th colspan="6">37. Informar remuneração que o estrangeiro irá perceber no País:</th>
            </tr>
            <tr>
                <td colspan="6">{{ nl2br($candidato->remuneracao_br) }}</td>
            </tr>
            <tr>
                <th colspan="6">38. Experiência profissional: relação das empresas nas quais foi empregado, funções exercidas com a respectiva duração, local e data, por ordem cronológica, discriminando as atividades compatíveis com as que o candidato desempenhara no Brasil.</th>
            </tr>
            <tr>
                <td colspan="6">{{ nl2br($candidato->trabalho_anterior) }}</td>
            </tr>
        </table>
        <div class="page-break"></div>
        <table>
            <tr>
                <th colspan="6">39. Dependentes</th>
            </tr>
            <tr class="deps">
                <td>Nome</td>
                <td>Parentesco</td>
                <td>Data nasc.</td>
                <td>Nacionalidade</td>
                <td>Documento de viagem</td>
                <td>Validade</td>
            </tr>
            @if (count($candidato->dependentesnaos($os->id_os)) > 0)
            @foreach($candidato->dependentesnaos($os->id_os) as $dep)
            <tr class="deps">
                <td>{{$dep->nome_completo}}</td>
                <td>{{$dep->parentesco->descricao}}</td>
                <td style="text-align: center;">{{$dep->dt_nascimento}}</td>
                <td style="text-align: center;">@if(is_object($dep->nacionalidade))
                    {{$dep->nacionalidade->nacionalidade}}
                    @else
                    (n/d)
                    @endif
                </td>
                <td style="text-align: center;">{{$dep->nu_passaporte}}</td>
                <td style="text-align: center;">{{$dep->dt_validade_passaporte}}</td>
            </tr>
            @endforeach
            @else
            @endif
            <tr>
                <th colspan="3">40. Tipo de visto:</th>
                <th colspan="3">41. Prazo:</th>
            </tr>
            <tr>
                <td colspan="3">{{ $os->servico->classificacao->classificacao }}</td>
                <td colspan="3">{{ $os->prazo_solicitado }}</td>
            </tr>
            <tr>
                <th colspan="6">42. Repartição consular brasileira no exterior:</th>
            </tr>
            <tr>
                <td colspan="6">{{ $candidato->reparticao->descricao }}</td>
            </tr>
        </table>
        <div class="inseparavel">
            <div class="titulo">4. DO REPRESENTANTE LEGAL:</div>
            <table>
                <tr>
                    <th>43. Nome:</th>
                    <th>44. CPF:</th>
                    <th>45. Correio eletrônico:</th>
                </tr>
                <tr>
                    <td>{{ $os->empresa->procurador->nome }}</td>
                    <td>{{ $os->empresa->procurador->cpf }}</td>
                    <td>{{ $os->empresa->procurador->email }}</td>
                </tr>
            </table>
        </div>
        <div class="inseparavel">
            <div class="titulo">5. DECLARAÇÃO GERAL DE RESPONSABILIDADE:</div>
            <table>
                <tr>
                    <td style="border: solid 1px black;">46. {{ $os->empresa->razaosocial }}, representada por {{ $os->empresa->procurador->nome }}, DECLARA, sob as penas da Lei, em relação ao(s) estrangeiro(s) indicado(s) neste requerimento e seu(s) dependente(s) durante a sua permanência em Território Nacional, que:
                        <br/>a) Assume a responsabilidade por todas e quaisquer despesas médicas e/ou hospitalares do estrangeiro e seus dependentes (se houver);
                        <br/>b) Assume a responsabilidade pela repatriação do estrangeiro e de seus dependentes (se houver), ao país de origem;
                        <br/>c) Caso o(s) estrangeiro(s) continue(m) a perceber remuneração no exterior, comprometo-me a oferecer a tributação no Brasil, conforme determina a Secretaria da Receita Federal.
                        <br/>d) Informa que o estrangeiro exercerá suas funções no(s) endereço(s) abaixo relacionados, comprometendo-se a informar a Coordenação-Geral de Imigração qualquer outro endereço onde o estrangeiro vier a atuar:
                        <br/>a. {{ $os->empresa->endereco}} {{ $os->empresa->end_complemento }}, {{ $os->empresa->end_bairro }} - {{ $os->empresa->end_municipio }} - {{ $os->empresa->end_uf }}, bem como nas plataformas e embarcações dos principais clientes da {{ $os->empresa->razaosocial }};
                        <br/>
                    </td>
                </tr>
            </table>
            <div class="titulo">6. TERMO DE RESPONSABILIDADE:
                <table>
                    <tr>
                        <td style="border: solid 1px black;border-bottom:none;">
                            47. Declaro, sob as penas do art. 299 do Código Penal Brasileiro, serem verdadeiras as informações transcritas neste documento, comprometendo-me, inclusive, a comprová-las, mediante a apresentação dos documentos próprios a fiscalização.</th>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:solid 1px black; border-top: none; text-align: center;padding-top: 30px;">
                            <hr style="width:50%;color: #000;">
                            {{ $os->empresa->procurador->nome}}
                            <br/>{{ $os->empresa->procurador->titulo}}
                            <br/>CPF {{ $os->empresa->procurador->cpf }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
