@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Ordem de serviço {{substr('000000'.$obj->numero, -6)}}</h1>
    @if (is_object($obj->empresa))
    <h4>{{$obj->empresa->razaosocial}}</h4>
    @endif
    @if (is_object($obj->projeto))
    <h6>{{$obj->projeto->descricao}}</h6>
    @endif
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="box">
            <div class="box-title">
                <ul class="nav nav-tabs" style="float:left;">
                    <li class="active"><a href="#" ><i class="icon-tags"></i> Dados da OS</a></li>
                    <li class=""><a href="/os/candidatos/{{$obj->id_os}}" ><i class="icon-group"></i> Candidatos</a></li>
                    <li class=""><a href="/os/anexos/{{$obj->id_os}}" ><i class="icon-folder-open"></i> Arquivos</a></li>
                    <li class=""><a href="/os/despesas/{{$obj->id_os}}" ><i class="icon-dollar"></i> Despesas</a></li>
                    <li class=""><a href="/os/desmembrar/{{$obj->id_os}}" ><i class="icon-exchange"></i> Desmembrar</a></li>
                </ul>
                <div class="box-tool" style="float:right;">
                    @include('os.menu_os', array('obj', $obj))
                </div>
                <h3>&nbsp;</h3>
            </div>

            {{ Form::open(array('url' => 'os/store', "class"=>"form-horizontal")) }}
            <div class="box-content">
                <div class="tab-content">
                    <div id="dados" class="tab-pane active">
                        {{ Form::hidden('id_os', $obj->id_os, array('id' => 'id_os')) }}
                        {{ Form::hidden('tipopessoa', $obj->tipopessoa, array('id' => 'tipopessoa')) }}
                        @if($obj->tipopessoa == 'J')
                        @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => $obj->id_empresa, 'valores' => $empresas , 'help' => '',  array()))
                        @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Projeto', 'valor' => $obj->id_projeto, 'valores' => $projetos , 'help' => '',  array()))
                        @include('html/campo-select', array('id' => 'id_empresa_armador', 'label' => 'Armador', 'valor' => $obj->id_empresa_armador, 'valores' => $armadores , 'help' => '',  array()))
                        @include('html/campo-select', array('id' => 'id_centro_de_custo', 'label' => 'Centro de custo', 'valor' => $obj->id_centro_de_custo, 'valores' => $centros , 'help' => '',  array()))
                        @include('html/campo-select', array('id' => 'id_empresa_intermediaria', 'label' => 'Empresa intermediária', 'valor' => $obj->id_empresa_intermediaria, 'valores' => $intermediarias , 'help' => '',  array()))
                        @else
                        @include('html/campo-texto', array('id' => 'nomecompleto', 'label' => 'Nome completo', 'valor' => $obj->nomecompleto,  'help' => '', 'valores', array()))
                        @include('html/campo-texto', array('id' => 'cpf', 'label' => 'CPF', 'valor' => $obj->cpf,  'help' => '', 'valores', array()))
                        @endif
                        @include('html/campo-data', array('id' => 'dt_solicitacao', 'label' => 'Data entrada', 'valor' => $obj->dt_solicitacao, 'help' => '', array()))
                        @include('html/campo-data', array('id' => 'dt_execucao', 'label' => 'Data execução', 'valor' => $obj->dt_execucao, 'help' => '', array()))
                        @include('html/campo-data', array('id' => 'dt_assinatura', 'label' => 'Data assinatura', 'valor' => $obj->dt_assinatura, 'help' => '', array()))
                        <div class="form-group">
                            {{Form::label('id_servico', 'Serviço', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label')) }}
                            <div class="col-sm-8 col-md-10 col-lg-10 controls">
                                {{ Form::select('id_servico',$servicos , Input::old('id_servico', $obj->id_servico), $attributes = array('class' => 'form-control chosen')); }}
                                @if(isset($help))
                                <span class="help-inline">{{ $help }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('id_classificacao_visto', 'Tipo', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label')) }}
                            <div class="col-sm-8 col-md-10 col-lg-10 controls">
                                {{ Form::select('id_classificacao_visto',$classificacoes , Input::old('id_classificacao_visto', $obj->id_classificacao_visto), $attributes = array('class' => 'form-control chosen')); }}
                                @if(isset($help))
                                <span class="help-inline">{{ $help }}</span>
                                @endif
                            </div>
                        </div>
                        @include('html/campo-texto', array('id' => 'prazo_solicitado', 'label' => 'Prazo solicitado', 'valor' => $obj->prazo_solicitado, 'help' => 'O prazo solicitado, conforme deve aparecer no formulário', array()))
                        @include('html/campo-texto', array('id' => 'solicitante', 'label' => 'Solicitante', 'valor' => $obj->solicitante,  'help' => '', array()))
                        @include('html/campo-texto', array('id' => 'motorista', 'label' => 'Motorista', 'valor' => $obj->motorista,  'help' => '', array()))
                        @include('html/campo-texto', array('id' => 'dados_do_voo', 'label' => 'Dados do vôo', 'valor' => $obj->dados_do_voo,  'help' => '', array()))
                        @include('html/campo-texto', array('id' => 'local_trabalho', 'label' => 'Local de trabalho', 'valor' => $obj->local_trabalho,  'help' => '', array()))
                        @include('html/campo-select', array('id' => 'id_status_os', 'label' => 'Status', 'valor' => $obj->id_status_os, 'valores' => array('' => '(não informado)') + StatusOs::orderBy('nome')->lists('nome', 'id_status_os') , 'help' => '', array()))
                        <div class="form-group">
                            {{Form::label('', 'Encerrada por', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label')) }}
                            <div class="col-sm-8 col-md-10 col-lg-10 controls">
                                {{ Form::text('encerrada_por', $obj->dados_conclusao , $attributes = array('class' => 'form-control', 'disabled'=>'disabled')); }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('', 'Faturada por', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label')) }}
                            <div class="col-sm-8 col-md-10 col-lg-10 controls">
                                {{ Form::text('faturada_por', $obj->dados_faturamento , $attributes = array('class' => 'form-control', 'disabled'=>'disabled')); }}
                            </div>
                        </div>
                        @include('html/campo-textarea', array('id' => 'observacao', 'label' => 'Observação', 'valor' => $obj->observacao,  'help' => '', array()))
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4 col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                {{ HTML::link('os', 'Cancelar', array('class' => 'btn')) }}
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop

@section('scripts')
<script>
$(document).ready(function() {
    VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajson/', '(selecione a empresa)', false);
    VinculaCombos('id_empresa', 'id_centro_de_custo', '/centrodecusto/daempresajson/', '(selecione a empresa)', false);
});

function ChamaRota(form, rota, retorno)
{
    var xdata = $(form).serialize();
    $(retorno).html('<div style="text-align:center"><i class="icon-spinner icon-spin icon-3x"></i> carregando...</div>');
    $.ajax({
        type: 'POST'
        , url: rota
        , data: xdata
        , dataType: 'html'
        , success:
                function(data)
                {
                    $(retorno).html(data);
                }
    });
}


</script>

@stop
