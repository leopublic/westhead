<table class="table table-striped table-hover fill-head">
    <thead>
        <tr>
            <th style="width: 40px">#</th>
            <th style="width: 100px">Ação</th>
            <th style="width: 120px; text-align: center;">Data</th>
            <th style="width: auto;">Tipo</th>
            <th style="width: 120px; text-align: right;">Quantidade</th>
            <th style="width: 120px; text-align: right;">Valor unit. R$</th>
            <th style="width: 120px; text-align: right;">Total R$</th>
        </tr>
    </thead>
    <tbody>
    @if(count($despesas) > 0)
        <? $i = 0; ?>
        @foreach($despesas as $despesa)
        <? $i++; ?>
        <tr>
            <td><? print $i; ?></td>
            <td>
                <a href="/os/despesas/{{$despesa->id_os}}/{{$despesa->id_despesa}}" class="btn btn-primary btn-sm" title="Clique para alterar essa despesa"><i class="icon-edit"></i></a>
                @include('html.link_exclusao', array('url' => '/os/excluadespesa/'.$despesa->id_os.'/'.$despesa->id_despesa, 'msg_confirmacao' => 'Clique para confirmar a exclusão dessa despesa'))
                </div>
            </td>
            <td style="text-align:center;">{{$despesa->dt_ocorrencia}}</td>
            <td>{{$despesa->tipodespesa->descricao_pt_br}}<br/><span style="color:#aeaeae; font-style: italic;">{{nl2br($despesa->observacao)}}</span></td>
            <td style="text-align:right;">{{number_format($despesa->qtd, 2, ',', '.')}}</td>
            <td style="text-align:right;">{{number_format($despesa->valor, 2, ',', '.')}}</td>
            <td style="text-align:right;">{{number_format($despesa->valor_total(), 2, ',', '.')}}</td>
        </tr>
        @endforeach
    @else
    <tr><td colspan="7">(nenhuma despesa cadastrada)</td></tr>
    @endif
    </tbody>
</table>