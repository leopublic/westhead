<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddApelidoToEmpresaTable extends Migration {

    public function up() {
        Schema::table('empresa', function(Blueprint $table) {
            $table->string('apelido', 500)->nullable();
        });
    }

    public function down() {
        Schema::table('empresa', function(Blueprint $table) {
            $table->dropColumn('apelido');
        });
    }

}
