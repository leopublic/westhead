@if (isset($tela['urlEdit']))
    <a class="btn btn-primary btn-sm" href="{{ URL::to($tela['urlEdit'], array($registro->$tela['nomeCampoChave']) ) }}"><i class="icon-edit"></i></a>
@endif
@if($registro->fl_andamento == '1')
	<a class="btn btn-warning btn-sm" href="{{ URL::to('/servico/retiradoandamento', array($registro->$tela['nomeCampoChave']) ) }}" title="Esse serviço participa do andamento. Clique para retirá-lo do andamento."><i class="icon-random"></i></a>
@else
	<a class="btn btn-sm" href="{{ URL::to('/servico/incluinoandamento', array($registro->$tela['nomeCampoChave']) ) }}" title="Esse serviço NÃO participa do andamento. Clique para incluí-lo do andamento."><i class="icon-random"></i></a>
@endif
@if (isset($tela['urlDel']))
    <a class="btn btn-danger btn-sm" href="{{ URL::to($tela['urlDel'], array($registro->$tela['nomeCampoChave']) ) }}"><i class="icon-trash"></i></a>
@endif
