<style>
    table{border-spacing: 0;}
    table tr td, table tr th {vertical-align: top;border:solid 1px black;}
    table.check{border-spacing: 5px; font-size:12px}
    p.check {border: solid 1px black; width:100%;}
    div.box{padding:10px; border:solid 1px black;}
    div.subtitulo{width:100%; text-align:left;color:white;font-size:16px; padding:5px;margin-top:15px; margin-bottom:5px;background-color: #ccc;}
</style>
<body>
    @if ($empresa->temLogo())
        <div style="width:100%; text-align:left;font-size:25px;"><img src="{{$empresa->caminhoLogo()}}"/></div>
        <br/>
    @endif
    <div style="width:100%; text-align:center;font-size:18px;border-bottom:solid 2px #999;">RELAÇÃO DE EMBARCAÇÕES AFRETADAS<br/>NÚMERO DE TRIPULANTES POR EMBARCAÇÃO</div>

    <br/>
    <br/>
    <br/>
    <table style="width:100%;">
        <? $tem_1asterisco = false; ?>
        <? $tem_2asterisco = false; ?>
        <tr><th>Nome da<br/>Embarcação</th><th>Bandeira</th><th style="width:90px;">Quant.<br/>tripulantes<br/>brasileiros</th><th style="width:90px;">Quant.<br/>tripulantes<br/>estrangeiros</th></tr>
        @foreach ($empresa->embarcacoes() as $projeto)
        <tr>
            <td>{{$projeto->descricao}}
            @if ($projeto->dt_inicio_operacao == '' || $projeto->dt_inicio_operacao == '00/00/0000')
            (*)
                <? $tem_1asterisco = true; ?>
            @else
                @if ($projeto->dias_em_operacao <= 90)
                (**)
                <? $tem_2asterisco = true; ?>
                @endif
            @endif
            </td>
            @if(is_object($projeto->pais))
            <td style="text-align: center;">{{$projeto->pais->descricao}}</td>
            @else
            <td style="text-align: center;">--</td>
            @endif
            <td style="text-align: center;">{{number_format(intval($projeto->qtd_tripulantes) - intval($projeto->qtd_estrangeiros), 0, ',', '.')}}</td>
            <td style="text-align: center;">{{number_format(intval($projeto->qtd_estrangeiros), 0, ',', '.')}}</td>
        </tr>
        @endforeach
    </table>
    @if ($empresa->fl_obs_embarcacoes == 1)
        @if (count($projetos_com_1asterisco) > 0)
        <div style="font-size:12px; margin-top:5px;">
            @if (count($projetos_com_1asterisco) == 1)
                * Vale ressaltar que a embarcação 
                <? $adentrou = 'adentrou'; ?>
            @else
                * Vale ressaltar que as embarcações 
                <? $adentrou = 'adentraram'; ?>
            @endif
            <? $virg = ''; ?>
            <strong>
            @foreach($projetos_com_1asterisco as $projeto)
                {{$virg.trim($projeto->descricao)}}
                <? $virg = ', '; ?>
            @endforeach
            </strong> ainda não {{$adentrou}} em AJB para início de suas operações, e por este motivo ainda não {{$adentrou}} ao período de obrigatoriedade de contratação de mão de obra     nacional.
        </div>    
        @endif
        @if (count($projetos_com_2asterisco) > 0)
        <div style="font-size:12px; margin-top:5px;">
            @if (count($projetos_com_2asterisco) == 1)
                ** Vale ressaltar que a embarcação 
                <? $adentrou = 'adentrou'; ?>
                <? $iniciou = 'iniciou'; ?>
            @else
                ** Vale ressaltar que as embarcações 
                <? $adentrou = 'adentraram'; ?>
                <? $iniciou = 'iniciaram'; ?>
            @endif
            <? $virg = ''; ?>
            <strong>
            @foreach($projetos_com_2asterisco as $projeto)
                {{$virg.trim($projeto->descricao)}}
                <? $virg = ', '; ?>
            @endforeach
            </strong>, {{$iniciou}} suas operações recentemente em AJB, e por este motivo ainda não {{$adentrou}} ao
        período de obrigatoriedade de contratação de mão de obra nacional.
        </div>    
        @endif
    @endif
        <br/>
    <p style="text-indent:30px;">Atestamos sob a pena do art. 299 do Código Penal Brasileiro serem verdadeiras as informações transcritas neste
    documento.</p>
    <br/><br/>
    <div style="text-align:center; width:100%;">
        Termos em que pede deferimento,<br/>
    Rio de Janeiro, {{ date('d')}} de {{$mes}} de {{ date('Y')}}.        
    </div>
    <br/><br/><br/><br/>
    @if (is_object($empresa->procurador))
    <div style="width:100%; text-align:center;font-weight:bold;">
        <span style="font-weight: normal">___________________________________________</span>
        <br/>{{$empresa->procurador->nome}}
        <br/>{{$empresa->procurador->titulo}}
        <br/>CPF:{{$empresa->procurador->cpf}}
    </div>
    @else
    <br/>(procurador não informado no cadastro da empresa)
    @endif
</body>