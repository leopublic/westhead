<ul class="nav nav-tabs hidden-xs">
@if ($ativo == 'os/show')
    <li class="active"><a href="#" >Dados da OS</a></li>
@else
    <li class=""><a href="/os/show/{{$id_os}}" >Dados da OS</a></li>
@endif
    <li class=""><a href="/os/candidatos/{{$id_os}}" >Candidatos</a></li>
    <li class=""><a href="/os/anexos/{{$id_os}}" >Arquivos</a></li>
    <li class=""><a href="/os/despesas/{{$id_os}}" >Despesas</a></li>
</ul>
