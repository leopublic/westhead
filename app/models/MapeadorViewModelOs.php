<?php
class MapeadorViewModelOs implements InterfaceMapeadorViewModel{
	public function atribua($dados, $os)
	{
		$os->id_empresa = $dados['id_empresa'];
		$os->id_projeto = $dados['id_projeto'];
		if(isset($dados['id_armador'])){
			$os->id_armador = $dados['id_armador'];					
		}
		if(isset($dados['id_centro_de_custo'])) {
			$os->id_centro_de_custo = $dados['id_centro_de_custo'];
		}
		$os->dt_solicitacao = $dados['dt_solicitacao'];
		$os->solicitante = $dados['solicitante'];
		$os->motorista = $dados['motorista'];
		$os->dados_do_voo = $dados['dados_do_voo'];
		$os->id_servico = $dados['id_servico'];
		$os->observacao = $dados['observacao'];

		return $os;
	}
}