<?php

class ProcessoController extends \ProcessoGenericoController {

    public function getSemnumero() {
        return $this->listaProcessosSemNumero();
    }

    public function postSemnumero() {
        Input::flash();
        return $this->listaProcessosSemNumero();
    }

    public function listaProcessosSemNumero($id_os = '') {
        set_time_limit(0);
        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", empresa.razaosocial"
                . ", empresa.apelido"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.string_1"
                . ", date_format(processo.date_1, '%d/%m/%Y') date_1"
                . " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join processo on processo.id_os = os.id_os"
                . " where 1=1";
        $filtrou = false;
        if (Input::get('situacao', Input::old('situacao', 'P')) == 'P') {
            $sql .= " and (processo.string_1 is null or processo.string_1 = '') ";
        }
        if (Input::get('tipo_servico', Input::old('tipo_servico')) != '') {
            $sql .= " and servico.id_tipo_servico in (" . Input::get('tipo_servico') . ")";
            $filtrou = true;
        } else {
            $sql .= " and servico.id_tipo_servico in (1,2,5,8)";
        }
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $sql .= " and os.id_empresa = " . Input::get('id_empresa', Input::old('id_empresa'));
            $filtrou = true;
        }
        if (Input::get('id_projeto', Input::old('id_projeto')) > 0) {
            $sql .= " and os.id_projeto = " . Input::get('id_projeto', Input::old('id_projeto'));
            $filtrou = true;
        }
        if (Input::get('numero', Input::old('numero')) > 0) {
            $sql .= " and os.numero = " . Input::get('numero', Input::old('numero'));
            $filtrou = true;
        }
        if ($id_os > 0) {
            $sql .= " and os.id_os= " . $id_os;
            $filtrou = true;
        }
        if ($filtrou) {
            $registros = \DB::select(\DB::raw($sql));
        } else {
            $registros = null;
        }
        $empresas = array('' => '(todas)') + \Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        $servicos = array('' => '(todos)') + \Servico::orderBy('descricao')->lists('descricao', 'id_servico');
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $projetos = \Projeto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))
                    ->orderBy('descricao')
                    ->lists('descricao', 'id_projeto');
            $projetos = array('' => '(todos)') + $projetos;
        } else {
            $projetos = array();
        }
        return View::make('processo.semnumero')
                        ->with('registros', $registros)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
                        ->with('servicos', $servicos)
        ;
    }

    public function getPrecadastro() {
        return $this->listaProcessosPreCadastro();
    }

    public function postPrecadastro() {
        Input::flash();
        return $this->listaProcessosPreCadastro();
    }

    public function getRegistrosemaberto() {
        set_time_limit(0);
        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", empresa.razaosocial"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.string_1"
                . ", date_format(processo.date_1, '%d/%m/%Y') date_1"
                . " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join processo on processo.id_os = os.id_os"
                . " where 1=1";
        if (Input::get('situacao', Input::get('situacao', 'P')) == 'P') {
            $sql .= " and (processo.string_1 is null or processo.string_1 = '') ";
        }
        if (Input::get('id_empresa', Input::get('id_empresa')) > 0) {
            $sql .= " and os.id_empresa = " . Input::get('id_empresa', Input::old('id_empresa'));
        }
        if (Input::get('id_projeto', Input::get('id_projeto')) > 0) {
            $sql .= " and os.id_projeto = " . Input::get('id_projeto', Input::old('id_projeto'));
        }
        if (Input::get('numero', Input::get('numero')) > 0) {
            $sql .= " and os.numero = " . Input::get('numero', Input::old('numero'));
        }
        if (Input::get('id_empresa', Input::get('id_empresa')) > 0) {
            $registros = \DB::select(\DB::raw($sql));
        } else {
            $registros = null;
        }
        $empresas = array('' => '(todas)') + \Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $projetos = \Projeto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))
                    ->orderBy('descricao')
                    ->lists('descricao', 'id_projeto');
            $projetos = array('' => '(todos)') + $projetos;
        } else {
            $projetos = array();
        }
        return View::make('processo.registroemaberto')
                        ->with('registros', $registros)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
        ;
    }

    public function postRegistrosemaberto() {
        return $this->getRegistrosemaberto();
    }

    public function listaProcessosPreCadastro() {
        set_time_limit(0);
        $sql = "select distinct os.id_os"
                . ", os.numero"
                . ", empresa.razaosocial"
                . ", empresa.apelido"
                . ", projeto.descricao descricao_projeto"
                . ", servico.descricao descricao_servico"
                . ", processo.string_4"
                . ", date_format(processo.date_3, '%d/%m/%Y') date_3"
                . " from os "
                . " join empresa on empresa.id_empresa = os.id_empresa"
                . " join projeto on projeto.id_projeto = os.id_projeto"
                . " join servico on servico.id_servico = os.id_servico"
                . " join processo on processo.id_os = os.id_os"
                . " where processo.id_tipoprocesso = 5";
        if (Input::get('situacao', Input::old('situacao', 'P')) == 'P') {
            $sql .= " and (processo.string_4 is null or processo.string_4 = '') ";
        }
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $sql .= " and os.id_empresa = " . Input::get('id_empresa', Input::old('id_empresa'));
        }
        if (Input::get('id_projeto', Input::old('id_projeto')) > 0) {
            $sql .= " and os.id_projeto = " . Input::get('id_projeto', Input::old('id_projeto'));
        }
        if (Input::get('numero', Input::old('numero')) > 0) {
            $sql .= " and os.numero = " . Input::get('numero', Input::old('numero'));
        }
//        if (Input::get('id_empresa', Input::old('id_empresa')) > 0){
        $registros = \DB::select(\DB::raw($sql));
//        } else {
//            $registros = null;
//        }
        $empresas = array('' => '(todas)') + \Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        $servicos = array('' => '(todos)') + \Servico::orderBy('descricao')->lists('descricao', 'id_servico');
        if (Input::get('id_empresa', Input::old('id_empresa')) > 0) {
            $projetos = array('' => '(todas)') + \Projeto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))
                            ->orderBy('descricao')
                            ->lists('descricao', 'id_projeto');
        } else {
            $projetos = array();
        }
        return View::make('processo.precadastro')
                        ->with('registros', $registros)
                        ->with('empresas', $empresas)
                        ->with('projetos', $projetos)
                        ->with('servicos', $servicos)
        ;
    }

    public function postAtualizacampo() {
        $ret = array();
        try {
            $campo_chave = Input::get('campo_chave');
            $valor_chave = Input::get('valor_chave');
            $campo = Input::get('campo');
            $valor = Input::get('valor');

            $processo = \Processo::where($campo_chave, '=', $valor_chave)
                    ->update(array($campo => $valor));
            $ret["msg"] = array(
                "tipoMsg" => "success"
            );
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
            $ret["msg"] = array(
                "tipoMsg" => "nop"
                , "textoMsg" => $msg
            );
        }
        return json_encode($ret);
    }

    public function postAtualizacampodata() {
        $ret = array();
        try {
            $campo_chave = Input::get('campo_chave');
            $valor_chave = Input::get('valor_chave');
            $campo = Input::get('campo');
            $valor = Input::get('valor');
            $x = new \Modelo;
            $valor = $x->dtViewParaModel($valor);

            $processo = \Processo::where($campo_chave, '=', $valor_chave)
                    ->update(array($campo => $valor));
            $ret["msg"] = array(
                "tipoMsg" => "success"
            );
        } catch (Exception $ex) {
            $msg = $ex->getMessage();
            $ret["msg"] = array(
                "tipoMsg" => "nop"
                , "textoMsg" => $msg
            );
        }
        return json_encode($ret);
    }

    public function getAdicione($id_visto) {
        $rep = new Westhead\Repositorios\RepositorioProcesso;
        $processo = $rep->criaProcessoNoVisto(1, $id_visto);
        return $this->rotaParaVisualizarProcesso($processo);
    }

    public function getParacima($id_processo) {
        $rep = new \Westhead\Repositorios\RepositorioProcesso;
        $processo = \Processo::where('id_processo', '=', $id_processo)->first();
        $rep->paraCima($processo);
        return $this->rotaParaVisualizarProcesso($processo);
    }

    public function getParabaixo($id_processo) {
        $rep = new \Westhead\Repositorios\RepositorioProcesso;
        $processo = \Processo::where('id_processo', '=', $id_processo)->first();
        $rep->paraBaixo($processo);
        return $this->rotaParaVisualizarProcesso($processo);
    }

    public function getTransferirpara($id_processo, $id_visto) {
        $processo = \Processo::find($id_processo);
        $processo->id_visto = $id_visto;
        $processo->save();
        return $this->rotaParaVisualizarProcesso($processo);
    }

    /**
     * Torna o processo , processo principal do visto
     * @param type $id_processo
     */
    public function getPrincipal($id_processo) {
        $processo = \Processo::find($id_processo);
        $processo->torneSePrincipal();
        return $this->rotaParaVisualizarProcesso($processo);
    }

    public function getNovovisto($id_processo) {
        $processo = \Processo::find($id_processo);
        $rep = new \Westhead\Repositorios\RepositorioVisto();
        $visto = $rep->criaNovoVisto($processo->candidato);
        $processo->id_visto = $visto->id_visto;
        $processo->save();
        $processo->torneSePrincipal();
        return $this->rotaParaVisualizarProcesso($processo);
    }

    public function getExcluir($id_processo) {
        $proc = \Processo::find($id_processo);
        $visto = $proc->visto;
        $proc->delete();
        Session::flash("flash_msg", "Processo excluído com sucesso!");
        return $this->rotaParaVisualizarProcesso(null, $visto);
    }

    public function rotaParaVisualizarProcesso(\Processo $processo = null, \Visto $visto = null) {
        if (isset($processo)) {
            $visto = \Visto::find($processo->id_visto);
            if ($visto->processo_atual) {
                return Redirect::to('/candidato/vistoatual/' . $processo->id_candidato . '#processo' . $processo->id_processo);
            } else {
                return Redirect::to('/candidato/vistosinativos/' . $processo->id_candidato . '/' . $processo->id_visto . '#processo' . $processo->id_processo);
            }
        } else {
            if (!isset($visto)) {
                throw new Exception("Nem processo nem visto informados para rota");
            } else {
                if ($visto->processo_atual) {
                    return Redirect::to('/candidato/vistoatual/' . $visto->id_candidato);
                } else {
                    return Redirect::to('/candidato/vistosinativos/' . $visto->id_candidato . '/' . $processo->id_visto);
                }
            }
        }
    }

    public function getCorrigevistos($id = 0){
        set_time_limit(0);
        $rep = new \Westhead\Repositorios\RepositorioProcesso;
        \Candidato::where('id_candidato', '>', $id)->orderBy('id_candidato')->chunk(200, function($candidatos) use($rep){
            foreach($candidatos as $candidato){
                $rep->corrige_processos_do_candidato($candidato->id_candidato);
            }
        });
    }

    public function getCorrigevistosprocessos(){
        $rep = new \Westhead\Repositorios\RepositorioProcesso;
        $rep->corrige_vinculos_todos_processos();
    }

    public function getCorrigevistocandidato($id_candidato){
        $rep = new \Westhead\Repositorios\RepositorioProcesso;
        $rep->corrige_processos_do_candidato($id_candidato);
    }

    public function getCorrigevistoempresa($id_empresa){
        set_time_limit(0);
        $rep = new \Westhead\Repositorios\RepositorioProcesso;
        \Candidato::where('id_empresa', '=', $id_empresa)->chunk(200, function($candidatos) use($rep){
            foreach($candidatos as $candidato){
                $rep->corrige_processos_do_candidato($candidato->id_candidato);
            }
        });
    }
}