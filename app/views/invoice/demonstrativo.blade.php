<html>
    <head>
        <style>
            *, body{font-family: Verdana, Arial, sans-serif;}
            table tr td{border:solid 1px black; border-collapse: collapse; font-size:12px;}
            table tr th{border:solid 1px black; padding-top:3px; padding-bottom:3px; font-size:12px;}
            table.semBorda tr td{border:none;}
            table.semBorda tr th{border:none;}
            table.detalhe {border: solid 1px black; width:100%; border-collapse: collapse}
            table.detalhe tr td{padding:3px;}

            table.detalhe tr.comdesp td{border-bottom: none}
            table.detalhe tr.desp td {border-top: none;}

            div.titulo{text-align: center; font-weight: bold;}
            table.semLinhas tr td
            ,table.semLinhas tr th{
                border: none;
                font-family: inherit;
            }
        </style>
    </head>
    <body>
        <div class="titulo">INVOICE N&ordm; {{$invoice->numeroFmt}}</div>
        <table style="width:100%;" class="semBorda">
            <tr>
                <td style="width:40%">
                    <table style="width:100%;" class="semBorda">
                        <tr>
                            <td style="text-decoration: underline; vertical-align: top; width:80px;">DATE:</td><td>{{$invoice->dataFmt}}</td>
                        </tr>
                        <tr>
                            <td style="text-decoration: underline; vertical-align: top;">INVOICE TO:</td><td style="font-weight:bold;">{{nl2br($invoice->end_cobr)}}</td>
                        </tr>
                    </table>
                </td>
                <td style="width:60%; text-align: right; font-weight: bold; vertical-align: top;">{{$os}}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="border: solid 1px black; text-align: center;">Due date {{$invoice->vencimentoFmt}}</td>
            </tr>
        </table>

        <table style="width:100%;" class="semBorda">
            <tr>
                <td style="width:50%; vertical-align: top;">
                    <span style="text-decoration: underline;">PAYMENT INSTRUCTIONS	:</span><br/>
                    <span style="font-weight: bold;">BANCO SANTANDER BRASIL S.A. (033)<br/><br/>
                        Av. Pres J. Kubitschek, 2041/2235A<br/>
                        São Paulo - SP<br/>
                        BRAZIL</span>
                </td>
                <td style="width:50%; vertical-align: top;">
                    <span style="text-decoration: underline;">Bank Transfer:</span><br/>
                    <span style="font-weight: bold;">Westhead Assessoria e Serviços de  Legalização LTDA.<br/>
                        CNPJ/MF: 14.984.183/0001-31<br/>
                        SWIFT CODE: BSCHBRSP<br/>
                        BANK: Banco Santander (Brasil) S.A.<br/>
                        AGENCY: 3826<br/>
                        BANK ACCOUNT Nº: 13.003174-2
                    </span>
                </td>
            </tr>
        </table>
        <br/>
        <table class="detalhe">
            <thead>
                <tr>
                    <th style="text-align:center;width:90px;">QUANT.</th>
                    <th>DESCRIPTION OF SERVICES</th>
                    <th style="text-align:right;width:150px;">UNIT PRICE<br/>USD</th>
                    <th style="text-align:right;width:150px;">TOTAL PRICE<br/>USD</th>
                </tr>                
            </thead>
            <tbody>
                {{ $total = 0}}
                @foreach($servicos as $servico)
                {{ $total += round($servico->val_unitario / $invoice->cotacao, 2) * $servico->qtd }}
                @if ($servico->val_despesas > 0)
                <tr class="comdesp">
                    @else
                <tr>
                    @endif
                    <td style="text-align:center; vertical-align: top; padding-top: 20px;">{{$servico->qtd}}</td>
                    <td>
                        {{nl2br($servico->descricao_invoice)}}
                    </td>
                    <td style="text-align:right;">{{number_format(round($servico->val_unitario / $invoice->cotacao, 2), 2, '.', ',')}}</td>
                    <td style="text-align:right;">{{number_format(round($servico->val_unitario / $invoice->cotacao, 2) * $servico->qtd, 2, '.', ',')}}</td>
                </tr>
                @if ($servico->val_despesas > 0)
                {{ $total += round($servico->val_despesas / $invoice->cotacao, 2) }}
                <tr class="desp">
                    <td style="text-align:center; vertical-align: top; padding-top: 20px;">1</td>
                    <td>
                        {{nl2br($servico->descricao_despesas)}}
                    </td>
                    <td style="text-align:right;">&nbsp;</td>
                    <td style="text-align:right;">{{number_format(round($servico->val_despesas / $invoice->cotacao, 2), 2, '.', ',')}}</td>
                </tr>
                @endif

                @endforeach
                <tr>
                    <td style="text-align:right;">&nbsp;</td>
                    <td style="font-weight:bold;">TOTAL AMOUNT (USD)</td>
                    <td style="text-align:right; font-weight:bold;">--</td>
                    <td style="text-align:right; font-weight:bold;">{{ number_format($total, 2, '.', ',') }}</td>
                </tr>

            </tbody>
        </table>
        <div style="font-weight: bold; font-size:12px;">Note: USD = American Dollars<br/>Dolar rate: R$ {{number_format($invoice->cotacao, 4, ".", ",")}}</div>