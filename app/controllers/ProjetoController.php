<?php

class ProjetoController extends \CadastroController {

    public function getIndex() {
        $registros = $this->RegistrosIndex();
        $tela = $this->TelaIndex();
        return View::make('projeto/list')
                        ->with('tela', $tela)
                        ->with('registros', $registros)
        ;
    }

    public function RegistrosIndex() {
        $data = Projeto::todos()->get();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(array("titulo" => "Empresa", "campo" => "razaosocial_empresa")
            , array("titulo" => "Armador", "campo" => "razaosocial_armador")
            , array("titulo" => "Projeto", "campo" => "descricao")
            , array("titulo" => "Início op.", "campo" => "inicio_operacao")
            , array("titulo" => "Venc. contrato", "campo" => "vencimento_contrato")
            , array("titulo" => "Venc. contrato", "campo" => "fl_ativa")
        );
        return $data;
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = Projeto::find($id);
        } else {
            $this->obj = new Projeto();
        }
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = "Projetos";
        $data['titulo_grid'] = "Clique na ação ao lado do projeto";
        $data['subTitulo'] = "Administrar projetos";
        $data['urlEdit'] = 'projeto/show';
        $data['urlDel'] = 'projeto/del';
        $data['urlCreate'] = 'projeto/create';
        $data['nomeCampoChave'] = 'id_projeto';
        return $data;
    }

    public function get_ViewShow() {
        return 'projeto/show';
    }

    public function MontaCampos() {
        //	$this->campos[] = new
    }

    public function getDel($id_projeto){
        $projeto = Projeto::find($id_projeto);
		try{
            $msg = '';
            if($projeto->oss()->count() > 0){
                $msg = 'Não é possível excluir esse projeto pois ele possui ordens de serviço associadas a ele.';
            }
            if($projeto->candidatos()->count() > 0){
                $msg = 'Não é possível excluir esse projeto pois ele possui candidatos associadas a ele.';
            }
            if ($msg ==''){
                $projeto->delete();
                $msg = 'Projeto excluído com sucesso';
            }

			return Redirect::to('/projeto')
					->with('flash_msg', $msg);
		}
		catch(Exception $e){
			return Redirect::to('/projeto')
					->with('flash_error', $e->getMessage());
		}
    }

    public function postUpdate($id = 0) {
        if ($id > 0) {
            $projeto = Projeto::find($id);
        } else {
            $projeto = new Projeto();
        }
        $projeto->id_empresa = Input::get('id_empresa');
        if (Input::get('id_empresa_armador') > 0){
            $projeto->id_empresa_armador = Input::get('id_empresa_armador');
        } else {
            $projeto->id_empresa_armador = null;
        }
        $projeto->descricao = Input::get('descricao');
        $projeto->dt_inicio_operacao = Input::get('dt_inicio_operacao');
        $projeto->dt_vencimento_contrato = Input::get('dt_vencimento_contrato');
        $projeto->justificativa = Input::get('justificativa');
        $projeto->fl_ativa = intval(Input::get('fl_ativa'));
        $projeto->fl_embarcacao = intval(Input::get('fl_embarcacao'));
        $projeto->qtd_tripulantes = intval(Input::get('qtd_tripulantes'));
        $projeto->qtd_estrangeiros = intval(Input::get('qtd_estrangeiros'));
        if (Input::get('id_pais') > 0) {
            $projeto->id_pais = Input::get('id_pais');
        } else {
            $projeto->id_pais = null;
        }
        // Validações
        $messages = array(
            'required' => 'O campo :attribute é obrigatório.'
        );
        $data = Input::all();
        $validator = Validator::make(
                        $data, array(
                    'id_empresa' => 'required',
//				'descricao' => 'required|unique:projeto,descricao,'.$id.',id_projeto'
                        ), $messages);
        if ($validator->passes()) {
            $projeto->save();
            return Redirect::to('projeto');
        } else {
            Input::flash();
            return Redirect::to('projeto/show/' . $id)->withErrors($validator);
        }
    }

    public function getDaempresajson($id_empresa) {
        return Response::json($this->projetosDaEmpresa($id_empresa, '(não informado)'));
    }

    public function getDaempresajsonfiltro($id_empresa) {
        return json_encode($this->projetosDaEmpresa($id_empresa, '(todas)'));
    }

    public function projetosDaEmpresa($id_empresa, $default){
        $projetos = DB::table('projeto')
                    ->select(array('id_projeto as id','descricao') )
                    ->where('id_empresa', '=', $id_empresa)
                    ->orderBy('descricao')->get();
        array_unshift($projetos, array('id'=> '0', 'descricao' => $default)) ;
        return $projetos;
    }
    
    
    public function getProporcionalidade(){
        $registros = Projeto::todos()->get();
        return View::make('projeto/proporcionalidade')
                        ->with('registros', $registros)
        ;        
    }
}
