<?php

class Perfil extends Modelo {

    protected $table = 'perfil';
    protected $primaryKey = 'id_perfil';
    protected $guarded = array();

    public function assumeDefaults() {
        
    }

    public static function regras() {
        return array(
            'nome' => 'required'
        );
    }

    public static function mensagens() {
        return array(
            'nome.required' => 'O nome é obrigatório!'
        );
    }

    public function usuarios() {
        return $this->belongsToMany('Usuario', 'usuario_perfil', 'id_perfil', 'id_usuario');
    }

}
