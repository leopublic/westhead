<?php
class TipoAnexoController extends \CadastroController {
	const raiz = "tipoanexo";
	const campoChave = 'id_tipo_anexo';
	const nomeSingular = 'Tipo de anexo';
	const nomePlural = 'Tipos de anexo';
	const artigo = 'o';
	public function RegistrosIndex()
	{
		$data = TipoAnexo::orderBy('nome_pt_br')->get();
		return $data;
	}

	public function ColunasIndex()
	{
		$data = array(
				array("titulo" => "Descrição", "campo" => "nome_pt_br"),
				array("titulo" => "Descrição (en)", "campo" => "nome_en_us")
			);
		return $data;
	}
	
	public function ObtemInstancia($id = 0)
	{
		if($id > 0){
			$this->obj = TipoAnexo::find($id);
		}
		else{
			$this->obj = new TipoAnexo;
		}
	}
	
	public function TelaIndex()
	{
		$data = array();
		$data['titulo_tela'] = self::nomeSingular;
		$data['titulo_grid'] = "Lista";
		$data['subTitulo'] = "Administrar ".self::nomePlural;
		$data['urlEdit'] = self::raiz.'/show';
		$data['urlDel'] = self::raiz.'/del';
		$data['urlCreate'] = self::raiz.'/create';
		$data['nomeCampoChave'] = self::campoChave;
        $data['menu'] = 'menuTabelas';
        $data['item'] = 'itemTipoAnexo';
		return $data;
	}

	public function get_ViewShow()
	{
		return self::raiz.'/show';
	}

	public function MontaCampos()
	{
	//	$this->campos[] = new 
	}

	public function getDel($id = 0)
	{
		try{
			TipoAnexo::destroy($id);			
			return Redirect::to(self::raiz)
					->with('flash_msg', self::nomeSingular.' excluíd'.self::artigo.' com sucesso!');		
		}
		catch(Exception $e){
			return Redirect::to(self::raiz)
					->with('flash_error', $e->getMessage());		
		}
	}

	public function postUpdate($id = 0)
	{
		if($id > 0){
			$obj = TipoAnexo::find($id);
		}
		else{
			$obj = new TipoAnexo();
		}
		$obj->fill(Input::all());

		if($obj->save()){
			return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular.' atualizad'.self::artigo.' com sucesso!');		
		}
		else{
			Input::flash();
			return Redirect::to(self::raiz.'/show/'.$id)->withErrors($obj->errors);			
		}
	}
}