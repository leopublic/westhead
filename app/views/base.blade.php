<!DOCTYPE html>
<html>
    <head>
        @include('html/head')
        <style>
            @yield('styles')
            @-webkit-keyframes animok
                {
                0%   {background: transparent;}
                25%  {background: green;}
                100% {background: transparent;}
                }
            table tr.topo-borda td{border-top: solid 2px #000;}

            table.tablesorter thead th{
                cursor:pointer;
            }
        </style>
    </head>

    <body class="skin-black">

        @include('navbar')

        <!-- BEGIN Container -->
        <div class="container" id="main-container">
            <!-- BEGIN Sidebar -->
            <div id="sidebar" class="navbar-collapse collapse">
                @include('menu')
                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-lg">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->
            <div id="main-content">
                <!-- BEGIN Content -->
                @yield ('content')
                <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a>
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->
        <!--basic scripts-->
        <script src="/assets/jquery/jquery-2.0.3.min.js"></script>
        <script src="/assets/jquery-ui/jquery-ui.min.js"></script>
        <script src="/assets/chosen-bootstrap/chosen.jquery.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script src="/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="/assets/jquery-cookie/jquery.cookie.js"></script>        <!--page specific plugin scripts-->
        <script src="/assets/flot/jquery.flot.js"></script>
        <script src="/assets/flot/jquery.flot.resize.js"></script>
        <script src="/assets/flot/jquery.flot.pie.js"></script>
        <script src="/assets/flot/jquery.flot.stack.js"></script>
        <script src="/assets/flot/jquery.flot.crosshair.js"></script>
        <script src="/assets/flot/jquery.flot.tooltip.min.js"></script>
        <script src="/assets/sparkline/jquery.sparkline.min.js"></script>
        <script src="/assets/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script src="/assets/bootbox/bootbox.min.js"></script>
        <script src="/assets/tablesorter/js/jquery.tablesorter.js" type="text/javascript" ></script>
        <script src="/assets/tablesorter/js/jquery.tablesorter.widgets.js" type="text/javascript" ></script>

        <!--flaty scripts-->
        <script src="/js/flaty.js"></script>
        <script>
            $(document).ready(function(){
                $(".tablesorter").tablesorter(); 
            });

            function RecarregaCombo(nomeCombo, dados)
            {
                $('#' + nomeCombo).find('option').remove();
                for (var i = 0, ilen = dados.length; i < ilen; i += 1) {
                    $('<option>').val(dados[i].id).text(dados[i].descricao).appendTo('#' + nomeCombo);
                    // etc
                }
                //$.each(dados, function(k, v) {
                //    $('<option>').val(k).text(v).appendTo('#' + nomeCombo);
                //});
            }
            /**
             * 
             * @param {type} nomeComboPai
             * @param {type} nomeComboFilho
             * @param {type} url
             * @param {type} msgVazio
             * @param {type} recarrega Indica se deve acionar a recarga quando o o vínculo for estabelecido (no load)
             * @returns {undefined}
             */
            function VinculaCombos(nomeComboPai, nomeComboFilho, url, msgVazio, recarrega)
            {
                $(document).on('change','#'+nomeComboPai,function(){
                        var id = $('#'+nomeComboPai).val();
                        RecarregaCombo(nomeComboFilho, [{id: '', descricao:'(atualizando...aguarde)'}]);
                        if (id > 0) {
                            $.ajax({
                                url: url + id,
                                dataType: "json"
                            }).done(function(data) {
                                RecarregaCombo(nomeComboFilho, data);
                            });
                        } else {
                            RecarregaCombo(nomeComboFilho, {'': msgVazio});
                        }
                    });
                if(recarrega){
                   $('#'+nomeComboPai).change();
                }
            }

            function atualizaCombo(id_combo, id, url, msgVazio){
                RecarregaCombo(id_combo, {'': '(atualizando...aguarde)'});
                if (id > 0) {
                    $.ajax({
                        url: url + id,
                        dataType: "json"
                    }).done(function(data) {
                        RecarregaCombo(id_combo, data);
                    });
                } else {
                    RecarregaCombo(id_combo, {'': msgVazio});
                }
            }

            function confirmaLink(msg, titulo, link){
                bootbox.dialog({
                  message: msg,
                  title: titulo,
                  buttons: {
                    success: {
                      label: "Sim! Pode excluir.",
                      className: "btn-success",
                      callback: function() {
                        window.location.replace(link);
                      }
                    },
                    danger: {
                      label: "Não! Me enganei.",
                      className: "btn-danger",
                      callback: function(){
                        bootbox.alert("Exclusão cancelada!");                          
                      }
                    }
                  }
                });                
            }

        </script>
        @yield ('scripts')
    </body>
</html>
