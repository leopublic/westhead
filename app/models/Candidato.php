<?php


/**
 * @property int $id_candidato
 * @property varchar $nome_completo
 * @property varchar $nome_pai
 * @property varchar $nome_mae
 * @property date $dt_nascimento
 * @property varchar $nu_passaporte
 * @property varchar $email
 * @property varchar $nu_cpf
 * @property varchar $nu_rne
 * @property varchar $nu_ctps
 * @property varchar $nu_cnh
 * @property date $dt_validade_cnh
 * @property date $dt_validade_ctps
 * @property varchar $sexo
 * @property text $trabalho_anterior
 * @property text $descricao_atividades
 * @property varchar $remuneracao_br
 * @property varchar $remuneracao_ext
 * @property varchar $local_nascimento
 * @property int $id_pais_nacionalidade
 * @property int $id_estadocivil
 * @property int $id_escolaridade
 * @property int $id_profissao
 * @property int $id_funcao
 * @property int $id_empresa
 * @property int $id_projeto
 * @property varchar $endereco_res
 * @property varchar $cidade_res
 * @property varchar $telefone_res
 * @property int $id_pais_res
 * @property varchar $endereco_estr
 * @property varchar $cidade_estr
 * @property varchar $telefone_estr
 * @property int $id_pais_estr
 * @property date $dt_emissao_passaporte
 * @property date $dt_validade_passaporte
 * @property int $id_pais_passaporte
 * @property int $id_candidato_principal
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property date $dt_emissao_cpf
 * @property int $id_reparticao
 * @property text $justificativa
 * @property int $id_pais_nacionalidade_pai
 * @property int $id_pais_nacionalidade_mae
 * @property int $id_parentesco 
 * @property date $data_ultima_entrada 
 */
class Candidato extends Modelo {

    protected $table = 'candidato';
    protected $primaryKey = 'id_candidato';
    protected $guarded = array('dias_desde_ultima_entrada');

    public function getDtNascimentoAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtNascimentoAttribute($data) {
        $this->attributes['dt_nascimento'] = $this->dtViewParaModel($data);
    }

    public function getDtEmissaoCpfAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtEmissaoCpfAttribute($data) {
        $this->attributes['dt_emissao_cpf'] = $this->dtViewParaModel($data);
    }

    public function getDtValidadeCnhAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtValidadeCnhAttribute($data) {
        $this->attributes['dt_validade_cnh'] = $this->dtViewParaModel($data);
    }

    public function getDtValidadeCtpsAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtValidadeCtpsAttribute($data) {
        $this->attributes['dt_validade_ctps'] = $this->dtViewParaModel($data);
    }

    public function getDataUltimaEntradaAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDataUltimaEntradaAttribute($data) {
        $this->attributes['data_ultima_entrada'] = $this->dtViewParaModel($data);
    }

    public function getDtEmissaoPassaporteAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtEmissaoPassaporteAttribute($data) {
        $this->attributes['dt_emissao_passaporte'] = $this->dtViewParaModel($data);
    }

    public function getDtValidadePassaporteAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtValidadePassaporteAttribute($data) {
        $this->attributes['dt_validade_passaporte'] = $this->dtViewParaModel($data);
    }

    public function getNuCpfLimpoAttribute($data){
        return str_replace('.', '', str_replace('-', '', str_replace(" ", "", $this->nu_cpf)));
    }

    public function empresa() {
        return $this->belongsTo('Empresa', 'id_empresa');
    }

    public function projeto() {
        return $this->belongsTo('Projeto', 'id_projeto');
    }

    public function visto() {
        return $this->hasMany('Visto', 'id_candidato');
    }

    public function vistos() {
        return $this->hasMany('Visto', 'id_candidato');
    }

    public function vistoAtual() {
        return Visto::where('id_candidato', '=', $this->id_candidato)
                    ->where('processo_atual', '=', 1)
                    ->first();
    }

    public function vistosinativos(){
        return Visto::docandidato($this->id_candidato)
                    ->inativos()
                    ->get();
        
    }
    
    public function anexos() {
        return $this->hasMany('Anexo', 'id_candidato');
    }

    public function dependentes() {
        return $this->hasMany('Candidato', 'id_candidato_principal');
    }

    public function dependentesforadaos($id_os) {
        return Candidato::where('id_candidato_principal', '=', $this->id_candidato)
                ->whereNotIn('id_candidato', function($query) use ($id_os){
                    $query->select('id_candidato')
                    ->from('os_candidato')
                    ->where('id_os', '=', $id_os);
                })->get();
    }

    public function dependentesnaos($id_os) {
        return Candidato::where('id_candidato_principal', '=', $this->id_candidato)
                ->whereIn('id_candidato', function($query) use ($id_os){
                    $query->select('id_candidato')
                    ->from('os_candidato')
                    ->where('id_os', '=', $id_os);
                })->get();
    }

    public function scopeDaEmpresa($query, $id_empresa){
        if ($id_empresa > 0){
            return $query->where('id_empresa', '=', $id_empresa);
        } else {
            return $query;
        }
    }

    public function scopeDoProjeto($query, $id_projeto){
        if ($id_projeto > 0){
            return $query->where('id_projeto', '=', $id_projeto);
        } else {
            return $query;
        }
    }

    public function scopeDoNome($query, $nome){
        return $query->where('nome_completo', 'like', '%'.str_replace(" ", "%", $nome).'%');
    }

    public function scopeDaFuncao($query, $id_funcao){
        if ($id_funcao > 0){
            return $query->where('id_funcao', '=', $id_funcao);
        } else {
            return $query;
        }
    }

    public function paispassaporte(){
        return $this->belongsTo('Pais', 'id_pais_passaporte');
    }

    public function nacionalidade(){
        return $this->belongsTo('Pais', 'id_pais_nacionalidade');
    }

    public function nacionalidadepai(){
        return $this->belongsTo('Pais', 'id_pais_nacionalidade_pai');
    }

    public function nacionalidademae(){
        return $this->belongsTo('Pais', 'id_pais_nacionalidade_mae');
    }

    public function profissao(){
        return $this->belongsTo('Profissao', 'id_profissao');
    }

    public function estadocivil(){
        return $this->belongsTo('Estadocivil', 'id_estadocivil');
    }

    public function escolaridade(){
        return $this->belongsTo('Escolaridade', 'id_escolaridade');
    }

    public function funcao(){
        return $this->belongsTo('Funcao', 'id_funcao');
    }

    public function reparticao(){
        return $this->belongsTo('Reparticao', 'id_reparticao');
    }

    public function tratamento(){
        if($this->sexo = "M"){
            return 'Sr.';
        } else {
            return 'Sra.';
        }
    }

    public function atualizaEmpresaProjeto($id_empresa, $id_projeto){
        $this->id_empresa = $id_empresa;
        $this->id_projeto = $id_projeto;
        $this->save();
    }
    
    public function candidatoprincipal(){
        return $this->belongsTo('Candidato', 'id_candidato_principal');
    }

    public function parentesco(){
        return $this->belongsTo('Parentesco', 'id_parentesco');
    }
   
    public function getQtdAnexosAttribute($data){
        return $this->anexos->count();
    }
   
    public function getQtdDependentesAttribute($data){
        return $this->dependentes->count();
    }
   
    public function getQtdVistosAttribute($data){
        return $this->vistos->count();
    }
   
    public function getQtdVistosInativosAttribute($data){
        return Visto::docandidato($this->id_candidato)
                    ->inativos()
                    ->count();
    }

    public function getDiasDesdeUltimaEntradaAttribute(){
        try{
            if($this->attributes['data_ultima_entrada'] != ''){
                $agora = \Carbon::now('America/Sao_Paulo');
                $data_ultima_entrada = \Carbon::createFromFormat('Y-m-d', $this->attributes['data_ultima_entrada']);
                return $data_ultima_entrada->diffInDays($agora);
            } else {
                return '--';
            }

        } catch(\Exception $e){
            return '--';
        }
    }
}
