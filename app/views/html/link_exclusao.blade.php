@if (!isset($class_btn))
<? $class_btn = 'btn-danger btn-sm';?>
@endif

<div class="btn-group">
    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle {{$class_btn}}"><i class="icon-trash"></i></a>
    <ul class="dropdown-menu btn-danger">
        <li><a href="{{ URL::to($url) }}">{{ $msg_confirmacao }}</a></li>
    </ul>
</div>

