<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminOrdenaProcessos extends Command {

    /**
     * The console command name.
     * @var string
     */
    protected $name = 'admin:ordenaprocessos';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Reordena os processos de todos os vistos pela ordem de criação dos mesmos';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return void
     */
    public function fire() {
        $rep = new \Westhead\Repositorios\RepositorioProcesso;
        $rep->reordenaDefault();
        $this->info('Processos atualizados com sucesso!');
        
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments() {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }
}