<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdTipoServicoToServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasColumn('servico', 'id_tipo_servico')){
			Schema::table('servico', function(Blueprint $table) {
				$table->integer('id_tipo_servico')->unsigned()->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasColumn('servico', 'id_tipo_servico')){
			Schema::table('servico', function(Blueprint $table) {
				$table->dropColumn('id_tipo_servico');
			});
		}
	}

}
