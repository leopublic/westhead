<table class="table table-striped table-hover fill-head">
    <thead>
        <tr>
            <th style="width: 40px">#</th>
            <th style="width: 80px">Ação</th>
            <th style="width: auto">Nome</th>
            <th style="width: auto;">Nome mãe</th>
            <th style="width: 150px;">Data nasc.</th>
            <th style="width: 80px;">Passaporte</th>
        </tr>
    </thead>
    @if(isset($registros))
    <tbody>
		<? $i = 0; ?>
		@foreach($registros as $registro)
		<? $i++; ?>
        <tr>
            <td><? print $i;?></td>
            <td>
                <a class="btn btn-primary btn-sm" href="{{ URL::to('/os/adicionarcandidato/'.$id_os.'/'.$registro->id_candidato) }}"><i class="icon-plus"></i></a>
                <a class="btn btn-primary btn-sm" href="{{ URL::to('/candidato/show/'.$registro->id_candidato) }}" target="_blank"><i class="icon-search"></i></a>
            </td>
            <td>{{ $registro->nome_completo }}</td>
            <td>{{ $registro->nome_mae }}</td>
            <td>{{ $registro->dt_nascimento }}</td>
            <td>{{ $registro->nu_passaporte }}</td>
        </tr>
		@endforeach
    </tbody>
    @endif
</table>
