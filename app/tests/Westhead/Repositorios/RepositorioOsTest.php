<?php

use Westhead\Repositorios;

class RepositorioOsTest extends TestCase {

    public function setUp() {
        parent::setUp();
             // add this to remove all event listeners
        Os::flushEventListeners();
        // reboot the static to reattach listeners
        Os::boot();
    }
    public function testAdicionaCandidato_atualizaEmpresa() {
        Auth::setUser(\Usuario::find(2));
        $emp1 = new \Empresa;
        $emp1->razaosocial = 'emp1';
        $emp1->save();
        $this->assertNotEmpty($emp1->id_empresa);

        $emp2 = new \Empresa;
        $emp2->razaosocial = 'emp2';
        $emp2->save();
        $this->assertNotEmpty($emp2->id_empresa);

        $cand = new \Candidato;
        $cand->nome_completo = 'xxx';
        $cand->nome_mae = 'yyy';
        $cand->dt_nascimento = '12/12/2012';
        $cand->save();
        $this->assertNotEmpty($cand->id_candidato);

        $cand->id_empresa = $emp1->id_empresa;

        $os = new \Os;
        $os->id_empresa = $emp2->id_empresa;
        $os->save();
        $this->assertNotEmpty($os->id_os);

        $rep = new \Westhead\Repositorios\RepositorioOs;
        $rep->adicionaCandidato($os, $cand);

        $cand2 = \Candidato::find($cand->id_candidato);

        $this->assertEquals($emp2->id_empresa, $cand2->id_empresa);

        \OsCandidato::where('id_candidato', '=', $cand->id_candidato)->delete();
        if (isset($cand)) {
            $cand->delete();
        }
        if (isset($os)) {
            $os->delete();
        }

        if (isset($emp1)) {
            $emp1->delete();
        }
        if (isset($emp2)) {
            $emp2->delete();
        }
    }

    public function testAdicionaCandidato_atualizaProjeto() {
        Auth::setUser(\Usuario::find(2));
        $emp1 = new \Empresa;
        $emp1->razaosocial = 'emp1';
        $emp1->save();
        $this->assertNotEmpty($emp1->id_empresa);

        $proj1 = new \Projeto;
        $proj1->descricao = 'proj1';
        $proj1->save();
        $this->assertNotEmpty($proj1->id_projeto);

        $proj2 = new \Projeto;
        $proj2->descricao = 'proj2';
        $proj2->save();
        $this->assertNotEmpty($proj2->id_projeto);

        $cand = new \Candidato;
        $cand->nome_completo = 'xxx';
        $cand->nome_mae = 'yyy';
        $cand->dt_nascimento = '12/12/2012';
        $cand->save();
        $this->assertNotEmpty($cand->id_candidato);

        $cand->id_empresa = $emp1->id_empresa;
        $cand->id_projeto = $proj1->id_projeto;

        $os = new \Os;
        $os->id_empresa = $emp1->id_empresa;
        $os->id_projeto = $proj2->id_projeto;
        $os->save();
        $this->assertNotEmpty($os->id_os);

        $rep = new \Westhead\Repositorios\RepositorioOs;
        $rep->adicionaCandidato($os, $cand);

        $cand2 = \Candidato::find($cand->id_candidato);

        $this->assertEquals($proj2->id_projeto, $cand2->id_projeto);


        \OsCandidato::where('id_candidato', '=', $cand->id_candidato)->delete();
        if (isset($cand)) {
            $cand->delete();
        }
        if (isset($os)) {
            $os->delete();
        }
        if (isset($emp1)) {
            $emp1->delete();
        }
        if (isset($proj1)) {
            $proj1->delete();
        }
        if (isset($proj2)) {
            $proj2->delete();
        }
    }


    public function testExcluiCandidato() {
        Auth::setUser(\Usuario::find(2));
        $emp1 = new \Empresa;
        $emp1->razaosocial = 'emp1';
        $emp1->save();
        $this->assertNotEmpty($emp1->id_empresa);

        $proj1 = new \Projeto;
        $proj1->descricao = 'proj1';
        $proj1->save();
        $this->assertNotEmpty($proj1->id_projeto);

        $serv = \Servico::where('id_tipo_servico', '=', 1)->first();
        $this->assertNotEmpty($serv->id_servico);
        
        $cand = new \Candidato;
        $cand->nome_completo = 'xxx';
        $cand->nome_mae = 'yyy';
        $cand->dt_nascimento = '12/12/2012';
        $cand->save();
        $this->assertNotEmpty($cand->id_candidato);

        $cand->id_empresa = $emp1->id_empresa;
        $cand->id_projeto = $proj1->id_projeto;

        $os = new \Os;
        $os->id_empresa = $emp1->id_empresa;
        $os->id_projeto = $proj1->id_projeto;
        $os->id_servico = $serv->id_servico;
        $os->save();
        $this->assertNotEmpty($os->id_os);

        $rep = new \Westhead\Repositorios\RepositorioOs;
        $rep->adicionaCandidato($os, $cand);

        $proc = \ProcessoAutorizacao::where('id_os', '=', $os->id_os)->first();
        $this->assertNotEmpty($proc->id_processo);
        
        $rep->removeCandidato($os, $cand);
        
        $proc = \ProcessoAutorizacao::where('id_os', '=', $os->id_os)->first();
        $this->assertEmpty($proc);
        
        $oscand = \OsCandidato::where('id_os', '=', $os->id_os)
                    ->where('id_candidato', '=', $cand->id_candidato)
                    ->first();
        $this->assertEmpty($oscand);
                
        if (isset($cand)) {
            $cand->delete();
        }
        if (isset($os)) {
            $os->delete();
        }
        if (isset($emp1)) {
            $emp1->delete();
        }
        if (isset($proj1)) {
            $proj1->delete();
        }
    } 
    
    public function testDesmembrar(){
        Auth::setUser(\Usuario::find(2));
        $emp1 = new \Empresa;
        $emp1->razaosocial = 'emp1';
        $emp1->save();
        $this->assertNotEmpty($emp1->id_empresa);

        $proj1 = new \Projeto;
        $proj1->descricao = 'proj1';
        $proj1->save();
        $this->assertNotEmpty($proj1->id_projeto);

        $serv = \Servico::where('id_tipo_servico', '=', 1)->first();
        $this->assertNotEmpty($serv->id_servico);

        $campos = array('id_empresa' => $emp1->id_empresa, 'id_projeto' => $proj1->id_projeto, 'id_servico' => $serv->id_servico);
        $os = new \Os($campos);
        $os->save();
        $this->assertNotEmpty($os->id_os);
        $this->assertNotEmpty($os->numero);

        $cand = new \Candidato;
        $cand->id_empresa = $emp1->id_empresa;
        $cand->id_projeto = $proj1->id_projeto;
        $cand->nome_completo = 'xxx';
        $cand->nome_mae = 'yyy';
        $cand->dt_nascimento = '12/12/2012';
        $cand->save();
        $this->assertNotEmpty($cand->id_candidato);

        $rep = new \Westhead\Repositorios\RepositorioOs;
        $rep->adicionaCandidato($os, $cand);

        $cand2 = new \Candidato;
        $cand2->nome_completo = 'xxx';
        $cand2->nome_mae = 'yyy';
        $cand2->dt_nascimento = '12/12/2012';
        $cand2->save();
        $this->assertNotEmpty($cand2->id_candidato);

        $rep->adicionaCandidato($os, $cand2);

        $id_candidatos = array($cand2->id_candidato);
        $novaOs = $rep->desmembrar($os, $id_candidatos);
        
        $this->assertEquals($os->id_empresa, $novaOs->id_empresa);
        $this->assertEquals($os->id_servico, $novaOs->id_servico);
        $this->assertEquals($os->id_projeto, $novaOs->id_projeto);
        $this->assertEquals($os->numero + 1, $novaOs->numero);
        $this->assertLessThan($os->created_at, $novaOs->created_at);
        
        $xcand2 = \OsCandidato::where('id_os', '=', $os->id_os)
                    ->where('id_candidato', '=', $cand2->id_candidato)->first();
        
        $this->assertEmpty($xcand2);
        $xcand3 = \OsCandidato::where('id_os', '=', $novaOs->id_os)->first();
        $this->assertEquals($cand2->id_candidato, $xcand3->id_candidato );

        
       
        if (isset($cand)) {
            \Processo::where('id_candidato', '=', $cand->id_candidato)->delete();
            $cand->delete();
        }
        if (isset($cand2)) {
            \Processo::where('id_candidato', '=', $cand2->id_candidato)->delete();
            $cand->delete();
        }
        if (isset($os)) {
            $os->delete();
        }
        if (isset($novaOs)) {
            $novaOs->delete();
        }

        if (isset($emp1)) {
            $emp1->delete();
        }
        if (isset($proj1)) {
            $proj1->delete();
        }
    }
}
