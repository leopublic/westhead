        <!-- BEGIN Navbar -->
        <div id="navbar" class="navbar navbar-gray">
            <button type="button" class="navbar-toggle navbar-btn collapsed" data-toggle="collapse" data-target="#sidebar">
                <span class="icon-reorder"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">
                <small>
                    <i class="icon-check-sign"></i>
                    Westhead Visas & Solutions
                </small>
            </a>

            <!-- BEGIN Navbar Buttons -->
            <ul class="nav flaty-nav pull-right">
                <li class="user-profile">
                    <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
                        <span id="user_info">
                            {{ Auth::user()->nome }}
                        </span>
                        <i class="icon-caret-down"></i>
                    </a>

                    <!-- BEGIN User Dropdown -->
                    <ul class="dropdown-menu dropdown-navbar" id="user_menu">
                        <li>
                            <a href="#">
                                <i class="icon-time"></i> Entrou às {{Auth::user()->dt_ult_acesso}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('auth/altereminhasenha')}}">
                                <i class="icon-lock"></i> Alterar senha
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{ URL::to('auth/logout')}}">
                                <i class="icon-off"></i> Sair
                            </a>
                        </li>
                    </ul>
                    <!-- BEGIN User Dropdown -->
                </li>
                <!-- END Button User -->
            </ul>
            <!-- END Navbar Buttons -->
        </div>
        <!-- END Navbar -->