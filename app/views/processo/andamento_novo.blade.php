

{{ Form::open(array("class"=>"form-horizontal" , 'id' => 'frmAdicionaProcesso')  ) }}
{{ Form::hidden('id_os', $os->id_os) }}
	<div class="form-group">
        {{Form::label('datahora', 'Data', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label', 'data-mask' => '99/99/9999')) }}
        <div class="col-sm-8 col-md-10 col-lg-10 controls">
            {{Form::text('datahora', $datahora, array('id' => 'datahora', 'class' => 'form-control') )}}
        </div>
    </div>
	<div class="form-group">
        {{Form::label('datahora', 'Status', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label')) }}
        <div class="col-sm-8 col-md-10 col-lg-10 controls">
            {{Form::select('id_status_andamento', $statuses_andamento, null, array('class' => 'form-control')) }}
        </div>
    </div>
	<div class="form-group">
        {{Form::label('datahora', 'Observações', array('class' => 'col-sm-4 col-md-2 col-lg-2 control-label')) }}
        <div class="col-sm-8 col-md-10 col-lg-10 controls">
            {{Form::textarea('obs_andamento', null, array('class' => 'form-control')) }}
        </div>
    </div>
    <div class="row" style="display: none;" id="msgandamento">
        <div class="col-md-12">
          <div class="alert alert-danger" id="textoMsgAndamento">
          </div>
        </div>
    </div>

{{Form::close()}}