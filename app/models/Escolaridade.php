<?php
class Escolaridade extends Eloquent {
	protected $table = 'escolaridade';
	protected $primaryKey = 'id_escolaridade';

	protected $guarded = array();

	public function assumeDefaults()
	{
		if($this->descricao_en = ''){
			$this->descricao_en = $this->descricao;
		}
	}

	public static function regras()
	{
		return array(
				'descricao' => 'required'
				);
	} 

	public static function mensagens()
	{
		return array(
				'descricao.required' => 'A descrição é obrigatória!'
			);
	}
	
}
