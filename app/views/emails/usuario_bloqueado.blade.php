<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
    Sistema Westhead - Atenção<br/>
    <br/>
    <br/>Às {{date('Y-m-d H:i:s')}} o usuário "{{$username}}" tentou acessar o sistema mas errou a senha por mais de 5 vezes seguidas e teve seu acesso bloqueado.<br/>
    <br/>O acesso foi feito a partir do IP {{$ip}}
    <br/>
    <br/>
    <br/>Atenciosamente,
    <br/>Westhead
    </body>
</html>