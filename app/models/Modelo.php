<?php

class Modelo extends Eloquent {

    public $errors;

    public function assumeDefaults() {

    }

    public function valido($regras, $mensagens) {
        $validador = Validator::make($this->attributes, $regras, $mensagens);
        if ($validador->passes()) {
            return true;
        } else {
            $this->errors = $validador->messages();
            return false;
        }
    }

    public function dtModelParaView($data) {
        if ($data != '' && $data != '00/00/0000' && $data != '0000-00-00') {
            return Carbon::createFromFormat('Y-m-d', $data)->format('d/m/Y');
        } else {
            return null;
        }
    }

    public function dtHrModelParaView($data) {
        if ($data != '' ) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y H:i:s');
        } else {
            return $data;
        }
    }

    public function dtViewParaModel($data) {
        if ($data != '' && $data != '00/00/0000' && $data != '0000-00-00') {
            return Carbon::createFromFormat('d/m/Y', substr($data, 0,10))->format('Y-m-d');
        } else {
            return null;
        }
    }

    public function getCaracteresTd($propriedade, $tam_max, $inicio = 0){
        $cols = 0;
        $ret = '';
        $valor = trim($this->$propriedade);
        for ($index = $inicio; $index < strlen($valor); $index++) {
            $cols++;
            if ($cols <= $tam_max){
                $ret .= '<td>' . mb_substr($valor, $index, 1) . '</td>';
            }
        }
        if ($cols < $tam_max){
            $ret .= str_repeat('<td>&nbsp;</td>', $tam_max - $cols);
        }
        return $ret;
    }
    
    public static function dataOk($valor){
        $dia = substr($valor, 0,2);
        $mes = substr($valor, 3,2);
        $ano = substr($valor, 6,4);
        return checkdate($mes, $dia, $ano);
    }
}
