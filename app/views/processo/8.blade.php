@extends('processo.comum')

<? $tipo_processo = 'cancelamento'; ?>
<? $nome_servico = 'Cancelamento'; ?>

@section('campos')
    <div class="row">
        @if($processo->id_os > 0)
            @include('html/campo-texto', array('id' => 'razaosocial', 'label' => 'Empresa', 'valor' => $processo->razaosocial_empresa, 'atributos'=> array( "disabled"=>"disabled")))
            @include('html/campo-texto', array('id' => 'projeto', 'label' => 'Proj./Emb.', 'valor' => $processo->descricao_projeto, 'atributos'=> array( "disabled"=>"disabled")))
            @include('html/campo-texto', array('id' => 'servico', 'label' => 'Serviço', 'valor' => $processo->descricao_servico, 'atributos'=> array( "disabled"=>"disabled")))
        @else
            @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => $processo->id_empresa, 'valores' => array('0' => '(não informado)') +  Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa'),  'help' => '', array()))
            @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Proj./Emb.', 'valor' => $processo->id_projeto, 'valores' => array('0' => '(não informado)') +  Projeto::orderBy('descricao')->lists('descricao', 'id_projeto'),  'help' => '', array()))
            @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => $processo->id_servico, 'valores' => array('0' => '(não informado)') +  Servico::where('id_tipo_servico', '=', 8)->orderBy('descricao')->lists('descricao', 'id_servico'),  'help' => '', array()))
        @endif
        @include('html/campo-texto', array('id' => 'numero_processo', 'label' => 'Processo', 'valor' => $processo->numero_processo,  'help' => '', 'atributos'=> array( "data-mask"=>"99999.999999/9999-99")))
        @include('html/campo-data', array('id' => 'data_requerimento', 'label' => 'Data requerimento', 'valor' => $processo->data_requerimento,  'help' => '',  array()))
        @include('html/campo-texto', array('id' => 'numero_oficio', 'label' => 'Número ofício', 'valor' => $processo->numero_oficio,  'help' => '', array()))
        @include('html/campo-data', array('id' => 'data_deferimento', 'label' => 'Data deferimento', 'valor' => $processo->data_deferimento,  'help' => '', array()))
        @include('html/campo-texto', array('id' => 'dt_envio_bsb_fmt', 'label' => 'Envio para BSB', 'valor' => $processo->dt_envio_bsb_fmt,  'help' => '', array()))
    </div>
@overwrite
