@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Atualizar andamento de autorizações', 'subTitulo' => ''))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
                {{ Form::open(array( "class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'numero', 'label' => 'Número', 'valor' => Input::old('numero', Session::get('numero')),  'help' => '',  array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => Input::old('id_empresa', Session::get('id_empresa')), 'valores' => $empresas , 'help' => '', 'valores', array()))
                    </div>

                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Emb./Proj.', 'valor' => Input::old('id_projeto', Session::get('id_projeto')), 'valores' => $projetos , 'help' => '', 'valores', array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => Input::old('id_servico'), 'valores' => $servicos , 'help' => '',  array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_status_andamento', 'label' => 'Andamento', 'valor' => Input::old('id_status_andamento'), 'valores' => $status_andamento , 'help' => '', 'valores', array()))
                    </div>
                </div>
            </div>
        </div>
            {{ Form::close()}}
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Processos</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                    <div id="msg"></div>
                @if(isset($registros))
                <ul class="pagination pagination-xs pagination-colory">
                    <li class="disabled"><span>«</span></li>
                    <?php for($i=0;$i<$qtd_pag;$i++) { ?>
                        @if($i+1 == $pag)
                            <li class="active"><span>{{ $i+1 }}</span></li>
                        @else
                            <li><a href="/andamento?pag={{ $i+1 }}">{{ $i+1 }}</a></li>
                        @endif
                    <?php } ?>
                </ul>
                <table class="table table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 80px; text-align:center;">Os</th>
                            <th style="width: auto;">Empresa<br/><span style="font-style:italic;">Projeto/Emb.</span></th>
                            <th style="width: 200px">Candidato</th>
                            <th style="width: auto;">Serviço</th>
                            <th style="width: auto; text-align: center;">Dt. req.</th>
                            <th style="width: auto;">Número</th>
                            <th style="width: auto;">Status and. atual</th>
                            <th style="width: auto;">Atualizado em</th>
                            <th style="width: auto; text-align: center;">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $registro)
                        <tr>
                            <td><a class="btn btn-primary btn-sm" href="{{ URL::to('/os/show/'.$registro->id_os) }}" target="_blank">{{ substr('000000'.$registro->numero, -6) }}</a></td>
                            <td>{{ $registro->apelido}}<br/><span style="font-style: italic; color:#AAAAAA;">{{ $registro->descricao_projeto }}</span></td>
                            <td>
                                <? $br = '';
                                $os = \Os::find($registro->id_os);
                                $candidatos = $os->candidatos;
                                ?>
                                @foreach($candidatos as $candidato)
                                {{$br}}<a href="{{URL::to('/candidato/vistoatual/'.$candidato->id_candidato)}}" target="_blank" title="Clique para ver o candidato em uma nova janela">{{ $candidato->nome_completo }}</a>
                                <? $br = '<br/>';?>
                                @endforeach
                            </td>
                            <td>
                               {{ $registro->descricao_servico  }}
                            </td>
                            <td style="text-align: center;">{{$registro->date_1}}</td>
                            <td style="text-align: center;">{{$registro->string_1}}</td>
                            <td style="text-align: center;">{{$registro->nome_status_andamento}}</td>
                            <td style="text-align: center;">{{$registro->datahora}}</td>
                            <td><a href="javascript:adicionaAndamento(this, '{{$registro->id_os}}');" class="btn" id="btn{{$registro->id_os}}">Adicionar</a>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                Informe um filtro
                @endif
            </div>
    </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="mdlAdicionaProcesso">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Adicionar andamento</h4>
      </div>
      <div class="modal-body" id="bodyAdicionarProcesso">
        <p></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <a id="btnEnviar" href="javascript:enviaAndamento();" class="btn btn-primary">Adicionar</a>
        <span id="btnEnviando" class="btn btn-primary" style="display:none;">Enviando...</span>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop
@section('scripts')
<script>
$( document ).ready(function(){
    VinculaCombos('id_empresa', 'id_projeto', '/projeto/daempresajsonfiltro/', '(selecione a empresa)', false);
});
$( document ).ready(function(){
    $('#menuAndamento').addClass('active');
    $('#itemAutorizacoes').addClass('active');
});

function adicionaAndamento(botao, id_os){
    $(botao).text = 'Abrindo...';
    $('#btn'+id_os).text="Abrindo...";
    var url = 'processo1/adicionarandamento/'+id_os;
    $('#bodyAdicionarProcesso').html('');
    $('#btnEnviar').show();
    $('#btnEnviando').hide();
    $('#bodyAdicionarProcesso').load(url, function(response, status, xhr){
        $('#mdlAdicionaProcesso').modal();
        $(botao).text = 'Adicionado';

    });
}

function enviaAndamento(){
    var url = 'processo1/adicionarandamento';
    $('#btnEnviar').hide();
    $('#btnEnviando').show();
    $('#msgandamento').hide();
    $.post(url, $('#frmAdicionaProcesso').serialize(), function(data){
            console.log(data);
            if (data.codret == 1){
                $('#mdlAdicionaProcesso').modal('hide');
                var mensagem = '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Andamento adicionado com sucesso!</div>';
                $('#msg').append(mensagem);

            } else {
                $('#textoMsgAndamento').html('Não foi possível adicionar o andamento. Verifique:'+data.msg);
                $('#msgandamento').show();
                $('#btnEnviar').show();
                $('#btnEnviando').hide();
            }
        }, "json");
}

function atualizaCampo(pinput, ptipo){
    var valor = $(pinput).val();
    var td = $(pinput).parent();
    if (ptipo == '9'){
        var valor_limpo = valor.replace(/\D/g,'');        
    } else {
        var valor_limpo = valor;        
    }
    valor_original = $(pinput).attr('data-valor-original');
    if (valor_limpo != valor_original  && valor.indexOf("_") == -1){
        td.animate({backgroundColor: "#ede32d"}, 250 );
        var pParametros = $(pinput).attr('data-parameters');
        var parm = jQuery.parseJSON(pParametros);
        var url = '{{URL::to('')}}/'+parm.controller+'/'+parm.metodo;
        parm['valor'] = valor;
        $.noty.closeAll();
        $.ajax({
            type: 'POST'
          , url: url
          , data: parm
          , dataType: 'json'
          , success:
                function(data) 
                {
                    if(data.msg.tipoMsg == 'success'){
                        $(pinput).attr('data-valor-original', valor);
                        td.animate({backgroundColor: "#8de580"}, 250 );
                        if(data.novoConteudo){
                            $(td).html(data.novoConteudo);
                        }
                    }
                    else{
                        if(data.msg.tipoMsg == 'nop'){
                            $.noty.closeAll();
                            data.msg.textoMsg = 'Não foi possível atualizar devido ao seguinte erro: '+data.msg.textoMsg;
                            var xx= noty({text: data.msg.textoMsg, timeout: false, type: 'error', closeWith: 'click', layout:'top' });
                        }
                    }
                }
        });        
    }
}

</script>
@stop
