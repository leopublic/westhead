@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-map-marker"></i>{{ $tela['titulo_tela'] }}</h1>
                        <h4>{{ $tela['subTitulo'] }}</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Usuários</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                @if(count($errors->all())> 0)
                <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-danger">
                        <ul class="errors">
                          @foreach($errors->all() as $message)
                          <li>{{ $message }}</li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                </div>
                @endif
                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Criar/alterar usuário</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                {{ Form::open(array('url' => 'usuario/update/'.$obj->id_usuario, "class"=>"form-horizontal")) }}
                                    @include('html/campo-texto', array('id' => 'nome', 'label' => 'Nome', 'valor' => $obj->nome,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'username', 'label' => 'Login', 'valor' => $obj->username,  'help' => '',  array()))
                                    @if($obj->id_usuario == 0)
                                      @include('html/campo-password', array('id' => 'password', 'label' => 'Senha', 'help' => '',  array()))
                                      @include('html/campo-password', array('id' => 'password_confirmation', 'label' => '(repita)', 'help' => '',  array()))
                                    @endif
                                    @include('html/campo-texto', array('id' => 'email', 'label' => 'Email', 'valor' => $obj->email,  'help' => '',  array()))
                                    @include('html/campo-texto', array('id' => 'observacao', 'label' => 'Observação', 'valor' => $obj->observacao,  'help' => '',  array()))

                                    <div class="form-group">
                                      {{Form::label('id_perfil', 'Perfis do usuário', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                                       <div class="col-sm-9 col-lg-10 controls">
                                            <div class="row">
                                            @foreach(Perfil::orderBy('nome')->get() as $perfil)

                                             <div class="col-md-3">
                                            <input type="checkbox" name="id_perfil[]" value="{{$perfil->id_perfil}}"
                                                 @if ( in_array($perfil->id_perfil, $obj->perfis->lists('id_perfil') ) )
                                                    checked="checked"
                                                 @endif
                                                  />{{$perfil->nome}}
                                            </div>
                                            @endforeach
                                            </div>

                                       </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                           {{ HTML::link('usuario', 'Cancelar', array('class' => 'btn')) }}
                                        </div>
                                    </div>
                                 {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

             </div>
@stop