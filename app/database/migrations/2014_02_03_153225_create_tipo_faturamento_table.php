<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoFaturamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipo_faturamento', function(Blueprint $table) {
			$table->increments('id_tipo_faturamento');
			$table->string('descricao');
			$table->timestamps();
		});

        DB::table('tipo_faturamento')->truncate();

        $obj = array("descricao" => "Global Westhead");
        DB::table('tipo_faturamento')->insert($obj);
        $obj = array("descricao" => "Global empresa");
        DB::table('tipo_faturamento')->insert($obj);
        $obj = array("descricao" => "Cotação do dia");
        DB::table('tipo_faturamento')->insert($obj);

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipo_faturamento');
	}

}
