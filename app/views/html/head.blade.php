<!-- END head -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Westhead</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

<!--base css styles-->
<link rel="stylesheet" type="text/css" href="/assets/chosen-bootstrap/chosen.min.css">
<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/assets/jquery-tags-input/jquery.tagsinput.css">
<link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/assets/bootstrap-datepicker/css/datepicker.css">
<link rel="stylesheet" type="text/css" href="/assets/dropzone-3.8.3/downloads/css/dropzone.css">
<link rel="stylesheet" type="text/css" href="/assets/jquery-ui/jquery-ui.min.css">
<!--page specific css styles-->

<!--flaty css styles-->
<!--
<link rel="stylesheet" href="/css/flaty.css">
<link rel="stylesheet" href="/css/flaty-responsive.css">
-->
<link rel="stylesheet" href="/flatty/css/flaty.css">
<link rel="stylesheet" href="/flatty/css/flaty-responsive.css">
<link rel="shortcut icon" href="/img/favicon.png">
<!-- END head -->