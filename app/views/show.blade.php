  @extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>{{ $tela['titulo_tela'] }}</h1>
                        <h4>{{ $tela['subTitulo'] }}</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Layout</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Simple Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                 <form action="#" class="form-horizontal">
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Small Input</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <input type="text" placeholder="small" class="form-control input-sm" />
                                          <span class="help-inline">Some hint here</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Disabled Input</label>
                                       <div class="col-sm-9 col-lg-10 controls">   
                                          <input class="form-control" type="text" placeholder="Disabled input here..." disabled />
                                          <span class="help-inline">Some hint here</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Readonly Input</label>
                                       <div class="col-sm-9 col-lg-10 controls">   
                                          <input class="form-control" readonly type="text" placeholder="Readonly input here..." disabled />
                                          <span class="help-inline">Some hint here</span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Small Dropdown</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <select class="form-control input-sm" tabindex="1">
                                             <option value="Category 1">Category 1</option>
                                             <option value="Category 2">Category 2</option>
                                             <option value="Category 3">Category 5</option>
                                             <option value="Category 4">Category 4</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Default Dropdown</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <select class="form-control" tabindex="1">
                                             <option value="Category 1">Category 1</option>
                                             <option value="Category 2">Category 2</option>
                                             <option value="Category 3">Category 5</option>
                                             <option value="Category 4">Category 4</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Large Dropdown</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <select class="form-control input-lg" tabindex="1">
                                             <option value="Category 1">Category 1</option>
                                             <option value="Category 2">Category 2</option>
                                             <option value="Category 3">Category 5</option>
                                             <option value="Category 4">Category 4</option>
                                          </select>
                                       </div>
                                    </div>

                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Radio Buttons</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <label class="radio">
                                              <input type="radio" name="optionsRadios1" value="option1" /> Option 1
                                          </label>
                                          <label class="radio">
                                              <input type="radio" name="optionsRadios1" value="option2" checked /> Option 2
                                          </label>  
                                          <label class="radio">
                                              <input type="radio" name="optionsRadios1" value="option2" /> Option 3
                                          </label>  
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Inline Radio Buttons</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <label class="radio-inline">
                                              <input type="radio" name="optionsRadios2" value="option1" /> Option 1
                                          </label>
                                          <label class="radio-inline">
                                              <input type="radio" name="optionsRadios2" value="option2" checked /> Option 2
                                          </label>  
                                          <label class="radio-inline">
                                              <input type="radio" name="optionsRadios2" value="option2" /> Option 3
                                          </label>  
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Checkbox</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <label class="checkbox">
                                            <input type="checkbox" value="" /> Checkbox 1
                                          </label>
                                          <label class="checkbox">
                                            <input type="checkbox" value="" /> Checkbox 2
                                          </label>
                                          <label class="checkbox">
                                            <input type="checkbox" value="" /> Checkbox 3
                                          </label>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Inline Checkbox</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <label class="checkbox-inline">
                                            <input type="checkbox" value="" /> Checkbox 1
                                          </label>
                                          <label class="checkbox-inline">
                                            <input type="checkbox" value="" /> Checkbox 2
                                          </label>
                                          <label class="checkbox-inline">
                                            <input type="checkbox" value="" /> Checkbox 3
                                          </label>
                                       </div>
                                    </div>

                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Textarea</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <textarea class="form-control" rows="3"></textarea>
                                       </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                           <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Save</button>
                                           <button type="button" class="btn">Cancel</button>
                                        </div>
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>

             </div>
@stop