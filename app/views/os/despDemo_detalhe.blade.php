<style>
    *{font-family: arial; font-size:12px;}
    table tr th{text-align: left; font-weight:bold;}
    table tr.subtitulo {margin-top: 25px;}
    table tr.subtitulo {margin-top:15px;margin-bottom:5px;}
    table tr.subtitulo th{text-align:left;color:white;font-size:16px; padding:5px;margin-top:15px; margin-bottom:5px;background-color: #ccc;}

    table{font-size:12px;}
    table.despesa{border: solid 2px #000; border-collapse: collapse; width:100%}
    table.despesa tr th{background-color: #ccc; border: solid 1px #000;  padding: 3px;}
    table.despesa tr td{border: solid 1px #000; padding: 3px;}
    table.despesa tr.fim th{border-top: solid 2px #000; padding:3px; font-size: 14px;}
    table.despesa tr.vazia td{border: none;}
</style>
<body>
    <table style="border-bottom:solid 4px #ccc; width:100%;">
        <tr>
            <td style="width:300px;font-weight:bold;font-size:30px;color:#666;">OS {{substr('000000'.$os->numero, -6)}}</td>
            <td style="text-align:right;widht:40%;font-weight:bold;font-size:30px;font-family:serif">WESTHEAD</td>
        </tr>
    </table>
    <span style="font-weight:bold;">{{$os->empresa->razaosocial}} - {{$os->dt_execucao}}</span>
    <br/><span>{{$os->projeto->descricao}}</span>
<h1 style="text-align: center;font-size:25px;">FINANCE REPORT / RELATÓRIO DE DESPESAS</h1>

<table style="background-color:#ccc; border: solid 2px #000; font-weight: bold;">
    <tr><td style="vertical-align: top;">Company / Cliente</td><td>:</td><td>{{$os->empresa->razaosocial}}</td></tr>
    <tr><td style="vertical-align: top;">Type of Services / Serviços</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;">{{$os->servico->descricao}} - {{$os->qtdcandidatos()}}
            <br/>{{$os->servico->descricao_en}} - {{$os->qtdcandidatos()}}
        </td></tr>
</table>
    <br/>
    <br/>
<table class="despesa">
    <tr>
        <th style="text-align: center;">#</th>
        <th>DESPESAS / EXPENSES</th>
        <th>OBSERVAÇÕES</th>
        <th style="text-align:center;">Qtd</th>
        <th style="text-align:right;">Val. unit.<br>(R$)</th>
        @if ($os->valorCotacao() > 0)
        <th style="text-align:right;">Val. unit.<br>(US$)<br>(Câmbio:<br/>R$ {{ number_format($os->valorCotacao(), 2, ',', '.')}})</th>
        @else
        <th style="text-align:right;">Val. unit.<br>(US$)<br>(Câmbio: n/d)</th>
        @endif
        <th style="text-align:right;width:120px;">Valor total<br/>R$</th>
        <th style="text-align:right;width:120px;">Valor total<br/>US$</th>
    </tr>
    <? $i = 0;
    $sum_r = 0;
    $sum_u = 0;
    ?>
    @foreach ($os->despesas as $desp)
    <? $i++; ?>
    <tr>
        <td style="text-align: center;">{{$i}}</td>
        <td>{{$desp->tipodespesa->descricao_pt_br}} / {{$desp->tipodespesa->descricao_en_us}}</td>
        <td>{{$desp->observacao}}</td>
        <td style="text-align:center;">{{ number_format($desp->qtd, 0, ',', '.')}}</td>
        <td style="text-align:right;">R$ {{ number_format($desp->valor, 2, ',', '.')}}</td>
        <td style="text-align:right;">
        @if ($os->valorCotacao() > 0)
            US$ {{ number_format($desp->valor_usd(), 2, ',', '.')}}
        @else
            (n/d)
        @endif
        </td>
        <td style="text-align:right;">R$ {{ number_format($desp->valor_total(), 2, ',', '.')}}</td>
        <td style="text-align:right;">
        @if ($os->valorCotacao() > 0)
            US$ {{ number_format($desp->valor_total_usd(), 2, ',', '.')}}
        @else
            (n/d)
        @endif
</td>
    </tr>
    <?
    $sum_r += $desp->valor_total();
    $sum_u += $desp->valor_total_usd();
    ?>
    @endforeach
    <tr class="fim">
        <th colspan="6">TOTAL DE DESPESAS / TOTAL EXPENSES</th>
        <th style="text-align:right;">R$ {{ number_format($sum_r, 2, ',', '.')}}</th>
        @if ($os->valorCotacao() > 0)
        <th style="text-align:right;">US$ {{ number_format($sum_u, 2, ',', '.')}}</th>
        @else
        <th style="text-align:right;">(n/d)</th>
        @endif
    </tr>
</table>