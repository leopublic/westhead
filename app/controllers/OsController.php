<?php

class OsController extends BaseController {

    protected $campos;
    protected $obj;

    public function __construct(InterfaceRepositorio $repositorio) {
        $this->repositorio = $repositorio;
    }

    public function getIndex() {
        $filtros = array();
        $post = array('id_empresa', 'id_projeto', 'id_centro_de_custo', 'nome_completo', 'dt_solicitacao_ini', 'dt_solicitacao_fim', 'dt_execucao_ini', 'dt_execucao_fim', 'id_funcao', 'id_empresa_armador', 'id_servico', 'numero', 'id_status_os');
        foreach($post as $key ){
            $filtros[$key] = Input::old($key, Session::get($key));
        }
        $empresas = array('' => '(todas)') + Empresa::where('fl_armador', '=', 0)->lists('razaosocial', 'id_empresa');
        if (isset($filtros['id_empresa'])){
            $id_empresa = $filtros['id_empresa'];
        } else {
            $id_empresa = '';
        }
        if ($id_empresa > 0){
            $projetos = array('' => '(todas)') + Projeto::daEmpresa($id_empresa)->lists('descricao', 'id_projeto');
            $centros = array('' => '(todos)') + Centrodecusto::daEmpresa($id_empresa)->lists('nome', 'id_centro_de_custo');
        } else {
            $projetos = array('' => '(selecione a empresa)');
            $centros = array('' => '(selecione a empresa');
        }
        $armadores = array('' => '(todos)') + Armador::where('fl_armador', '=', 1)->lists('razaosocial', 'id_empresa');
        $servicos = array('' => '(todos)') + Servico::orderBy('descricao')->lists('descricao', 'id_servico');
        $statusos = array('' => '(todos)') + StatusOs::orderBy('nome')->lists('nome', 'id_status_os');
        
        $perfis = \Auth::user()->listaPerfis();
        \log::info('perfis='.$perfis);
        if (strstr($perfis, "4")){
            $financeiro = true;
        } else {
            $financeiro = false;
        }

        $registros = $this->repositorio->all($filtros);
//        $queries = DB::getQueryLog();
//        $last_query = end($queries);
//        var_dump($last_query);

        return View::make('os/index')
                        ->with('registros', $registros)
                        ->with('empresas', $empresas)
                        ->with('armadores', $armadores)
                        ->with('projetos', $projetos)
                        ->with('centros', $centros)
                        ->with('servicos', $servicos)
                        ->with('statusos', $statusos)
                        ->with('financeiro', $financeiro)
        ;
    }

    public function postExcel(){
        set_time_limit(0);
        ini_set('memory_limit','256M'); 
        $post = array('id_empresa', 'id_projeto', 'id_centro_de_custo', 'nome_completo', 'dt_solicitacao_ini', 'dt_solicitacao_fim', 'dt_execucao_ini', 'dt_execucao_fim', 'id_funcao', 'id_empresa_armador', 'id_servico', 'numero', "id_status_os");
        foreach($post as $key ){
            $filtros[$key] = Input::old($key, Session::get($key));
        }

        $arq = "../public/tmp/os".Auth::user()->id_usuario.'.xls';

        $fp = fopen($arq, 'w');
        $head = View::make('os/excel_cab');
        $head = iconv('UTF-8', 'ISO-8859-1', $head);
        fwrite($fp, $head);
        
        $i = 0;
        $reg = new \Os;

        if (isset($filtros['id_empresa']) && $filtros['id_empresa'] > 0) {
            $reg = $reg->daEmpresa($filtros['id_empresa']);
        }
        if (isset($filtros['id_servico'])) {
            $reg = $reg->doServico($filtros['id_servico']);
        }
        if (isset($filtros['id_empresa_armador']) && $filtros['id_empresa_armador'] > 0) {
            $reg = $reg->doArmador($filtros['id_empresa_armador']);
        }
        if (isset($filtros['id_projeto']) ) {
            $reg = $reg->doProjeto($filtros['id_projeto']);
        }
        if (isset($filtros['id_centro_de_custo']) ) {
            $reg = $reg->doCentro($filtros['id_centro_de_custo']);
        }
        if (isset($filtros['numero'])) {
            $reg = $reg->numero($filtros['numero']);
        }
        if (isset($filtros['dt_solicitacao_ini'])) {
            if (isset($filtros['dt_solicitacao_fim']) && $filtros['dt_solicitacao_fim'] != '') {
                $reg = $reg->SolicitadaEntre($filtros['dt_solicitacao_ini'], $filtros['dt_solicitacao_fim']);
            } else {
                $reg = $reg->SolicitadaEm($filtros['dt_solicitacao_ini']);
            }
        }
        if (isset($filtros['dt_execucao_ini'])) {
            if (isset($filtros['dt_execucao_fim']) && $filtros['dt_execucao_fim'] != '' ) {
                $reg = $reg->ExecutadaEntre($filtros['dt_execucao_ini'], $filtros['dt_execucao_fim']);
            } else {
                $reg = $reg->ExecutadaEm($filtros['dt_execucao_ini']);
            }
        }
        if (isset($filtros['nome_completo'])) {
            $reg = $reg->doCandidatoNome($filtros['nome_completo']);
        }
        if (isset($filtros['id_funcao'])) {
            $reg = $reg->doCandidatoFuncao($filtros['id_funcao']);
        }
        $reg->statusos($filtros['id_status_os']);
        $reg->orderBy('numero', 'desc');
        \DB::disableQueryLog();
        $reg->chunk(200, function ($registros) use ( &$i, &$fp ) {
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            
            foreach($registros as $registro){
                $i++;
                $html = View::make('os/excel_det')
                                ->with('i', $i)
                                ->with('registro', $registro)
                ;
                $html = iconv('UTF-8', 'ISO-8859-1', $html);
                fwrite($fp, $html);                
            }
        });
        $rod = View::make('os/excel_rod');
        fwrite($fp, $rod);
        fclose($fp);

        return Redirect::to('/tmp/os'.Auth::user()->id_usuario.'.xls'); 
        
    }
    
    public function postIndex() {
        Input::flash();
        $post = Input::all();
        foreach($post as $key => $value){
            Session::set($key, $value);
        }
        return Redirect::to('os')->withInput();
    }

    public function getCancelar($id_os){
        try{
            $rep = new \Westhead\Repositorios\RepositorioOs;
            $os = \Os::find($id_os);
            $rep->excluir($os, \Auth::user()->id_usuario);
            Session::flash('flash_msg', 'Ordem de serviço cancelada com sucesso');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/os');
    }

    public function getCreate() {
        $obj = new Os;

        $empresas = array('' => '(não informado)') + Empresa::where('fl_armador', '=', 0)->orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        $intermediarias = array(''=>'(não se aplica)') + Empresa::where('fl_armador', '=', 0)->lists('razaosocial', 'id_empresa');
        $armadores = array('' => '(não informado)') + Armador::where('fl_armador', '=', 1)->orderBy('razaosocial')->lists('razaosocial', 'id_empresa');
        if(Input::has('id_empresa') || Input::old('id_empresa')!= ''){
            $projetos = array('' => '(não informado)') + Projeto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))->lists('descricao', 'id_projeto');
            $centros = array('' => '(não informado)') + Centrodecusto::where('id_empresa', '=', Input::get('id_empresa', Input::old('id_empresa')))->lists('nome', 'id_centro_de_custo');
        } else {
            $projetos = array('' => '(selecione a empresa)');
            $centros = array('' => '(selecione a empresa)');
        }
        $servicos = array('' => '(não informado)') + Servico::orderBy('descricao')->lists('descricao', 'id_servico');


        return View::make('os/create')
                        ->with('empresas', $empresas)
                        ->with('intermediarias', $intermediarias)
                        ->with('armadores', $armadores)
                        ->with('projetos', $projetos)
                        ->with('centros', $centros)
                        ->with('servicos', $servicos)
                        ->with('obj', $obj)
        ;
    }

    public function getShow($id) {
        $obj = Os::find($id);

        $empresas = Empresa::where('fl_armador', '=', 0)->lists('razaosocial', 'id_empresa');
        $intermediarias = array(''=>'(não se aplica)') + Empresa::where('fl_armador', '=', 0)->lists('razaosocial', 'id_empresa');
        $armadores = Armador::where('fl_armador', '=', 1)->lists('razaosocial', 'id_empresa');
        if($obj->id_empresa > 0){
            $projetos = array('' => '(não informado)') + Projeto::where('id_empresa', '=', $obj->id_empresa)->lists('descricao', 'id_projeto');
            $centros = array('' => '(não informado)') + Centrodecusto::where('id_empresa', '=', $obj->id_empresa)->lists('nome', 'id_centro_de_custo');
        } else {
            $projetos = array('' => '(selecione a empresa)');
            $centros = array('' => '(selecione a empresa)');
        }
        $servicos =  array('' => '(não informado)') + Servico::orderBy('descricao')->lists('descricao', 'id_servico');
        $classificacoes =  array('' => '(não se aplica)') + ClassificacaoVisto::orderBy('classificacao')->lists('classificacao', 'id_classificacao_visto');
        
        $armadores = array('' => '(não informado)') + $armadores;

        $candidatos = Os::find($id)->candidatos()->orderBy('nome_completo')->get();

        $tipoanexo = array(''=>'(não informado)') + TipoAnexo::orderBy('nome_pt_br')->lists('nome_pt_br', 'id_tipo_anexo');

        return View::make('os/show')
                        ->with('empresas', $empresas)
                        ->with('classificacoes', $classificacoes)
                        ->with('intermediarias', $intermediarias)
                        ->with('armadores', $armadores)
                        ->with('projetos', $projetos)
                        ->with('centros', $centros)
                        ->with('servicos', $servicos)
                        ->with('candidatos', $candidatos)
                        ->with('tipoanexo', $tipoanexo)
                        ->with('obj', $obj)
        ;
    }

    public function postStore() {
        $campos = Input::all();
        $msg = '';
        if ($campos['tipopessoa'] == 'J') {
            if (intval(Input::get('id_empresa')) == 0) {
                unset($campos['id_empresa']);
            }
            if (intval(Input::get('id_projeto')) == 0) {
                unset($campos['id_projeto']);
            }
            if (intval(Input::get('id_empresa_armador')) == 0) {
                unset($campos['id_empresa_armador']);
            }
            if (intval(Input::get('id_centro_de_custo')) == 0) {
                unset($campos['id_centro_de_custo']);
            }
            if (!isset($campos['id_empresa']) || $campos['id_empresa']== ''){
                $msg .= "<br/>A empresa é obrigatória";
            }
            if (!isset($campos['id_projeto']) || $campos['id_projeto']== ''){
                $msg .= "<br/>O projeto é obrigatório";
            }
        }
        if (!isset($campos['id_servico']) || $campos['id_servico']== ''){
            $msg .= "<br/>O serviço é obrigatório";
        }
        
        if ($msg == ''){
            if (isset($campos['id_os'])) {
                $os = Os::find($campos['id_os']);
                $os->id_empresa_armador = null;
                $os->fill($campos);
                $msg = "Ordem de serviço atualizada com sucesso.";
                $destino = '/os/show/';
            } else {
                $os = new Os($campos);
                $os->id_usuario_abertura = Auth::user()->id_usuario;
                $msg = "Ordem de serviço criada com sucesso. AGORA INFORME OS CANDIDATOS!";
                $destino = '/os/candidatos/';
            }
            if(!Input::has('id_status_os')){
                $os->id_status_os= 1;
            }
            if(!isset($campos['dt_solicitacao']) || $campos['dt_solicitacao'] == ''){
                $os->dt_solicitacao = null;
            }
            if(!isset($campos['dt_execucao']) || $campos['dt_execucao'] == ''){
                $os->dt_execucao = null;
            }
            if(!isset($campos['dt_assinatura']) || $campos['dt_assinatura'] == ''){
                $os->dt_assinatura = null;
            }

            if ($os->save()) {
                return Redirect::to($destino.$os->id_os)->with('flash_msg', $msg);
            } else {
                return Redirect::back()
                                ->withInput()
                                ->withErrors($os->errors)
                ;
            }
        } else {
            $msg = "Não foi possível salvar a OS pois:".$msg;
            return Redirect::back()
                            ->withInput()
                            ->withErrors($msg)
            ;            
        }
    }

    public function getCandidatos($id){
        $obj = Os::find($id);

        $candidatos = Os::find($id)->candidatos()
                ->whereNull('id_candidato_principal')
                ->orderBy('nome_completo')
                ->get();

        return View::make('os/candidatos')
                        ->with('candidatos', $candidatos)
                        ->with('obj', $obj)
        ;

    }

    public function getDesmembrar($id){
        $obj = Os::find($id);

        $candidatos = Os::find($id)->candidatos()
                ->whereNull('id_candidato_principal')
                ->orderBy('nome_completo')
                ->get();

        return View::make('os/desmembrar')
                        ->with('candidatos', $candidatos)
                        ->with('obj', $obj)
        ;

    }

    public function postDesmembrar($id = ''){
        $id_os = Input::get('id_os');
        try{
            $rep = new \Westhead\Repositorios\RepositorioOs;
            $id_candidatos = Input::get('id_candidato');
            $os = \Os::find($id_os);
            $nova = $rep->desmembrar($os, $id_candidatos);
            return Redirect::to('/os/desmembrar/'.$id_os)->with('flash_msg', 'Candidatos enviados para a nova OS <a href="/os/candidatos/'.$nova->id_os.'" target="_blank">'.$nova->numero.'</a>');
        } catch (Exception $ex) {
            Input::flash();
            return Redirect::to('/os/desmembrar/'.$id_os)->with('errors', $ex->getMessage());
        }
    }
    
    public function getAnexos($id) {
        $obj = Os::find($id);

        $tipoanexo = array(''=>'(não informado)') + TipoAnexo::orderBy('nome_pt_br')->lists('nome_pt_br', 'id_tipo_anexo');

        return View::make('os/anexos')
                        ->with('tipoanexo', $tipoanexo)
                        ->with('obj', $obj)
        ;
    }

    public function getDespesas($id, $id_despesa = 0) {
        $obj = Os::find($id);
        if ($id_despesa > 0){
            $desp = \Despesa::find($id_despesa);
        } else {
            $desp = new \Despesa;
        }
        $despesas = $obj->despesas;
        $tipodespesa = array(''=>'(não informado)') + TipoDespesa::orderBy('descricao_pt_br')->lists('descricao_pt_br', 'id_tipo_despesa');

        return View::make('os/despesas')
                        ->with('tipodespesas', $tipodespesa)
                        ->with('despesas', $despesas)
                        ->with('obj', $obj)
                        ->with('desp', $desp)
        ;
    }

    public function getGriddespesas($id_os){
        $obj = Os::find($id_os);

        $despesas = $obj->despesas;
        return View::make('os/griddespesas')
                        ->with('despesas', $despesas)
        ;

    }

    public function getAdicionarcandidato($id_os, $id_candidato) {
        $os = Os::find($id_os);
        $candidato = Candidato::find($id_candidato);

        $rep = new \Westhead\Repositorios\RepositorioOs();
        $rep->adicionaCandidato($os, $candidato);

        return Redirect::to('/os/candidatos/' . $id_os)->with('flash_msg', 'Candidato adicionado com sucesso!');
    }

    public function getRemovercandidato($id_os, $id_candidato) {
        try{
            $os = \Os::find($id_os);
            $cand = \Candidato::find($id_candidato);
            $rep = new \Westhead\Repositorios\RepositorioOs();
            $rep->removeCandidato($os, $cand);
            return Redirect::to('/os/candidatos/' . $id_os)->with('flash_msg', 'Candidato removido com sucesso!');
        } catch (Exception $ex) {
            return Redirect::to('/os/candidatos/' . $id_os)->with('errors', 'Não foi possível remover o candidato:<br/>'.$ex->getMessage());
        }
    }

    public function postNovoanexo($id_os) {
        $extensoesAutorizadas = array('jpg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'tiff', 'bmp', 'msg');
        $ds = DIRECTORY_SEPARATOR;  //1
        $storeFolder = '../public/arquivos/anexos/';   //2
        if (!empty($_FILES)) {

            $tempFile = $_FILES['file']['tmp_name'];          //3
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];
            if (array_has_key(strtolower($extension), $extensoesAutorizadas)) {
                $targetFile = $storeFolder . $id_empresa . "." . $extension;  //5
                try {
                    unlink($targetFile);
                } catch (Exception $e) {

                }

                move_uploaded_file($tempFile, $targetFile); //6
            } else {
                throw new exception('Extensão não autorizada');
            }
        }
    }
    /**
     * Imprime a capa da OS
     * @param type $id_candidato
     * @return type
     */
    public function getCapa($id_os) {
        $os = Os::find($id_os);
        $html = View::make('os/capa')
                        ->with('os', $os)
        ;

        $mpdf =new mPDF('utf-8', 'A4', '','','20','20','20','20','0','0');		// largura x altura
        $mpdf->allow_charset_conversion = false;
        $html = iconv("UTF-8","UTF-8//IGNORE",$html);
        $mpdf->WriteHTML($html);
		return $mpdf->Output();
    }

    public function getForm104($id_os, $id_candidato, $data = ''){
        $os = Os::find($id_os);
        $candidato = Candidato::find($id_candidato);
        if($os->id_empresa_intermediaria > 0){
            $intermediaria = Empresa::find($os->id_empresa_intermediaria);
        } else {
            $intermediaria = new Empresa;
            $intermediaria->razaosocial = '(não se aplica)'; 
            $intermediaria->email = '(não se aplica)'; 
            $intermediaria->cnpj = '(não se aplica)'; 
        }
        $os_candidato = OsCandidato::where('id_os', '=', $id_os)
                            ->where('id_candidato', '=', $id_candidato)
                            ->first();
        $html = View::make('os/form104')
                        ->with('os', $os)
                        ->with('os_candidato', $os_candidato)
                        ->with('candidato', $candidato)
                        ->with('intermediaria', $intermediaria)
                        ->with('data', $data)
        ;

        $mpdf =new mPDF('utf-8', 'A4', '','','20','20','30','20','10','0');		// largura x altura
        $mpdf->allow_charset_conversion = false;
        $mpdf->showImageErrors = true;
        if(count($os->empresa) > 0 && $os->empresa->temLogo()){
            $header = '<div style="width:100%; text-align:left;"><img src="'.$os->empresa->caminhoLogo().'" style="height:50px;"/></div>';
            $mpdf->SetHTMLHeader($header);
        }

        $html = iconv("UTF-8","UTF-8//IGNORE",$html);
        $mpdf->WriteHTML($html);
//        return $html;
		return $mpdf->Output();
    }

    public function getForm104teste($id_os, $id_candidato){
        $os = Os::find($id_os);
        $candidato = Candidato::find($id_candidato);
        $os_candidato = OsCandidato::where('id_os', '=', $id_os)
                            ->where('id_candidato', '=', $id_candidato)
                            ->first();
        $html = View::make('os/form104debug')
                        ->with('os', $os)
                        ->with('os_candidato', $os_candidato)
                        ->with('candidato', $candidato)
        ;

        $mpdf =new mPDF('utf-8', 'A4', '','','20','20','30','20','10','0');		// largura x altura
        $mpdf->allow_charset_conversion = false;
        $mpdf->showImageErrors = true;
        if($os->empresa->temLogo()){
            $header = '<div style="width:100%; text-align:left;"><img src="'.$os->empresa->caminhoLogo().'" style="height:50px;"/></div>';
            $mpdf->SetHTMLHeader($header);
        }

        $html = iconv("UTF-8","UTF-8//IGNORE",$html);
        $mpdf->WriteHTML($html);
//        return $html;
		return $mpdf->Output();
    }

    public function getDeclaracaoresp($id_os, $id_candidato){
        $meses = array('', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
        $mes = $meses[intval(date('m'))];
        $os = Os::find($id_os);
        $candidato = Candidato::find($id_candidato);
        $html = View::make('os/declaracao_resp')
                        ->with('os', $os)
                        ->with('mes', $mes)
                        ->with('cand', $candidato)
        ;

        $mpdf =new mPDF('utf-8', 'A4', '','','20','20','30','20','10','0');		// largura x altura
        $mpdf->allow_charset_conversion = false;
        $mpdf->showImageErrors = true;
        if($os->empresa->temLogo()){
            $header = '<div style="width:100%; text-align:left;"><img src="'.$os->empresa->caminhoLogo().'" style="height:50px;"/></div>';
            $mpdf->SetHTMLHeader($header);
        }

        $html = iconv("UTF-8","UTF-8//IGNORE",$html);
        $mpdf->WriteHTML($html);
//        return $html;
	return $mpdf->Output();
    }

    public function getAnexoprorrogacao($id_os, $id_candidato){
        $meses = array('', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
        $mes = $meses[intval(date('m'))];
        $os = Os::find($id_os);
        $candidato = Candidato::find($id_candidato);
        $html = View::make('os/anexo_prorrogacao')
                        ->with('os', $os)
                        ->with('mes', $mes)
                        ->with('candidato', $candidato)
        ;

        $mpdf =new mPDF('utf-8', 'A4', '','','20','20','25','20','10','0');		// largura x altura
        $mpdf->allow_charset_conversion = false;
        $mpdf->showImageErrors = true;
        if($os->empresa->temLogo()){
            $header = '<div style="width:100%; text-align:left;"><img src="'.$os->empresa->caminhoLogo().'" style="height:50px;"/></div>';
            $mpdf->SetHTMLHeader($header);
        }

        $html = iconv("UTF-8","UTF-8//IGNORE",$html);
        $mpdf->WriteHTML($html);
//        return $html;
		return $mpdf->Output();
    }

    public function getForm104debug($id_os, $id_candidato){
        $os = Os::find($id_os);
        $candidato = Candidato::find($id_candidato);
        $os_candidato = OsCandidato::where('id_os', '=', $id_os)
                            ->where('id_candidato', '=', $id_candidato)
                            ->first();
        $html = View::make('os/form104')
                        ->with('os', $os)
                        ->with('os_candidato', $os_candidato)
                        ->with('candidato', $candidato)
        ;

        $mpdf =new mPDF('utf-8', 'A4', '','','20','20','30','20','10','0');		// largura x altura
        $mpdf->allow_charset_conversion = false;
        if($os->empresa->temLogo()){
            $header = '<div style="width:100%; text-align:left;"><img src="'.$os->empresa->caminhoLogo().'" style="height:50px;"/></div>';
            $mpdf->SetHTMLHeader($header);
        }

        $html = iconv("UTF-8","UTF-8//IGNORE",$html);
        return $html;
    }

    public function form104($id_os, $id_candidato){
        $os = Os::find($id_os);
        $candidato = Candidato::find($id_candidato);
        $os_candidato = OsCandidato::where('id_os', '=', $id_os)
                            ->where('id_candidato', '=', $id_candidato)
                            ->first();
        $html = View::make('os/form104')
                        ->with('os', $os)
                        ->with('os_candidato', $os_candidato)
                        ->with('candidato', $candidato)
        ;

    }

    public function postAdicionedespesa(){
        $rep = new \Westhead\Repositorios\RepositorioDespesa;
        $post = Input::all();
        $id_antes = Input::get('id_despesa');
        try{
            if ($id_antes > 0){
                $desp = \Despesa::find(Input::get('id_despesa'));
                $msg = 'Despesa atualizada com sucesso';
            } else {
                $msg = 'Despesa adicionada com sucesso';
                $desp = $rep->nova();
            }
            $desp = $rep->crie($post, $desp, \Auth::user()->id_usuario);
            if ($desp){
                return Redirect::to('/os/despesas/'.$post['id_os'])->with('flash_msg', $msg);
            } else {
                Input::flash();
                $errors = $rep->getErrors();
                Session::flash('errors', $errors);
                return Redirect::to('/os/despesas/'.$post['id_os'].'/'.$post['id_despesa']);
            }
        } catch (Exception $ex) {
            Input::flash();
            return Redirect::back()->with('errors', $ex->getMessage());
        }

    }

    public function getExcluadespesa($id_os, $id_despesa){
        $rep = new \Westhead\Repositorios\RepositorioDespesa;
        try{
            $rep->exclua($id_despesa);
            return Redirect::to('/os/despesas/'.$id_os)->with('flash_msg', 'Despesa excluída com sucesso');
        } catch (Exception $ex) {
            Input::flash();
            return Redirect::back()->with('errors', $rep->getErrors());
        }
    }

    public function getRelatoriodespesas($id_os){
        $os = \Os::find($id_os);
        $html = View::make('os/despDemo_detalhe')
                        ->with('os', $os)
                ;
        $mpdf =new mPDF('utf-8', 'A4-L', '','','15','15','15','15','0','0');		// largura x altura
        $mpdf->allow_charset_conversion = false;
        $html = iconv("UTF-8","UTF-8//IGNORE",$html);
        $mpdf->WriteHTML($html);
		return $mpdf->Output();
    }

    public function getExecutadahoje($id_os){
        Input::flash();
        $rep = new \Westhead\Repositorios\RepositorioOs;
        $os = \Os::find($id_os);
        $rep->executadaHoje($os);
        return Redirect::to('/os/#os'.$id_os)->with('flash_msg', 'Ordem de serviço '.substr('000000'.$os->numero, -6).' atualizada com sucesso');
    }

    public function getConcluida($id_os){
        Input::flash();
        $rep = new \Westhead\Repositorios\RepositorioOs;
        $os = \Os::find($id_os);
        $rep->concluida($os, \Auth::user()->id_usuario);
        return Redirect::to('/os/#os'.$id_os)->with('flash_msg', 'Ordem de serviço '.substr('000000'.$os->numero, -6).' alterada com sucesso');
    }

    public function getFaturada($id_os){
        Input::flash();
        $rep = new \Westhead\Repositorios\RepositorioOs;
        $os = \Os::find($id_os);
        $rep->faturada($os, \Auth::user()->id_usuario);
        return Redirect::to('/os/#os'.$id_os)->with('flash_msg', 'Ordem de serviço '.substr('000000'.$os->numero, -6).' alterada com sucesso');
    }

    public function getLiberada($id_os){
        Input::flash();
        $rep = new \Westhead\Repositorios\RepositorioOs;
        $os = \Os::find($id_os);
        $rep->liberada($os, \Auth::user()->id_usuario);
        return Redirect::to('/os/#os'.$id_os)->with('flash_msg', 'Ordem de serviço '.substr('000000'.$os->numero, -6).' alterada com sucesso');
    }

    public function getFormtrans($id_os, $id_candidato){
        set_time_limit(0);
        $candidato = \Candidato::find($id_candidato);
        $estadocivil = array(1 => '', 2 => '', 3 => '', 4 => '', 5 => '', 6=> '');
        if (is_object($candidato->estadocivil)){
            if ($candidato->estadocivil->cod_pf > 0){
                $estadocivil[$candidato->estadocivil->cod_pf] = ' sel';                
            }
        }

        $sexo = array('M' => '', 'F' => '');
        if ($candidato->sexo != ''){
            $sexo[$candidato->sexo] = ' sel';                
        }

        $coleta = new ProcessoColetaVisto;
        
        $html = View::make('os/formTrans')
                        ->with('candidato', $candidato)
                        ->with('coleta', $coleta)
                        ->with('estadocivil', $estadocivil)
                        ->with('sexo', $sexo)
                ;
        $mpdf =new mPDF('utf-8', 'A4', '','','12','12','12','10','0','0');		// largura x altura
        $mpdf->debug = true;
        $mpdf->allow_charset_conversion = true;
        $mpdf->img_dpi = 300;
        $html = iconv("UTF-8","UTF-8//IGNORE",$html);

        $mpdf->WriteHTML($html);
		return $mpdf->Output();

    }
}
