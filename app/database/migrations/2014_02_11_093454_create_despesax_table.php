<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDespesaxTable extends Migration {

    public function up() {
        Schema::dropIfExists('despesa');
        Schema::create('despesa', function(Blueprint $table) {
            $table->increments('id_despesa');
            $table->integer('id_tipo_despesa')->unsigned();
            $table->integer('id_os')->unsigned();
            $table->decimal('qtd', 10, 2);
            $table->decimal('valor', 10, 2);
            $table->date('dt_ocorrencia');
            $table->text('observacao');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('despesa');
    }

}
