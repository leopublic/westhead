<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuario', function(Blueprint $table) {
			$table->increments('id_usuario');
			$table->integer('id_empresa')->unsigned()->nullable();
			$table->string('nome',500);
			$table->string('username', 50);
			$table->string('password', 64)->nullable();
			$table->string('email', 500)->nullable();
			$table->boolean('fl_interno')->nullable();
			$table->text('observacao')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuario');
	}

}
