<?php

/**
 * @property int $id_os
 * @property int $numero
 * @property int $id_empresa
 * @property int $id_projeto
 * @property int $id_empresa_armador
 * @property int $id_centro_de_custo
 * @property int $id_status_os
 * @property int $id_servico
 * @property int $id_usuario_abertura
 * @property int $id_usuario_execucao
 * @property date $dt_solicitacao
 * @property date $dt_execucao
 * @property varchar $motorista
 * @property varchar $dados_do_voo
 * @property varchar $solicitante
 * @property text $observacao
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property varchar $nomecompleto
 * @property varchar $cpf
 * @property varchar $tipopessoa
 * @property date $dt_assinatura
 * @property varchar $prazo_solicitado
 * @property varchar $local_trabalho
 * @property int $id_tipo_faturamento
 * @property int $id_invoice
 * @property int $id_usuario_conclusao
 * @property int $id_usuario_faturamento
 * @property datetime $dt_conclusao
 * @property datetime $dt_faturamento
 * @property int $id_empresa_intermediaria
 */
class Os extends Modelo {

    protected $table = 'os';
    protected $primaryKey = 'id_os';
    protected $guarded = array('numero');

    public static function boot() {
        parent::boot();

        static::saving(function($modelo) {
            $modelo->assumeDefaults();
            //return $modelo->valido($modelo->regras(), $modelo->mensagens());
        });

        static::saved(function($modelo) {
            // registra log
        });
    }

    public function assumeDefaults() {
        if ($this->dt_solicitacao == '') {
            $this->dt_solicitacao = date('d/m/Y');
        }
        if ($this->id_os == '') {
            $this->id_usuario_abertura = Auth::user()->id_usuario;
            $max = intval(DB::table('os')->max('numero'));
            $this->numero = $max + 1;
        }

        if ($this->tipopessoa == '') {
            $this->tipopessoa = 'J';
        }

        if ($this->tipopessoa == 'J') {
            unset($this->nomecompleto);
            unset($this->cpf);
        } else {
            unset($this->id_empresa);
            unset($this->id_projeto);
            unset($this->id_empresa_armador);
            unset($this->id_centro_de_custo);
        }
    }

    public function regras() {
        if ($this->tipopessoa == 'J') {
            return array(
                'id_empresa' => 'required',
                'id_projeto' => 'required',
                'id_servico' => 'required',
                'solicitante' => 'required'
            );
        } else {
            return array(
                'nomecompleto' => 'required',
                'cpf' => 'required'
            );
        }
    }

    public static function mensagens() {
        return array(
            'solicitante.required' => 'O solicitante é obrigatório!',
            'id_empresa.required' => 'A empresa é obrigatória!',
            'id_projeto.required' => 'O projeto é obrigatório!',
            'id_servico.required' => 'O serviço é obrigatório!',
            'nomecompleto.required' => 'O nome é obrigatório!',
            'cpf.required' => 'O CPF é obrigatório!'
        );
    }

    public function getDtSolicitacaoAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtSolicitacaoAttribute($data) {
        $this->attributes['dt_solicitacao'] = $this->dtViewParaModel($data);
    }

    public function getDtExecucaoAttribute($data) {
        if (isset($this->attributes['dt_execucao'])) {
            return $this->dtModelParaView($this->attributes['dt_execucao']);
        }
    }

    public function setDtExecucaoAttribute($data) {
        $this->attributes['dt_execucao'] = $this->dtViewParaModel($data);
    }

    public function getDtAssinaturaAttribute($data) {
        if (isset($this->attributes['dt_assinatura'])) {
            return $this->dtModelParaView($this->attributes['dt_assinatura']);
        }
    }

    public function setDtAssinaturaAttribute($data) {
        $this->attributes['dt_assinatura'] = $this->dtViewParaModel($data);
    }

    public function getDtConclusaoFmtAttribute($data) {
        if (isset($this->attributes['dt_conclusao'])) {
            return $this->dtHrModelParaView($this->attributes['dt_conclusao']);
        }
    }

    public function setDtConclusaoFmtAttribute($data) {
        $this->attributes['dt_conclusao'] = $this->dtHrViewParaModel($data);
    }

    public function getDtFaturamentoFmtAttribute($data) {
        if (isset($this->attributes['dt_faturamento'])) {
            return $this->dtHrModelParaView($this->attributes['dt_faturamento']);
        }
    }

    public function setDtFaturamentoFmtAttribute($data) {
        $this->attributes['dt_faturamento'] = $this->dtHrViewParaModel($data);
    }

    public function empresa() {
        return $this->belongsTo('Empresa', 'id_empresa');
    }

    public function projeto() {
        return $this->belongsTo('Projeto', 'id_projeto');
    }

    public function servico() {
        return $this->belongsTo('Servico', 'id_servico');
    }

    public function armador() {
        return $this->belongsTo('Empresa', 'id_empresa_armador');
    }

    public function centrodecusto() {
        return $this->belongsTo('Centrodecusto', 'id_centro_de_custo');
    }

    public function classificacao(){
        return $this->belongsTo('ClassificacaoVisto', 'id_classificacao_visto');
    }

    public function candidatos() {
        return $this->belongsToMany('Candidato', 'os_candidato', 'id_os', 'id_candidato')->withTimestamps()->orderBy('nome_completo');
    }

    public function despesas() {
        return $this->hasMany('Despesa', 'id_os')->orderBy('dt_ocorrencia');
    }

    public function anexos() {
        return $this->hasMany('Anexo', 'id_os');
    }

    public function invoice() {
        return $this->belongsTo('Invoice', 'id_invoice');
    }

    public function usuarioabertura() {
        return $this->belongsTo('Usuario', 'id_usuario_abertura');
    }

    public function usuarioQueExecutou() {
        return $this->belongsTo('Usuario', 'id_usuario_execucao');
    }

    public function usuarioconclusao() {
        return $this->belongsTo('Usuario', 'id_usuario_conclusao');
    }

    public function usuariofaturamento() {
        return $this->belongsTo('Usuario', 'id_usuario_faturamento');
    }

    public function getNomeUsuarioConclusaoAttribute($val) {
        if (is_object($this->usuarioconclusao)) {
            return $this->usuarioconclusao->nome;
        } else {
            return '--';
        }
    }

    public function getDadosConclusaoAttribute($data) {
        return $this->nome_usuario_conclusao . ' em ' . $this->dt_conclusao_fmt;
    }

    public function getDadosFaturamentoAttribute($data) {
        return $this->nome_usuario_faturamento . ' em ' . $this->dt_faturamento_fmt;
    }

    public function getNomeUsuarioFaturamentoAttribute($val) {
        if (is_object($this->usuariofaturamento)) {
            return $this->usuariofaturamento->nome;
        } else {
            return '--';
        }
    }

    public function statusos() {
        return $this->belongsTo('StatusOs', 'id_status_os');
    }

    public function scopeExecutadaEm($query, $executada_em) {
        try {
            $xdata = $this->dtViewParaModel($executada_em);
        } catch (Exception $ex) {
            
        }
        if ($xdata != '') {
            $query->where('dt_execucao', '=', $xdata);
        }
        return $query;
    }

    public function scopeExecutadaEntre($query, $data_ini, $data_fim) {
        try {
            $xdata_ini = $this->dtViewParaModel($data_ini);
        } catch (Exception $ex) {
            
        }
        try {
            $xdata_fim = $this->dtViewParaModel($data_fim);
        } catch (Exception $ex) {
            
        }
        if ($xdata_ini != '') {
            $query->where('dt_execucao', '>=', $xdata_ini);
        }
        if ($xdata_fim != '') {
            $query->where('dt_execucao', '<=', $xdata_fim);
        }
        return $query;
    }

    public function scopeSolicitadaEm($query, $data) {
        try {
            $xdata = $this->dtViewParaModel($data);
        } catch (Exception $ex) {
            
        }
        if ($xdata != '') {
            $query->where('dt_solicitacao', '=', $xdata);
        }
        return $query;
    }

    public function scopeSolicitadaEntre($query, $data_ini, $data_fim) {
        try {
            $xdata_ini = $this->dtViewParaModel($data_ini);
        } catch (Exception $ex) {
            
        }
        try {
            $xdata_fim = $this->dtViewParaModel($data_fim);
        } catch (Exception $ex) {
            
        }
        if ($xdata_ini != '') {
            $query->where('dt_solicitacao', '>=', $xdata_ini);
        }
        if ($xdata_fim != '') {
            $query->where('dt_solicitacao', '<=', $xdata_fim);
        }
        return $query;
    }

    public function scopeDaEmpresa($query, $empresa) {
        if (is_object($empresa)) {
            $query->where('os.id_empresa', '=', $empresa->id_empresa);
        } else {
            if (intval($empresa) > 0) {
                $query->where('os.id_empresa', '=', $empresa);
            }
        }
        return $query;
    }

    public function scopeDoProjeto($query, $projeto) {
        if (is_object($projeto)) {
            $query->where('os.id_projeto', '=', $projeto->id_projeto);
        } else {
            if (intval($projeto) > 0) {
                $query->where('os.id_projeto', '=', $projeto);
            }
        }
        return $query;
    }

    public function scopeDoCentro($query, $centro) {
        if (is_object($centro)) {
            $query->where('id_centro_de_custo', '=', $centro->id_centro_de_custo);
        } else {
            if (intval($centro) > 0) {
                $query->where('id_centro_de_custo', '=', $centro);
            }
        }
        return $query;
    }

    public function scopeDoCandidatoNome($query, $nome) {
        if (trim($nome) != '') {
            $query->whereIn('id_os', function($query) use ($nome) {
                $query->select('id_os')
                        ->from('os_candidato')
                        ->join('candidato', 'candidato.id_candidato', '=', 'os_candidato.id_candidato')
                        ->where('candidato.nome_completo', 'like', '%' . $nome . '%');
            });
        }
        return $query;
    }

    public function scopeDoCandidatoFuncao($query, $id_funcao) {
        if (intval($id_funcao) > 0) {
            $query->whereIn('id_os', function($query) use ($id_funcao) {
                $query->select('id_os')
                        ->from('os_candidato')
                        ->join('candidato', 'candidato.id_candidato', '=', 'os_candidato.id_candidato')
                        ->where('candidato.id_funcao', '=', $id_funcao);
            });
        }
        return $query;
    }

    public function scopeDoArmador($query, $armador) {
        if (is_object($armador)) {
            $query->where('os.id_empresa_armador', '=', $armador->id_empresa);
        } else {
            if (intval($armador) > 0) {
                $query->where('os.id_empresa_armador', '=', $armador);
            }
        }
        return $query;
    }

    public function scopeDoServico($query, $servico) {
        if (is_object($servico)) {
            $query->where('os.id_servico', '=', $servico->id_servico);
        } else {
            if (intval($servico) > 0) {
                $query->where('os.id_servico', '=', $servico);
            }
        }
        return $query;
    }

    public function scopeNumero($query, $numero) {
        if (intval($numero) > 0) {
            $query->where('numero', '=', $numero);
        }
        return $query;
    }

    public function getCliente() {
        if ($this->tipopessoa == 'J') {
            if (is_object($this->empresa)) {
                return $this->empresa->razaosocial;
            } else {
                return '--';
            }
        } else {
            return $this->nomecompleto;
        }
    }

    public function getArmador() {
        if ($this->tipopessoa == 'J') {
            if (isset($this->id_empresa_armador)) {
                return $this->armador->razaosocial;
            } else {
                return '(n/a)';
            }
        } else {
            return '(n/a)';
        }
    }

    public function getProjeto() {
        if ($this->tipopessoa == 'J') {
            if (is_object($this->projeto)) {
                return $this->projeto->descricao;
            } else {
                return '(n/a)';
            }
        } else {
            return '(n/a)';
        }
    }

    public function getChecklists() {
        return array('pro_rn72' => 'Prorrogação RN72', 'rn6104_360' => 'Autorização RN 61/04 (1 ano)', 'rn6104_90' => 'Autorização RN 61/04 (90 dias)', 'rn72' => 'Autorização RN 72', 'rn80' => 'Autorização RN 80', 'trans_perm' => 'Transformação em permanente');
    }

    public function qtdcandidatos() {
        $sql = "select count(*) as qtd from os_candidato where id_os = " . $this->id_os;
        $xx = DB::select(DB::raw($sql));
        return $xx[0]->qtd;
    }

    public function cotacao() {
        if (is_object($this->empresa)) {
            return $this->empresa->cotacao($this->dt_execucao);
        } else {
            return 0;
        }
    }

    public function valorCotacao() {
        return $this->cotacao();
    }

    public function concluiPeloProcesso($data) {
        if ($this->dt_execucao == '') {
            $this->dt_execucao = $data;
            $this->save();
        }
    }

    public function getObservacaoAttribute() {
        if (isset($this->attributes['observacao'])) {
            return str_replace('<br/>', "\n", $this->attributes['observacao']);
        }
    }

    public function getNomesCandidatos() {
        $sql = "select nome_completo from candidato , os_candidato
                where candidato.id_candidato = os_candidato.id_candidato
                and os_candidato.id_os = " . $this->id_os;
        $candidatos = \DB::select(\DB::raw($sql));
        $ret = '';
        $br = '';
        foreach ($candidatos as $candidato) {
            $ret .= $br . $candidato->nome_completo;
            $br = '<br/>';
        }
        return $ret;
    }

    public function scopeStatusos($query, $id_status_os) {
        if (intval($id_status_os) > 0) {
            return $query->where('os.id_status_os', '=', $id_status_os);
        } else {
            return $query;
        }
    }

}
