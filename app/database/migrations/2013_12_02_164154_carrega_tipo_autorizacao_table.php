<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CarregaTipoAutorizacaoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('tipo_autorizacao')->truncate();

        $obj = array("descricao" => "Art. 13, item V, Lei nº 6.815/80 - Resolução Normativa 72/2006 - CNIg", "descricao_res" => "VITEM V - RN 72");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, lei nº 6.815/80 - Art. 6º, Resolução Norm, 61/04 - CNIg", "descricao_res" => "VITEM V-61-90");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, lei nº 6.815/80 - Art. 4º, Resolução Norm. 61/04 - CNIg", "descricao_res" => "VITEM V-61-1A");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 16, Lei 6815/80 c/c Resolução Normativa 84/2009", "descricao_res" => "VIPERM RN 84");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 16, Lei 6.815/80 - Inciso I, Artigo 3º da Resolução Normativa 62/04 - CNIg", "descricao_res" => "VIPERM RN 62 - I");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, lei nº 6.815/80 - Resolução Normativa 80/08 - CNIg", "descricao_res" => "VITEM V-80-2A");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 16, Lei 6.815/80 - Inciso II, Artigo 3º da Resolução Normativa 62/04 – CNIg", "descricao_res" => "VIPERM RN 62 - II");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item I, lei nº 6.815/80 - Art. 3º, Resolução Norm. 42/1999 - CNIg", "descricao_res" => "VITEM V-42-99");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 16, Lei 6.815/80 - Parágrafo 1º, Artigo 5º da Resolução Normativa 62/04 - CNIg", "descricao_res" => "VIPERM RN 62 - III");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Acordo Brasil X Uruguai", "descricao_res" => "AC-BRxURUGUAI");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 16, Lei 6.815/80 - Inc. I, Art. 3º da Res. Normativa 62/04 c/c Res. Normativa 27/98 - CNIg", "descricao_res" => "VIPERM RN 62 c/c 27");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 37 da Lei 6.815/80 e art . 69 do Decreto 86.715/81", "descricao_res" => "TRANSF VITEM V P/ VI");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, lei nº 6.815/80 - Resolução Normativa 80/08 - CNIg c/c Res. Normativa 27/98 - CNIg", "descricao_res" => "VITEM V 80 c/c 27");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, Lei 6.815/80 - Resolução Normativa 37/99 - CNIg", "descricao_res" => "Vitem V - 37/99");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, Lei 6.815/80 - Resolução Normativa 87/10 - CNIg", "descricao_res" => "Vitem V - 87/10");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 16, Lei 6.815/80 - Inciso II, Artigo 2º da Resolução Normativa 62/04 – CNIg", "descricao_res" => "VIPERM RN62 - ART 2");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, lei nº 6.815/80 - Resolução Normativa 99/12 - CNIg", "descricao_res" => "VITEM V- RN 99");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, lei nº 6.815/80 - Resolução Normativa 99/12 - CNIg c/c Res. Normativa 27/98 - CNIg", "descricao_res" => "VITEM V 99 c/c 27");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => "Art. 13, item V, lei nº 6.815/80 - Resolução Normativa 100/13 – CNIg", "descricao_res" => "VITEM V – RN100/13");
        DB::table('tipo_autorizacao')->insert($obj);
        $obj = array("descricao" => " Art. 13, item V, lei nº 6.815/80 - Resolução Normativa 69/06 - CNIg", "descricao_res" => "VITEM V - RN 69");
        DB::table('tipo_autorizacao')->insert($obj);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

    }

}
