@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="icon-map-marker"></i>{{ $tela['titulo_tela'] }}</h1>
        <h4>{{ $tela['subTitulo'] }}</h4>
    </div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <span class="divider"><i class="icon-angle-right"></i></span>
        </li>
        <li class="active">Centro de custo</li>
    </ul>
</div>
<!-- END Breadcrumb -->
@if(count($errors->all())> 0)
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            <ul class="errors">
                @foreach($errors->all() as $message)
                <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-reorder"></i> Criar/Alterar centro de custo</h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                {{ Form::open(array('url' => 'centrodecusto/update/'.$obj->id_centro_de_custo, "class"=>"form-horizontal")) }}
                <div class="form-group">
                    {{Form::label('id_empresa', 'Empresa', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                    <div class="col-sm-9 col-lg-10 controls">
                        {{ Form::select('id_empresa', Empresa::where('fl_armador','=',0)->lists('razaosocial', 'id_empresa'), Input::old('id_empresa', $obj->id_empresa), $attributes = array('class' => 'form-control')); }}
                        <span class="help-inline">Informe a empresa</span>
                    </div>
                </div>
                <div class="form-group">
                    {{Form::label('nome', 'Nome', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                    <div class="col-sm-9 col-lg-10 controls">
                        {{ Form::text('nome',  Input::old('nome', $obj->nome), $attributes = array('class' => 'form-control')); }}
                        <span class="help-inline">Nome do centro de custo</span>
                    </div>
                </div>
                <div class="form-group">
                    {{Form::label('end_cobr', 'Endereço de cobrança', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                    <div class="col-sm-9 col-lg-10 controls">
                        {{ Form::textarea('end_cobr',  Input::old('end_cobr', $obj->end_cobr), $attributes = array('class' => 'form-control')); }}
                        <span class="help-inline">Endereço que constará nas invoices emitidas contra esse centro de custos</span>
                    </div>
                </div>
                <div class="form-group">
                    {{Form::label('observacao', 'Observação', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                    <div class="col-sm-9 col-lg-10 controls">
                        {{ Form::textarea('observacao',  Input::old('observacao', $obj->observacao), $attributes = array('class' => 'form-control')); }}
                        <span class="help-inline">Informações complementares sobre o centro de custo</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                        {{ HTML::link('empresa', 'Cancelar', array('class' => 'btn')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

</div>
@stop