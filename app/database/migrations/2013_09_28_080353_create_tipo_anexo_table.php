<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoAnexoTable extends Migration {

	public function up()
	{
		Schema::create('tipo_anexo', function(Blueprint $table) {
			$table->increments('id_tipo_anexo');
			$table->string('nome_pt_br', 500);
			$table->string('nome_en_us', 500);
			$table->boolean('fl_cand_obrigatorio');
			$table->boolean('fl_os_obrigatorio');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('tipo_anexo');
	}

}
