<?php

class ModelTest extends TestCase {

	/**
     * @dataProvider provider
	 */
	public function testDtModelParaView($dataIn, $dataOut)
	{
		$x = new Modelo;
		$data = $x->DtModelParaView($dataIn);
		$this->assertEquals($dataOut, $data);
	}

	/**
     * @dataProvider provider
	 */
	public function testDtViewParaModel($dataOut = '', $dataIn = '')
	{
		$x = new Modelo;
		$data = $x->DtViewParaModel($dataIn);
		$this->assertEquals($dataOut, $data);
	}
	
	public function provider()
    {
        return array(
          array('2013-09-15', '15/09/2013'),
          array('2013-09-15', '15/09/2013'),
          array('', '')
		);
    }


}