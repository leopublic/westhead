<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableEscolaridade extends Migration {
	public function up()
	{
        Schema::create('escolaridade', function( $table) {
            $table->increments('id_escolaridade');
			$table->string('descricao',500);
			$table->string('descricao_en', 500)->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('escolaridade');
	}
}