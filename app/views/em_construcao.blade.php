@extends('base')

@section('content')
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>Em construção</h1>
                        <h4></h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
<!--                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Basic Tables</li>
                    </ul>
                </div>-->
                <!-- END Breadcrumb -->
                @if (Session::has('flash_error'))
                <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-danger">
                        {{ Session::get('flash_error') }}
                      </div>
                    </div>
                </div>
                @endif
                @if (Session::has('flash_msg'))
                <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-success">
                        {{ Session::get('flash_msg') }}
                      </div>
                    </div>
                </div>
                @endif

                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="well">
                            <h4>Está página ainda não está concluída</h4>
                            <p>Em breve as funcionalidades previstas para essa tela estarão disponíveis. Por favor aguarde</p>
                        </div>
                </div>
@stop