@extends('processo.comum')

<? $tipo_processo = 'atendimento'; ?>
<? $nome_servico = 'Atendimento pessoal'; ?>

@section('titulo')
    @if(is_object($processo->servico))
        {{$processo->servico->descricao}}
    @else
        Atendimento pessoal: (?)
    @endif
@overwrite

@section('campos')
    <div class="row">
        @if (intval($processo->id_os) == 0)
            @if (Servico::where('id_tipo_servico', '=', 7)->count() > 1)
                @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => $processo->id_servico, 'valores' => array('0' => '(não informado)') +  Servico::where('id_tipo_servico', '=', 7)->lists('descricao', 'id_servico'),  'help' => '', array()))
            @else
                @include('html/campo-texto', array('id' => 'servico', 'label' => 'Serviço', 'valor' => $processo->descricao_servico,  'atributos'=> array( "disabled"=>"disabled")))        
            @endif
        @endif
    </div>
@overwrite
