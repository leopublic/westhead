<?php
/**
 * @property string $id_menu Id do menu
 * @property string $descricao Nome do menu
 * @property datetime $created_at Data em que o menu foi cadastrado
 * @property datetime $updated_at Data em que foi atualizado
 */
class Menu extends Modelo {
    protected $table = 'menu';
    protected $primaryKey = 'id_menu';
    protected $guarded = array();
    
    public function telas(){
        return $this->hasMany('Tela', 'id_menu');
    }
}
