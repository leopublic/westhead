<?php

class TipoServicoTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('servico')->truncate();
		DB::table('tipo_servico')->truncate();

		$tiposervico = array('nome' => 'Autorização permanente');	// 1
		DB::table('tipo_servico')->insert($tiposervico);
		$tiposervico = array('nome' => 'Autorização temporária');	// 2
		DB::table('tipo_servico')->insert($tiposervico);	
		$tiposervico = array('nome' => 'Registro');					// 3
		DB::table('tipo_servico')->insert($tiposervico);
		$tiposervico = array('nome' => 'Coleta de visto');			// 4 
		DB::table('tipo_servico')->insert($tiposervico);
		$tiposervico = array('nome' => 'Prorrogação');				// 5
		DB::table('tipo_servico')->insert($tiposervico);
		$tiposervico = array('nome' => 'Outros serviços');			// 6
		DB::table('tipo_servico')->insert($tiposervico);
		$tiposervico = array('nome' => 'Atendimento pessoal');		// 7
		DB::table('tipo_servico')->insert($tiposervico);
		$tiposervico = array('nome' => 'Cancelamento');				// 8
		DB::table('tipo_servico')->insert($tiposervico);

		// Uncomment the below to run the seeder
	}

}
