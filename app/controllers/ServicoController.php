<?php

class ServicoController extends \CadastroController {

    const raiz = "servico";
    const campoChave = 'id_servico';
    const nomeSingular = 'Serviço';
    const nomePlural = 'Serviços';
    const artigo = 'o';

    public function RegistrosIndex() {
        $data = Servico::leftJoin('tipo_servico', 'tipo_servico.id_tipo_servico', '=', 'servico.id_tipo_servico')
                ->leftJoin('tipo_autorizacao', 'tipo_autorizacao.id_tipo_autorizacao', '=', 'servico.id_tipo_autorizacao')
                ->select(array('id_servico', 'servico.descricao', 'servico.descricao_en', DB::raw("coalesce(tipo_servico.nome,'--')  as tipo_servico_nome"), DB::raw("coalesce(tipo_autorizacao.descricao, '--') as tipo_autorizacao_descricao"), 'fl_andamento'))
                ->orderBy('descricao')
                ->get();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(
            array("titulo" => "Descrição", "campo" => "descricao"),
            array("titulo" => "Descrição (en)", "campo" => "descricao_en"),
            array("titulo" => "Tipo de serviço", "campo" => "tipo_servico_nome"),
            array("titulo" => "Autorização", "campo" => "tipo_autorizacao_descricao")
        );
        return $data;
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = Servico::find($id);
        } else {
            $this->obj = new Servico;
        }
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = self::nomeSingular;
        $data['titulo_grid'] = "Lista";
        $data['subTitulo'] = "Administrar " . self::nomePlural;
        $data['urlEdit'] = self::raiz . '/show';
        $data['urlDel'] = self::raiz . '/del';
        $data['urlCreate'] = self::raiz . '/create';
        $data['nomeCampoChave'] = self::campoChave;
        $data['menu'] = 'menuCadastros';
        $data['item'] = 'itemServicos';
        return $data;
    }

    public function get_ViewShow() {
        return self::raiz . '/show';
    }
    public function get_ViewAcoes() {
        return self::raiz . '/acoes';
    }

    public function MontaCampos() {
        //	$this->campos[] = new
    }

    public function getDel($id = 0) {
        try {
            Servico::destroy($id);
            return Redirect::to(self::raiz)
                            ->with('flash_msg', self::nomeSingular . ' excluíd' . self::artigo . ' com sucesso!');
        } catch (Exception $e) {
            return Redirect::to(self::raiz)
                            ->with('flash_error', $e->getMessage());
        }
    }

    public function postUpdate($id = 0) {
        if ($id > 0) {
            $obj = Servico::find($id);
        } else {
            $obj = new Servico();
        }
        $obj->fill(Input::all());
        if ($obj->id_tipo_servico == '') {
            Input::flash();
            return Redirect::to(self::raiz . '/show/' . $id)->withErrors('Informe o tipo de serviço');
        } else {
            if ($obj->save()) {
                return Redirect::to(self::raiz)->with('flash_msg', self::nomeSingular . ' atualizad' . self::artigo . ' com sucesso!');
            } else {
                Input::flash();
                return Redirect::to(self::raiz . '/show/' . $id)->withErrors($obj->errors);
            }
        }
    }

    public function getIncluinoandamento($id){
        $obj = Servico::find($id);
        $obj->fl_andamento = 1;
        $obj->save();
        return Redirect::to(self::raiz)->with('flash_msg', 'Serviço adicionado ao andamento com sucesso!');

    }

    public function getRetiradoandamento($id){
        $obj = Servico::find($id);
        $obj->fl_andamento = 0;
        $obj->save();
        return Redirect::to(self::raiz)->with('flash_msg', 'Serviço removido do andamento com sucesso!');
    }

}
