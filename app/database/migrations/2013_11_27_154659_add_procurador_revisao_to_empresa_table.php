<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddProcuradorRevisaoToEmpresaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('empresa', function(Blueprint $table) {
            $table->integer('id_procurador')->unsigned()->nullable();
            $table->string('rev_embarcacoes', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('empresa', function(Blueprint $table) {
            $table->dropColumn('id_procurador');
            $table->dropColumn('rev_embarcacoes');
        });
    }

}
