<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CarregaAcoesCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'acoes:carregar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Carrega o cadastro de telas e ações.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        print "#======================================================#\n";
        print "# Processando telas...                                 #\n";
        print "#======================================================#\n";
        \Tela::carregaTelas();
        print "#======================================================#\n";
        print "# Processando acoes...                                 #\n";
        print "#======================================================#\n";
        \Acao::carregaAcoes();
        print "#======================================================#\n";
        print "Finalizado com sucesso!!\n";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
        );
    }

}
