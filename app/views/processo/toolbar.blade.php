<div class="box-tool">
    <div class="btn-group">
        @if ($processo->id_os > 0)
        <a href="/os/show/{{$processo->id_os}}" class="btn btn-sm btn-warning" target="_blank" title="Clique para ver a OS numa nova aba">OS {{$processo->os->numero}}</a>
        @endif
    </div>
    <div class="btn-group">
        <a href="#" data-toggle="dropdown" class="btn btn-sm btn-gray dropdown-toggle"><i class="icon-cog"></i> Ajustar <span class="caret"></span></a>
        <ul class="dropdown-menu dropdown-gray pull-right">
            @if ($processo->id_visto > 0)
                @if (Auth::user()->temAcessoA('PROC', 'ORD'))
                    <li><a href="/processo/paracima/{{$processo->id_processo}}" class="" title="Mover esse processo para cima na ordem"><i class="icon-arrow-up"></i> Mover para cima</a></li>
                    <li><a href="/processo/parabaixo/{{$processo->id_processo}}" class=""  title="Mover esse processo para baixo na ordem"><i class="icon-arrow-down"></i> Mover para baixo</a></li>
                @endif
                @if (!$processo->eh_principal)
                    @if (Auth::user()->temAcessoA('PROC', 'PRIN'))
                        <li><a href="/processo/principal/{{$processo->id_processo}}" class=""  title="Transformar esse processo no processo principal do visto"><i class="icon-star"></i> Principal</a></li>
                    @endif
                @endif
                @if (Auth::user()->temAcessoA('PROC', 'NVIS'))
                    <li><a href="/processo/novovisto/{{$processo->id_processo}}" class=""  title="Transformar esse processo em um novo visto"><i class="icon-magic"></i> Transformar em visto</a></li>
                @endif
                @if (Auth::user()->temAcessoA('PROC', 'EXCL'))
                    <li><a href="#" onClick="javascript:confirmaLink('Tem certeza que deseja excluir esse processo?', 'Atenção', '/processo/excluir/{{$processo->id_processo}}')" class=""  title="Excluir esse processo"><i class="icon-trash"></i> Excluir</a></li>
                @endif
            @endif
        </ul>
    </div>
    @if (Auth::user()->temAcessoA('PROC', 'TRAN'))
        @if (count($processo->outrosVistos()) > 0)
            <div class="btn-group">
                <a href="#" data-toggle="dropdown" class="btn btn-sm btn-gray dropdown-toggle"><i class="icon-random"></i> Transferir para <span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-gray pull-right">
                    @foreach($processo->outrosVistos() as $visto)
                    <li><a href="/processo/transferirpara/{{$processo->id_processo}}/{{$visto->id_visto}}" class="" title="Mover esse processo para o visto indicado"><i class="icon-mail-forward"></i> {{$visto->titulo}}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endif
    <a data-action="collapse"><i class="icon-chevron-up"></i></a>
</div>
