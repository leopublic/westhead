<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDtEmissaoCpfToCandidatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('candidato', function(Blueprint $table) {
			$table->date('dt_emissao_cpf')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if(Schema::hasColumn('candidato', 'dt_emissao_cpf')){
            Schema::table('candidato', function(Blueprint $table) {
                $table->dropColumn('dt_emissao_cpf');
            });
        }
	}

}
