<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkIdTipoServicoToServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('servico', function(Blueprint $table) {
			$table->foreign('id_tipo_servico')->references('id_tipo_servico')->on('tipo_servico');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('servico', function(Blueprint $table) {
			$table->dropForeign('servico_id_tipo_servico_foreign');
		});
	}

}
