<?php
class ProcessoOutros extends Processo {

    protected $guarded = array('id_status_andamento', 'obs_andamento', 'tem_andamento');

    public static function boot() {
        parent::boot();

        static::saving(function($modelo) {
        });

        static::saved(function($modelo) {
            // registra log
        });
    }

    public function recupereDadosDaOs() {
        parent::recupereDadosDaOs();
    }

    public function assumeDefaults() {
        $this->id_tipoprocesso = 6;
    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

}
