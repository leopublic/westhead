@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Ordem de serviço {{substr('000000'.$obj->numero, -6)}}</h1>
    @if (is_object($obj->empresa))
    <h4>{{$obj->empresa->razaosocial}}</h4>
    @endif
    @if (is_object($obj->projeto))
    <h6>{{$obj->projeto->descricao}}</h6>
    @endif
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>&nbsp;</h3>
                <ul class="nav nav-tabs  pull-left">
                    <li class=""><a href="/os/show/{{$obj->id_os}}" ><i class="icon-tags"></i> Dados da OS</a></li>
                    <li class=""><a href="/os/candidatos/{{$obj->id_os}}" ><i class="icon-group"></i> Candidatos</a></li>
                    <li class="active"><a href="#" ><i class="icon-folder-open"></i> Arquivos</a></li>
                    <li class=""><a href="/os/despesas/{{$obj->id_os}}" ><i class="icon-dollar"></i> Despesas</a></li>
                    <li class=""><a href="/os/desmembrar/{{$obj->id_os}}" ><i class="icon-exchange"></i> Desmembrar</a></li>
                </ul>
                <div class="box-tool">
                    @include('os.menu_os', array('obj', $obj))
                </div>
            </div>

            <div class="box-content">
                {{Form::hidden('id_os', $obj->id_os, array('id'=>'id_os'))}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-folder-open"></i> Adicionar anexos</h3>
                            </div>
                            <div class="box-content">
                                <form action="/anexo/novoos/{{$obj->id_os}}" class=" dropzone hidden-xs dz-clickable" id="my-awesome-dropzone" method="POST">
                                    <div class="dz-message" style="text-align:center;height:">
                                        Arraste os arquivos para cá ou clique para selecionar
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- coluna candidatos -->
                    <div class="col-md-12" id="containerAnexos">
                        @include('anexos', array('anexos' => $obj->anexos) )
                    </div>
                    <!-- /coluna candidatos -->
                </div>

                </div>
            </div>
        </div>
    </div>
</div>

</div>




@stop

@section('scripts')
<script src="/assets/dropzone-3.8.3/downloads/dropzone.min.js"></script>
<script>
    function ChamaRota(form, rota, retorno)
    {
        var xdata = $(form).serialize();
        $(retorno).html('<div style="text-align:center"><i class="icon-spinner icon-spin icon-3x"></i> carregando...</div>');
        $.ajax({
            type: 'POST'
            , url: rota
            , data: xdata
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        $(retorno).html(data);
                    }
        });
    }

    function ExcluirAnexo(id)
    {
        $.ajax({
            type: 'GET'
            , url: '/anexo/excluir/' + id
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        AtualizarAnexos();
                    }
        });

    }

    function AtualizaTipoAnexo(id, id_tipo_anexo)
    {
        $.ajax({
            type: 'POST'
            , url: '/anexo/atualizartipo/' + id
            , data: {"id_tipo_anexo": $(id_tipo_anexo).val()}
            , success:
                    function(data)
                    {
                        AtualizarAnexos();
                    }
        });

    }

    function AtualizarAnexos()
    {
        $('#containerAnexos').html('<div style="text-align:center; padding:20px;"><i class="icon-spinner icon-spin icon-4x"></i></div>');

        $.ajax({
            type: 'GET'
            , url: '/anexo/daos/' + $('#id_os').val()
            , dataType: 'html'
            , success:
                    function(data)
                    {
                        $('#containerAnexos').html(data);
                    }
        });

    }

    $(document).ready(function() {
        Dropzone.options.myAwesomeDropzone = {
            paramName: "file", // The name that will be used to transfer the file
            complete: function(file, done) {
                AtualizarAnexos();
            }
        }

    })

    $(document).ready(function() {
        $(window).keydown(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });


</script>

@stop
