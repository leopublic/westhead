<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableCandidato extends Migration {

	public function up()
	{
        Schema::create('candidato', function( $table) {
            $table->increments('id_candidato');
			$table->string('nome_completo', 1000);
			$table->string('nome_pai', 1000)->nullable();
			$table->string('nome_mae', 1000)->nullable();
			$table->date('dt_nascimento')->nullable();
			$table->string('nu_passaporte', 50)->nullable();
			$table->string('email', 500)->nullable();
			$table->string('nu_cpf', 50)->nullable();
			$table->string('nu_rne', 50)->nullable();
			$table->string('nu_ctps', 50)->nullable();			
			$table->string('nu_cnh', 50)->nullable();
			$table->date('dt_validade_cnh')->nullable();
			$table->date('dt_validade_ctps')->nullable();

			$table->string('sexo', 1)->nullable();
			
			$table->text('trabalho_anterior')->nullable();
			$table->text('descricao_atividades')->nullable();

			$table->decimal('remuneracao_br', 10,2)->nullable();
			$table->decimal('remuneracao_ext', 10,2)->nullable();
			
			$table->string('local_nascimento')->nullable();

			$table->integer('id_pais_nacionalidade')->unsigned()->nullable();
			$table->integer('id_estadocivil')->unsigned()->nullable();
			$table->integer('id_escolaridade')->unsigned()->nullable();
			$table->integer('id_profissao')->unsigned()->nullable();
			$table->integer('id_funcao')->unsigned()->nullable();
			// Empresa e projeto
			$table->integer('id_empresa')->unsigned()->nullable();
			$table->integer('id_projeto')->unsigned()->nullable();
			// Endereco residencial
			$table->string('endereco_res', 1000)->nullable();
			$table->string('cidade_res', 1000)->nullable();
			$table->string('telefone_res', 50)->nullable();
			$table->integer('id_pais_res')->unsigned()->nullable();
			// Endereco estrangeiro
			$table->string('endereco_estr', 1000)->nullable();
			$table->string('cidade_estr', 1000)->nullable();
			$table->string('telefone_estr', 50)->nullable();
			$table->integer('id_pais_estr')->unsigned()->nullable();
			// Passaporte
			$table->date('dt_emissao_passaporte')->nullable();
			$table->date('dt_validade_passaporte')->nullable();
			$table->integer('id_pais_passaporte')->unsigned()->nullable();
			// Parente
			$table->integer('id_candidato_principal')->unsigned()->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('candidato');
	}

}