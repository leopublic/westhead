<?php
/**
 * @property string $numero_rne
 * @property date   $data_expedicao
 * @property date   $validade_cie
 */
class ProcessoColeta extends Processo {

    protected $guarded = array('id_status_andamento', 'obs_andamento', 'tem_andamento');

    public function assumeDefaults() {
        $this->id_tipoprocesso = 9;
    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

    public function recupereDadosDaOs() {
        parent::recupereDadosDaOs();
    }

    public function getNumeroRneAttribute() {
        return $this->attributes['string_1'];
    }

    public function setNumeroRneAttribute($valor) {
        $this->attributes['string_1'] = $valor;
    }

    public function getDataExpedicaoAttribute() {
        return $this->dtModelParaView($this->attributes['date_1']);
    }

    public function setDataExpedicaoAttribute($valor) {
        $this->attributes['date_1'] = $this->dtViewParaModel($valor);
    }

    public function getValidadeCieAttribute() {
        return $this->dtModelParaView($this->attributes['date_2']);
    }

    public function setValidadeCieAttribute($valor) {
        $this->attributes['date_2'] = $this->dtViewParaModel($valor);
    }

    public function getTituloAttribute(){
        return 'Coleta';
    }
}
