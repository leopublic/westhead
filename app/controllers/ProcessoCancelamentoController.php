<?php

class ProcessoCancelamentoController extends \ProcessoGenericoController {

    public function getShow($id) {
        
    }

    public function postUpdate($id) {
        $processo = ProcessoCancelamento::find($id);
        try {
            $msg = '';
            if (strlen(str_replace("_", "", Input::get('numero_processo'))) < 20) {
                $msg .= "<br/>- O número do processo está incompleto.";
            }
            if (Input::get('data_requerimento') == '' || Input::get('data_requerimento') == '__/__/____') {
                $msg .= "<br/>- A data do requerimento é obrigatória.";
            } else {
                if (!Modelo::dataOk(Input::get('data_requerimento'))){
                    $msg .= "<br/>- A data do requerimento é inválida: '".Input::get('data_requerimento')."'";
                }
            }
            if ($msg != '') {
                Session::flash($processo->processo_id."_error", "O processo não foi atualizado. Verifique o preenchimento:".$msg);
                return Redirect::to('/candidato/show/' . $processo->id_candidato . '#processo' . $id);
            } else {
                $processo->fill(Input::all());
                $processo->save();

                if (Input::has('tem_andamento')){
                    $this->repositorio()->atualiza_status_andamento($processo, Input::get('id_status_andamento'), Input::get('obs_andamento'), \Auth::user()->id_usuario);
                }

                if ($processo->dataRequerimento != '' && $processo->dataRequerimento != '00/00/0000' && is_object($processo->os)) {
                    $processo->os->concluiPeloProcesso($processo->dataRequerimento);
                }
                Session::flash($processo->id_processo."_success", 'Processo atualizado com sucesso!');
            }
        } catch (Exception $ex) {
            Session::flash($processo->processo_id."_error", $ex->getMessage());
        }
        $proccontrol = new \ProcessoController();
        return $proccontrol->rotaParaVisualizarProcesso($processo);
    }

    public function getAdicione($id_visto) {
        $rep = new Westhead\Repositorios\RepositorioProcesso;
        $processo = $rep->criaProcessoNoVisto(8, $id_visto);
        return Redirect::to('/candidato/show/' . $processo->id_candidato . '#processo' . $processo->id_processo);
    }

}
