<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableProcesso extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('processo', function( $table) {
            $table->increments('id_processo');
			$table->integer('id_tipoprocesso')->unsigned();
			$table->integer('id_candidato')->unsigned();
			$table->integer('id_empresa')->unsigned()->nullable();
			$table->integer('id_projeto')->unsigned()->nullable();
			$table->integer('id_os')->unsigned()->nullable();
			$table->integer('id_visto')->unsigned()->nullable();
			$table->integer('id_status_processo')->unsigned()->nullable();
			$table->integer('id_servico')->unsigned()->nullable();
			$table->integer('ordem')->unsigned()->nullable();
			$table->date('date_1')->nullable();
			$table->date('date_2')->nullable();
			$table->date('date_3')->nullable();
			$table->date('date_4')->nullable();
			$table->date('date_5')->nullable();
			$table->date('date_6')->nullable();
			$table->string('string_1')->nullable();
			$table->string('string_2')->nullable();
			$table->string('string_3')->nullable();
			$table->string('string_4')->nullable();
			$table->string('string_5')->nullable();
			$table->string('string_6')->nullable();
			$table->integer('integer_1')->unsigned()->nullable();
			$table->integer('integer_2')->unsigned()->nullable();
			$table->integer('integer_3')->unsigned()->nullable();
			$table->integer('integer_4')->unsigned()->nullable();
			$table->integer('integer_5')->unsigned()->nullable();
			$table->integer('integer_6')->unsigned()->nullable();
			$table->text('observacao')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('processo')){
			Schema::drop('processo');
		}
	}

}