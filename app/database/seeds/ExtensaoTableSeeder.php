<?php

class ExtensaoTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('extensao')->truncate();

        $extensao = array('extensao' => 'bmp'   , 'mime' => 'image/x-windows-bmp');
        DB::table('extensao')->insert($extensao);
		$extensao = array('extensao' => 'jpg'   , 'mime' => 'image/jpeg');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'png'   , 'mime' => 'image/png');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'gif'   , 'mime' => 'image/gif');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'tiff'  , 'mime' => 'image/tiff');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'pdf'   , 'mime' => 'application/pdf');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'doc'   , 'mime' => 'application/msword');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'docx'  , 'mime' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'xls'   , 'mime' => 'application/vnd.ms-excel');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'xlsx'  , 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'ppt'   , 'mime' => 'application/vnd.ms-powerpoint');
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'pptx'  , 'mime' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation
');
        DB::table('extensao')->insert($extensao);

		// Uncomment the below to run the seeder
	}

}
