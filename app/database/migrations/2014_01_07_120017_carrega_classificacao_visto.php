<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CarregaClassificacaoVisto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::table('classificacao_visto')->truncate();

        $obj = array("sigla" => "P", "classificacao" => "Permanente");
        DB::table('classificacao_visto')->insert($obj);
        $obj = array("sigla" => "T", "classificacao" => "Temporário");
        DB::table('classificacao_visto')->insert($obj);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
