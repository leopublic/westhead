<?php
namespace Westhead\Validadores;

class ValidadorCandidatoIncompleto extends Validador {
    public static $rules = array(
        'nome_completo'                 => 'required',
        'nome_mae'                      => 'required',
        'dt_nascimento'                 => 'required|datavalida:dd/mm/yyyy',
    );
    public static $messages = array(
        'nome_completo.required'        => 'O nome do candidato é obrigatório',
        'nome_mae.required'             => 'O nome da mãe do candidato é obrigatório para a validação de homônimos',
        'dt_nascimento.required'        => 'A data de nascimento é obrigatória para a validação de homônimos',
        'dt_nascimento.datavalida'      => 'Informe uma data de nascimento válida (dd/mm/aaaa)',
    );
}
