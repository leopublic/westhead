<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFlEmbarcacaoToProjetoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projeto', function(Blueprint $table) {
			$table->boolean('fl_embarcacao')->nullable();
		});
        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projeto', function(Blueprint $table) {
			$table->dropColumn('fl_embarcacao');
		});
	}

}
