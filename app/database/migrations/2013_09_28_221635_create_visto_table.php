<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVistoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visto', function(Blueprint $table) {
			$table->increments('id_visto');
			$table->integer('id_candidato')->unsigned();
			$table->string('processo_atual')->nullable();
			$table->date('dt_prazo_estada')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visto');
	}

}
