
<?php

/**
 * @property date $dt_solicitacao
 * @property date $dt_execucao
 * @property dateTime $dt_assinatura Data da liberação da assinatura
 */
class OsCandidato extends Modelo {

    protected $table = 'os_candidato';
    protected $primaryKey = 'id_oscandidato';
    protected $guarded = array('numero');

    public static function boot() {
        parent::boot();

        static::saving(function($modelo) {
            $modelo->assumeDefaults();
            // 		return $modelo->valido($modelo->regras(), $modelo->mensagens());
        });

        static::saved(function($modelo) {
            // registra log
        });
    }


}
