<!DOCTYPE html>
<html>
    <head>
        @include('html/head')
        @yield('styles')
    </head>
    <body class="login-page" style="background: #EAF4F6 !important;">
        <!-- BEGIN Main Content -->
        <div class="login-wrapper">
            <div style="text-align: center; margin:auto; margin-bottom:15px;">
            <img src="/img/logo_ok.jpg" style="width:340px;"/>
            </div>
            <!-- BEGIN Login Form -->
            {{ Form::open() }}

                <h3>Westhead Visa Control</h3>
                <hr/>
                <div class="form-group">
                    <div class="controls">
                        <input type="text" name="username" placeholder="Login" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <input type="password" name="password" placeholder="Senha" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary form-control">Entrar</button>
                    </div>
                </div>
                <hr/>

                @if (Session::has('login_errors'))
                    <span class="error">{{ Session::get('msg') }}</span><br/>
                @endif
                <a href="/auth/esquecisenha">Esqueci minha senha!</a>
            {{ Form::close() }}
            </form>
            <!-- END Login Form -->
        </div>
        <!-- END Main Content -->

        <!--basic scripts-->
        <script src="assets/jquery/jquery-2.0.3.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            function goToForm(form)
            {
                $('.login-wrapper > form:visible').fadeOut(500, function(){
                    $('#form-' + form).fadeIn(500);
                });
            }
            $(function() {
                $('.goto-login').click(function(){
                    goToForm('login');
                });
                $('.goto-forgot').click(function(){
                    goToForm('forgot');
                });
                $('.goto-register').click(function(){
                    goToForm('register');
                });
            });
        </script>
    </body>
</html>
