<?php
/**
 * @property string $numero_visto           string_1
 * @property date   $data_expedicao         date_1
 * @property string $prazo_estada           string_2
 * @property int    $id_reparticao          integer_1
 * @property date   $data_entrada           date_2
 * @property string $local_entrada          string_3
 */
class ProcessoColetaVisto extends Processo {

    protected $guarded = array('id_status_andamento', 'obs_andamento', 'tem_andamento');

    public function assumeDefaults() {
        $this->id_tipoprocesso = 4;
    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

    public function recupereDadosDaOs() {
        parent::recupereDadosDaOs();
    }

    public function getNumeroVistoAttribute() {
        return $this->attributes['string_1'];
    }

    public function setNumeroVistoAttribute($valor) {
        $this->attributes['string_1'] = $valor;
    }

    public function getDataExpedicaoAttribute() {
        return $this->dtModelParaView($this->attributes['date_1']);
    }

    public function setDataExpedicaoAttribute($valor) {
        $this->attributes['date_1'] = $this->dtViewParaModel($valor);
    }

    public function getPrazoEstadaAttribute() {
        return $this->attributes['string_2'];
    }

    public function setPrazoEstadaAttribute($valor) {
        $this->attributes['string_2'] = $valor;
    }

    public function getIdReparticaoAttribute() {
        return $this->attributes['integer_1'];
    }

    public function setIdReparticaoAttribute($valor) {
        $this->attributes['integer_1'] = $valor;
    }

    public function getLocalEntradaAttribute() {
        return $this->attributes['string_3'];
    }

    public function setLocalEntradaAttribute($valor) {
        $this->attributes['string_3'] = $valor;
    }

    public function getDataEntradaAttribute() {
        return $this->attributes['date_2'];
    }

    public function setDataEntradaAttribute($valor) {
        $this->attributes['date_2'] = $valor;
    }

    public function getDataEntradaFmtAttribute() {
        return $this->dtModelParaView($this->attributes['date_2']);
    }

    public function setDataEntradaFmtAttribute($valor) {
        $this->attributes['date_2'] = $this->dtViewParaModel($valor);
    }

    public function getTituloAttribute(){
        return 'Coleta de visto';
    }
}