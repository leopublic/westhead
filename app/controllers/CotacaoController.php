<?php

class CotacaoController extends BaseController {

    protected $campos;
    protected $obj;

    public function getIndex(){

        $mes = Input::old('mes', date('m'));
        $ano = Input::old('ano', date('Y'));

        $cotacao = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        $cotacoes = \Cotacao::whereRaw('month(data) = '.$mes)->whereRaw('year(data) = '.$ano)
                ->select(array(DB::raw('day(data) dia'), 'compra'))
                ->orderBy('data')->get();
        foreach($cotacoes as $c){
            $cotacao[$c->dia] = $c->compra;
        }

        return View::make('cotacao.index')
                        ->with('mes', $mes)
                        ->with('ano', $ano)
                        ->with('cotacao', $cotacao)
        ;
    }

    public function postIndex(){
        Input::flash();
        return Redirect::to('/cotacao')->withInput();
    }


    public function postAtualizar(){
        $mes = Input::get('mes');
        $ano = Input::get('ano');
        for($i=1; $i< 32; $i++){
            $data = $i.'/'.$mes.'/'.$ano;
            $dtBanco = $ano.'-'.$mes.'-'.$i;
            $x = \Westhead\Validadores\ValidatorM2::dataValida($data, 'dd/mm/yyyy');
            if ($x){
                $cot = \Cotacao::where('data', '=', $dtBanco)->first();
                if (!is_object($cot)){
                    $cot = new \Cotacao;
                    $cot->data = $dtBanco;
                }
                $cot->compra = str_replace(",", ".", str_replace(".", "", Input::get($i) ));
                $cot->save();
            }
        }
        Input::flashOnly(array('mes', 'ano'));
        return Redirect::to('/cotacao')->with('flash_msg', 'Cotações atualizadas com sucesso');
    }
}