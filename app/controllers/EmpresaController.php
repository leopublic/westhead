<?php

class EmpresaController extends \CadastroController {

    public function RegistrosIndex() {
        $data = Empresa::todos()->get();
        return $data;
    }

    public function ColunasIndex() {
        $data = array(
            array("titulo" => "Razão social", "campo" => "razaosocial")
            , array("titulo" => "CNPJ", "campo" => "cnpj")
            , array("titulo" => "# projetos", "campo" => "qtd_projetos")
            , array("titulo" => "# candidatos", "campo" => "qtd_candidatos")
            , array("titulo" => "# os", "campo" => "qtd_os")
            , array("titulo" => "Ativa", "campo" => "fl_ativo")
        );
        return $data;
    }

    public function get_ViewIndex() {
        return 'empresa.index';
    }

    public function ObtemInstancia($id = 0) {
        if ($id > 0) {
            $this->obj = Empresa::find($id);
        } else {
            $this->obj = new Empresa();
        }
    }

    public function TelaIndex() {
        $data = array();
        $data['titulo_tela'] = "Empresas";
        $data['titulo_grid'] = "Clique na ação ao lado da empresa para alterar ou excluir";
        $data['subTitulo'] = "Administrar empresas (clientes)";
        $data['urlEdit'] = 'empresa/show';
        $data['urlDel'] = 'empresa/del';
        $data['urlCreate'] = 'empresa/create/';
        $data['nomeCampoChave'] = 'id_empresa';
        $data['menu'] = 'menuCadastros';
        $data['item'] = 'itemEmpresas';
        return $data;
    }

    public function get_ViewShow() {
        return 'empresa_show';
    }

    public function MontaCampos() {
        //	$this->campos[] = new
    }

    public function postUpdate($id = 0) {
        Input::flash();
        if ($id > 0) {
            $empresa = Empresa::find($id);
        } else {
            $empresa = new Empresa();
        }
        $input = Input::all();
        $empresa->fill($input);
        // Validações
        $messages = array(
            'razaosocial.unique' => 'O campo razão social não pode assumir valores iguais em outra empresa.',
            'required' => 'O campo :attribute é obrigatório.',
            'regex' => 'Formato do campo :attribute inválido.'
        );
        $data = Input::all();
        $validator = Validator::make(
                        $data, array(
                    'razaosocial' => 'required|unique:empresa,razaosocial,' . $id . ',id_empresa',
                    'cnpj' => 'regex:/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/'
                        ), $messages);
        if ($validator->passes()) {
            $empresa->save();
            return Redirect::to('empresa')->with('flash_msg', 'Empresa atualizada com sucesso!');
        } else {
            return Redirect::to('empresa/show/' . $id)->withErrors($validator);
        }
    }

    public function postNovologo($id_empresa) {
        $ds = DIRECTORY_SEPARATOR;  //1
        $storeFolder = '../public/arquivos/logos/';   //2
        if (!empty($_FILES)) {

            $tempFile = $_FILES['file']['tmp_name'];          //3
            $path_parts = pathinfo($_FILES["file"]["name"]);
            $extension = $path_parts['extension'];
            $targetFile = $storeFolder . $id_empresa . "." . $extension;  //5
            try {
                unlink($targetFile);
            } catch (Exception $e) {
                
            }
            move_uploaded_file($tempFile, $targetFile); //6
            $empresa = Empresa::find($id_empresa);
            $empresa->extensao_logo = $extension;
            $empresa->save();
        }
    }

    public function getRelacaoembarcacoes($id_empresa) {
        $empresa = Empresa::find($id_empresa);
        $projetos_com_1asterisco = \Projeto::where('id_empresa', '=', $id_empresa)
                ->whereNull('dt_inicio_operacao')
                ->orWhere('dt_inicio_operacao', '=', '0000-00-00')
                ->orderBy('descricao')
                ->get()
        ;
        $projetos_com_2asterisco = \Projeto::where('id_empresa', '=', $id_empresa)
                ->whereNotNull('dt_inicio_operacao')
                ->where('dt_inicio_operacao', '<>', '0000-00-00')
                ->whereRaw('datediff(now(), dt_inicio_operacao) <= 90')
                ->orderBy('descricao')
                ->get()
        ;

        $meses = array('', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
        $mes = $meses[intval(date('m'))];

        $html = View::make('empresa.relacao_embarcacoes')
                ->with('empresa', $empresa)
                ->with('mes', $mes)
                ->with('projetos_com_1asterisco', $projetos_com_1asterisco)
                ->with('projetos_com_2asterisco', $projetos_com_2asterisco)
        ;

        $mpdf = new mPDF('utf-8', 'A4', '', '', '10', '10', '10', '20', '0', '10');  // largura x altura
        if ($empresa->rev_embarcacoes != '') {
            $mpdf->setFooter('||' . $empresa->rev_embarcacoes);
        }
        $mpdf->allow_charset_conversion = false;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $mpdf->WriteHTML($html);
        return $mpdf->Output();
    }

    public function getDel($id_empresa){
        try{
            DB::beginTransaction();
            \Candidato::where('id_empresa', '=', $id_empresa)->update(array('id_empresa' => null, 'id_projeto'=>null));
            \Projeto::where('id_empresa_armador', '=', $id_empresa)->update(array('id_empresa_armador' => null));
            \Os::where('id_empresa', '=', $id_empresa)->update(array('id_empresa'=> null));
            \Os::whereIn('id_projeto', function($query) use ($id_empresa){
                            $query->select('id_projeto')
                            ->from('projeto')
                            ->where('id_empresa', '=', $id_empresa);
                        })->update(array('id_projeto'=> null));
            \Processo::where('id_empresa', '=', $id_empresa)->update(array('id_empresa' => null, 'id_projeto'=>null));
            \Projeto::where('id_empresa', '=', $id_empresa)->delete();
            \Empresa::where('id_empresa', '=', $id_empresa)->delete();
            DB::commit();
            return Redirect::to('empresa')->with('flash_msg', 'Empresa excluída com sucesso!');

        } catch(Exception $e){
            return Redirect::to('empresa')->with('flash_error', 'Não foi possível excluir a empresa pois: '.$e->getMessage());
        }

    }

}
