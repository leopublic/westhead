<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoDespesaxTable extends Migration {

    public function up() {
        Schema::dropIfExists('tipo_despesa');

        Schema::create('tipo_despesa', function(Blueprint $table) {
            $table->increments('id_tipo_despesa');
            $table->string('descricao_pt_br', 1000);
            $table->string('descricao_en_us', 1000);
            $table->boolean('fl_valor_fixo')->nullable();
            $table->decimal('valor_fixo', 10, 2)->unsigned()->nullable();
            $table->timestamps();
        });

		$tipodespesa = array('descricao_pt_br' => 'Malote', 'descricao_en_us' => 'Courier');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Taxa de registro / restabelecimento', 'descricao_en_us' => 'Registration Fee');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Taxa de Visto', 'descricao_en_us' => 'Brazilian Working Visa Governamental Fee');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Autenticações Cartorárias', 'descricao_en_us' => 'Notarized copies');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Reconhecimento de firma', 'descricao_en_us' => 'Notarized Signature');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Transporte', 'descricao_en_us' => 'Transportation');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Ônibus', 'descricao_en_us' => 'Bus');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Mêtro', 'descricao_en_us' => 'Subway');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Táxi', 'descricao_en_us' => 'Taxi');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Xerox', 'descricao_en_us' => 'Photocopies');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Internet', 'descricao_en_us' => 'Internet');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Fotos', 'descricao_en_us' => 'Photographs');
        DB::table('tipo_despesa')->insert($tipodespesa);
        $tipodespesa = array('descricao_pt_br' => 'Taxa ASO Panamenho', 'descricao_en_us' => 'Panama Medical fee');
        DB::table('tipo_despesa')->insert($tipodespesa);
    }

    public function down() {
        Schema::drop('tipo_despesa');
    }

}
