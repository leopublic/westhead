<html>
	<head>
		<style>
			*, body{font-family: Verdana, Arial, sans-serif;}
			h1{
				font-family: inherit;
				text-align: center;
				font-size: 20px;
				margin-top: 0px;
				padding-top: 0px;
			}
			div.titulo{
				font-family: inherit;
				font-size:14px;
				font-weight: bold;
				margin-top:5px;
				margin-bottom:5px;
                width:100%;
                text-align:center;
			}
			table{
				font-family: inherit;
				font-size:12px;
				margin:0;
				padding:0;
				border-spacing: 0;
				border-collapse: collapse;
				width:100%;
			}
			table tr td{
				padding: 2px;
				font-family: inherit;
				font-size:inherit;
				border: solid 1px #000;
				vertical-align: bottom;
				page-break-inside: avoid;
				border-top: none;
                padding-top:none;
			}
			table tr th{
				padding: 2px;
				font-family: inherit;
				font-size:inherit;
				font-weight: bold;
				border: solid 1px #000;
				border-bottom: none;
				vertical-align: top;
				text-align: left;
                padding-bottom:none;
			}
			table tr.deps td{
				border: solid 1px #000;
			}
			div.inseparavel{
				page-break-inside: avoid;
			}
			table.semLinhas tr td
			,table.semLinhas tr th{
				border: none;
				font-family: inherit;
			}
			table tbody{
				page-break-inside: avoid;
			}
			@media all {
				.page-break	{ display: none; }
			}

			@media print {
				.page-break	{ display: block; page-break-before: always; }
			}
		</style>
	</head>
	<body>
		<div class="titulo">A N E X O</div>
		<div class="titulo">Formulário de Requerimento de Autorização de Trabalho</div>
		<table>
			<tbody>
			<tr><th>PROCESSO N&ordm;</th></tr>
			</tbody>
		</table>
		<table>
			<tr>
				<th colspan="4">1. Requerente</th>
				<th style="width:35%">2. Ativ. Econômica</th>
			</tr>
			<tr>
				<td colspan="4">{{ $os->empresa->razaosocial }}</td>
				<td>{{ $os->empresa->atividade_economica }}</td>
			</tr>
			<tr>
				<th colspan="4">3. Endereço</th>
				<th>4. Cidade</th>
			</tr>
			<tr>
				<td colspan="4">{{ $os->empresa->endereco }} {{ $os->empresa->end_complemento }} - {{ $os->empresa->end_bairro }}</td>
				<td>{{ $os->empresa->end_municipio }}</td>
			</tr>
			<tr>
				<th colspan="2">5. UF</th>
				<th colspan="2">6. CEP</th>
				<th>7. Telefone</th>
			</tr>
			<tr>
				<td colspan="2">{{ $os->empresa->end_uf }}</td>
				<td colspan="2">{{ $os->empresa->end_cep }}</td>
				<td>({{ $os->empresa->tel_ddd }}){{ $os->empresa->tel_num }}</td>
			</tr>
			<tr>
				<th colspan="4">8. E-mail</th>
				<th>9. CNPJ/CPF</th>
			</tr>
			<tr>
				<td colspan="4">{{ $os->empresa->email }}</td>
				<td>{{ $os->empresa->cnpj }}</td>
			</tr>
		</table>
		<div class="titulo">VEM REQUERER, COM FUNDAMENTO LEGAL.</div>
		<table>
			<tbody>
			<tr><th>10. Lei/Decreto/Resolução:</th></tr>
			<tr><td>{{ $os->servico->tipoautorizacao->descricao }}</td></tr>
			</tbody>
		</table>
        <div class="titulo">AUTORIZAÇÃO DE TRABALHO PARA O ESTRANGEIRO ABAIXO QUALIFICADO</div>

		<table>
			<tr>
				<th colspan="6">11. Nome </th>
			</tr>
			<tr>
				<td colspan="6">{{ $candidato->nome_completo }}</td>
			</tr>
			<tr>
				<th colspan="6">12 Filiação:</th>
			</tr>
			<tr>
				<td colspan="6"><b>Pai:</b> {{ $candidato->nome_pai }}<br/><b>Mãe:</b> {{ $candidato->nome_mae }}</td>
			</tr>
			<tr>
				<th colspan="3">13. Sexo</th>
				<th colspan="3">14. Estado civil</th>
			</tr>
			<tr>
				<td colspan="3">{{ $candidato->sexo }}</td>
				<td colspan="3">{{ $candidato->estadocivil->descricao}}</td>
			</tr>
			<tr>
				<th colspan="3">15. Data de nascimento</th>
				<th colspan="3">16. Escolaridade</th>
			</tr>
			<tr>
				<td colspan="3">{{$candidato->dt_nascimento}}</th>
				<td colspan="3">{{ $candidato->escolaridade->descricao }}</td>
			</tr>
			<tr>
				<th colspan="3">17. Profissão</th>
				<th colspan="3">18. Nacionalidade</th>
			</tr>
			<tr>
				<td colspan="3">{{ $candidato->profissao->descricao }}</td>
				<td colspan="3">{{ $candidato->nacionalidade->descricao }}</td>
			</tr>
			<tr>
				<th colspan="3">19. Documento de viagem - Validade</th>
				<th colspan="3">20. Função no Brasil</th>
			</tr>
			<tr>
				<td colspan="3">Passaporte n&ordm; {{ $candidato->nu_passaporte }} - {{$candidato->dt_validade_passaporte}}</td>
				<td colspan="3">{{ $candidato->funcao->descricao}}</td>
			</tr>
			<tr>
				<th colspan="3">21. CBO</th>
				<th colspan="3">22. Local de trabalho</th>
			</tr>
			<tr>
				<td colspan="3">{{ $candidato->funcao->codigo_pf }}</td>
				<td colspan="3">{{ $os->local_trabalho }}</td>
			</tr>
			<tr>
				<th colspan="6">23. Dependentes</th>
			</tr>
			<tr class="deps">
				<td>Nome</td>
				<td>Parentesco</td>
				<td>Data nasc.</td>
				<td>Nacionalidade</td>
				<td>Documento de viagem</td>
				<td>Validade</td>
			</tr>
			<tr class="deps">
				<td>não se aplica</td>
				<td>não se aplica</td>
				<td>não se aplica</td>
				<td>não se aplica</td>
				<td>não se aplica</td>
				<td>não se aplica</td>
			</tr>
			<tr>
				<th colspan="3">24. Tipo de visto:</th>
				<th colspan="3">25. Prazo:</th>
			</tr>
			<tr>
				<td colspan="3">{{ $os->servico->classificacao->classificacao }}</td>
				<td colspan="3">{{ $os->prazo_solicitado }}</td>
			</tr>
			<tr>
				<th colspan="6">26. Repartição consular brasileira no exterior:</th>
			</tr>
			<tr>
				<td colspan="6">{{ $candidato->reparticao->descricao }}</td>
			</tr>
            <tr>
                <th colspan="4">27. Procurador:</th>
                <th colspan="2">45. Correio eletrônico:</th>
            </tr>
            <tr>
                <td colspan="4">{{ $os->empresa->procurador->nome }}   CPF: {{ $os->empresa->procurador->cpf }}</td>
                <td colspan="2">{{ $os->empresa->procurador->email }}</td>
            </tr>
			<tr>
                <td colspan="6">Termo em que pede deferimento<br/><br/><br/></td>
			</tr>
			<tr>
                <td colspan="6" style="text-align:center;">
Rio de Janeiro, {{ date('d')}} de {{$mes}} de {{ date('Y')}}.<br/><br/>
____________________________________________________________________<br/>
<strong>{{ $os->empresa->razaosocial }}</strong><br/>
{{ $os->empresa->procurador->nome}}<br/>
{{ $os->empresa->procurador->titulo}}<br/>
CPF {{ $os->empresa->procurador->cpf }}<br/>
                </td>
			</tr>
		</table>
        <div class="page-break">&nbsp;</div>
        <div class="titulo">JUSTIFICATIVA PARA PRORROGAÇÃO / DESCRIÇÃO DE ATIVIDADES</div>
        <strong>01. Justificativa para prorrogação:</strong><br/>
<br/>Em razão do prazo do Contrato de Afretamento da embarcação <strong>{{$os->projeto->descricao}}</strong> até {{$os->projeto->dt_vencimento_contrato}}, os marítimos que atualmente possuem vistos válidos para tripular a referida embarcação, necessitam dar continuidade a operação da mesma em razão de possuírem alto nível de qualificação e experiência além do grande conhecimento das rotinas de trabalho adotadas pelo Armador.
<br/><br/>
A embarcação <strong>{{$os->projeto->descricao}}</strong> é utilizada no apoio logístico às atividades de Exploração e Prospecção de Hidrocarbonetos na plataforma continental brasileira. É um navio de grande robustez, alta potência de máquina e boa mobilidade, destinado a rebocar e auxiliar outras embarcações tendo como seu principal cliente a Petrobrás.
<br/><br/><br/><br/>
<strong>02. Descrição das Atividades</strong>
<br/><br/>O {{$candidato->tratamento()}} <strong>{{$candidato->nome_completo}}</strong>  exerceu no seu período de estada inicial e continuará exercendo as seguintes atividades na função de <strong>{{$candidato->funcao->descricao}}</strong>:
<br/>
<div style="width:100%; margin-left:40px;font-size:12px;">
@if($candidato->descricao_atividades != '')
{{ nl2br($candidato->descricao_atividades) }}
@else
{{ nl2br($candidato->funcao->atividades) }}
@endif
</div>
<br/><br/><div style="width:100%; text-align: center;">
<br/>Termos em que pede deferimento,
<br/>Rio de Janeiro, {{ date('d')}} de {{$mes}} de {{ date('Y')}}.<br/><br/>
____________________________________________________________________<br/>
<strong>{{ $os->empresa->razaosocial }}</strong><br/>
{{ $os->empresa->procurador->nome}}<br/>
{{ $os->empresa->procurador->titulo}}<br/>
CPF {{ $os->empresa->procurador->cpf }}<br/>
</div>
	</body>
</html>
