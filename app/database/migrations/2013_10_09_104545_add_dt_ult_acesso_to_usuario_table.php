<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDtUltAcessoToUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasColumn('usuario', 'dt_ult_acesso')){
			Schema::table('usuario', function(Blueprint $table) {
				$table->dateTime('dt_ult_acesso')->nullable();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasColumn('usuario', 'dt_ult_acesso')){
			Schema::table('usuario', function(Blueprint $table) {
				$table->dropColumn('dt_ult_acesso');
			});
		}
	}

}
