@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Invoice {{$invoice->numeroFmt}}</h1>
    @if (is_object($invoice->centrodecusto))
    <h4>Centro de custo: "{{$invoice->centrodecusto->nome}}"</h4>
    @endif
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-dollar"></i>Dados da Invoice</h3>
                <div class="box-tool">
                    <div class="btn-group">
                        @if ($invoice->cotacao > 0)
                        <a href="{{URL::to('/invoice/demonstrativo/'.$invoice->id_invoice)}}" target="_blank" class="btn btn-primary">Demonstrativo</a>
                        @else
                        <a href="#" onClick="alert('Só é possível emitir o demonstrativo depois de informada a cotação do dólar.')" class="btn btn-primary">Demonstrativo</a>
                        @endif
                    </div>
                    <div class="btn-group">
                        <a href="{{URL::to('/invoice/servicos/'.$invoice->id_invoice)}}" target="_blank" class="btn btn-primary">Serviços</a>
                    </div>
                    <a data-action="collapse"><i class="icon-chevron-up"></i></a>
                </div>
            </div>

            <div class="box-content">
                <div class="row" >
                    <div class="col-md-12 col-sm-12">
                        {{Form::open(array("class"=>"form-horizontal")) }}
                        {{Form::hidden('id_invoice', $invoice->id_invoice)}}
                        @include('html/campo-data', array('id' => 'dataFmt', 'label' => 'Data da invoice', 'valor' => $invoice->dataFmt, 'help' => '', array()))
                        @include('html/campo-data', array('id' => 'vencimentoFmt', 'label' => 'Vencimento', 'valor' => $invoice->vencimentoFmt, 'help' => '', array()))
                        @include('html/campo-texto', array('id' => 'cotacaoFmt', 'label' => 'Cotação do US$', 'valor' => $invoice->cotacaoFmt, 'help' => '', array()))

                        @include('html/campo-texto', array('id' => 'numero', 'label' => 'Sequencial', 'valor' => $invoice->numero, 'help' => '', array()))

                        @include('html/campo-textarea', array('id' => 'observacao', 'label' => 'Observação', 'valor' => $invoice->observacao,  'help' => '', array()))


                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                                {{ HTML::link('os', 'Cancelar', array('class' => 'btn')) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="box">
            <div class="box-title">
                <h3>Ordens de serviço na Invoice</h3>
            </div>

            <div class="box-content">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        <table class="table table-hover fill-head">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 200px">Ação</th>
                                    <th style="width: 80px">Núm.</th>
                                    <th style="width: auto;">Empresa<br/>Projeto/Emb.</th>
                                    <th style="width: auto;">Armador</th>
                                    <th style="width: auto;">Solicitante</th>
                                    <th style="width: 200px">Candidatos</th>
                                    <th style="width: auto;">Serviço</th>
                                    <th style="width: auto; text-align: center;">Entrada</th>
                                    <th style="width: auto; text-align: center;">Execução</th>
                                    <th style="width: auto; text-align: center;">Status</th>
                                </tr>
                            </thead>
                            @if(isset($osdentros))
                            <tbody>
                                <? $i = 0; ?>
                                @foreach($osdentros as $osdentro)
                                <? $i++; ?>
                                @if ($osdentro->id_status_os == 2)
                                <tr style="background-color: #36c77b;">
                                    @else
                                <tr>
                                    @endif
                                    <td><? print $i; ?></td>
                                    <td>
                                        <a class="btn btn-danger btn-sm" href="{{ URL::to('/invoice/retirar/'.$invoice->id_invoice.'/'.$osdentro->id_os) }}" title="Clique para retirar essa OS da Invoice"><i class="icon-minus"></i></a>
                                        <a class="btn btn-sm" href="{{ URL::to('/os/show/'.$osdentro->id_os) }}" target="_blank"><i class="icon-tags"></i></a>
                                        <a class="btn btn-sm" href="{{ URL::to('/os/candidatos/'.$osdentro->id_os) }}" target="_blank"><i class="icon-group"></i></a>
                                        <a class="btn btn-sm" href="{{ URL::to('/os/anexos/'.$osdentro->id_os) }}" target="_blank"><i class="icon-folder-open"></i></a>
                                        <a class="btn btn-sm" href="{{ URL::to('/os/despesas/'.$osdentro->id_os) }}" target="_blank"><i class="icon-dollar"></i></a>
                                    </td>
                                    <td>{{ substr('000000'.$osdentro->numero, -6) }}</td>
                                    <td>{{ $osdentro['razaosocial'] }}<br/><span style="color:#979797; font-style: italic;">{{ $osdentro->descricao_projeto }}</span></td>
                                    <td>{{ $osdentro->razaosocial_armador }}</td>
                                    <td>{{ $osdentro->solicitante }}</td>
                                    <td>
                                        <? $br = ''; ?>
                                        @foreach($osdentro->candidatos as $candidato)
                                        <a href="{{URL::to('/candidato/show/'.$candidato->id_candidato)}}" target="_blank" title="Clique para ver o candidato em uma nova janela">{{ $br.$candidato->nome_completo }}</a>
                                        <? $br = '<br/>'; ?>
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ $osdentro->descricao_servico  }}
                                    </td>
                                    <td style="text-align: center;">{{$osdentro->dt_solicitacao}}
                                    <td style="text-align: center;">
                                        @if ($osdentro->dt_execucao == '' )
                                        <div class="btn-group">
                                            <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-warning btn-sm" title="Clique para indicar que essa OS foi executada HOJE!"><i class="icon-ok"></i></a>
                                            <ul class="dropdown-menu btn-danger pull-right">
                                                <li><a href="{{ URL::to('/os/executadahoje/'.$osdentro->id_os) }}">Clique para confirmar que essa essa OS foi executada hoje ({{date('d/m/Y')}})</a></li>
                                            </ul>
                                        </div>
                                        @else
                                        {{$osdentro->dt_execucao}}
                                        @endif
                                    </td>
                                    <td style="text-align: center;">{{$osdentro->nome_status != '' ? $osdentro->nome_status : '--'}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            @endif
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>Ordens de serviço que podem ser incluídas na Invoice</h3>
            </div>

            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-hover fill-head">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 200px">Ação</th>
                                    <th style="width: 80px">Núm.</th>
                                    <th style="width: auto;">Empresa<br/>Projeto/Emb.</th>
                                    <th style="width: auto;">Armador</th>
                                    <th style="width: auto;">Solicitante</th>
                                    <th style="width: 200px">Candidatos</th>
                                    <th style="width: auto;">Serviço</th>
                                    <th style="width: auto; text-align: center;">Entrada</th>
                                    <th style="width: auto; text-align: center;">Execução</th>
                                    <th style="width: auto; text-align: center;">Status</th>
                                </tr>
                            </thead>
                            @if(isset($osforas))
                            <tbody>
                                <? $i = 0; ?>
                                @foreach($osforas as $osfora)
                                <? $i++; ?>
                                @if ($osfora->id_status_os == 2)
                                <tr style="background-color: #36c77b;">
                                    @else
                                <tr>
                                    @endif
                                    <td><? print $i; ?></td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" href="{{ URL::to('/invoice/adicionar/'.$invoice->id_invoice.'/'.$osfora->id_os) }}" title="Clique para incluir essa OS na Invoice"><i class="icon-plus"></i></a>
                                        <a class="btn btn-sm" href="{{ URL::to('/os/show/'.$osfora->id_os) }}" target="_blank"><i class="icon-tags"></i></a>
                                        <a class="btn btn-sm" href="{{ URL::to('/os/candidatos/'.$osfora->id_os) }}" target="_blank"><i class="icon-group"></i></a>
                                        <a class="btn btn-sm" href="{{ URL::to('/os/anexos/'.$osfora->id_os) }}" target="_blank"><i class="icon-folder-open"></i></a>
                                        <a class="btn btn-sm" href="{{ URL::to('/os/despesas/'.$osfora->id_os) }}" target="_blank"><i class="icon-dollar"></i></a>
                                    </td>
                                    <td>{{ substr('000000'.$osfora->numero, -6) }}</td>
                                    <td>{{ $osfora['razaosocial'] }}<br/><span style="color:#979797; font-style: italic;">{{ $osfora->descricao_projeto }}</span></td>
                                    <td>{{ $osfora->razaosocial_armador }}</td>
                                    <td>{{ $osfora->solicitante }}</td>
                                    <td>
                                        <? $br = ''; ?>
                                        @foreach($osfora->candidatos as $candidato)
                                        <a href="{{URL::to('/candidato/show/'.$candidato->id_candidato)}}" target="_blank" title="Clique para ver o candidato em uma nova janela">{{ $br.$candidato->nome_completo }}</a>
                                        <? $br = '<br/>'; ?>
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ $osfora->descricao_servico  }}
                                    </td>
                                    <td style="text-align: center;">{{$osfora->dt_solicitacao}}
                                    <td style="text-align: center;">
                                        @if ($osfora->dt_execucao == '' )
                                        <div class="btn-group">
                                            <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-warning btn-sm" title="Clique para indicar que essa OS foi executada HOJE!"><i class="icon-ok"></i></a>
                                            <ul class="dropdown-menu btn-danger pull-right">
                                                <li><a href="{{ URL::to('/os/executadahoje/'.$osfora->id_os) }}">Clique para confirmar que essa essa OS foi executada hoje ({{date('d/m/Y')}})</a></li>
                                            </ul>
                                        </div>
                                        @else
                                        {{$osfora->dt_execucao}}
                                        @endif
                                    </td>
                                    <td style="text-align: center;">{{$osfora->nome_status != '' ? $osfora->nome_status : '--'}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            @endif
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@stop
