@extends('processo.comum')

<? $tipo_processo = 'prorrogacao'; ?>
<? $nome_servico = 'Prorrogação'; ?>

@section('campos')
    <div class="row">
        @if($processo->id_os > 0)
            @include('html/campo-texto', array('id' => 'razaosocial', 'label' => 'Empresa', 'valor' => $processo->razaosocial_empresa,   'atributos'=> array( "disabled"=>"disabled")))
            @include('html/campo-texto', array('id' => 'projeto', 'label' => 'Proj./Emb.', 'valor' => $processo->descricao_projeto,   'atributos'=> array( "disabled"=>"disabled")))
            @include('html/campo-texto', array('id' => 'servico', 'label' => 'Serviço', 'valor' => $processo->descricao_servico,   'atributos'=> array( "disabled"=>"disabled")))
        @else
            @include('html/campo-select', array('id' => 'id_empresa', 'label' => 'Empresa', 'valor' => $processo->id_empresa, 'valores' => array('0' => '(não informado)') +  Empresa::orderBy('razaosocial')->lists('razaosocial', 'id_empresa'),  'help' => '', array()))
            @include('html/campo-select', array('id' => 'id_projeto', 'label' => 'Proj./Emb.', 'valor' => $processo->id_projeto, 'valores' => array('0' => '(não informado)') +  Projeto::orderBy('descricao')->lists('descricao', 'id_projeto'),  'help' => '', array()))
            @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => $processo->id_servico, 'valores' => array('0' => '(não informado)') +  Servico::where('id_tipo_servico', '=', 5)->orderBy('descricao')->lists('descricao', 'id_servico'),  'help' => '', array()))
        @endif
        @include('html/campo-texto', array('id' => 'numero_processo', 'label' => 'Processo', 'valor' => $processo->numero_processo,  'help' => '',  array()))
        @include('html/campo-data', array('id' => 'data_requerimento', 'label' => 'Data requerimento', 'valor' => $processo->data_requerimento,  'help' => '',  array()))
        @include('html/campo-texto', array('id' => 'prazo_pretendido', 'label' => 'Prazo pretendido', 'valor' => $processo->prazo_pretendido,  'help' => '',  array()))
        @include('html/campo-data', array('id' => 'data_publicacao_dou', 'label' => 'Data publicação DOU', 'valor' => $processo->data_publicacao_dou,  'help' => '', array()))
        @include('html/campo-texto', array('id' => 'pagina_dou', 'label' => 'Página DOU', 'valor' => $processo->pagina_dou,  'help' => '', array()))
        @include('html/campo-texto', array('id' => 'numero_precadastro', 'label' => 'Pré-cadastro', 'valor' => $processo->numero_precadastro,  'help' => '', array()))
        @include('html/campo-texto', array('id' => 'dt_envio_bsb_fmt', 'label' => 'Envio para BSB', 'valor' => $processo->dt_envio_bsb_fmt,  'help' => '', array()))
    </div>
@overwrite
