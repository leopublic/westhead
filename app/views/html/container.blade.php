<?php
if(!isset($qtdCol) || $qtdCol == '1'){
    $larg_label = 'col-sm-4 col-md-2 col-lg-2';
    $larg_input = 'col-sm-8 col-md-10 col-lg-10';
} else {
    $larg_label = 'col-sm-4 col-md-4 col-lg-4';
    $larg_input = 'col-sm-8 col-md-8 col-lg-8';
}
?>
    @if(!isset($qtdCol) || $qtdCol == '1')
        <div class="form-group">
            {{Form::label($id, $label, array('class' => $larg_label.' control-label')) }}
            <div class="{{$larg_input}} controls">
                @yield('campo')
                @if(isset($help))
                <span class="help-inline">{{ $help }}</span>
                @endif
            </div>
        </div>
    @else
        @if($qtdCol == '2')
            <div class="col-md-6">
                <div class="form-group">
                    {{Form::label($id, $label, array('class' => $larg_label.' control-label')) }}
                    <div class="{{$larg_input}} controls">
                        @yield('campo')
                        @if(isset($help))
                        <span class="help-inline">{{ $help }}</span>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    @endif
