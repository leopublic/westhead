@extends('processo.comum')

<? $tipo_processo = 'Outros'; ?>
<? $nome_servico = 'Outros'; ?>

@section('titulo')
    @if(is_object($processo->servico))
        {{$processo->servico->descricao}}
    @else
        Outros serviços: (?)
    @endif
@overwrite

@section('campos')
    <div class="row">
        @if (intval($processo->id_os) == 0)
            @if (Servico::where('id_tipo_servico', '=', 1)->orWhere('id_tipo_servico', '=', 6)->count() > 1)
                @include('html/campo-select', array('id' => 'id_servico', 'label' => 'Serviço', 'valor' => $processo->id_servico, 'valores' => array('0' => '(não informado)') +  Servico::where('id_tipo_servico', '=', 6)->lists('descricao', 'id_servico'),  'help' => '', array()))
            @else
                @include('html/campo-texto', array('id' => 'servico', 'label' => 'Serviço', 'valor' => $processo->descricao_servico,  'atributos'=> array( "disabled"=>"disabled")))
            @endif
        @endif
    </div>
@overwrite
