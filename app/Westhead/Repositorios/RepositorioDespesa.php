<?php

namespace Westhead\Repositorios;

class RepositorioDespesa extends Repositorio{

    public function crie($post, \Despesa $despesa, $id_usuario){
        // Corrige formato dos campos de valor
        if (isset($post['qtd'])){
            $post['qtd'] = str_replace(',', '.', str_replace('.', '', $post['qtd']));
        }
        if (isset($post['valor'])){
            $post['valor'] = str_replace(',', '.', str_replace('.', '', $post['valor']));
        }
        $this->validador = new \Westhead\Validadores\ValidadorDespesa($post) ;
        if ($this->validador->passes()) {
            $despesa->fill($post);
            $despesa->save();
            return $despesa;
        } else {
            return false;
        }
    }

    public function nova(){
        return new \Despesa;
    }

    public function exclua($id){
        \Despesa::destroy($id);
        return true;
    }
}