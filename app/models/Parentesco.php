<?php
/**
 * @property int @id_parentesco Chave
 * @property string $descricao Descrição do parentesco
 * @property string $descricao_en Descrição do parentesco em ingles
 */
class Parentesco extends Eloquent {
	protected $table = 'parentesco';
	protected $primaryKey = 'id_parentesco';

	protected $guarded = array();

	public function assumeDefaults()
	{
		if($this->descricao_en = ''){
			$this->descricao_en = $this->descricao;
		}
	}

	public static function regras()
	{
		return array(
				'descricao' => 'required'
				);
	} 

	public static function mensagens()
	{
		return array(
				'descricao.required' => 'A descrição é obrigatória!'
			);
	}
	
}
