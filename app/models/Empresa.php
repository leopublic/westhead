<?php

/**
 * @property int $id_empresa Chave
 * @property string $razaosocial
 * @property string $nomefantasia
 * @property string $cnpj
 * @property string $insc_estadual
 * @property string $insc_municipal
 * @property int $id_endereco_corr Description
 * @property int $id_endereco_cobr Description
 * @property int $id_tipo_cobranca Indica se é cobranca no brasil ou no exterior
 * @property int $id_tipo_calc_desp Indica a metodologia do cálculo do valor em dolar das despesas
 * @property decimal $valor_cotacao_propria Indica o valor da cotação que deve ser usado no cálculo das despesas em dolar
 * @property boolean $fl_obs_embarcacoes Indica se a observação de embarcações fora do prazo de exigencia deve ser exibida ou não
 */
class Empresa extends Modelo {

    protected $table = 'empresa';
    protected $primaryKey = 'id_empresa';

    protected $guarded = array();
    public function __construct() {
        $this->fl_armador = 0;
    }

    public function scopeTodos($query) {
        return $query->where('fl_armador', '=', 0);
    }

    public function projetos() {
        return $this->hasMany('Projeto', 'id_empresa');
    }

    public function centrosdecusto() {
        return $this->hasMany('Centrodecusto', 'id_empresa');
    }

    public function procurador(){
        return $this->belongsTo('Procurador', 'id_procurador');
    }
    /**
     * Informa se a empresa tem um arquivo de logo
     */
    public function temLogo(){
        if(file_exists('../public/arquivos/logos/'.$this->id_empresa.".".$this->extensao_logo)){
            return true;
        } else {
            return false;
        }
    }

    public function caminhoLogo(){
        if(file_exists('../public/arquivos/logos/'.$this->id_empresa.".".$this->extensao_logo)){
            return '../public/arquivos/logos/'.$this->id_empresa.".".$this->extensao_logo;
        } else {
            return '';
        }

    }

    public function getDtConstituicaoAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtConstituicaoAttribute($data) {
        $this->attributes['dt_constituicao'] = $this->dtViewParaModel($data);
    }

    public function getDtCadastroBcAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtCadastroBcAttribute($data) {
        $this->attributes['dt_cadastro_bc'] = $this->dtViewParaModel($data);
    }

    public function getDtAlteracaoContratualAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtAlteracaoContratualAttribute($data) {
        $this->attributes['dt_alteracao_contratual'] = $this->dtViewParaModel($data);
    }

    public function getDtInvestimentoEstrangeiroAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDtInvestimentoEstrangeiroAttribute($data) {
        $this->attributes['dt_investimento_estrangeiro'] = $this->dtViewParaModel($data);
    }

    public function getEndCepLimpoAttribute($data){
        return trim(str_replace('.', '', str_replace('-', '', $this->end_cep)));
    }
    
    public function embarcacoes(){
        return Projeto::embarcacoes($this->id_empresa)->get();
    }

    public function enderecoCompleto(){
        $end = $this->endereco;
        if($this->end_complemento != ''){
            $end .= ', '.$this->end_complemento;
        }
        if($this->end_bairro != ''){
            $end .= ', '.$this->end_bairro;
        }
        if($this->end_municipio != ''){
            $end .= ', '.$this->end_municipio;
        }
        if($this->end_uf != ''){
            $end .= '-'.$this->end_uf;
        }
        if($this->end_cep != ''){
            $end .= ', '.$this->end_cep;
        }

        return $end;
    }

    public function getEndFormTransAttribute($valor){
        $end = $this->endereco;
        if($this->end_complemento != ''){
            $end .= ', '.$this->end_complemento;
        }
        return $end;
    }
    public function getTelCompletoAttribute($valor){
        $ret = $this->tel_ddd. $this->tel_num;
        return $ret;
    }

    public function cotacao($data){
        if ($this->id_tipo_calc_desp== 1){
            $x = $this->dtViewParaModel($data);
            $cot =  \Cotacao::where('data', '=', $x)->first();
            if (is_object($cot)){
                return $cot->compra;
            } else {
                return 0;
            }
        } else {
            if (is_numeric($this->valor_cotacao_propria)){
                return $this->valor_cotacao_propria;
            } else {
                return 0;
            }
        }
    }

    public function getQtdProjetosAttribute(){
        return \Projeto::where('id_empresa', '=', $this->id_empresa)->count();
    }

    public function getQtdCandidatosAttribute(){
        return \Candidato::where('id_empresa', '=', $this->id_empresa)->count();
    }

    public function getQtdOsAttribute(){
        return \Os::where('id_empresa', '=', $this->id_empresa)->count();
    }

}
