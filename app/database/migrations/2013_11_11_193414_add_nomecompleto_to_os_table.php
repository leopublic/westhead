<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNomecompletoToOsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('os', function(Blueprint $table) {
			$table->string('nomecompleto', 1000)->nullable();
			$table->string('cpf', 50)->nullable();
			$table->string('tipopessoa', 1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('os', function(Blueprint $table) {
			$table->dropColumn('nomecompleto');
			$table->dropColumn('cpf');	
			$table->dropColumn('tipopessoa');
		});
	}

}
