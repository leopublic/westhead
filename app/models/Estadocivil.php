<?php
/**
 * @property int $cod_pf Código usado pela PF no formulário de transformação
 */
class Estadocivil extends Eloquent {
	protected $table = 'estadocivil';
	protected $primaryKey = 'id_estadocivil';

	protected $guarded = array();

	public function assumeDefaults()
	{
		if($this->descricao_en = ''){
			$this->descricao_en = $this->descricao;
		}
	}

	public static function regras()
	{
		return array(
				'descricao' => 'required'
				);
	} 

	public static function mensagens()
	{
		return array(
				'descricao.required' => 'A descrição é obrigatória!'
			);
	}
	
}
