<html>
    <head>
        <style>
            br {mso-data-placement:same-cell;}
            th {vertical-align: bottom; border:solid 1px #000;}
            td {vertical-align: top; border:solid 1px #000;}
        </style>
    </head>
    <body>
        <table class="table table-hover fill-head">
            <thead>
                <tr>
                    <td colspan="8" style="font-size:200%;">Invoices</td>
                </tr>
                <tr>
                    <th style="width:30px">#</th>
                    <th style="width: auto">Número</th>
                    <th style="width: auto">Centro de custos</th>
                    <th style="width: auto">Projeto</th>
                    <th style="width: auto; text-align: center;">Data</th>
                    <th style="width: auto; text-align: center;">Vencimento</th>
                    <th style="width: auto; text-align: left;">Ordens de serviço</th>
                    <th style="text-align: right;">Valor total R$</th>
                </tr>
            </thead>
            <tbody>
