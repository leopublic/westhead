@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="icon-file-alt"></i>{{ $tela['titulo_tela'] }}</h1>
        <h4>{{ $tela['subTitulo'] }}</h4>
    </div>
</div>
<!-- END Page Title -->

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>{{ $tela['titulo_grid'] }}</h3>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 150px">Ação</th>
                            @foreach($colunas as $coluna)
                            <th>{{ $coluna['titulo'] }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $registro)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="#"><i class="icon-edit"></i></a>
                                <a class="btn btn-danger btn-sm" href="#"><i class="icon-trash"></i></a>
                            </td>
                            @foreach($colunas as $coluna)
                            <td>{{ $registro->$coluna['campo'] }}</td>
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop