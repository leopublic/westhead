<?php
class TipoServico extends Modelo {
	protected $table = 'tipo_servico';
	protected $primaryKey = 'id_tipo_servico';

	protected $guarded = array();

	public function assumeDefaults()
	{
	}

	public static function regras()
	{
		return array(
				'nome' => 'required'
				);
	} 

	public static function mensagens()
	{
		return array(
				'nome.required' => 'O nome é obrigatório!'
			);
	}

	public function novo_processo(){
		switch($this->id_tipoprocesso){
			case 1: 
				return ProcessoAutorizacao::first();
				break;
			case 7: 
				return ProcessoAtendimento::first();
				break;
			case 8: 
				return ProcessoCancelamento::first();
				break;
			case 9: 
				return ProcessoColeta::first();
				break;
			case 4: 
				return ProcessoColetaVisto::first();
				break;
			case 6: 
				return ProcessoOutros::first();
				break;
			case 5: 
				return ProcessoProrrogacao::first();
				break;
			case 3: 
				return ProcessoRegistro::first();
				break;
			case 10: 
				return ProcessoTraducao::first();
				break;
		}
	}
}
