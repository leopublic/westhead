@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="icon-file-alt"></i>FAQ</h1>
        <h4>Sistema de Gestão de Estrangeiros Westhead</h4>
    </div>
</div>
<!-- END Page Title -->
@include('html.mensagens')
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Controle de acesso</h3>
            </div>
            <div class="box-content">
                <div class="panel-group" id="accordion1">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#faq1_1">Como conseguir um login para acessar o sistema?</a>
                            </h4>
                        </div>
                        <div id="faq1_1" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                Anim pariatur cliche...Lorem ipsum dolore dolor occaecat dolore elit deserunt incididunt ex sed nostrud aute aliquip ut elit sed nisi. 
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#faq1_2">Esqueci minha senha. Como obter uma nova?</a>
                            </h4>
                        </div>
                        <div id="faq1_2" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                Anim pariatur cliche...Lorem ipsum Duis occaecat Excepteur est magna tempor ex ea enim sunt mollit proident. Lorem ipsum sed laboris ut adipisicing ut et aute occaecat aute enim occaecat. 
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#faq1_3">Como suspender o acesso de um usuário?</a>
                            </h4>
                        </div>
                        <div id="faq1_3" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                Anim pariatur cliche...Lorem ipsum do culpa adipisicing quis non nisi ullamco. Lorem ipsum velit dolore qui Excepteur fugiat et dolor proident reprehenderit magna aliqua enim consectetur nisi. Lorem ipsum sit laborum est magna veniam ex ut velit do aliqua amet dolore enim minim eu. Lorem ipsum adipisicing officia occaecat deserunt enim minim veniam sint amet sed consectetur nisi quis. 
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#faq1_4">Como controlar as telas que um usuário terá acesso?</a>
                            </h4>
                        </div>
                        <div id="faq1_4" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                Anim pariatur cliche...Lorem ipsum do culpa adipisicing quis non nisi ullamco. Lorem ipsum velit dolore qui Excepteur fugiat et dolor proident reprehenderit magna aliqua enim consectetur nisi. Lorem ipsum sit laborum est magna veniam ex ut velit do aliqua amet dolore enim minim eu. Lorem ipsum adipisicing officia occaecat deserunt enim minim veniam sint amet sed consectetur nisi quis. 
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#faq1_5">Como definir as telas e funções que um perfil terá acesso?</a>
                            </h4>
                        </div>
                        <div id="faq1_5" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                Anim pariatur cliche...Lorem ipsum do culpa adipisicing quis non nisi ullamco. Lorem ipsum velit dolore qui Excepteur fugiat et dolor proident reprehenderit magna aliqua enim consectetur nisi. Lorem ipsum sit laborum est magna veniam ex ut velit do aliqua amet dolore enim minim eu. Lorem ipsum adipisicing officia occaecat deserunt enim minim veniam sint amet sed consectetur nisi quis. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Empresas</h3>
            </div>
            <div class="box-content">
                <div class="panel-group" id="accordion2">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#faq2_1">O que são "empresas" no sistema?</a>
                            </h4>
                        </div>
                        <div id="faq2_1" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                Empresas são os clientes da LPLAW. Todo candidato está associado a uma e somente uma empresa. Um candidato não pode pertencer a mais de uma empresa, mas pode ter pertencido a várias empresas durante o seu histórico de vistos.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#faq2_2">Como eu cadastro uma nova empresa?</a>
                            </h4>
                        </div>
                        <div id="faq2_2" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                nono ononono ono noono on o nononon ono ono 
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#faq2_3">O cadastro da empresa tem muitos campos, eles são realmente necessários?</a>
                            </h4>
                        </div>
                        <div id="faq2_3" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                nono ononono ono noono on o nononon ono ono 
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#faq2_4">Como desativar uma empresa sem perder o seu histórico?</a>
                            </h4>
                        </div>
                        <div id="faq2_4" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                nono ononono ono noono on o nononon ono ono 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Embarcações e projetos</h3>
            </div>
            <div class="box-content">
                <div class="panel-group" id="accordion3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#faq3_1">O que são "embarcações e projetos" no sistema?</a>
                            </h4>
                        </div>
                        <div id="faq3_1" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                onononono non onon ono on on onononon ononono ono nono nono nononono on o no
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#faq3_2">Como cadastrar uma nova embarcação para uma empresa?</a>
                            </h4>
                        </div>
                        <div id="faq3_2" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                onononono non onon ono on on onononon ononono ono nono nono nononono on o no
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#faq3_3">O que são "embarcações e projetos" no sistema?</a>
                            </h4>
                        </div>
                        <div id="faq3_3" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body">
                                Quando um estrangeiro recebe uma autorização de trabalho, essa autorização é específica para um determinada embarcação ou projeto. E as empresas controlam a situação dos seus estrangeiros por embarcação/projeto, pois a fiscalização da polícia federal é feita por embarcação. Por isso essa informação é importante e consta em todas as ordens de serviço.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop